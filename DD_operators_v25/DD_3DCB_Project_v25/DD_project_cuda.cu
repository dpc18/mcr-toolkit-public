// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// External headers

    #ifdef MEX_COMPILE_FLAG
    #include "mex.h"
    #endif

    #include "cuda_runtime.h"
    #include "cufft.h"
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>
    #include <math.h>
    #include <stdint.h>
    #include "omp.h"          // CPU multi-threading directives

// Custom headers

    #include "error_chk.h"                    // Check for CUDA errors
    #include "DD_v25_macros.h"                // Environment variables
    #include "DD_v25_data_structures.h"       // Data structures: Private_GPU_Data, Public_GPU_Data, recon_parameters
    #include "DD_v25_GPU_support_functions.h" // Device functions

// Redirected weights

    extern double np_eff_sub;
    extern double *weights_sub;

// External reference to reconstruction allocation / parameters

    extern recon_parameters *rp;
    
// Projection kernels

    #include "DD_v25_project.h"               

// Distribute projections over GPUs and GPU streams. Each GPU stream is assigned its own CPU thread using OpenMP.
void DD_project_cuda( float *Y, float *X, int output_on_GPU ) {
    
    // Check for any outstanding CUDA errors
    
        check(0);
        
    // for each GPU and stream
    
        // declare private variables
        int tid;
        Private_GPU_Data *GPUpri;
        Public_GPU_Data  *GPUpub;
        int GPUid, GPUnum, c, p, q, cnt, cnt_host;
        int proj_start, proj_end;
        int z_size, p_size;

        // make sure we will cover all projections
        // Cannot assume this is evenly divisible by a certain number! (e.g. subsets, prime numbers of projections, weighted temporal problems, ...)
        int proj_increment = ( rp->np + NUMSTREAMS * rp->nDevices - 1) / ( NUMSTREAMS * rp->nDevices ); // round up w/ integer truncation
        
    // Copy data to each GPU
    // Do this in its own pragma to avoid difficult thread/stream synchronization issues.
    #pragma omp parallel private( tid, GPUpub, GPUid, GPUnum ) default( shared ) num_threads(THREAD_COUNT)
    {

        // Which CPU thread are we running on?

            tid = omp_get_thread_num();
            
        if ( ( tid < NUMSTREAMS*rp->nDevices ) && ( ( tid % NUMSTREAMS ) == 0 ) ) {
            
                // which GPU are we working on?
            
                GPUnum = tid / NUMSTREAMS; // integer truncation
                GPUid = rp->GPU_indices[ GPUnum ];
                cudaSetDevice( GPUid );
                
            // Direct private pointers to the correct data structures within rp

                GPUpub = &(rp->GPU_pub[GPUnum]);
                
            // Copy data once per GPU
         
                cudaMemcpy( GPUpub[0].vol_, X, rp->vol_size, cudaMemcpyDefault );
            
        }

    }
        
    #pragma omp parallel private( tid, GPUpri, GPUpub, GPUid, GPUnum, proj_start, proj_end, z_size, p_size, c, p, q, cnt, cnt_host ) default( shared ) num_threads(THREAD_COUNT)
    {
        
        // Which CPU thread are we running on?

            tid = omp_get_thread_num();
        
        // Since we need to have a static number of threads, leave threads that we are not used unoccupied.
        if ( tid < NUMSTREAMS*rp->nDevices ) {
            
            // which GPU are we working on?
            
                GPUnum = tid / NUMSTREAMS; // integer truncation
                GPUid = rp->GPU_indices[ GPUnum ];
                cudaSetDevice( GPUid );
                
            // Direct private pointers to the correct data structures within rp
            
                GPUpri = &(rp->GPU_pri[GPUnum*NUMSTREAMS + (tid % NUMSTREAMS)]);
                GPUpub = &(rp->GPU_pub[GPUnum]);

            // Which projections is this stream / thread responsible for?
                
                proj_start = tid * proj_increment;
                proj_end   = min( (tid + 1) * proj_increment, (int) rp->np ); // Make sure we do not go out of bounds!
                
            // Allocate events to control data flow on the host
                
                cudaEvent_t *done_events;
                if ( output_on_GPU == 0 ) {
                    
                    done_events = (cudaEvent_t *) malloc( ( ( proj_end - proj_start + PROJ_CHUNK - 1 ) / PROJ_CHUNK ) * sizeof(cudaEvent_t) );
                    cnt = 0;

                }

            // Project host data in chunks
                
                for ( q = proj_start; q < proj_end; q += PROJ_CHUNK ) {
                    
                    p_size = min( PROJ_CHUNK, proj_end - q);
                    
                    // v24: Make sure all non-zero weighted projections begin at zero
                    for ( p = q; p < (p_size + q); p++) {
                        
                        if ( weights_sub[p] == 0 ) continue;
                     
                        cudaMemsetAsync( &(GPUpub[0].proj_[p*rp->nuv]), 0, rp->proj_size, GPUpri[0].mystream);
                        
                    }

                    // loop over volume chunks
                    for ( c = 0; c < rp->nz ; c += VOL_CHUNK ) {
                        
                        z_size = min( VOL_CHUNK, rp->nz - c );

                        // Project chunk and add to projection data
                        DD_project_cuda( &GPUpub[0], &GPUpri[0], q, min( q + PROJ_CHUNK, proj_end), c, z_size, GPUpri[0].mystream );

                    }
                    
                    // v24: copy out all non-zero weighted projections to...
                    //      pinned memory, if the output is on the host
                    //      GPU memory, if the output is on the GPU or managed
                    for ( p = q; p < (p_size + q); p++) {
                     
                        if ( weights_sub[p] == 0 ) continue;
                        
                        if ( output_on_GPU == 0 ) { // output on the host
                            
                            cudaMemcpyAsync( &(rp->Pinned_Buffer[p*rp->nuv]), &(GPUpub[0].proj_[p*rp->nuv]), rp->proj_size, cudaMemcpyDefault, GPUpri[0].mystream );
                            // cudaMemPrefetchAsync( &(GPUpub[0].proj_[p*rp->nuv]), rp->proj_size, cudaCpuDeviceId, GPUpri[0].mystream );
                            
                        }
                        else { // output on the GPU
                            
                            cudaMemcpyAsync( &(Y[p*rp->nuv]), &(GPUpub[0].proj_[p*rp->nuv]), rp->proj_size, cudaMemcpyDefault, GPUpri[0].mystream );
                            
                        }
                        
                    }
                    
                    if ( output_on_GPU == 0 ) {
                    
                        // v24: Record an event we can use to copy data out of the pinned memory on the host.
                        cudaEventCreateWithFlags( &done_events[cnt], cudaEventDisableTiming );
                        cudaEventRecord( done_events[cnt], GPUpri[0].mystream );
                        cnt++;
                        
                    }
                        
                }
                
                // If the output is on the host, copy out of pinned memory as soon as possible.
                if ( output_on_GPU == 0 ) {
                
                    for ( cnt_host = 0; cnt_host < cnt; cnt_host++ ) {
                        
                        cudaEventSynchronize( done_events[cnt_host] );

                        q = proj_start + PROJ_CHUNK*cnt_host;

                        p_size = min( PROJ_CHUNK, proj_end - q );

                        for ( p = q; p < (q + p_size); p++ ) {
                            
                            if ( weights_sub[p] == 0 ) continue;
                            
                            memcpy( &(Y[p*rp->nuv]), &(rp->Pinned_Buffer[p*rp->nuv]), rp->proj_size );
                            // memcpy( &(Y[p*rp->nuv]), &(GPUpub[0].proj_[p*rp->nuv]), rp->proj_size );

                        }

                    }
                    
                    free(done_events);
                
                }

        }
        
    }
    
    // Check for any outstanding CUDA errors
    
        check(1);
        
    // Make sure all GPUs are done before returning
            
        for ( int i = 0; i < rp->nDevices; i++ ) {

            GPUid = rp->GPU_indices[ i ];

            cudaSetDevice( GPUid );

            cudaDeviceSynchronize();

        }

}
