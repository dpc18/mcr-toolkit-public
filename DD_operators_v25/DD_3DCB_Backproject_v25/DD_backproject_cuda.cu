// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// External headers

#ifdef MEX_COMPILE_FLAG
#include "mex.h"
#endif

#include "cuda_runtime.h"
#include "cufft.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include "omp.h"

// Custom headers

#include "error_chk.h"
#include "DD_v25_macros.h"                 // Environment variables and length checker function
#include "DD_v25_data_structures.h"        // Data structure definitions
#include "DD_v25_GPU_support_functions.h"

// Redirected weights
extern int    perform_filtration;
extern double np_eff_sub;
extern double *weights_sub;
extern double np_sub;

// External reference to reconstruction allocation / parameters
extern recon_parameters *rp;

#include "DD_v25_backproject.h"

// Host functions

// Distribute projections over GPUs and GPU streams. Each GPU stream is assigned its own CPU thread using OpenMP.
void DD_backproject_cuda( float *X, float *Y, int output_on_GPU ) {
    
    // Check for outstanding CUDA errors
    
        check(0);
        
    // for each GPU and stream
    
        // declare private variables
        int tid;
        Private_GPU_Data *GPUpri;
        Public_GPU_Data  *GPUpub;
        int GPUid, GPUnum, c, q, p, cnt, cnt_host;
        int vol_start, vol_end;
        int proj_start, proj_end;
        int p_size, z_size;
        size_t os;

        // make sure we will cover the entire volume
        int vol_increment = ( rp->nz + NUMSTREAMS * rp->nDevices - 1) / ( NUMSTREAMS * rp->nDevices ); // round up w/ integer truncation
                
    // Perform projection filtration
    // Do this in its own loop, since all streams use all projections later.

        if (rp->filtration && perform_filtration) {
            
            // projections per stream (round up w/ integer truncation)
            // op repeated on each GPU, so all filtered projections are available later
            int proj_increment = ( rp->np + NUMSTREAMS - 1 ) / ( NUMSTREAMS );
            
            // private(i)
            #pragma omp parallel private( tid, GPUpri, GPUpub, GPUid, GPUnum, proj_start, proj_end, q, p, os ) default( shared ) num_threads(THREAD_COUNT)
            {
                
                // Which CPU thread are we running on?
                
                    tid = omp_get_thread_num();
                    
                // Since we need to have a static number of threads, leave threads that we are not using unoccupied.
         
                    if ( tid < NUMSTREAMS*rp->nDevices ) {
    
                        // which GPU are we working on?

                            GPUnum = tid / NUMSTREAMS; // integer truncation

                            GPUid = rp->GPU_indices[ GPUnum ];

                            cudaSetDevice( GPUid );

                        // Direct private pointers to the correct data structures within rp

                            GPUpri = &(rp->GPU_pri[GPUnum*NUMSTREAMS + (tid % NUMSTREAMS)]);
                            GPUpub = &(rp->GPU_pub[GPUnum]);
                            
                        // Which projections are we responsible for filtering?
                        
                            proj_start = (tid % NUMSTREAMS) * proj_increment;
                            proj_end   = min( (tid % NUMSTREAMS + 1) * proj_increment, (int) rp->np);
                            
                        // Copy projections to managed memory
                            
                            for ( p = proj_start; p < proj_end; p++ ) {

                                if ( weights_sub[p] == 0 ) continue;

                                os = p*rp->nuv;

                                cudaMemcpyAsync( &(GPUpub[0].proj_[os]), &(Y[os]), rp->proj_size, cudaMemcpyDefault, GPUpri[0].mystream );

                            }
                            
                        // Perform filtration
                        for ( p = proj_start; p < proj_end; p++) {
                                
                            // no point in filtering projections we will not use...
                            if ( weights_sub[p] == 0 ) continue;

                            os = p*rp->nuv;

                            // copy projection to complex 
                            realtocomplex<<<rp->pgrid_fft, rp->pblock, 0, GPUpri[0].mystream>>>( &(GPUpub[0].proj_[os]), GPUpri[0].fftproj_, rp->nu, rp->nv, rp->nu_fft);

                            // fft of projection
                            cufftExecC2C(GPUpri[0].fftplan, GPUpri[0].fftproj_, GPUpri[0].fftproj_, CUFFT_FORWARD);

                            // multiply fft by filter
                            filterprojection<<<rp->pgrid_fft, rp->pblock, 0, GPUpri[0].mystream>>>( GPUpri[0].fftproj_, GPUpub[0].filt_, rp->nu_fft, rp->nv);

                            // ifft
                            cufftExecC2C(GPUpri[0].ifftplan, GPUpri[0].fftproj_, GPUpri[0].fftproj_, CUFFT_INVERSE);

                            // copy complex to projection
                            complextoreal<<<rp->pgrid, rp->pblock, 0, GPUpri[0].mystream>>>(GPUpri[0].fftproj_, &(GPUpub[0].proj_[os]), rp->nu, rp->nv, rp->nu_fft);

                        }

                    }
            
            }
            
            // Check for outstanding CUDA errors
    
                check(1);
            
            // Synchronize memory before we access all projections with each stream
            for ( int i = 0; i < rp->nDevices; i++ ) {

                GPUid = rp->GPU_indices[ i ];

                cudaSetDevice( GPUid );
                
                cudaDeviceSynchronize();
                
            }
            
        }
        
    // Copy data to each GPU, if we did not filter the data
        
        if ( !(rp->filtration) || !(perform_filtration) ) {

            // Do this in its own pragma to avoid difficult thread/stream synchronization issues.
            #pragma omp parallel private( tid, GPUpub, GPUid, GPUnum, p, os ) default( shared ) num_threads(THREAD_COUNT)
            {

                // Which CPU thread are we running on?

                    tid = omp_get_thread_num();
                    
                // Perform the copy only once per GPU

                    if ( ( tid < NUMSTREAMS*rp->nDevices ) && ( ( tid % NUMSTREAMS ) == 0 ) ) {

                        // Which GPU are we working on?

                            GPUnum = tid / NUMSTREAMS; // integer truncation
                            GPUid = rp->GPU_indices[ GPUnum ];
                            cudaSetDevice( GPUid );

                        // Direct private pointers to the correct data structures within rp

                            GPUpub = &(rp->GPU_pub[GPUnum]);

                        // Copy data once per GPU
                            
                            for ( p = 0; p < rp->np; p++ ) {

                                if ( weights_sub[p] == 0 ) continue;

                                os = p*rp->nuv;

                                cudaMemcpy( &(GPUpub[0].proj_[os]), &(Y[os]), rp->proj_size, cudaMemcpyDefault );

                            }

                    }

            }
            
        }

    // Perform backprojection
        
        #pragma omp parallel private( tid, GPUpri, GPUpub, GPUid, GPUnum, c, vol_start, vol_end, q, p, z_size, p_size, cnt, cnt_host ) default( shared ) num_threads(THREAD_COUNT)
        {

            // Which CPU thread are we running on?

                tid = omp_get_thread_num();

            // Since we need to have a static number of threads, leave threads that we are not useing unoccupied.
            if ( tid < NUMSTREAMS*rp->nDevices ) {

                // which GPU are we working on?

                    GPUnum = tid / NUMSTREAMS; // integer truncation

                    GPUid = rp->GPU_indices[ GPUnum ];

                    cudaSetDevice( GPUid );

                // Direct private pointers to the correct data structures within rp

                    GPUpri = &(rp->GPU_pri[GPUnum*NUMSTREAMS + (tid % NUMSTREAMS)]);
                    GPUpub = &(rp->GPU_pub[GPUnum]);
                    
                // Which z slices is this stream / thread responsible for?

                    vol_start = tid * vol_increment;
                    vol_end   = min( (tid + 1) * vol_increment, (int) rp->nz ); // Make sure we do not go out of bounds!

                // Create events to let the host know when we are done processing each chunk of volume data.
                    
                    cudaEvent_t *done_events;
                    if ( output_on_GPU == 0 ) {
                    
                        done_events = (cudaEvent_t *) malloc( ( ( vol_end - vol_start + VOL_CHUNK - 1 ) / VOL_CHUNK ) * sizeof(cudaEvent_t) );
                        cnt = 0;

                    }

                // Backproject host data in chunks

                    // loop over volume chunks
                    for ( c = vol_start; c < vol_end; c += VOL_CHUNK ) {
                        
                        z_size = min( vol_end - c, VOL_CHUNK );

                        cudaMemsetAsync( &(GPUpub[0].vol_[c*rp->nxy]), 0, z_size * rp->slice_size, GPUpri[0].mystream);
                        
                        if ( rp->use_norm_vol == 1 ) {
                         
                            cudaMemsetAsync( &(GPUpub[0].norm_vol_[c*rp->nxy]), 0, z_size * rp->slice_size, GPUpri[0].mystream );
                            
                        }

                        // NOTE: The header has already been removed from the rmask_ volume.
                        // This could be set up to only copy mask slices within the valid z range; however, it is not clear how helpful this would be
                        // since this is in the outer loop and the data is uint16 precision.
                        if ( rp->explicit_mask == 2 ) { // x,y,z mask w/ z range
                                
                            cudaMemPrefetchAsync( &(GPUpub[0].rmask_[c*rp->nxy]), z_size*rp->nxy*sizeof(unsigned short), GPUid, GPUpri[0].mystream );
                            
                        }
                        
                        for ( q = 0; q < rp->np; q += PROJ_CHUNK ) {
                            
                            p_size = min( rp->np - q, PROJ_CHUNK );

                            // Backproject projection chunk into our volume data
                            DD_backproject_cuda( &GPUpub[0], &GPUpri[0], q, min( q + PROJ_CHUNK, rp->np), c, z_size, GPUpri[0].mystream, perform_filtration );

                        }
                        
                        // perform count normalization
                        if ( rp->use_norm_vol == 1 ) {
                            
                            volume_normalization<<<rp->vgrid, rp->vblock, 0, GPUpri[0].mystream>>>( &(GPUpub[0].vol_[c*rp->nxy]), &(GPUpub[0].norm_vol_[c*rp->nxy]), make_int3( rp->nx, rp->ny, z_size ), np_sub ); 
                    
                        }
                        
                        // v24: copy out volume data chunk to...
                        //      pinned memory, if the output is on the host
                        //      GPU memory, if the output is on the GPU or managed

                        if ( output_on_GPU == 0 ) { // output on the host

                            cudaMemcpyAsync( &(rp->Pinned_Buffer[c*rp->nxy]), &(GPUpub[0].vol_[c*rp->nxy]), z_size*rp->slice_size, cudaMemcpyDefault, GPUpri[0].mystream );
                            
                            // v24: Record an event we can use to copy data out of the pinned memory on the host.
                            cudaEventCreateWithFlags( &done_events[cnt], cudaEventDisableTiming );
                            cudaEventRecord( done_events[cnt], GPUpri[0].mystream );
                            cnt++;

                        }
                        else { // output on the GPU or managed

                            cudaMemcpyAsync( &(X[c*rp->nxy]), &(GPUpub[0].vol_[c*rp->nxy]), z_size*rp->slice_size, cudaMemcpyDefault, GPUpri[0].mystream );

                        }

                    }
                    
                    // If the output is on the host, copy out of pinned memory as soon as possible.
                    if ( output_on_GPU == 0 ) {

                        for ( cnt_host = 0; cnt_host < cnt; cnt_host++ ) {

                            cudaEventSynchronize( done_events[cnt_host] );

                            c = vol_start + VOL_CHUNK*cnt_host;
                            
                            memcpy( &(X[c*rp->nxy]), &(rp->Pinned_Buffer[c*rp->nxy]), min( vol_end - c, VOL_CHUNK ) * rp->slice_size );

                        }

                        free(done_events);

                    }

            }

        } // implicit synchronization between threads
        
        // Check for outstanding CUDA errors
    
            check(2);
            
        // Make sure all devices are done
            
            for ( int i = 0; i < rp->nDevices; i++ ) {

                GPUid = rp->GPU_indices[ i ];

                cudaSetDevice( GPUid );
                
                cudaDeviceSynchronize();
                
            }

}
