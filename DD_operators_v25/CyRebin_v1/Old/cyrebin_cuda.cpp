// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Rebinning of cylindrical, helical projections

// TO DO: Does this properly handle vc != nv/2?

// External headers

    #include "cuda_runtime.h"
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>
    #include <math.h>
    #include <stdint.h>

    #ifdef MEX_COMPILE_FLAG
    #include "mex.h"
    #endif

    #include "error_chk.h"

// Macros

    #define THETA_TRANS 3.0f // 5.0f // Degrees of overlap when transitioning from truncated Chain B data to Chain A data

// Global Variables

/* FUNCTIONS */

    __global__ void cyrebin( cudaTextureObject_t projs_in, float *proj_out, float Rf, float zrot, int rotdir,
                             float uc, float delta_beta, float delta_theta, float dv, // input variables
                             float uc_out, float delta_theta_out, float du_out,       // output variables
                             unsigned int nu_out, unsigned int nv, unsigned int np,   // output variables
                             float mag );                                             // geometric magnification at the central ray
    
    __global__ void cyrebin_B( cudaTextureObject_t projs_A, cudaTextureObject_t projs_B, // Texture objects, input projection data
                               float *proj_out,                                          // rebinned + extrapolated projections: nu_out * np * nv
                               float delta_beta, float delta_theta, float dv,            // currently, the same for A and B, input and output
                               float Rf, float zrot, int rotdir, float du_out,           // currently, the same for A and B, input and output
                               float uc_B, float uc_A, float uc_out,                     // uc_B vs. uc_A * resampling_factor (r_os)
                               float theta_B, float scale_A_B,                           // A to B angle offset, A to B attn. scaling
                               unsigned int nu_in, unsigned int nu_out,                  // nu_B vs. nu_A * resampling_factor
                               unsigned int nv, unsigned int np,                         // shared size variables
                               float mag );                                              // geometric magnification at the central ray
    
    __global__ void rebin_flat( cudaTextureObject_t projs_in, float *proj_out, float Rf, float dsd, int rotdir,
                                float uc, float theta_per_projection, float mm_per_projection, float dv, float du, // input variables
                                float uc_out, float du_out,                                                        // output variables
                                unsigned int nu_out, unsigned int nv, unsigned int np,                             // output variables
                                float mag );                                                                       // geometric magnification at the central ray

    __device__ void sort2(float* a, float* b);

    __global__ void temporal_cyrebin( cudaTextureObject_t projs_in, float *proj_out,
                                  float *proj_time, float *time_out,                       // keep track of the time data was acquired when rebinning
                                  float Rf, float zrot, int rotdir,
                                  float uc, float delta_beta, float delta_theta, float dv, // input variables
                                  float uc_out, float delta_theta_out, float du_out,       // output variables
                                  unsigned int nu_out, unsigned int nv, unsigned int np,   // output variables
                                  float mag );



//    __global__ void temporal_cyrebin( const float *proj_in  , float *proj_out,
//                                      const float *proj_time, float target_time,               // relative time?
//                                      float *norm_vol,                                         // accumulate weights to average overlapping measurements
//                                      float scan_angle,                                        // weight rebinned data by the angular coverage required to reconstruct each slice
//                                      float rotation_time,                                     // time to rotate the gantry by 360 degrees
//                                      int r_os,                                                // output row oversampling factor
//                                      float Rf, float zrot, int rotdir, int nu, float vc,      // input variables
//                                      float uc, float delta_beta, float delta_theta, float dv, // input variables
//                                      float uc_out, float du_out,                              // output variables
//                                      unsigned int nu_out, unsigned int nv, unsigned int np,   // output variables
//                                      float mag );                                             // geometric magnification at the central ray
//
//    __global__ void temporal_cyrebin_v2( cudaTextureObject_t projs_in, float *proj_out,
//                                         const float *proj_time, float target_time,               // relative time?
//                                         float scan_angle,                                        // weight rebinned data by the angular coverage required to reconstruct each slice
//                                         float rotation_time,                                     // time to rotate the gantry by 360 degrees
//                                         int r_os,                                                // output row oversampling factor
//                                         float Rf, float zrot, int rotdir, int nu, float vc,      // input variables
//                                         float uc, float delta_beta, float delta_theta, float dv, // input variables
//                                         float uc_out, float du_out, float delta_theta_out,       // output variables
//                                         unsigned int nu_out, unsigned int nv, unsigned int np,   // output variables
//                                         float mag );                                             // geometric magnification at the central ray

    void cy_rebin( const float *Y_A, const float *Y_B, float *Y_b, int *int_params, double *double_params, int GPU_idx, int det_type );
    
// cylindrical=>parallel rebinning: parallel rebinning of rows in preparation for filtration
//
// Assumes the angle increment remains constant between the input and output projections
    
__global__ void cyrebin( cudaTextureObject_t projs_in, float *proj_out, float Rf, float zrot, int rotdir,
                         float uc, float delta_beta, float delta_theta, float dv, // input variables
                         float uc_out, float delta_theta_out, float du_out,       // output variables
                         unsigned int nu_out, unsigned int nv, unsigned int np,   // output variables
                         float mag )
 {

    // Output coordinates
 
        int u_idx = (blockIdx.x * blockDim.x) + threadIdx.x;
        int v_idx0 = (blockIdx.y * blockDim.y) + threadIdx.y;
        // int theta_idx = (blockIdx.y * blockDim.y) + threadIdx.y;
        
        if (u_idx >= nu_out) return;
        if (v_idx0 >= nv) return;
        
    // Precompute p, beta, beta_idx, which are constant on this thread
        
        // float p         = du_out * ( ( (float) u_idx ) - uc_out ); // p = dx * ( u_idx - uc );
        float p         = du_out * ( ( (float) u_idx ) - uc_out ) * mag; // p = dx * ( u_idx - uc );
        float beta      = asinf( p / Rf ) * 180.0f / 3.14159265f; // degrees
        float beta_idx  = beta / delta_beta + uc;
        
    // Loop over z
        
        float theta, alpha, val, alpha_idx, fv, v_alpha, z_idx, v_idx;
        
        uint64_t idx;
        
        for ( unsigned int np_out_idx = 0; np_out_idx < np; np_out_idx++ ) {
            
            // Compute parameters which depend on theta
            
                v_idx = ( (float) v_idx0 ) + ( (float) np_out_idx ) * delta_theta_out * zrot / dv;

                theta = ( (float) np_out_idx ) * delta_theta_out;

                alpha = theta - rotdir * beta;

                alpha_idx = alpha / delta_theta;

                fv      = v_idx * dv;   // relative z position of rebinned projection
                v_alpha = zrot * alpha; // relative z position of source projection

                z_idx = ( fv - v_alpha ) / dv;
            
            // Read output value at alpha, beta, z
                
                val = tex3D<float>( projs_in, beta_idx + 0.5f, z_idx + 0.5f, alpha_idx + 0.5f);
                
                idx = nu_out * nv * np_out_idx + nu_out * v_idx0 + u_idx;
            
                proj_out[ idx ] = val;
            
        }
    
}

// cylindrical=>parallel rebinning: parallel rebinning of rows in preparation for filtration
// Using Y_A to extrapolate Y_B.
//
// Assumes the angle increment remains constant between the input and output projections
// Output has nu elements along p

__global__ void cyrebin_B( cudaTextureObject_t projs_A, cudaTextureObject_t projs_B, // Texture objects, input projection data
                           float *proj_out,                                          // rebinned + extrapolated projections: nu_out * np * nv
                           float delta_beta, float delta_theta, float dv,            // currently, the same for A and B, input and output
                           float Rf, float zrot, int rotdir, float du_out,           // currently, the same for A and B, input and output
                           float uc_B, float uc_A, float uc_out,                     // uc_B vs. uc_A vs. uc_A * resampling_factor (r_os)
                           float theta_B, float scale_A_B,                           // A to B angle offset, A to B attn. scaling
                           unsigned int nu_in, unsigned int nu_out,                  // nu_B vs. nu_A * resampling_factor
                           unsigned int nv, unsigned int np,                         // shared size variables
                           float mag )                                               // geometric magnification at the central ray
{
    
    // Output coordinates
 
        int u_idx  = (blockIdx.x * blockDim.x) + threadIdx.x;
        int v_idx0 = (blockIdx.y * blockDim.y) + threadIdx.y;
        
        if (u_idx >= nu_out) return;
        if (v_idx0 >= nv) return;
        
    // Precompute p, beta, beta_idx, which are constant on this thread
        
        // float p         = du_out * ( ( (float) u_idx ) - uc_out ); // p = dx * ( u_idx - uc );
        float p         = du_out * ( ( (float) u_idx ) - uc_out ) * mag; // p = dx * ( u_idx - uc );
        float beta      = asinf( p / Rf ) * 180.0f / 3.14159265f; // degrees
        float beta_idx  = beta / delta_beta + uc_B;
        
    // Loop over z
        
        float theta, alpha, val, alpha_idx, fv, v_alpha, z_idx, v_idx, beta_offset, weight_A, weight_B;
        
        float theta_trans_idx = THETA_TRANS / delta_beta;
        
        uint64_t idx;
        
        // Copy A data from the direction of minimum offset
        float rotdir_A = 1.0f;
        if ( theta_B > 90.0f ) {
            
            theta_B = -1.0 * (180.0 - theta_B);
            rotdir_A *= -1;
            
        }
        else {
         
            rotdir_A = rotdir;
            
        }
            
        for ( unsigned int np_out_idx = 0; np_out_idx < np; np_out_idx++ ) {
            
            // Read output value at alpha, beta, z
                
                if ( beta_idx < 0 || beta_idx >= nu_in ) { // If we are out of bounds in chain B, use chain A only.
                    
                    // Compute parameters which depend on theta
            
                    v_idx = ( (float) v_idx0 ) + ( (float) np_out_idx ) * delta_theta * zrot / dv;

                    theta = ( (float) np_out_idx ) * delta_theta + theta_B;

                    alpha = theta - rotdir_A * beta;

                    alpha_idx = alpha / delta_theta;

                    fv      = v_idx * dv;   // relative z position of rebinned projection
                    v_alpha = zrot * alpha; // relative z position of source projection

                    z_idx = ( fv - v_alpha ) / dv;   
                    
                    val = tex3D<float>( projs_A, beta_idx + 0.5f - uc_B + uc_A, z_idx + 0.5f, alpha_idx + 0.5f);
                    
                    val *= scale_A_B;
                    
                }
                else if ( fabsf( beta_idx - uc_B ) <= ( (float) nu_in - 2.0f * theta_trans_idx ) / 2.0f ) { // use B only
                    
                    // Compute parameters which depend on theta
            
                    v_idx = ( (float) v_idx0 ) + ( (float) np_out_idx ) * delta_theta * zrot / dv;

                    theta = ( (float) np_out_idx ) * delta_theta;

                    alpha = theta - rotdir * beta;

                    alpha_idx = alpha / delta_theta;

                    fv      = v_idx * dv;   // relative z position of rebinned projection
                    v_alpha = zrot * alpha; // relative z position of source projection

                    z_idx = ( fv - v_alpha ) / dv;   
                    
                    val = tex3D<float>( projs_B, beta_idx + 0.5f, z_idx + 0.5f, alpha_idx + 0.5f);
                    
                }
                else { // blend A and B
                    
                    // beta_offset > 0
                    beta_offset = fabsf( beta_idx - uc_B ) - ( (float) nu_in - 2.0f * theta_trans_idx ) / 2.0f;
                    
                    val = ( 3.14159265f / 2.0f ) * ( beta_offset * delta_beta / THETA_TRANS );
                    weight_B = cosf( val ); weight_B *= weight_B;
                    weight_A = sinf( val ); weight_A *= weight_A;
                    
                    // Compute A value
                    
                        v_idx = ( (float) v_idx0 ) + ( (float) np_out_idx ) * delta_theta * zrot / dv;

                        theta = ( (float) np_out_idx ) * delta_theta + theta_B;

                        alpha = theta - rotdir_A * beta;

                        alpha_idx = alpha / delta_theta;

                        fv      = v_idx * dv;   // relative z position of rebinned projection
                        v_alpha = zrot * alpha; // relative z position of source projection

                        z_idx = ( fv - v_alpha ) / dv;   

                        val = tex3D<float>( projs_A, beta_idx + 0.5f - uc_B + uc_A, z_idx + 0.5f, alpha_idx + 0.5f);

                        val *= scale_A_B * weight_A;
                    
                    // Compute B value
                    
                        v_idx = ( (float) v_idx0 ) + ( (float) np_out_idx ) * delta_theta * zrot / dv;

                        theta = ( (float) np_out_idx ) * delta_theta;

                        alpha = theta - rotdir * beta;

                        alpha_idx = alpha / delta_theta;

                        fv      = v_idx * dv;   // relative z position of rebinned projection
                        v_alpha = zrot * alpha; // relative z position of source projection

                        z_idx = ( fv - v_alpha ) / dv;   

                        val += weight_B * tex3D<float>( projs_B, beta_idx + 0.5f, z_idx + 0.5f, alpha_idx + 0.5f);
                    
                }
                
                idx = nu_out * nv * np_out_idx + nu_out * v_idx0 + u_idx;
            
                proj_out[ idx ] = val;
            
        }
    
}


// flat cone=>parallel rebinning: parallel rebinning of rows in preparation for filtration
//
// Assumes the angle increment remains constant between the input and output projections
    
__global__ void rebin_flat( cudaTextureObject_t projs_in, float *proj_out, float Rf, float dsd, int rotdir,
                            float uc, float theta_per_projection, float mm_per_projection, float dv, float du, // input variables
                            float uc_out, float du_out,                                                        // output variables
                            unsigned int nu_out, unsigned int nv, unsigned int np,                             // output variables
                            float mag )                                                                        // geometric magnification at the central ray
{
    
    // Output coordinates
 
        int u_out  = (blockIdx.x * blockDim.x) + threadIdx.x;
        int v_out0 = (blockIdx.y * blockDim.y) + threadIdx.y;
        
        if (u_out >= nu_out) return;
        if (v_out0 >= nv) return;
        
    // Precompute values which are constant on this thread
        
        // Distance from uc at the detector plane
        float delta_u = du_out * ( ( (float) u_out ) - uc_out ) * mag;
        
        // Offset angle
        float delta_theta = asinf( delta_u / Rf );
        
        // Input u index
        float u_in = dsd * ( delta_u / Rf ) / du + uc;
        // float u_in = delta_u / du + uc;
        
        // Convert to offset projections
        delta_theta = rotdir * delta_theta * ( 180.0f / 3.14159265f ) / theta_per_projection;
        
    // Loop over z
        
        float theta_in, v_in, val, v_idx; 
        
        uint64_t idx;
        
        for ( unsigned int np_out_idx = 0; np_out_idx < np; np_out_idx++ ) {
            
            // Translate delta_u into a rotation angle (theta_in) and row (v_in)
            
                v_idx = ( (float) v_out0 ) + ( (float) np_out_idx ) * mm_per_projection / dv;
            
                theta_in = ( (float) np_out_idx ) - delta_theta;

                v_in = ( v_idx * dv - theta_in * mm_per_projection ) / dv;
            
            // Read output value at alpha, beta, z
                
                val = tex3D<float>( projs_in, u_in + 0.5f, v_in + 0.5f, theta_in + 0.5f);
                
                idx = nu_out * nv * np_out_idx + nu_out * v_out0 + u_out;
            
                proj_out[ idx ] = val;
            
        }
    
}



__device__ void sort2(float* a, float* b)
{
    if (*a > *b)
    {
        float tmp = *b;
        *b = *a;
        *a = tmp;
    }
}


__global__ void temporal_cyrebin( cudaTextureObject_t projs_in, float *proj_out,
                                  float *proj_time, float *time_out,                       // keep track of the time data was acquired when rebinning
                                  float Rf, float zrot, int rotdir,
                                  float uc, float delta_beta, float delta_theta, float dv, // input variables
                                  float uc_out, float delta_theta_out, float du_out,       // output variables
                                  unsigned int nu_out, unsigned int nv, unsigned int np,   // output variables
                                  float mag )
 {

    // Output coordinates

        int u_idx = (blockIdx.x * blockDim.x) + threadIdx.x;
        int v_idx0 = (blockIdx.y * blockDim.y) + threadIdx.y;
        // int theta_idx = (blockIdx.y * blockDim.y) + threadIdx.y;

        if (u_idx >= nu_out) return;
        if (v_idx0 >= nv) return;

    // Precompute p, beta, beta_idx, which are constant on this thread

        // float p         = du_out * ( ( (float) u_idx ) - uc_out ); // p = dx * ( u_idx - uc );
        float p         = du_out * ( ( (float) u_idx ) - uc_out ) * mag; // p = dx * ( u_idx - uc );
        float beta      = asinf( p / Rf ) * 180.0f / 3.14159265f; // degrees
        float beta_idx  = beta / delta_beta + uc;

    // Loop over z

        float theta, alpha, val, alpha_idx, fv, v_alpha, z_idx, v_idx;
        int alpha_idx_i;

        uint64_t idx;

        for ( unsigned int np_out_idx = 0; np_out_idx < np; np_out_idx++ ) {

            // Compute parameters which depend on theta

                alpha = theta - rotdir * beta;

                alpha_idx = alpha / delta_theta;

                alpha_idx_i = __float2int_rn(alpha_idx);

                if ( alpha_idx_i >= 0 && alpha_idx_i < np ) {

                    v_idx = ( (float) v_idx0 ) + ( (float) np_out_idx ) * delta_theta_out * zrot / dv;

                    theta = ( (float) np_out_idx ) * delta_theta_out;

                    alpha = theta - rotdir * beta;

                    alpha_idx = alpha / delta_theta;

                    fv      = v_idx * dv;   // relative z position of rebinned projection
                    v_alpha = zrot * alpha; // relative z position of source projection

                    z_idx = ( fv - v_alpha ) / dv;

                // Read output value at alpha, beta, z

                    val = tex3D<float>( projs_in, beta_idx + 0.5f, z_idx + 0.5f, alpha_idx + 0.5f);

                    idx = nu_out * nv * np_out_idx + nu_out * v_idx0 + u_idx;

                    proj_out[ idx ] = val;

                    time_out[ idx ] = proj_time[np];

                }

        }

}


// Rebin data for a particular time point
//__global__ void temporal_cyrebin( const float *proj_in  , float *proj_out,
//                                  const float *proj_time, float target_time,               // relative time?
//                                  float *norm_vol,                                         // accumulate weights to average overlapping measurements
//                                  float scan_angle,                                        // weight rebinned data by the angular coverage required to reconstruct each slice
//                                  float rotation_time,                                     // time to rotate the gantry by 360 degrees
//                                  int r_os,                                                // output row oversampling factor
//                                  float Rf, float zrot, int rotdir, int nu, float vc,      // input variables
//                                  float uc, float delta_beta, float delta_theta, float dv, // input variables
//                                  float uc_out, float du_out,                              // output variables
//                                  unsigned int nu_out, unsigned int nv, unsigned int np,   // output variables
//                                  float mag )                                              // geometric magnification at the central ray
//{
//
//    // zrot, delta_theta must be consistent between the input and output projections
//
//    // zrot       : mm / degree
//    // delta_theta: degree / projection
//    // delta_beta : degree / detector element
//
//    // Output projection and threads assigned to this kernel
//
//        int out_proj_idx = blockIdx.x;  // 0 => np-1
//        int tid          = threadIdx.x;
//
//    // Variables
//
//        float u_out[2] = {0.0,0.0};
//        float v_out[2] = {0.0,0.0};
//
//        int   u_in , v_in;
//        float v_idx;
//        // float u_out, v_out;
//        float theta, alpha, beta;
//        float p;
//        float v_alpha, fv;
//        float in_vox;
//        float in_time;
//        int i,j;
//
//        int u_ref0, u_ref1, v_ref0, v_ref1;
//
//        uint64_t idx_in, idx_out;
//
//        float f1, f2;
//
//        float time_distance;
//
//        // float in_theta, out_theta, angle_distance;
//        float cone_angle_2 = delta_beta * max( uc, nu - uc );
//
//    // Time-space equivalence for weighting rebinned projection data based on z offset and temporal offset
//
//        float FWHM_v  = (scan_angle * zrot) / dv / 10.0f;
//        float sigma_v = FWHM_v * 2.354820045030949f;
//        float norm_v  = 1.0f / ( sigma_v * sqrtf( 2.0f * 3.14159265f ) );
//
//        // Multiply this by 1 / the number of beats per second? (maximum temporal resolution possible from multi-sector reconstruction)
//        float FWHM_time  = scan_angle * rotation_time / 360.0f;
//        float sigma_time = FWHM_time * 2.354820045030949f;
//        float norm_time  = 1.0f / ( sigma_time * sqrtf( 2.0f * 3.14159265f ) );
//
//        float weight;
//
//    // Determine which input projections overlap the current output projection along z
//
//        int end_proj   = __float2int_rn( out_proj_idx + ceilf ( ( nv - vc ) * dv / zrot / delta_theta ) );
//        int start_proj = __float2int_rn( out_proj_idx - floorf( ( vc      ) * dv / zrot / delta_theta ) );
//
//        end_proj   = min( np, end_proj   );
//        start_proj = max(  0, start_proj );
//
//    // for all input projections which overlap the current output projection along z
//    for ( int in_proj = start_proj; in_proj < end_proj; in_proj++) {
//
//        //in_theta       = fmodf(in_proj * delta_theta     , 360.0f);
//        //out_theta      = fmodf(out_proj_idx * delta_theta, 360.0f);
//        //angle_distance = fabsf(in_theta - out_theta);
//        //angle_distance = fminf( angle_distance, 360.0f-angle_distance ); // minimum cyclic distance
//        //
//        //// Check to make sure the input projection is within the cone angle of the output projection
//        //// in_theta  = fmodf(in_proj * delta_theta     , 180.0f);
//        //// out_theta = fmodf(out_proj_idx * delta_theta, 180.0f);
//        //
//        // float in_theta  = fmodf(in_proj * delta_theta     , 360.0f);
//        // float out_theta = fmodf(out_proj_idx * delta_theta, 360.0f);
//        //
//        //// angle_distance = fabsf(in_theta - out_theta);
//        //// angle_distance = fminf( angle_distance, 180.0f-angle_distance ); // minimum cyclic distance
//
//        //float angle_distance = fabsf(in_theta - out_theta);
//        //
//        //if ( angle_distance > cone_angle_2 ) {
//        //
//        //    continue;
//        //
//        //}
//
//        // Each thread reads in a pixel from the current input projection
//        // Perform coherent memory reads across threads
//        for (int tid2 = tid; tid2 < nu*nv; tid2 += blockDim.x) {
//
//            // Map tid2 to u,v coordinates in the current input projection, in_proj
//
//                u_in = tid2 % nu;
//                v_in = tid2 / nu;
//
//            // Remap the position of each input pixel to rebinned output coordinates
//
//                // u axis
//                beta     = (u_in - 0.5 - uc) * delta_beta; // beta  = (u_idx - uc) * delta_beta;
//                p        = Rf * sinf( beta * 3.14159265f / 180.0f );
//                u_out[0] = p / ( mag * du_out ) + uc_out;
//
//                beta     = (u_in + 0.5 - uc) * delta_beta; // beta  = (u_idx - uc) * delta_beta;
//                p        = Rf * sinf( beta * 3.14159265f / 180.0f );
//                u_out[1] = p / ( mag * du_out ) + uc_out;
//
//                sort2(u_out,u_out+1);
//
//                // v axis (v_in = z_idx in other code; v_out = v_idx0 in other code)
//                theta = in_proj * delta_theta;
//                beta  = (u_in - uc) * delta_beta; // beta  = (u_idx - uc) * delta_beta;
//                alpha = theta - rotdir * beta;
//
//                v_alpha = zrot * alpha;
//
//                fv       = (v_in - vc - 0.5) * dv + v_alpha;
//                v_idx    = fv / dv;
//                v_out[0] = v_idx - out_proj_idx * delta_theta * zrot / dv + vc;
//
//                fv       = (v_in - vc + 0.5) * dv + v_alpha;
//                v_idx    = fv / dv;
//                v_out[1] = v_idx - out_proj_idx * delta_theta * zrot / dv + vc;
//
//                sort2(v_out,v_out+1);
//
//            // Check if the output point is in bounds for the current output projection
//            // Allow a margin to account for partial overlap of pixel area
//
//                if ( u_out[1] > 0 && u_out[0] < nu_out && v_out[1] > 0 && v_out[0] < nv ) {
//
//                    // Intensity of the input pixel
//                    idx_in = nu * nv * in_proj + nu * v_in + u_in;
//                    in_vox = proj_in[idx_in];
//
//                    // Relative time at which the input projection was acquired
//                    in_time = proj_time[in_proj];
//
//                    // Weight each input pixel intensity by the time and z position relative to the rebinned output time and z position
//                    time_distance = fabsf(in_time - target_time);
//                    time_distance = fminf( time_distance, 1.0f-time_distance ); // minimum cyclic distance
//
//                    // weight = 1.0;
//
//                    // weight = expf( -0.5f * ( time_distance  *  time_distance ) / (sigma_time * sigma_time) );
//
//                    weight = ( norm_v    * expf( -0.5f * ( v_in - ( 0.5f * v_out[1] + 0.5f * v_out[0] ) )
//                                                       * ( v_in - ( 0.5f * v_out[1] + 0.5f * v_out[0] ) )
//                                                       / ( sigma_v * sigma_v ) ) ) *  // prefer source data with similar cone angle
//                             ( norm_time * expf( -0.5f * ( time_distance  *  time_distance ) / (sigma_time * sigma_time) ) );   // prefer input data acquired at the target time
//
//                    // weight = 1.0f;
//
//                    //weight = expf( -0.5f * ( v_in - ( 0.5f * v_out[1] + 0.5f * v_out[0] ) )
//                    //                     * ( v_in - ( 0.5f * v_out[1] + 0.5f * v_out[0] ) )
//                    //                     / ( sigma_v * sigma_v ) ) *  // prefer source data with similar cone angle
//                    //         expf( -0.5f * ( time_distance  *  time_distance ) / (sigma_time * sigma_time) );   // prefer input data acquired at the target time
//                    //
//                    //weight *= weight;
//
//                    // weight = expf( -0.5f * ( v_in - ( 0.5f * v_out[1] + 0.5f * v_out[0] ) ) * ( v_in - ( 0.5f * v_out[1] + 0.5f * v_out[0] ) ) / ( sigma_v * sigma_v ) );  // prefer source data with similar cone angle
//
//                    // Early stopping to save computation time
//                    if (weight < 0.001) {
//
//                        continue;
//
//                    }
//
//                    //if ( time_distance > FWHM_time / 2.0f || fabsf( v_in - ( 0.5f * v_out[1] + 0.5f * v_out[0] ) ) > FWHM_v / 2.0f ) {
//                    //
//                    //    continue;
//                    //
//                    //}
//
//                    // weight = ( FWHM_time / ( time_distance + 1e-6 ) ) * ( FWHM_v / ( fabsf( v_in - ( 0.5f * v_out[1] + 0.5f * v_out[0] ) ) + 1e-6) );
//
//                    // Add the input data point to the rebinned projections
//                    // Apply and keep track of the weights
//                    // Weight by overlap fractions
//
//                        u_ref0 = __float2int_rn( floorf( u_out[0] ) );
//                        u_ref1 = __float2int_rn( ceilf ( u_out[1] ) );
//
//                        v_ref0 = __float2int_rn( floorf( v_out[0] ) );
//                        v_ref1 = __float2int_rn( ceilf ( v_out[1] ) );
//
//                        for (i = u_ref0; i < u_ref1; i++) {
//
//                            f1 = fmaxf(fminf( i+1, u_out[1] ) - fmaxf( i, u_out[0] ),0);
//
//                            for (j = v_ref0; j < v_ref1; j++) {
//
//                                f2 = fmaxf(fminf( j+1, v_out[1] ) - fmaxf( j, v_out[0] ),0);
//
//                                idx_out = nu_out * nv * out_proj_idx + nu_out * j + i;
//
//                                // check to make sure we have not gone out of bounds before updating the rebinned projection and weights
//                                if ( i >= 0 && i < nu_out && j >= 0 && j < nv  ) {
//
//                                    atomicAdd( proj_out + idx_out, f1 * f2 * in_vox * weight );
//                                    atomicAdd( norm_vol + idx_out, f1 * f2 * weight );
//
//                                }
//
//                            }
//
//                        }
//
//
//                }
//
//        }
//
//    }
//
//    // Weight normalize projections after all data points have been added
//    __syncthreads();
//
//    for (int tid2 = tid; tid2 < nu_out*nv; tid2 += blockDim.x) {
//
//        // Map tid2 to u,v coordinates in the current output projection, out_proj_idx
//
//            u_ref0 = tid2 % nu_out;
//            v_ref0 = tid2 / nu_out;
//
//        // Normalize weighted sum of contributing data points
//
//            idx_out = nu_out * nv * out_proj_idx + nu_out * v_ref0 + u_ref0;
//
//            proj_out[idx_out] = proj_out[idx_out] / max( norm_vol[idx_out], 1e-6 );
//
//    }
//
//}
//
//
//__global__ void temporal_cyrebin_v2( cudaTextureObject_t projs_in, float *proj_out,
//                                     const float *proj_time, float target_time,               // relative time?
//                                     float scan_angle,                                        // weight rebinned data by the angular coverage required to reconstruct each slice
//                                     float rotation_time,                                     // time to rotate the gantry by 360 degrees
//                                     int r_os,                                                // output row oversampling factor
//                                     float Rf, float zrot, int rotdir, int nu, float vc,      // input variables
//                                     float uc, float delta_beta, float delta_theta, float dv, // input variables
//                                     float uc_out, float du_out, float delta_theta_out,       // output variables
//                                     unsigned int nu_out, unsigned int nv, unsigned int np,   // output variables
//                                     float mag )                                              // geometric magnification at the central ray
// {
//
//    // Output coordinates
//
//        int u_idx = (blockIdx.x * blockDim.x) + threadIdx.x;
//        int v_idx0 = (blockIdx.y * blockDim.y) + threadIdx.y;
//        // int theta_idx = (blockIdx.y * blockDim.y) + threadIdx.y;
//
//        if (u_idx >= nu_out) return;
//        if (v_idx0 >= nv) return;
//
//    // Precompute p, beta, beta_idx, which are constant on this thread
//
//        // float p         = du_out * ( ( (float) u_idx ) - uc_out ); // p = dx * ( u_idx - uc );
//        float p         = du_out * ( ( (float) u_idx ) - uc_out ) * mag; // p = dx * ( u_idx - uc );
//        float beta      = asinf( p / Rf ) * 180.0f / 3.14159265f; // degrees
//        float beta_idx  = beta / delta_beta + uc;
//
//    // Time-space equivalence for weighting rebinned projection data based on z offset and temporal offset
//
//        float FWHM_v  = (scan_angle * zrot) / dv / 5.0f;
//        float sigma_v = FWHM_v * 2.354820045030949f;
//        float norm_v  = 1.0f / ( sigma_v * sqrtf( 2.0f * 3.14159265f ) );
//
//        // Multiply this by 1 / the number of beats per second? (maximum temporal resolution possible from multi-sector reconstruction)
//        float FWHM_time  = scan_angle * rotation_time / 360.0f;
//        float sigma_time = FWHM_time * 2.354820045030949f;
//        float norm_time  = 1.0f / ( sigma_time * sqrtf( 2.0f * 3.14159265f ) );
//
//        float time_distance, weight;
//
//        float in_time;
//
//        int alpha_idx_i;
//
//        float v_distance;
//
//    // Interpolation weights - keep track of weights when mapping data points to subsequent rotations
//    // Rotate weights in a cycle, using the oldest for normalization
//
//        //float total_weight[15] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
//
//        //int current_weight_index = 0;
//
//        float total_weight;
//
//    // Loop over z
//
//        float theta, alpha, val, alpha_idx, fv, v_alpha, z_idx, v_idx;
//
//        uint64_t idx;
//
//        for ( int np_out_idx = 0; np_out_idx < np; np_out_idx++ ) {
//
//            total_weight = 0.0f;
//
//            // output index
//            idx = nu_out * nv * np_out_idx + nu_out * v_idx0 + u_idx;
//
//            for ( int rot_offset = -7; rot_offset <= 7; rot_offset++ ) {
//
//                v_idx = v_idx0 + ( np_out_idx * delta_theta_out + rot_offset * 360.0f ) * zrot / dv;
//
//                theta = np_out_idx * delta_theta_out + rot_offset * 360.0f;
//
//                alpha = theta - rotdir * beta;
//
//                alpha_idx = alpha / delta_theta;
//
//                alpha_idx_i = __float2int_rn(alpha_idx);
//
//                if ( alpha_idx_i >= 0 && alpha_idx_i <= np ) {
//
//                    fv      = v_idx * dv;   // relative z position of rebinned projection
//                    v_alpha = zrot * alpha; // relative z position of source projection
//
//                    z_idx = ( fv - v_alpha ) / dv;
//
//                    val = tex3D<float>( projs_in, beta_idx + 0.5f, z_idx + 0.5f, alpha_idx + 0.5f); // Read output value at alpha, beta, z
//
//                    // Weight each input pixel intensity by the time and z position relative to the rebinned output time and z position
//                    in_time = proj_time[ alpha_idx_i ];
//                    time_distance = fabsf(in_time - target_time);
//                    time_distance = fminf( time_distance, 1.0f-time_distance ); // minimum cyclic distance
//
//                    v_distance = fabsf( rot_offset * 360.0f * zrot / dv );
//
//                    weight = expf( -0.5f * ( time_distance  *  time_distance ) / (sigma_time * sigma_time) ) *
//                             expf( -0.5f * ( v_distance     *  v_distance    ) / (sigma_v    * sigma_v   ) );
//
//                    // Write output value
//                    proj_out[ idx ] += val * weight;
//                    total_weight += weight;
//
//                }
//
//            }
//
//            // Normalize the target data point
//            proj_out[ idx ] /= max( total_weight, 1e-6 );
//
//        }
//
//}
//
//
//__global__ void temporal_cyrebin_v3( cudaTextureObject_t projs_in, float *proj_out,
//                                     const float *proj_time, float target_time,               // relative time?
//                                     float scan_angle,                                        // weight rebinned data by the angular coverage required to reconstruct each slice
//                                     float rotation_time,                                     // time to rotate the gantry by 360 degrees
//                                     int r_os,                                                // output row oversampling factor
//                                     float Rf, float zrot, int rotdir, int nu, float vc,      // input variables
//                                     float uc, float delta_beta, float delta_theta, float dv, // input variables
//                                     float uc_out, float du_out, float delta_theta_out,       // output variables
//                                     unsigned int nu_out, unsigned int nv, unsigned int np,   // output variables
//                                     float mag )                                              // geometric magnification at the central ray
// {
//
//    // Output coordinates
//
//        int u_idx = (blockIdx.x * blockDim.x) + threadIdx.x;
//        int v_idx0 = (blockIdx.y * blockDim.y) + threadIdx.y;
//        // int theta_idx = (blockIdx.y * blockDim.y) + threadIdx.y;
//
//        if (u_idx >= nu_out) return;
//        if (v_idx0 >= nv) return;
//
//    // Precompute p, beta, beta_idx, which are constant on this thread
//
//        // float p         = du_out * ( ( (float) u_idx ) - uc_out ); // p = dx * ( u_idx - uc );
//        float p         = du_out * ( ( (float) u_idx ) - uc_out ) * mag; // p = dx * ( u_idx - uc );
//        float beta      = asinf( p / Rf ) * 180.0f / 3.14159265f; // degrees
//        float beta_idx  = beta / delta_beta + uc;
//
//    // Time-space equivalence for weighting rebinned projection data based on z offset and temporal offset
//
//        //float FWHM_v  = (scan_angle * zrot) / dv;
//        //float sigma_v = FWHM_v * 2.354820045030949f;
//        //float norm_v  = 1.0f / ( sigma_v * sqrtf( 2.0f * 3.14159265f ) );
//
//        // Multiply this by 1 / the number of beats per second? (maximum temporal resolution possible from multi-sector reconstruction)
//        //float FWHM_time  = scan_angle * rotation_time / 360.0f;
//        //float sigma_time = FWHM_time * 2.354820045030949f;
//        //float norm_time  = 1.0f / ( sigma_time * sqrtf( 2.0f * 3.14159265f ) );
//
//        float time_distance, weight;
//
//        float in_time;
//
//        int alpha_idx_i;
//
//        float v_distance;
//
//    // Interpolation weights - keep track of weights when mapping data points to subsequent rotations
//    // Rotate weights in a cycle, using the oldest for normalization
//
//        //float total_weight[15] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
//
//        //int current_weight_index = 0;
//
//        // float total_weight;
//
//    // Loop over z
//
//        float theta, alpha, val, alpha_idx, fv, v_alpha, z_idx, v_idx;
//
//        uint64_t idx;
//
//        for ( int np_out_idx = 0; np_out_idx < np; np_out_idx++ ) {
//
//            // total_weight = 0.0f;
//
//            // output index
//            idx = nu_out * nv * np_out_idx + nu_out * v_idx0 + u_idx;
//
//            v_idx = v_idx0 + np_out_idx * delta_theta_out * zrot / dv;
//
//            theta = np_out_idx * delta_theta_out;
//
//            alpha = theta - rotdir * beta;
//
//            alpha_idx = alpha / delta_theta;
//
//            alpha_idx_i = __float2int_rn(alpha_idx);
//
//            if ( alpha_idx_i >= 0 && alpha_idx_i <= np ) {
//
//                // Weight each input pixel intensity by the time and z position relative to the rebinned output time and z position
//                in_time = proj_time[ alpha_idx_i ];
//                time_distance = fabsf(in_time - target_time);
//                time_distance = fminf( time_distance, 1.0f-time_distance ); // minimum cyclic distance
//
//                if ( time_distance > 0.10f && time_distance < 0.12f ) {
//
//                    weight = ( 0.12f - time_distance ) / ( 0.12f - 0.10f );
//
//                }
//                else if ( time_distance < 0.10f ) {
//
//                    weight = 1.0f;
//
//                }
//                else {
//
//                    weight = 0.0;
//
//                }
//
//                fv      = v_idx * dv;   // relative z position of rebinned projection
//                v_alpha = zrot * alpha; // relative z position of source projection
//
//                z_idx = ( fv - v_alpha ) / dv;
//
//                val = tex3D<float>( projs_in, beta_idx + 0.5f, z_idx + 0.5f, alpha_idx + 0.5f); // Read output value at alpha, beta, z
//
//                // v_distance = fabsf( rot_offset * 360.0f * zrot / dv );
//
//                //weight = expf( -0.5f * ( time_distance  *  time_distance ) / (sigma_time * sigma_time) ); // *
//                // expf( -0.5f * ( v_distance     *  v_distance    ) / (sigma_v    * sigma_v   ) );
//
//                // Write output value
//                proj_out[ idx ] += val * weight;
//                // total_weight += weight;
//
//            }
//
//            // Normalize the target data point
//            // proj_out[ idx ] /= max( total_weight, 1e-6 );
//
//        }
//
//}


//__global__ void temporal_cyrebin_v4( cudaTextureObject_t projs_in, float *proj_out,
//                                     const float *proj_time, float target_time,               // relative time?
//                                     float scan_angle,                                        // weight rebinned data by the angular coverage required to reconstruct each slice
//                                     float rotation_time,                                     // time to rotate the gantry by 360 degrees
//                                     int r_os,                                                // output row oversampling factor
//                                     float Rf, float zrot, int rotdir, int nu, float vc,      // input variables
//                                     float uc, float delta_beta, float delta_theta, float dv, // input variables
//                                     float uc_out, float du_out, float delta_theta_out,       // output variables
//                                     unsigned int nu_out, unsigned int nv, unsigned int np,   // output variables
//                                     float mag )                                              // geometric magnification at the central ray
// {
//
//    // Output coordinates
//
//        int u_idx = (blockIdx.x * blockDim.x) + threadIdx.x;
//        int v_idx0 = (blockIdx.y * blockDim.y) + threadIdx.y;
//        // int theta_idx = (blockIdx.y * blockDim.y) + threadIdx.y;
//
//        if (u_idx >= nu_out) return;
//        if (v_idx0 >= nv) return;
//
//    // Precompute p, beta, beta_idx, which are constant on this thread
//
//        // float p         = du_out * ( ( (float) u_idx ) - uc_out ); // p = dx * ( u_idx - uc );
//        float p         = du_out * ( ( (float) u_idx ) - uc_out ) * mag; // p = dx * ( u_idx - uc );
//        float beta      = asinf( p / Rf ) * 180.0f / 3.14159265f; // degrees
//        float beta_idx  = beta / delta_beta + uc;
//
//    // Time-space equivalence for weighting rebinned projection data based on z offset and temporal offset
//
//        //float FWHM_v  = (scan_angle * zrot) / dv / 5.0f;
//        //float sigma_v = FWHM_v * 2.354820045030949f;
//        //float norm_v  = 1.0f / ( sigma_v * sqrtf( 2.0f * 3.14159265f ) );
//
//        // Multiply this by 1 / the number of beats per second? (maximum temporal resolution possible from multi-sector reconstruction)
//        float FWHM_time  = scan_angle * rotation_time / 360.0f / 2.0f;
//        float sigma_time = FWHM_time * 2.354820045030949f;
//        // float norm_time  = 1.0f / ( sigma_time * sqrtf( 2.0f * 3.14159265f ) );
//
//        float FWHM_I  = 50.0f;
//        float sigma_I = FWHM_v * 2.354820045030949f;
//        // float norm_I  = 1.0f / ( sigma_I * sqrtf( 2.0f * 3.14159265f ) );
//
//        float time_distance, weight;
//
//        float in_time;
//
//        int alpha_idx_i;
//
//        float v_distance;
//
//    // Interpolation weights - keep track of weights when mapping data points to subsequent rotations
//    // Rotate weights in a cycle, using the oldest for normalization
//
//        //float total_weight[15] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
//
//        //int current_weight_index = 0;
//
//        float total_weight;
//
//    // Loop over z
//
//        float theta, alpha, val, alpha_idx, fv, v_alpha, z_idx, v_idx;
//
//        uint64_t idx;
//
//        for ( int np_out_idx = 0; np_out_idx < np; np_out_idx++ ) {
//
//            total_weight = 0.0f;
//
//            // output index
//            idx = nu_out * nv * np_out_idx + nu_out * v_idx0 + u_idx;
//
//            for ( int rot_offset = -7; rot_offset <= 7; rot_offset++ ) {
//
//                v_idx = v_idx0 + ( np_out_idx * delta_theta_out + rot_offset * 360.0f ) * zrot / dv;
//
//                theta = np_out_idx * delta_theta_out + rot_offset * 360.0f;
//
//                alpha = theta - rotdir * beta;
//
//                alpha_idx = alpha / delta_theta;
//
//                alpha_idx_i = __float2int_rn(alpha_idx);
//
//                if ( alpha_idx_i >= 0 && alpha_idx_i <= np ) {
//
//                    fv      = v_idx * dv;   // relative z position of rebinned projection
//                    v_alpha = zrot * alpha; // relative z position of source projection
//
//                    z_idx = ( fv - v_alpha ) / dv;
//
//                    val = tex3D<float>( projs_in, beta_idx + 0.5f, z_idx + 0.5f, alpha_idx + 0.5f); // Read output value at alpha, beta, z
//
//                    // Weight each input pixel intensity by the time and z position relative to the rebinned output time and z position
//                    in_time = proj_time[ alpha_idx_i ];
//                    time_distance = fabsf(in_time - target_time);
//                    time_distance = fminf( time_distance, 1.0f-time_distance ); // minimum cyclic distance
//
//                    v_distance = fabsf( rot_offset * 360.0f * zrot / dv );
//
//                    if ( time_distance < 0.09f ) {
//
//                        weight = expf( -0.5f * ( v_distance     *  v_distance    ) / (sigma_v    * sigma_v   ) );
//
//                    }
//                    else {
//
//                        time_distance = time_distance - 0.09f;
//
//                        weight = expf( -0.5f * ( time_distance  *  time_distance ) / (sigma_time * sigma_time) ) *
//                                 expf( -0.5f * ( v_distance     *  v_distance    ) / (sigma_v    * sigma_v   ) );
//
//                    }
//
//                    // Write output value
//                    proj_out[ idx ] += val * weight;
//                    total_weight += weight;
//
//                }
//
//            }
//
//            // Normalize the target data point
//            proj_out[ idx ] /= max( total_weight, 1e-6 );
//
//        }
//
//}

    
void cy_rebin( const float *Y_A, const float *Y_B, float *Y_b, int *int_params, double *double_params, float *proj_time, float *proj_time_out, int GPU_idx, int det_type )
{

    // Extract parameters
    
        // Input size parameters
        uint64_t nu_A = int_params[0];
        uint64_t nu_B = int_params[1];
        uint64_t nv   = int_params[2];
        uint64_t np   = int_params[3];
        uint64_t r_os = int_params[4];
        int rotdir    = int_params[5]; // can be negative...
        
        double Rf, zrot, delta_theta, delta_beta, dv, uc_A, uc_B;
        double du_out, theta_B, scale_A_B, mag, dsd, theta_per_projection, mm_per_projection, du_in;
        double vc_A, scan_angle, rotation_time, target_time;

        dim3 grid, block;

        float *Y_Bb_d, *Y_Ab_d, *proj_time_d, *proj_time_out_d, *norm_vol, *Y_A_in_d;

    
        if (det_type == 1) { // cylindrical
            
            // Double parameters
            Rf          = double_params[0]; 
            zrot        = double_params[1];
            delta_theta = double_params[2];
            delta_beta  = double_params[3];
            dv          = double_params[4]; 
            uc_A        = double_params[5];
            uc_B        = double_params[6];
            du_out      = double_params[7];
            theta_B     = double_params[8];
            scale_A_B   = double_params[9];
            mag         = double_params[10];

        }
        else if (det_type == 2) { // cylindrical, temporal rebinning

            Rf            = double_params[0];
            zrot          = double_params[1];
            delta_theta   = double_params[2];
            delta_beta    = double_params[3];
            dv            = double_params[4];
            uc_A          = double_params[5];
            vc_A          = double_params[6];
            uc_B          = double_params[7];
            du_out        = double_params[8];
            theta_B       = double_params[9];
            scale_A_B     = double_params[10];
            mag           = double_params[11];
            // scan_angle    = double_params[12];
            // rotation_time = double_params[13];
            // target_time   = double_params[14];

        }
        else {               // flat, cone-beam

            // Double parameters
            Rf                   = double_params[0]; 
            dsd                  = double_params[1];
            theta_per_projection = double_params[2];
            mm_per_projection    = double_params[3];
            dv                   = double_params[4]; 
            uc_A                 = double_params[5];
            uc_B                 = double_params[6];
            du_in                = double_params[7];
            du_out               = double_params[8];
            theta_B              = double_params[9];
            scale_A_B            = double_params[10];
            mag                  = double_params[11];

        }
        
    // Set the GPU before we allocate GPU memory and do computation
        
        int nDevices_found;

        // Total number of GPUs visible to this program.
        cudaGetDeviceCount(&nDevices_found);
        
        if (nDevices_found == 0) {

            ERR("No GPUs found.");

        }
        
        if ( GPU_idx < 0 || GPU_idx > nDevices_found-1 ) {
         
            ERR("Input GPU_idx out of bounds for found devices (indexing starts at 0).");
            
        }
        
        cudaSetDevice( GPU_idx );
    
    // Set up texture, projs_A
    // We need this to rebin chain A or chain B.
    // Not used for temporal rebinning.

        // Local variables
        cudaTextureObject_t projs_A, projs_B;
        cudaArray *d_cuArr_A, *d_cuArr_B;
        cudaExtent extent_A, extent_B;
        cudaResourceDesc texRes_A, texRes_B;
        cudaTextureDesc texDescr_A, texDescr_B;
        cudaChannelFormatDesc channelDesc_A, channelDesc_B;
        cudaMemcpy3DParms memcpy3Dparams_A = {0}; // parameters for 3D memcpy from host memory to CUDA array
        cudaMemcpy3DParms memcpy3Dparams_B = {0};

        // Copy input projection data into CUDA array
        extent_A                = make_cudaExtent(nu_A,nv,np);    // CUDA array size
        channelDesc_A           = cudaCreateChannelDesc<float>(); // device array format
        cudaMalloc3DArray( &d_cuArr_A, &channelDesc_A, extent_A); // allocate CUDA array memory
        memcpy3Dparams_A.extent   = extent_A;
        memcpy3Dparams_A.srcPtr   = make_cudaPitchedPtr((void*) Y_A, extent_A.width*sizeof(float), extent_A.width, extent_A.height);
        memcpy3Dparams_A.dstArray = d_cuArr_A;
        memcpy3Dparams_A.kind     = cudaMemcpyHostToDevice;
        cudaMemcpy3D(&memcpy3Dparams_A);						  // copy volume to CUDA array

        // Set up texture object using the CUDA array
        memset(&texRes_A, 0, sizeof(cudaResourceDesc));
        texRes_A.resType            = cudaResourceTypeArray;
        texRes_A.res.array.array    = d_cuArr_A;
        memset(&texDescr_A, 0, sizeof(cudaTextureDesc));
        texDescr_A.normalizedCoords = false;
        texDescr_A.filterMode       = cudaFilterModeLinear; // value varies within a pixel
        texDescr_A.addressMode[0]   = cudaAddressModeClamp; // clamp
        texDescr_A.addressMode[1]   = cudaAddressModeClamp;
        texDescr_A.addressMode[2]   = cudaAddressModeClamp;
        texDescr_A.readMode         = cudaReadModeElementType;
        cudaCreateTextureObject(&projs_A, &texRes_A, &texDescr_A, NULL);

        check(0);
        
    // If we are rebinning chain_A
    if ( nu_B == 0 && det_type == 1 ) {

        // Declarations and allocations

            // Grid and block setup (divisibility not required)
            grid  = dim3( (r_os * nu_A + 8 - 1) / 8, (nv + 8 - 1) / 8, 1 );
            block = dim3( 8, 8, 1 );

            check(1);

        // Allocate output, Y_Ab_d

            cudaMallocManaged( (void **) &Y_Ab_d, r_os * nu_A * nv * np * sizeof(float), cudaMemAttachGlobal);

            cudaMemset( Y_Ab_d, 0, r_os * nu_A * nv * np * sizeof(float) );

            check(2);

        // Perform rebinning

            cyrebin<<<grid,block>>>( projs_A, Y_Ab_d, Rf, zrot, rotdir,
                                     uc_A, delta_beta, delta_theta, dv,
                                     ( (double) r_os ) * uc_A, delta_theta, du_out, // assume delta_theta_out = delta_theta
                                     r_os * nu_A, nv, np,
                                     mag );

            check(3);

        // Copy Y_A results to output Y_b

            cudaDeviceSynchronize(); // make sure our managed memory is up-to-date on the host...

            cudaMemcpy( Y_b, Y_Ab_d, r_os * nu_A * nv * np * sizeof(float), cudaMemcpyDefault );

            check(4);
            
        // Free memory
        
            cudaDestroyTextureObject( projs_A );
            cudaFreeArray( d_cuArr_A );

            cudaFree(Y_Ab_d);
            
    }
    else if ( nu_B > 0 && det_type == 1 ) { // Else if we are rebinning chain_B

        // Declarations and allocations

            // Grid and block setup (divisibility not required)
            grid  = dim3( (r_os * nu_A + 8 - 1) / 8, (nv + 8 - 1) / 8, 1 );
            block = dim3( 8, 8, 1 );

            check(5);
        
        // Set up texture, projs_B

            // Copy input projection data into CUDA array
            extent_B                = make_cudaExtent(nu_B,nv,np);    // CUDA array size
            channelDesc_B           = cudaCreateChannelDesc<float>(); // device array format
            cudaMalloc3DArray( &d_cuArr_B, &channelDesc_B, extent_B); // allocate CUDA array memory
            memcpy3Dparams_B.extent   = extent_B;
            memcpy3Dparams_B.srcPtr   = make_cudaPitchedPtr((void*) Y_B, extent_B.width*sizeof(float), extent_B.width, extent_B.height);
            memcpy3Dparams_B.dstArray = d_cuArr_B;
            memcpy3Dparams_B.kind     = cudaMemcpyHostToDevice;
            cudaMemcpy3D(&memcpy3Dparams_B);						  // copy volume to CUDA array

            // Set up texture object using the CUDA array
            memset(&texRes_B, 0, sizeof(cudaResourceDesc));
            texRes_B.resType            = cudaResourceTypeArray;
            texRes_B.res.array.array    = d_cuArr_B;
            memset(&texDescr_B, 0, sizeof(cudaTextureDesc));
            texDescr_B.normalizedCoords = false;
            texDescr_B.filterMode       = cudaFilterModeLinear; // value varies within a pixel
            texDescr_B.addressMode[0]   = cudaAddressModeClamp; // clamp
            texDescr_B.addressMode[1]   = cudaAddressModeClamp;
            texDescr_B.addressMode[2]   = cudaAddressModeClamp;
            texDescr_B.readMode         = cudaReadModeElementType;
            cudaCreateTextureObject(&projs_B, &texRes_B, &texDescr_B, NULL);

            check(6);

        // Allocate output, Y_Bb_d

            cudaMallocManaged( (void **) &Y_Bb_d, r_os * nu_A * nv * np * sizeof(float), cudaMemAttachGlobal);

            cudaMemset( Y_Bb_d, 0, r_os * nu_A * nv * np * sizeof(float) );

            check(7);

        // Grid and block setup (divisibility not required)

            // in the current version of the code, these stay the same...

        // Perform rebinning

            cyrebin_B<<<grid,block>>>( projs_A, projs_B,
                                       Y_Bb_d,
                                       delta_beta, delta_theta, dv,
                                       Rf, zrot, rotdir, du_out,
                                       uc_B, uc_A, ( (double) r_os ) * uc_A,
                                       theta_B, scale_A_B,
                                       nu_B, r_os * nu_A,
                                       nv, np,
                                       mag );

            check(8);

        // Copy Y_B results

            cudaDeviceSynchronize(); // make sure our managed memory is up-to-date on the host...

            cudaMemcpy( Y_b, Y_Bb_d, r_os * nu_A * nv * np * sizeof(float), cudaMemcpyDefault );

            check(9);

        // Free memory

            cudaDestroyTextureObject( projs_A );
            cudaFreeArray( d_cuArr_A );

            cudaDestroyTextureObject( projs_B );
            cudaFreeArray( d_cuArr_B );

            cudaFree(Y_Bb_d);
     
    }
    else if (det_type == 0) { // Flat, cone-beam rebinning

        // Declarations and allocations

            // Grid and block setup (divisibility not required)
            grid  = dim3( (r_os * nu_A + 8 - 1) / 8, (nv + 8 - 1) / 8, 1 );
            block = dim3( 8, 8, 1 );

            check(10);
        
        // Allocate output, Y_Ab_d

            cudaMallocManaged( (void **) &Y_Ab_d, r_os * nu_A * nv * np * sizeof(float), cudaMemAttachGlobal);

            cudaMemset( Y_Ab_d, 0, r_os * nu_A * nv * np * sizeof(float) );

            check(11);

        // Perform rebinning
            
            // Add: dsd, theta_per_projection, mm_per_projection, du_in
            // Remove: zrot, delta_beta, delta_theta
            
            rebin_flat<<<grid,block>>>( projs_A, Y_Ab_d, Rf, dsd, rotdir,
                                        uc_A, theta_per_projection, mm_per_projection, dv, du_in, // input variables
                                        ( (double) r_os ) * uc_A, du_out,                         // output variables
                                        r_os * nu_A, nv, np,                                      // output variables
                                        mag );                                                    // geometric magnification at the central ray
            
            check(12);

        // Copy Y_A results to output Y_b

            cudaDeviceSynchronize(); // make sure our managed memory is up-to-date on the host...

            cudaMemcpy( Y_b, Y_Ab_d, r_os * nu_A * nv * np * sizeof(float), cudaMemcpyDefault );

            check(13);

        // Free memory

            cudaDestroyTextureObject( projs_A );
            cudaFreeArray( d_cuArr_A );

            cudaFree(Y_Ab_d);
        
    }
    else if (det_type == 2) { // Cylindrical, temporal rebinning

        // Declarations and allocations

            // Grid and block setup (divisibility not required)
            grid  = dim3( (r_os * nu_A + 8 - 1) / 8, (nv + 8 - 1) / 8, 1 );
            block = dim3( 8, 8, 1 );

            check(14);

        // Allocate output, Y_Ab_d

            cudaMallocManaged( (void **) &Y_Ab_d         , r_os * nu_A * nv * np * sizeof(float), cudaMemAttachGlobal);
            cudaMallocManaged( (void **) &proj_time_out_d, r_os * nu_A * nv * np * sizeof(float), cudaMemAttachGlobal);

            cudaMemset( Y_Ab_d         , 0, r_os * nu_A * nv * np * sizeof(float) );
            cudaMemset( proj_time_out_d, 0, r_os * nu_A * nv * np * sizeof(float) );

            cudaMallocManaged( (void **) &proj_time_d, np * sizeof(float), cudaMemAttachGlobal);
            cudaMemcpy( proj_time_d, proj_time, np * sizeof(float), cudaMemcpyDefault );

            check(15);

        // Perform rebinning

            temporal_cyrebin<<<grid,block>>>( projs_A, Y_Ab_d,
                                              proj_time_d, proj_time_out_d,
                                              Rf, zrot, rotdir,
                                              uc_A, delta_beta, delta_theta, dv,
                                              ( (double) r_os ) * uc_A, delta_theta, du_out, // assume delta_theta_out = delta_theta
                                              r_os * nu_A, nv, np,
                                              mag );

            check(16);

        // Copy Y_A results to output Y_b

            cudaDeviceSynchronize(); // make sure our managed memory is up-to-date on the host...

            cudaMemcpy( Y_b          , Y_Ab_d         , r_os * nu_A * nv * np * sizeof(float), cudaMemcpyDefault );
            cudaMemcpy( proj_time_out, proj_time_out_d, r_os * nu_A * nv * np * sizeof(float), cudaMemcpyDefault );

            check(17);

        // Free memory

            cudaDestroyTextureObject( projs_A );
            cudaFreeArray( d_cuArr_A );

            cudaFree(Y_Ab_d);
            cudaFree(proj_time_out_d);

//        // Declarations and allocations
//
//            // Grid and block setup (divisibility not required)
//            grid  = dim3(  np, 1, 1 );
//            block = dim3( 128, 1, 1 );
//
//            check(14);
//
//        // Allocate output, Y_Ab_d, Y_W
//
//            cudaMallocManaged( (void **) &Y_Ab_d     , r_os * nu_A * nv * np * sizeof(float), cudaMemAttachGlobal);
//            cudaMallocManaged( (void **) &norm_vol   , r_os * nu_A * nv * np * sizeof(float), cudaMemAttachGlobal);
//            cudaMallocManaged( (void **) &proj_time_d,                    np * sizeof(float), cudaMemAttachGlobal);
//            cudaMallocManaged( (void **) &Y_A_in_d   ,        nu_A * nv * np * sizeof(float), cudaMemAttachGlobal);
//
//            cudaMemset( Y_Ab_d  , 0, r_os * nu_A * nv * np * sizeof(float) );
//            cudaMemset( norm_vol, 0, r_os * nu_A * nv * np * sizeof(float) );
//
//            cudaMemcpy( proj_time_d, proj_time,             np * sizeof(float), cudaMemcpyDefault );
//            cudaMemcpy(    Y_A_in_d,       Y_A, nu_A * nv * np * sizeof(float), cudaMemcpyDefault );
//
//            check(15);
//
//        // Perform rebinning
//
//            //__global__ void temporal_cyrebin( const float *proj_in  , float *proj_out,
//            //                                  const float *proj_time, float target_time,               // relative time?
//            //                                  float *norm_vol,                                         // accumulate weights to average overlapping measurements
//            //                                  float scan_angle,                                        // weight rebinned data by the angular coverage required to reconstruct each slice
//            //                                  float rotation_time,                                     // time to rotate the gantry by 360 degrees
//            //                                  int r_os,                                                // output row oversampling factor
//            //                                  float Rf, float zrot, int rotdir, int nu,                // input variables
//            //                                  float uc, float delta_beta, float delta_theta, float dv, // input variables
//            //                                  float uc_out, float du_out,                              // output variables
//            //                                  unsigned int nu_out, unsigned int nv, unsigned int np,   // output variables
//            //                                  float mag )                                              // geometric magnification at the central ray
//
//            temporal_cyrebin<<<grid,block>>>( Y_A_in_d, Y_Ab_d,
//                                              proj_time_d, target_time,
//                                              norm_vol,
//                                              scan_angle,
//                                              rotation_time,
//                                              r_os,
//                                              Rf, zrot, rotdir, nu_A, vc_A,
//                                              uc_A, delta_beta, delta_theta, dv,
//                                              ( (float) r_os ) * uc_A, du_out,
//                                              r_os * nu_A, nv, np,
//                                              mag );
//
//            check(16);
//
//        // Copy Y_A results to output Y_b
//
//            cudaDeviceSynchronize(); // make sure our managed memory is up-to-date on the host...
//
//            check(17);
//
//            cudaMemcpy( Y_b, Y_Ab_d, r_os * nu_A * nv * np * sizeof(float), cudaMemcpyDefault );
//
//            check(18);
//
//        // Free memory
//
//            cudaFree(Y_Ab_d);
//            cudaFree(norm_vol);
//            cudaFree(proj_time_d);
//            cudaFree(Y_A_in_d);

//        // Declarations and allocations
//
//            // Grid and block setup (divisibility not required)
//            grid  = dim3( (r_os * nu_A + 8 - 1) / 8, (nv + 8 - 1) / 8, 1 );
//            block = dim3( 8, 8, 1 );
//
//            check(14);
//
//        // Allocate output, Y_Ab_d
//
//            cudaMallocManaged( (void **) &Y_Ab_d, r_os * nu_A * nv * np * sizeof(float), cudaMemAttachGlobal);
//            cudaMemset( Y_Ab_d, 0, r_os * nu_A * nv * np * sizeof(float) );
//
//            cudaMallocManaged( (void **) &Y_Ab_d, r_os * nu_A * nv * np * sizeof(float), cudaMemAttachGlobal);
//            cudaMemset( proj_time_out_d, 0, np * sizeof(float) );
//
//            cudaMallocManaged( (void **) &proj_time_d, np * sizeof(float), cudaMemAttachGlobal);
//            cudaMemcpy( proj_time_d, proj_time, np * sizeof(float), cudaMemcpyDefault  );
//
//
//            cudaMemcpy( proj_time_out_d, proj_time, np * sizeof(float), cudaMemcpyDefault  );
//
//            check(15);
//
//        // Perform rebinning
//
//            temporal_cyrebin_v4<<<grid,block>>>( projs_A, Y_Ab_d,
//                                                 proj_time_d, target_time,                      // relative time?
//                                                 scan_angle,                                    // weight rebinned data by the angular coverage required to reconstruct each slice
//                                                 rotation_time,                                 // time to rotate the gantry by 360 degrees
//                                                 r_os,                                          // output row oversampling factor
//                                                 Rf, zrot, rotdir, nu_A, vc_A,                  // input variables
//                                                 uc_A, delta_beta, delta_theta, dv,             // input variables
//                                                 ( (float) r_os ) * uc_A, du_out, delta_theta,  // assume delta_theta_out = delta_theta  // output variables
//                                                 r_os * nu_A, nv, np,                           // output variables
//                                                 mag );                                         // geometric magnification at the central ray
//
//
//            check(16);
//
//        // Copy Y_A results to output Y_b
//
//            cudaDeviceSynchronize(); // make sure our managed memory is up-to-date on the host...
//
//            cudaMemcpy( Y_b, Y_Ab_d, r_os * nu_A * nv * np * sizeof(float), cudaMemcpyDefault );
//
//            check(17);
//
//        // Free memory
//
//            cudaDestroyTextureObject( projs_A );
//            cudaFreeArray( d_cuArr_A );
//
//            cudaFree(Y_Ab_d);
//            cudaFree(proj_time_d);


    }
        
    check(18);
    
}

