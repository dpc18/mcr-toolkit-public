% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%%

function [ recon_templates ] = Single_Source_Helical_Cylindrical_Recon_v2( param_files, save_rebinned_projections, suppress_outliers, GPUs )
% param_files: Cell array with full paths to parameter files describing the data to be reconstructed.
% save_rebinned_projections: (0) do not save rebinned projections, (1) save rebinned projections
% suppress_outliers: suppress unusually high line integral values
% GPUs: (< 0) Use the parameter file values, (0+) use these values instead

% Notes:
% - pitch >= 0.8: short-scan reconstruction (180 + fan angle)
% - There is currently some sort of bug or WFBP limitation which can be seen in short-scan reconstructions along the z axis (discontinuities).
% - pitch <  0.8: full-scan reconstruction (360 degrees)
% - Not currently set up to handle a flying focal spot.
% - Should the slice width always be equal to dv / magnification? The z translation per projection should be determined by this to obtain the target pitch.
% - We need to match the source and detector positions with DukeSim explicitly to minimize reconstruction errors and to reconstruct data in the space of the original phantom.

% v2: changed 'cutoff_frequency' field (cutoff frequency relative to Nyquist, fraction)
%     to 'f50' (50% modulation frequency in FBP filter; cycles / mm)

    % Save a recon_template for each parameter_file.
    % The recon_templates can be used to perform iterative reconstruction on the corresponding data.
    recon_templates = cell(size(param_files));

    % Loop through the parameter files.
    % Perform analtyical reconstructions as specified in each parameter file.
    % Iterative reconstructions are performed after looping through all of the parameter files.
    for recon = 1:length(param_files)    
    
    %% Import parameters
    
        struct = parse_DukeSim_params( param_files{recon} );
    
    % Paths
    
        projection_file = strtrim(struct.projection_file);
        outpath         = strtrim(struct.outpath);

    % Geometry
    
        rotation_time = str2num(struct.rot_time) / 1000;             % ms => sec
        pitch         = str2num(struct.pitch);
        lsd           = str2num(struct.source_to_detector_distance); % mm
        lso           = str2num(struct.source_to_object_distance);   % mm
        np_rot        = str2num(struct.proj_per_rotation);
        AMu           = str2num(struct.offset);                      % detector element offset along rows
        AMv           = 0;                                           % detector element offset along columns, is this ever non-zero?
        anode_angle   = str2num(struct.anode_angle);                 % degrees
        dv            = str2num(struct.row_pixel_size);              % detector pixel height, mm
        du            = str2num(struct.column_pixel_size);           % detector pixel width, mm
        table_feed    = str2num(struct.table_direction);             % (1) head first?, (-1) head last?
        rotdir        = str2num(struct.rotation_direction);          % +/- 1
        
        % Should be equal to dv / magnification?; this is an over-ride to get the correct z translation / slice
        % slicewidth0   = str2num(struct.slice_width);                 % override of 0.6 here seems to give the correct z translation
        slicewidth0 = dv / (lsd / lso);
        
        theta0        = str2num(struct.initial_angle); % degrees

    % ZFFS
    
        even_offset = str2num(struct.even_offset);
        odd_offset  = str2num(struct.odd_offset);

    % Projections
    
        nv             = str2num(struct.n_rows);
        nu             = str2num(struct.n_columns);
        kVp            = str2num(struct.n_energy_bins);    % Modeled source kVp
        rescale_factor = str2num(struct.rescaling_factor); % Rescale projection data by this factor
        mu_eff         = str2num(struct.mu_eff);           % convert from 1/cm to HU

    % Reconstruction
        
        recon_type  = strtrim(struct.recon_type);            % When more options are availabe, to be used to decide what type of reconstruction to perform
        % r_os        = str2num(struct.oversampling_factor); % Row oversampling factor to use when rebinning projection data
        filter_type = split(strtrim(struct.filter),',');     % Filter preset to use for analytical reconstruction
        gpu_list    = int32(str2num(struct.GPU_index));      % GPUs to use for reconstruction (starts from 0)
        % dx          = str2num(struct.recon_voxel_size);    % Voxel size, x
        % dy          = dx;                                  % Voxel size, y
        % dz          = dx;                                  % Voxel size, z => reconstruct thicker slices directly?
        % nx          = str2num(struct.vol_x);               % number of voxels to reconstruction along the x axis
        % ny          = str2num(struct.vol_y);               % number of voxels to reconstruction along the y axis
        % nz          = str2num(struct.vol_z);               % number of voxels to reconstruction along the z axis
        
        % Revision 1
        nx          = str2num(struct.matsize);             % number of voxels to reconstruction along the x axis 
        ny          = nx;                                  % number of voxels to reconstruction along the y axis
        % scan_range  = str2num(struct.scan_range);          % scan range to reconstruct (mm) starting from the z offset (determine automatically if scan_range < 0)
        z_offset    = str2num(struct.z_offset);            % offset the center of the reconstruction relative to the center of the scan

        % Revision 2
        recon_FoV = str2num(struct.recon_FoV);             % FoV to reconstruct (mm)
        % dz        = str2num(struct.recon_slice_width);     % reconstructed slice thickness (mm)
        dx        = recon_FoV / nx;
        
        % Revision 3
        % ker_smooth = strcmp('yes',struct.ker_smooth);      % "Smooth" reconstruction,  50% cutoff
        % ker_medium = strcmp('yes',struct.ker_medium);      % "Medium" reconstruction,  75% cutoff
        % ker_sharp  = strcmp('yes',struct.ker_sharp);       % "Sharp" reconstruction , 100% cutoff
        
        % Revision 4
        % Fix this value for now, so the frequency filter definition is handled appropriately.
        r_os = 1;
        
        % Revision 5 
        % Reconstruction filters and frequency cutoffs specified
        % cutoff_frequency = str2num(struct.cutoff_frequency);
        run_fbp          = strcmp('yes',strtrim(struct.run_fbp));
        run_iterative    = strcmp('yes',strtrim(struct.run_iterative));
        z_range          = str2num(struct.z_range); % Replaces "scan_range", (mm) starting from the z offset (determine automatically if < 0)
        dz               = str2num(struct.slice_thickness); % Replaces recon_slice_width
        
        % v2: changed 'cutoff_frequency' field (cutoff frequency relative to Nyquist, fraction)
        %     to 'f50' (50% modulation frequency in FBP filter; cycles / mm)
        f50 = str2num(struct.f50); % 
        
        % It may be feasible to reconstruct a smaller in-plane voxel size when using a flying focal spot... 
        if dx < slicewidth0
        
            warning(['Reconstruction FoV ' num2str(recon_FoV) ' (mm) and matrix size ' num2str(nx) ' imply a voxel size ' num2str(dx) ' which is smaller than the nominal slicewidth ' num2str(dv/(lsd/lso)) '.']);
        
        end
        dy = dx;
        
    %% Check to make sure the outpath exists.
    %  If it doesn't, create it.
    
        if ~exist(outpath, 'dir')
            
            mkdir(outpath);
            
        end
    
    %% What reconstruction(s) are we performing?
        
        if run_fbp && run_iterative
            
            fprintf(['\nPreparing ' param_files{recon} ' for analytical and iterative reconstruction...\n\n']);
            
        elseif run_fbp
        
            fprintf(['\nPreparing ' param_files{recon} ' for analytical reconstruction...\n\n']);
                
        else
            
            fprintf(['\nPreparing ' param_files{recon} ' for iterative reconstruction...\n\n']);
            
        end

    %% GPU override
    
        if GPUs(1) >= 0
           
            gpu_list = int32(GPUs);
            
        end
        
    %% Intensity rescaling: conversion factor from 1/(voxel size) to HU
    
        % Convert reconstructed intensity from 1/dx to 1/cm to HU
        % Note: For multi-channel data, should be compatbile with [x*y*z,p,e] format.
        HU = @(x) 1000 * ( x*(10/dx) / mu_eff - 1.0 ); 
        
    %% Reconstruction kernels
    
        % cutoffs = [0.50,0.75,1.00];
        % kernel_names = {'Smooth','Medium','Sharp'};
        % 
        % cutoffs      = cutoffs([ker_smooth,ker_medium,ker_sharp]);
        % kernel_names = kernel_names([ker_smooth,ker_medium,ker_sharp]);
        % 
        % if length(cutoffs) == 0
        % 
        %     error('No reconstruction kernel selected.');
        % 
        % end
        
        % Revision 5
        % Reconstruct every combination of kernel and cutoff frequency?
        if run_fbp
        
            nft     = length(filter_type);
            ncf     = length(f50);
            filters = cell(1,nft*ncf);

            for f_type = 1:nft

                for c50 = 1:ncf

                    filters{ncf*(f_type-1) + c50} = {filter_type{f_type},f50(c50)};

                end

            end
        
        end
        
    %% Load projection data
    
        % What projection file type are we importing?
        [~,~,ext] = fileparts(projection_file);
        
        if ~strcmp(ext,'.xcat') && ~strcmp(ext,'.nii') && ~strcmp(ext,'.nii.gz')
           
            error('Unrecognized projection file format (*.xcat binary file or *.nii (*.nii.gz) nifti file expected).');
            
        end
    
        if run_fbp
            
            get_projs = 1;
            
            if strcmp(ext,'.xcat')
    
                [Y, np] = read_binary_projections(projection_file,nu,nv,get_projs);
                
            else
               
                Y = load_nii(projection_file);
                Y = Y.img;
                
                np = size(Y,3);
                
                if ( nu ~= size(Y,1) || nv ~= size(Y,2) )
                    
                    error(['Inconsistent projection sizes. (projection file: ' num2str(size(Y,1)) ', ' num2str(size(Y,2)) '; parameter file: '...
                           num2str(nu) ', ' num2str(nv) ')']);
                    
                end
                
            end
            
            Y = Y * rescale_factor;

            % Check for nan values
            if sum(isnan(Y(:))) > 0

                error("Nan values found in projection data");

            end

            if suppress_outliers == 1

                temp = median(Y(:));

                Y(Y > (temp * 10)) = temp * 10;

            end
            
        else
            
            get_projs = 0;
            
            if strcmp(ext,'.xcat')
    
                [Y, np] = read_binary_projections(projection_file,nu,nv,get_projs);
                
            else
               
                hdr = load_nii_hdr(projection_file);
                
                np = hdr.dime.dim(4);
                
                if ( nu ~= hdr.dime.dim(2) || nv ~= hdr.dime.dim(3) )
                    
                    error(['Inconsistent projection sizes. (projection file: ' num2str(hdr.dime.dim(2)) ', ' num2str(hdr.dime.dim(3))...
                           '; parameter file: ' num2str(nu) ', ' num2str(nv) ')']);
                    
                end
                
                clear hdr;
                
            end
            
        end
        
    %% The +z axis in DukeSim and the +z axis in the Toolkit are in opposite directions
    
        % Requires the z axis to be centered.
        % Y = Y(:,end:-1:1,:);
        
        % For compatibility with pre-existing functions, it is easiest to flip the detector axis in the geometry file.
        % Note: As currently implemented, this will only work for an ideal cylindrical detector with no v/z offset at the detector.
        flip_z = -1;
        
    %% Additional settings

        % needed to have the correct z windowing during reconstruction
        mag = lsd/lso;
        % lod = lsd - lso;
        % slicewidth1 = dv / mag;
        
        % rotations = np / np_rot;
        
        % fan angle increment
        db = atan2( du, lsd ); % radians
        
        ap     = 360 / np_rot; % angular increment between projections
        angles = theta0:ap:(theta0 + np*ap)-ap; % projection angles

        fan_angle = db * nu * 180 / pi;

        uc = (nu - 1)/2; %  - AMu;
        vc = (nv - 1)/2; %  - AMv;
        
        % Detector rotations - currently, rotating a cylindrical detector will likely lead to calculation errors
        eta = 0;
        sig = 0;
        phi = 0;

        % z travel per projection
        detector_collimation = slicewidth0 * nv;
        mm_per_projection    = table_feed*pitch*(1/np_rot)*detector_collimation;
        
    %% Short-scan reconstruction
    
        if pitch >= 0.8 % "high" pitch, use short-scan reconstruction => better resolution, more noise
        
            % Minimum necessary projection data after rebinning
            limit_angular_range = detector_collimation * pitch * 180/360 / 2;
            
        else % "low" pitch, use full-scan reconstruction => lower resolution, lower noise
           
            limit_angular_range = detector_collimation * pitch / 2;
            
        end
        
        % Expand the minimum data range when performing algebraic reconstruction
        % 0 means do not limit the data range
        limit_angular_range2 = 0; % detector_collimation * pitch * (180 + fan_angle)/360 / 2;
        
    %% Calculate the z position for each projection and decide how many slices to reconstruct
    
        z_pos = 0:mm_per_projection:mm_per_projection*(np-1); % z position of the gantry per projection
        z_pos = z_pos - z_pos( np/2 );
        
        zo    = z_offset; % z position at the center of the reconstructed volume
        
        if z_range < 0 % determine the number of slices to reconstruct automatically
           
            nz = abs(round(mm_per_projection * np / dz));
            
        else % Reconstruct a specific z range
            
            nz = abs(round(z_range / dz));
            
        end
        
    %% Chain A - geometry file
    
        geofile = fullfile(outpath,'geofile_chainA.geo');
        
        ao = 0; % angle offset (degrees)
        
        params = [lsd lso uc vc eta sig phi zo ao];
    
        geolines = make_geo_file_cylindrical( geofile, params, du, flip_z * dv, rotdir, np, angles, AMu, AMv, z_pos, db );

    %% Declare other reconstruction flags, parameters

        % Flags
        implicit_mask = 1; % (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
        explicit_mask = 0; % (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask
        use_affine = 0;    % (0) - affine transform is not used (even if one is provided); (1) - affine transform is used

        % Explicit mask
        % Note: The mask_zrange function is currently in the toolkit Addons.
        if (explicit_mask == 0) % no explicit mask

            rmask = uint16(0);

        elseif (explicit_mask == 1)  % used z range explicit mask (good for boxes)

            % Test explicit mask
            % Reproduces implicit mask
            % rmask = zeros(nx,ny,'single');
            % [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            % rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            % rmask = repmat(rmask,[1 1 nz]);
            % rmask = rmask(:);
            % clear x1 x2 x3;

            mask = mask_zrange( mask );

            mask = uint16(mask(:));

        else % use for arbitrary shapes... slower

            % % Test explicit mask
            % % Reproduces implicit mask
            % rmask = zeros(nx,ny,'single');
            % [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            % rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            % rmask = repmat(rmask,[1 1 nz]);
            % % rmask = rmask(:);
            % clear x1 x2 x3;

            rmask = load_nii(fullfile(outpath,'rmask2.nii'));
            rmask = rmask.img;

            zmask = mask_zrange( rmask );

            rmask = uint16(cat(3,zmask,rmask));

            % rmask = uint16(rmask(:));

        end

        % Dummy parameters for no affine, minimal computation
        Rt_aff = single([1 0 0 0 1 0 0 0 1 0 0 0]);
        
        % FBP filters (only used after rebinning)
        % filt_names = cell(1,length(cutoffs));
        % for f = 1:length(cutoffs)
        % 
        %     cutoff        = cutoffs(f);
        %     lenu          = 2^(ceil(log(nu*r_os-1.0)/log(2.0))); % next power of 2
        %     filt_names{f} = fullfile(outpath,[filter_type '_' num2str(lenu) '_' kernel_names{f} '.txt']);
        %     make_FBP_filter( filt_names{f}, du/r_os/mag, lenu, filter_type, cutoff );
        % 
        % end
        
        % Revision 5
        % FBP filters (only used after rebinning)
        if run_fbp
            
            filt_files = cell(1,ncf*nft);
            
            for f = 1:ncf*nft
           
                filter_type = filters{f}{1};
                c50      = filters{f}{2};
                
                lenu = 2^(ceil(log(nu*r_os-1.0)/log(2.0))); % next power of 2
                filt_files{f} = fullfile(outpath,[filter_type '_' num2str(lenu) '_' num2str(c50) '.txt']);
                
                % make_FBP_filter( filt_files{f}, du/r_os/mag, lenu, filter_type, c50 );
                make_FBP_filter_v2( filt_files{f}, du/r_os/mag, lenu, filter_type, c50 );
            
            end
            
        end
        
        % Make sure we also have a ramp filter with cutoff 1.0 for algebraic, iterative reconstruction initialization
        ramp_filter = fullfile(outpath,['Ram-Lak_' num2str(lenu) '_1.0.txt']);
        make_FBP_filter( ramp_filter, du/r_os/mag, lenu, 'ram-lak', 1.0 );
        
        % Double parameters for reconstruction operations.
        scale = 1.0;
        double_params       = double([du          dv dx dy dz zo ao lsd lso uc      vc eta sig phi scale db limit_angular_range2]);
        double_params_rebin = double([du/r_os/mag dv dx dy dz zo ao lsd lso uc*r_os vc eta sig phi scale db limit_angular_range ]);
        
        % Integer parameters for cylindrical projections, algebraic reconstruction
        FBP              = 0; % (0) We cannot perform analytical reconstruction directly with clindrical projections.
        use_norm_vol     = 0; % (0) Use implicit weighting when performing algebraic reconstruction.
        det_type         = 1; % (0) - detector is flat; (1) detector is cylindrical; (2) detector is cylindrical, backproject rebinned projections
        int_params       = int32([nu nv np nx ny nz FBP use_affine implicit_mask explicit_mask det_type use_norm_vol]);
        
        % Integer parameters for rebinned projections, analytical reconstruction
        FBP              = 1; % (1) Load and use a Fourier filter for analytical reconstruction. Can be overridden when declaring the reconstruction operator.  
        use_norm_vol     = 1; % (1) Normalize each voxel by the weighted sum of contributing line integrals.
        det_type         = 2; % (0) - detector is flat; (1) detector is cylindrical; (2) detector is cylindrical, backproject rebinned projections
        int_params_f     = int32([nu*r_os nv np nx ny nz FBP use_affine implicit_mask explicit_mask det_type use_norm_vol]);
        

    %% Parameters for projection rebinning
    %  This script only works with chain A data, so chain B parameters are ignored.
    
    % Parameters - Chain B is not used here.

        Y_B      = single(0);
        GPU_idx  = int32(gpu_list(1)); % currently only runs on one GPU
        det_type = int32(1); % (0) flat-panel, (1) cylindrical, (2) cylindrical + temporal
        
        % should this be signed?
        % zrot = abs( z_pos(end)-z_pos(1) ) / ( ( rotations * np_rot - 1 ) * ap ); % z travel in mm / degree
        % zrot = abs( z_pos(end)-z_pos(1) ) / ( ( np - 1 ) * ap ); % z travel in mm / degree
        zrot = ( z_pos(end)-z_pos(1) ) / ( ( np - 1 ) * ap ); % z travel in mm / degree

        nu_A = nu;
        nu_B = 0;

        int_params_b = int32([ nu_A, nu_B, nv, np, r_os, rotdir ]);

        delta_theta = ap;                % assumes input angular increment = output angular increment
        delta_beta  = atan2d( du, lsd ); % degrees
        uc_A        = uc;
        % vc_A        = vc;              % Not currently supported.
        uc_B        = 0;
        du_out      = du / r_os / mag;   % oversample the output to better preserve spatial resolution    
        theta_B     = 0;                 % not used
        scale_A_B   = 1;                 % not used

        double_params_b = double([ lsd, zrot, delta_theta, delta_beta, flip_z * dv, uc_A, uc_B, du_out, theta_B, scale_A_B, mag, AMu ]);

    %% Perform projection rebinning
    
        % Skip this step if we are going to do iterative reconstruction
        if run_fbp
            
            disp('Rebinning projections...');

            clear cyrebin;
            clear DD_init;

            tic;

            Yb = cyrebin( Y(:), Y_B, int_params_b, double_params_b, GPU_idx, det_type ); % , proj_time );

            toc;

            Yb = reshape(Yb,[nu * r_os, nv, np]);

            % imtool(mat2gray(reshape(Yb(:,:,round(linspace(1,np,5))),[nu*r_os  nv*5])'));
            % drawnow;

            if save_rebinned_projections == 1

                save_nii(make_nii(reshape(Yb,[nu*r_os,nv,np]),[du_out,dv,1]),fullfile(outpath,'Yb.nii'));

            end
        
        end
        
    %% Update the geometry file to support rebinned data
    
        a2r = pi/180;

        rotx3 = @(x) ...
            [1 0 0;
            0 cos(x) -sin(x);
            0 sin(x) cos(x)];

        roty3 = @(x) ...
            [cos(x) 0 sin(x);
            0 1 0;
            -sin(x) 0 cos(x)];

        rotz3 = @(x) ...
            [cos(x) -sin(x) 0;
            sin(x) cos(x) 0;
            0 0 1];
    
        geolines_rebin = geolines;
        
        rot = rotx3(-eta)*roty3(-sig)*rotz3(-phi);
        puv = rot*[0; du_out; 0];
        
        % updated uc parameter
        geolines_rebin(:,15) = uc*r_os;
    
        for i = 1:np
            
            angle = angles(i) + ao;
            
            % u axis detector unit vector
            geolines_rebin(i,7:9) = rotz3(rotdir*angle*a2r)*puv;
            
            % in case there is ever a z component in the future
            geolines_rebin(i,9) = flip_z * geolines_rebin(i,9);
            
        end
        
        geofile_rebin = fullfile(outpath,'geofile_chainA_rebin.geo');
        delete(geofile_rebin);
        save(geofile_rebin,'geolines_rebin','-ASCII'); % save the geometry file
        
    %% Analytical reconstruction(s)
    
        % Skip this step if we are going to do iterative reconstruction
        if run_fbp
    
            for f = 1:ncf*nft
                
                filter_type = filters{f}{1};
                c50      = filters{f}{2};

                % disp(['WFBP reconstruction with ' filter_type ' kernel, cutoff frequency ' num2str(cutoff)]);
                disp(['WFBP reconstruction with ' filter_type ' kernel, 50% modulation frequency ' num2str(c50) ' (cycles/mm)']);

                % Initialize reconstruction operators
                % Be sure to "clear DD_init" before clearing recon. allocations or resetting the GPU(s)!
                % Note: Any outstanding recon allocations will be properly deallocated when Matlab is closed.

                    clear DD_init;

                    recon_alloc = DD_init(int_params_f(:), double_params_rebin(:), filt_files{f}, geofile_rebin, Rt_aff(:), rmask, gpu_list);

                    % Reconstruction operators
                    W   = ones(1,np);
                    % Rt  = @(x,w) DD_backproject(x,recon_alloc,int32(0),w);
                    Rtf = @(x,w) DD_backproject(x,recon_alloc,int32(1),w);

                % Single source reconstruction

                    tic;
                    X_out = reshape(Rtf(Yb(:), W),[nx ny nz]);
                    toc;
                    
                    % Flip the z axis so the saved reconstruction has the expected orientation.
                    % X_out2 = X_out(:,:,end:-1:1);

                    % Save the reconstruction after conversion to HU
                    save_nii(make_nii(HU(X_out),[dx,dy,dz]),fullfile(outpath,['X_WFBP_' filter_type '_' num2str(c50) '.nii']));
                    
            end
            
            clear X_out2;
                
        end
        
    %% Algebraic updates relative to the original projections to reduce artifacts and/or to sanity check the geometry
    
        % Skip this step if we are going to do iterative reconstruction
        % if skip_recon == 0
        % 
        %     disp('Performing algebraic reconstruction...');
        % 
        % % Start with a sharp initialization
        % 
        %     clear DD_init;
        % 
        %     recon_alloc = DD_init(int_params_f(:), double_params_rebin(:), ramp_filter, geofile_rebin, Rt_aff(:), rmask, gpu_list);
        % 
        %     Rtf = @(x,w) DD_backproject(x,recon_alloc,int32(1),w);
        % 
        %     X_out = Rtf(Yb(:), W);
        % 
        % % Reconstruction operators
        % 
        %     clear Rtf DD_init;
        % 
        %     tic;
        % 
        %     % NOTE: filt_name is not valid for the original projections, but is provided to prevent an error.
        %     recon_alloc = DD_init(int_params(:), double_params(:), filt_files{1}, geofile, Rt_aff(:), rmask, gpu_list);
        % 
        %     toc;
        % 
        %     R  = @(x,w) DD_project(x,recon_alloc,w);
        %     Rt = @(x,w) DD_backproject(x,recon_alloc,int32(0),w);
        % 
        % % Subsets
        % 
        %     % num_iter = 2; % 1;
        %     % snum     = 3; % 5;
        %     % ell      = 2;
        % 
        %     % num_iter = 4; % 1;
        %     % snum     = 3; % 5;
        %     % ell      = 2;
        % 
        %     num_iter = 12;
        %     snum     =  1;
        %     ell      =  2;
        % 
        %     subsets = int32(bitrevorder(1:2^ceil(log2(snum))));
        %     subsets = subsets(subsets <= snum);
        % 
        %     W = zeros(np,snum);
        % 
        %     for i = 1:snum
        % 
        %         W(i:snum:end,i) = 1;
        % 
        %     end
        % 
        %     W = W(:,subsets);
        % 
        % % BiCGSTAB(2)
        % 
        %     const = Rt( Y(:), sum(W,2));
        % 
        %     fun = @(x,w) Rt(R(x,w),w);
        % 
        %     tic;
        %     [ X_out2, resvec ] = bicgstabl_os( fun, const, X_out(:)*0, num_iter, W, ell );
        %     toc;
        % 
        %     disp('Algebraic reconstruction residuals: ');
        %     resvec
        % 
        %     % Flip the z axis so the saved reconstruction has the expected orientation.
        %     X_out2 = reshape(X_out2,[nx ny nz]);
        %     X_out2 = X_out2(:,:,end:-1:1);
        % 
        %     % save after normalizing the itensity to HU
        %     save_nii(make_nii(HU(X_out2),[dx,dy,dz]),fullfile(outpath,'X_Alg2.nii'));
        % 
        % end
        
    %% Prepare a reconstruction template for iterative reconstruction
     
        % Prepare a reconstruction template if we are going to do iterative reconstruction.
        if run_iterative

            % Reference attenuation measurements for water at each energy threhsold
            % Since this is a single energy script, this isn't relevant.
            attn_water = ones([1,1]);

        % Input / output
        
            [~,run_name,~] = fileparts(projection_file);

            rt.run_name       = run_name;         % identifier string for specific results

            rt.outpath        = outpath;          % location where results are saved

            rt.computeRMSE    = 0;                % (0) do not comptue RMSE (i.e. for in vivo data), (1) compute RMSE each time the reconstruction is updated (simulations)
            rt.X_ref          = '';               % reference volume string; noise free reference data

            rt.do_save        = 1;                % (0) return final results only, (1) save final results only, (2) save intermediate results and final results

            rt.Y              = projection_file;  % projection data path (*.nii, *.nii.gz, *.xcat)

            rt.breg_iter      = 4;                % total number of times to update the reconstructed results (initialization + regularized iterations)

            rt.bicgstabl_iter = 6;                % data fidelity update iterations (bicgstabl with l = 2)

            rt.preview_slice  = round(nz/2);      % z slice used for previewing reconstructed results
            rt.display_range  = [0 0.8];          % minimum and maximum fractions of the dynamic range of CT attenuation values for consistent display purposes

        % Projection / Backprojection

            rt.int_params    = int32(int_params);     % int32 parameters - algebraic reconstruction
            rt.int_params_f  = int32(int_params_f);   % int32 parameters - analytical reconstruction
            rt.double_params = double(double_params); % double precision parameters used for geometric calculations
            rt.filt_name     = ramp_filter;           % FBP filter
            rt.geo_all       = geofile;               % geometry file
            rt.Rt_aff        = single(Rt_aff);        % affine transform
            rt.rmask         = uint16(rmask);         % volumetric masking
            rt.gpu_list      = gpu_list;              % GPU(s) to use for reconstruction ( Matlab getDevice(#) indices - 1), int32

            rt.Wt            = ones(1,np);            % temporal weights
            rt.geo_lines     = geolines;              % We need the geometry file for all projections to allow us to sub-select rows for temporal reconstruction
            % rt.snum          = 1;                   % number of subsets to use for reconstruction
            
        % Additional rebinning parameters for cylindrical detectors
        
            % Note: int_params_f should be set up for analytical reconstruction with rebinned data.
            rt.double_params_rebin = double(double_params_rebin); % double parameters for WFBP with rebinned projections
            
            rt.geofile_rebin       = geofile_rebin;           % geometry file for WFBP with rebinned projections
            rt.geolines_rebin      = geolines_rebin;          % Override 'geolines' above.
            
            rt.int_params_b        = int32(int_params_b);     % int32 parameters for cone-parallel rebinning
            rt.double_params_b     = double(double_params_b); % double parameters for cone-parallel rebinning
            rt.det_type_rebin      = int32(det_type);         % detector type setting for cone-parallel rebinning
            
        % Import DukeSim projections in *.bin format, instead of the standard nifti format for projections
        
            rt.projection_rescale_factor = double(rescale_factor); % Rescaling value for DukeSim projections
            rt.suppress_outliers         = suppress_outliers;      % (0) Don't suppress outliers, (1) Suppress outliers relative to the median line integral value

        % Sizes

            rt.p   = 1;          % number of temporal phases
            rt.e   = 1;          % number of spectral samples
            rt.sz  = [nx,ny,nz]; % full volume size
            rt.szy = [nu,nv,np]; % projection data size
            
        % Unit conversion
        
            % Function for converting reconstructed intensity values in 1/(voxel size) to HU
            % Note: This should be compatible with the data in [x*y*z,p,e] format for multi-channel data.
            rt.HU = HU; 

        % Regularization

            % regularization strength
            rt.mu = 0.005;

            % regularization, RSKR (energy)
            rt.stride      = 3;            % subsampling to deal with correlated noise
            rt.w           = int32(6);     % filtration kernel radius
            rt.w2          = single(0.8);  % kernel resampling bandwidth
            rt.lambda0     = single(1.2);  % regularization strength
            rt.attn_water  = attn_water;   % attempt to equalize the noise level between spectral samples, following water normalization
            rt.g           = 0.5;          % exponential regularization scaling with singular value ratio ( lambda0*(E(1,1)./diag(E)').^(g) )

            % regularization, pSVT (time)
            rt.apply_pSVT      = 1;   % (0) do not apply pSVT; (1) Apply pSVT when there is a time dimension
            rt.w_pSVT          = 3;   % kernel diameter, pSVT
            rt.nu_pSVT         = 0.7; % non-convex singular value exponent
            rt.multiplier_pSVT = 0.3; % multiplier for singular value threshold

            % directory of the version of BF to use for RSKR
            % rt.BF_path = fileparts(which('jointBF4D'));
            
        % Depending on our coordinate system, we may need to flip the z axis
        % How to better handle this?
        
            rt.flip_z = 1; % 

        % Save the template

            rt_file = fullfile(outpath,[run_name '_recon_template.mat']);

            save(rt_file,'rt');
            
        % Since the iterative reconstruction function expects the template and the template name includes
        % information read from the parameter file, pass the template itself rather than the template file
        % name.

            recon_templates{recon} = rt;
        
        end

    end
    
    %% Perform any queued iterative reconstructions
    % Depending on the volume size, this can be very time consuming.
    % In most cases the reconstruction templates can be saved and reused later.
    
    for recon = 1:length(param_files)

        % Some parameter files may not ask for iterative reconstruction.
        if isempty(recon_templates{recon})

            continue;

        end

        fprintf(['\n\nBeginning iterative reconstruction with template ' recon_templates{recon}.run_name '_recon_template.mat...\n\n']);

        Recon_5D_Data_v3( recon_templates{recon} );

    end


end

