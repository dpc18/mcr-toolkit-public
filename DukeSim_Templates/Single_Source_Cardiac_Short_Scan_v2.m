% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%%

function [ recon_templates ] = Single_Source_Cardiac_Short_Scan_v2( param_files, save_rebinned_projections, suppress_outliers, make_temporal_map, skip_recon )
% param_files: Cell array with full paths to parameter files describing the data to be reconstructed.
% save_rebinned_projections: (0) do not save rebinned projections, (1) save rebinned projections
% suppress_outliers: suppress unusually high line integral values
% make_temporal_map: WIP feature to visualize temporal resolution in the image domain
% skip_recon: (0) Perform reconstruction within this function and return the recon_templates
%             (1) Skip reconstruction within this function, return the recon_templates only

% Approach to short-scan reconstruction: cone-parallel rebinning => WFBP w/ 180 degrees of data => algebraic updates with expanded
%                                        limit_angular_range parameter.
%
% Approach to short-scan cardiac reconstruction: cone-parallel rebinning (ignoring time dimension during rebinning) =>
%                                                cos^2 weighting of z position beyond 180 degree short-scan range +
%                                                narrow temporal weighting functions to maximize temporal resolution =>
%                                                algebraic updates with order-subsets to resolve artifacts from irregular backprojections

% Notes:
% - Not currently set up to handle a flying focal spot.
% - Should we update this to work from an ecg or gating signal? Currently, the first projection is assumed to be an R peak.
% - Should the slice width always be equal to dv / magnification? The z translation per projection should be determined by this to obtain the target pitch.
% - We need to match the source and detector positions with DukeSim explicitly to minimize reconstruction errors and to reconstruct data in the space of the original phantom.

    % Save a recon_template for each parameter_file.
    % The recon_templates can be used to perform iterative reconstruction on the corresponding data.
    recon_templates = cell(size(param_files));

    for recon = 1:length(param_files)
        
        if skip_recon ~= 0
            
            fprintf(['\nPreparing ' param_files{recon} ' for iterative reconstruction...\n\n']);
            
        else
            
            fprintf(['\nReconstructing ' param_files{recon} '...\n\n']);
            
        end
    
    %% Import parameters
    
        struct = parse_DukeSim_params( param_files{recon} );
    
    % Paths
    
        projection_file = struct.projection_file;
        outpath         = struct.outpath;

    % Geometry
    
        rotation_time = str2num(struct.rot_time) / 1000;             % ms => sec
        pitch         = str2num(struct.pitch);
        lsd           = str2num(struct.source_to_detector_distance); % mm
        lso           = str2num(struct.source_to_object_distance);   % mm
        np_rot        = str2num(struct.proj_per_rotation);
        AMu           = str2num(struct.offset);                      % detector element offset along rows
        AMv           = 0;                                           % detector element offset along columns, is this ever non-zero?
        anode_angle   = str2num(struct.anode_angle);                 % degrees
        du            = str2num(struct.row_pixel_size);              % detector pixel size along rows   , mm
        dv            = str2num(struct.column_pixel_size);           % detector pixel size along columns, mm
        table_feed    = str2num(struct.table_direction);             % (1) head first?, (-1) head last?
        rotdir        = str2num(struct.rotation_direction);          % +/- 1
        
        % Should be equal to dv / magnification?; this is an over-ride to get the correct z translation / slice
        slicewidth0   = str2num(struct.slice_width);                 % override of 0.6 here seems to give the correct z translation
        
        theta0        = str2num(struct.initial_angle); % degrees

    % ZFFS
    
        even_offset = str2num(struct.even_offset);
        odd_offset  = str2num(struct.odd_offset);

    % Projections
    
        nv             = str2num(struct.n_rows);
        nu             = str2num(struct.n_columns);
        kVp            = str2num(struct.n_energy_bins);    % Modeled source kVp
        rescale_factor = str2num(struct.rescaling_factor); % Rescale projection data by this factor

    % Reconstruction
        
        recon_type  = struct.recon_type;                   % When more options are availabe, to be used to decide what type of reconstruction to perform
        r_os        = str2num(struct.oversampling_factor); % Row oversampling factor to use when rebinning projection data
        filter_type = struct.filter;                       % Filter preset to use for analytical reconstruction
        gpu_list    = int32(str2num(struct.GPU_index));    % GPUs to use for reconstruction (starts from 0)
        dx          = str2num(struct.recon_voxel_size);    % Voxel size, x
        dy          = dx;                                  % Voxel size, y
        dz          = dx;                                  % Voxel size, z => reconstruct thicker slices directly?
        nx          = str2num(struct.vol_x);               % number of voxels to reconstruction along the x axis
        ny          = str2num(struct.vol_y);               % number of voxels to reconstruction along the y axis
        nz          = str2num(struct.vol_z);               % number of voxels to reconstruction along the z axis

    % Cardiac data - Should we update this to work from an ecg or gating signal? Currently, the first projection is assume to be an R peak.
    
        HR        = str2num(struct.heart_rate);        % heart rate
        p         = str2num(struct.n_cardiac_phases);  % number of cardiac phases to reconstruct
        subphases = str2num(struct.cardiac_subphases); % number of subphases to use when quantizing cardiac phase for projection weighting
        
    %% Load projection data
    
        if skip_recon == 0
            
            get_projs = 1;
    
            [Y, np] = read_binary_projections(projection_file,nu,nv,get_projs);
            
            Y = Y * rescale_factor;

            % Check for nan values
            if sum(isnan(Y(:))) > 0

                error("Nan values found in projection data");

            end

            if suppress_outliers == 1

                temp = median(Y(:));

                Y(Y > (temp * 10)) = temp * 10;

            end
            
        else
            
            get_projs = 0;
    
            [Y, np] = read_binary_projections(projection_file,nu,nv,get_projs);
            
        end
        
    %% Additional settings

        % needed to have the correct z windowing during reconstruction
        mag = lsd/lso;
        % lod = lsd - lso;
        % slicewidth1 = dv / mag;
        
        rotations = np / np_rot;
        
        % fan angle increment
        db = atan2( du, lsd ); % radians
        
        ap     = 360 / np_rot; % angular increment between projections
        angles = theta0:ap:(theta0 + np*ap)-ap; % projection angles

        fan_angle = db * nu * 180 / pi;

        uc = (nu - 1)/2; % - AMu;
        vc = (nv - 1)/2; % - AMv;
        
        % Detector rotations - currently, rotating a cylindrical detector will likely lead to calculation errors
        eta = 0;
        sig = 0;
        phi = 0;

        % z travel per projection
        mm_per_projection = table_feed*pitch*(1/np_rot)*nv*slicewidth0;
        
    %% Short-scan reconstruction
    
        detector_collimation = (dv / mag) * nv;
        
        % Control the use of divergent rays with cos^2 weighting (no computation benefit)
        % This will negotiate a trade-off between z spatial resolution, noise, and temporal resolution.
        % Currently this is used with both rebinned and original projections...
        short_scan_range    = detector_collimation * pitch * 180/360;
        limit_angular_range = short_scan_range / detector_collimation;
        
        limit_multiplier = 1.0;
        
    %% Chain A - geometry file
    
        geofile = fullfile(outpath,'geofile_chainA.geo');
        
        z_pos = -(0:mm_per_projection:mm_per_projection*(np-1)); % z position of the gantry per projection
        zo    = (z_pos(end)-z_pos(1))/2.0;                    % z position at the center of the reconstructed volume 
        
        ao = 0; % angle offset (degrees)
        
        params = [lsd lso uc vc eta sig phi zo ao];
    
        geolines = make_geo_file_cylindrical( geofile, params, du, dv, rotdir, np, angles, AMu, AMv, z_pos, db );


    %% Declare other reconstruction flags, parameters

        % Flags
        implicit_mask = 1; % (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
        explicit_mask = 0; % (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask
        use_affine = 0;    % (0) - affine transform is not used (even if one is provided); (1) - affine transform is used

        % Explicit mask
        % Note: The mask_zrange function is currently in the toolkit Addons.
        if (explicit_mask == 0) % no explicit mask

            rmask = uint16(0);

        elseif (explicit_mask == 1)  % used z range explicit mask (good for boxes)

            % Test explicit mask
            % Reproduces implicit mask
            % rmask = zeros(nx,ny,'single');
            % [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            % rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            % rmask = repmat(rmask,[1 1 nz]);
            % rmask = rmask(:);
            % clear x1 x2 x3;

            mask = mask_zrange( mask );

            mask = uint16(mask(:));

        else % use for arbitrary shapes... slower

            % % Test explicit mask
            % % Reproduces implicit mask
            % rmask = zeros(nx,ny,'single');
            % [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            % rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            % rmask = repmat(rmask,[1 1 nz]);
            % % rmask = rmask(:);
            % clear x1 x2 x3;

            rmask = load_nii(fullfile(outpath,'rmask2.nii'));
            rmask = rmask.img;

            zmask = mask_zrange( rmask );

            rmask = uint16(cat(3,zmask,rmask));

            % rmask = uint16(rmask(:));

        end

        % Dummy parameters for no affine, minimal computation
        Rt_aff = single([1 0 0 0 1 0 0 0 1 0 0 0]);
        
        % FBP filters (only needed after rebinning)
        cutoff    = 1.0;
        lenu      = 2^(ceil(log(nu*r_os-1.0)/log(2.0))); % next power of 2
        filt_name = fullfile(outpath,[filter_type '_' num2str(lenu) '.txt']);
        make_FBP_filter( filt_name, du/r_os/mag, lenu, filter_type, cutoff );
        
        % Double parameters for reconstruction operations.
        scale = 1.0;
        double_params       = double([du          dv dx dy dz zo ao lsd lso uc      vc eta sig phi scale db limit_multiplier * limit_angular_range]);
        double_params_rebin = double([du/r_os/mag dv dx dy dz zo ao lsd lso uc*r_os vc eta sig phi scale db                    limit_angular_range]);
        
        % Integer parameters for cylindrical projections, algebraic reconstruction
        FBP              = 0; % (0) We cannot perform analytical reconstruction directly with clindrical projections.
        use_norm_vol     = 0; % (0) Use implicit weighting when performing algebraic reconstruction.
        det_type         = 1; % (0) - detector is flat; (1) detector is cylindrical; (2) detector is cylindrical, backproject rebinned projections
        int_params       = int32([nu nv np nx ny nz FBP use_affine implicit_mask explicit_mask det_type use_norm_vol]);
        
        % Integer parameters for rebinned projections, analytical reconstruction
        FBP              = 1; % (1) Load and use a Fourier filter for analytical reconstruction. Can be overridden when declaring the reconstruction operator.  
        use_norm_vol     = 1; % (1) Normalize each voxel by the weighted sum of contributing line integrals.
        det_type         = 2; % (0) - detector is flat; (1) detector is cylindrical; (2) detector is cylindrical, backproject rebinned projections
        int_params_f     = int32([nu*r_os nv np nx ny nz FBP use_affine implicit_mask explicit_mask det_type use_norm_vol]);
        

    %% Parameters for projection rebinning
    %  This script only works with chain A data, so chain B parameters are ignored.
    
    % Parameters - Chain B is not used here.

        Y_B      = single(0);
        GPU_idx  = int32(gpu_list(1)); % currently only runs on one GPU
        det_type = int32(1); % (0) flat-panel, (1) cylindrical, (2) cylindrical + temporal
        
        % should this be signed?
        % zrot = abs( z_pos(end)-z_pos(1) ) / ( ( rotations * np_rot - 1 ) * ap ); % z travel in mm / degree
        zrot = abs( z_pos(end)-z_pos(1) ) / ( ( np - 1 ) * ap ); % z travel in mm / degree

        nu_A = nu;
        nu_B = 0;

        int_params_b = int32([ nu_A, nu_B, nv, np, r_os, rotdir ]);

        delta_theta = ap;                % assumes input angular increment = output angular increment
        delta_beta  = atan2d( du, lsd ); % degrees
        uc_A        = uc;
        % vc_A        = vc;              % Not currently supported.
        uc_B        = 0;
        du_out      = du / r_os / mag;   % oversample the output to better preserve spatial resolution    
        theta_B     = 0;                 % not used
        scale_A_B   = 1;                 % not used

        % The AMu parameter is set to zero, since it doesn't currently work.
        double_params_b = double([ lsd, zrot, delta_theta, delta_beta, dv, uc_A, uc_B, du_out, theta_B, scale_A_B, mag, AMu ]);

    %% Perform projection rebinning
    
        % Skip this step if we are going to do iterative reconstruction
        if skip_recon == 0

            clear cyrebin;
            clear DD_init;

            tic;

            Yb = cyrebin( Y(:), Y_B, int_params_b, double_params_b, GPU_idx, det_type ); % , proj_time );

            toc;

            Yb = reshape(Yb,[nu * r_os, nv, np]);

            % imtool(mat2gray(reshape(Yb(:,:,round(linspace(1,np,5))),[nu*r_os  nv*5])'));
            % drawnow;

            if save_rebinned_projections == 1

                save_nii(make_nii(reshape(Yb,[nu*r_os,nv,np]),[du_out,dv,1]),fullfile(outpath,'Yb.nii'));

            end
        
        end
        
    %% Simplfied cardiac gating used in DukeSim simulation.
    %  Assumes the first projection starts at an R peak.
    %  Assumes that the heart rate remains constant during the acquisition.
    
        % Temporal weights
        bps   = HR/60;
        time  = linspace(0,rotation_time*rotations,length(angles));
        phase = mod(time,1/bps) * bps;
        
    %% Update the geometry file to support rebinned data
    
        a2r = pi/180;

        rotx3 = @(x) ...
            [1 0 0;
            0 cos(x) -sin(x);
            0 sin(x) cos(x)];

        roty3 = @(x) ...
            [cos(x) 0 sin(x);
            0 1 0;
            -sin(x) 0 cos(x)];

        rotz3 = @(x) ...
            [cos(x) -sin(x) 0;
            sin(x) cos(x) 0;
            0 0 1];
    
        geolines_rebin = geolines;
        
        rot = rotx3(-eta)*roty3(-sig)*rotz3(-phi);
        puv = rot*[0; du_out; 0];
        
        % updated uc parameter
        geolines_rebin(:,15) = uc*r_os;
    
        for i = 1:np
            
            angle = angles(i) + ao;
            
            % u axis detector unit vector
            geolines_rebin(i,7:9) = rotz3(rotdir*angle*a2r)*puv;
            
        end
        
        geofile_rebin = fullfile(outpath,'geofile_chainA_rebin.geo');
        delete(geofile_rebin);
        save(geofile_rebin,'geolines_rebin','-ASCII'); % save the geometry file

    %% Initialize reconstruction operators
    % Be sure to "clear DD_init" before clearing recon. allocations or resetting the GPU(s)!
    % Note: Any outstanding recon allocations will be properly deallocated when Matlab is closed.
    
        % Skip this step if we are going to do iterative reconstruction
        if skip_recon == 0

            clear DD_init;

            tic;

            recon_alloc = DD_init(int_params_f(:), double_params_rebin(:), filt_name, geofile_rebin, Rt_aff(:), rmask, gpu_list);

            toc;

        % Reconstruction operators

            W   = ones(1,np);
            Rt  = @(x,w) DD_backproject(x,recon_alloc,int32(0),w);
            Rtf = @(x,w) DD_backproject(x,recon_alloc,int32(1),w);

        end
        
    %% Resolve temporal weights
    
        % More or less always have enough data for reconstruction at the expense of temporal resolution, computation time
        % [W_, perc_R_R] = Gaussian_weights_eps(p, subphases, 1e-6); % analytical recon
        
        % Improve temporal resolution, reduce reconstruction time at the expense of dose efficiency, reconstruction artifacts.
        % May result in gaps in the reconstructed images, depending on the acquisiton parameters.
        % std_devs = 3; % Gaussian basis functions, with projection within +/- 3 standard deviations
        % std_devs = 1; % Gaussian basis functions, with projection within +/- 1 standard deviations
        % [ W_, perc_R_R ] = Gaussian_weights_trunc_v2( p, subphases, std_devs );
        % 
        % disp(['Reconstructing the following R-R interval %: ' num2str(perc_R_R)]);
        % 
        % % W_ = circshift(W_,[-50,0]);
        % 
        % Wt = zeros(np,p);
        % 
        % for t = 1:p
        % 
        %     Wt(:,t) = W_(floor(subphases*phase)+1,t);
        % 
        % end
        % 
        % Wt = bsxfun(@rdivide,Wt,sum(Wt,1));
        
    %% Resolve temporal weights - v2
    
    % Check to make sure we have enough data for reconstruction without gaps
    
        if ( ( 60 * pitch ) / ( HR * rotation_time ) > 1.0 )
           
            error('The pitch is too high for cardiac reconstruction without gaps.');
            
        end
    
    % Compute short-scan weights for contiguous sectors
    
        pad_z = 1; % (0) variable z coverage, more consistent temporal resolution
                   % (1) uniform z coverage, temporal resolution falls off along the z axis
                   
        [ Wt, perc_R_R ] = short_scan_cardiac_weights( phase, p, np_rot, fan_angle, pad_z, limit_multiplier );
        
        disp(['Reconstructing the following R-R interval %: ' num2str(perc_R_R)]);

    %% Analytical reconstruction of rebinned projections

        % Skip this step if we are going to do iterative reconstruction
        if skip_recon == 0
            
            X_out = zeros([nx ny nz p],'single');

            % To debug reconstruction and data acquisition issues, replace
            % each projection with the relative time it was acquired
            % reconstructing a map of relative temporal resolution per
            % phase. Note, this does not account for the variability in
            % temporal resolution within a single projection introduced
            % during rebinning.
            if make_temporal_map == 1

                for t = 1:p

                    offset = 0.5 - perc_R_R(t)/100;
                    phase_ = mod(phase + offset,1.0);
                    phase_ = phase_ - 0.5;

                    Yb = Yb * 0 + reshape( phase_, [1,1,np] );

                    tic;
                    X_out(:,:,:,t) = reshape(Rt(Yb(:), Wt(:,t)),[nx ny nz]);
                    toc;

                end

                save_nii(make_nii(X_out,[dx,dy,dz]),fullfile(outpath,'temporal_map.nii'));

            else

                for t = 1:p

                    tic;
                    X_out(:,:,:,t) = reshape(Rtf(Yb(:), Wt(:,t)),[nx ny nz]);
                    toc;

                end

                save_nii(make_nii(X_out,[dx,dy,dz]),fullfile(outpath,'X_WFBP.nii'));

            end
        
        end

    %% Algebraic updates relative to the original projections to reduce artifacts when using truncated temporal weights (Gaussian_weights_trunc_v2)
    
        % Skip this step if we are going to do iterative reconstruction
        if skip_recon == 0
            
        % Reconstruction operators

            clear Rtf DD_init;

            tic;

            % NOTE: filt_name is not valid for the original projections, but is provided to prevent an error.
            recon_alloc = DD_init(int_params(:), double_params(:), filt_name, geofile, Rt_aff(:), rmask, gpu_list);

            toc;

            R  = @(x,w) DD_project(x,recon_alloc,w);
            Rt = @(x,w) DD_backproject(x,recon_alloc,int32(0),w);

        % Subsets

            num_iter = 2; % 1;
            snum     = 3; % 5;
            ell      = 2;

            subsets = int32(bitrevorder(1:2^ceil(log2(snum))));
            subsets = subsets(subsets <= snum);

            W = zeros(np,snum);

            for i = 1:snum

                W(i:snum:end,i) = 1;

            end

            W = W(:,subsets);
            
        % BiCGSTAB(2)

            X_out2 = zeros([nx ny nz p],'single');

            for t = 1:p

                Wt_ = Wt(:,t) .* W;

                const = Rt( Y(:), sum(Wt_,2));

                fun = @(x,w) Rt(R(x,w),w);

                X_WFBP = X_out(:,:,:,t);

                tic;
                [ X_out_, resvec ] = bicgstabl_os( fun, const, X_WFBP(:), num_iter, Wt_, ell );
                toc;

                disp(['Phase ' num2str(t) ' L2 Residuals: ']);
                resvec

                X_out2(:,:,:,t) = reshape(X_out_,[nx ny nz]);

            end

            % imc({X_WFBP(:,:,round(nz/2)),X_out2(:,:,round(nz/2))},[nx ny],1,1);

            save_nii(make_nii(X_out2,[dx,dy,dz]),fullfile(outpath,'X_Alg2.nii'));

        end

    %% Use the temporal weights to estimate temporal resolution per phase
    % Needs to be updated when z padding is used...

        % tres  = zeros([p,1]);
        % tres2 = zeros([p,1]);
        % 
        % for t = 1:p
        % 
        % % Average temporal resolution
        % 
        %     dist = abs(phase - perc_R_R(t)/100);
        % 
        %     dist = min(dist,1.0-dist);
        % 
        %     temp = sum(bsxfun(@times,Wt,reshape(dist,[np 1])),1);
        % 
        %     tres(t) = 2*temp(t);
        % 
        % % Worst case temporal resolution
        % 
        %     dist = abs(phase - perc_R_R(t)/100);
        % 
        %     dist = min(dist,1.0-dist);
        % 
        %     temp = max( dist(Wt(:,t) > 0) );
        % 
        %     tres2(t) = 2*temp;
        % 
        % end
        % 
        % disp(['Average temporal resolution (ms): ' num2str(1000*tres')]);
        % disp(['Worst   temporal resolution (ms): ' num2str(1000*tres2')]);
        
    %% Prepare a reconstruction template for iterative reconstruction
     
        % Prepare a reconstruction template if we are going to do iterative reconstruction.
        if skip_recon ~= 0

            % Reference attenuation measurements for water at each energy threhsold
            % Since this is a single energy script, this isn't relevant.
            attn_water = ones([1,1]);

        % Input / output
        
            [~,run_name,~] = fileparts(projection_file);

            rt.run_name       = run_name;         % identifier string for specific results

            rt.outpath        = outpath;          % location where results are saved

            rt.computeRMSE    = 0;                % (0) do not comptue RMSE (i.e. for in vivo data), (1) compute RMSE each time the reconstruction is updated (simulations)
            rt.X_ref          = '';               % reference volume string; noise free reference data

            rt.do_save        = 1;                % (0) return final results only, (1) save final results only, (2) save intermediate results and final results

            rt.Y              = projection_file;  % projection data path (*.nii, *.nii.gz, *.xcat)

            rt.breg_iter      = 4;                % total number of times to update the reconstructed results (initialization + regularized iterations)

            rt.bicgstabl_iter = 6;                % data fidelity update iterations (bicgstabl with l = 2)

            rt.preview_slice  = round(nz/2);      % z slice used for previewing reconstructed results
            rt.display_range  = [0 0.8];          % minimum and maximum fractions of the dynamic range of CT attenuation values for consistent display purposes

        % Projection / Backprojection

            rt.int_params    = int32(int_params);     % int32 parameters - algebraic reconstruction
            rt.int_params_f  = int32(int_params_f);   % int32 parameters - analytical reconstruction
            rt.double_params = double(double_params); % double precision parameters used for geometric calculations
            rt.filt_name     = filt_name;             % FBP filter
            rt.geo_all       = geofile;               % geometry file
            rt.Rt_aff        = single(Rt_aff);        % affine transform
            rt.rmask         = uint16(rmask);         % volumetric masking
            rt.gpu_list      = gpu_list;              % GPU(s) to use for reconstruction ( Matlab getDevice(#) indices - 1), int32

            rt.Wt            = Wt;                    % temporal weights
            rt.geo_lines     = geolines;              % We need the geometry file for all projections to allow us to sub-select rows for temporal reconstruction
            % rt.snum          = 1;                   % number of subsets to use for reconstruction
            
        % Additional rebinning parameters for cylindrical detectors
        
            % Note: int_params_f should be set up for analytical reconstruction with rebinned data.
            rt.double_params_rebin = double(double_params_rebin); % double parameters for WFBP with rebinned projections
            
            rt.geofile_rebin       = geofile_rebin;           % geometry file for WFBP with rebinned projections
            rt.geolines_rebin      = geolines_rebin;          % Override 'geolines' above.
            
            rt.int_params_b        = int32(int_params_b);     % int32 parameters for cone-parallel rebinning
            rt.double_params_b     = double(double_params_b); % double parameters for cone-parallel rebinning
            rt.det_type_rebin      = int32(det_type);         % detector type setting for cone-parallel rebinning
            
        % Import DukeSim projections in *.bin format, instead of the standard nifti format for projections
        
            rt.projection_rescale_factor = double(rescale_factor); % Rescaling value for DukeSim projections
            rt.suppress_outliers         = suppress_outliers;      % (0) Don't suppress outliers, (1) Suppress outliers relative to the median line integral value

        % Sizes

            rt.p   = p;          % number of temporal phases
            rt.e   = 1;          % number of spectral samples
            rt.sz  = [nx,ny,nz]; % full volume size
            rt.szy = [nu,nv,np]; % projection data size

        % Regularization

            % regularization strength
            rt.mu = 0.005;

            % regularization, RSKR (energy)
            rt.stride      = 3;            % subsampling to deal with correlated noise
            rt.w           = int32(6);     % filtration kernel radius
            rt.w2          = single(0.8);  % kernel resampling bandwidth
            rt.lambda0     = single(1.5);  % regularization strength
            rt.attn_water  = attn_water;   % attempt to equalize the noise level between spectral samples, following water normalization
            rt.g           = 0.5;          % exponential regularization scaling with singular value ratio ( lambda0*(E(1,1)./diag(E)').^(g) )

            % regularization, pSVT (time)
            rt.apply_pSVT      = 1;   % (0) do not apply pSVT; (1) Apply pSVT when there is a time dimension
            rt.w_pSVT          = 3;   % kernel diameter, pSVT
            rt.nu_pSVT         = 0.7; % non-convex singular value exponent
            rt.multiplier_pSVT = 0.3; % multiplier for singular value threshold

            % directory of the version of BF to use for RSKR
            % rt.BF_path = fileparts(which('jointBF4D'));

        % Save the template

            rt_file = fullfile(outpath,[run_name '_recon_template.mat']);

            save(rt_file,'rt');
            
        % Since the iterative reconstruction function expects the template and the template name includes
        % information read from the parameter file, pass the template itself rather than the template file
        % name.

            recon_templates{recon} = rt;
        
        end

    end


end

