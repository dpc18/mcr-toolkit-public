// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// *** Data structures for managing identical variables across multiple GPUs ***

#ifndef DD_v25_DATA_STRUCTURES

    #define DD_v25_DATA_STRUCTURES

    #include "cuda_runtime.h"
    #include "cufft.h"

    // Private GPU variables - one copy per GPU stream

        typedef struct
        {

            // Buffer for double precision atomic add operations to minimize the effects of random rounding order
            // NOTE: Requires compute capability 6.1+ which is only on Pascal GPUs...
            // Double precision atomic add function included for backward compatibility; however, it is very slow.
            double *projs_temp_; 

            // Buffer for performing complex arithmetic on projections
            cufftComplex *fftproj_; 

            // Data structure for calling cuFFT operations, has to be associated with a stream specific stream for asynchronous operation
            cufftHandle fftplan;
            cufftHandle ifftplan;

            // Private device stream for parallel computations and data transfers.
            cudaStream_t mystream;

        } Private_GPU_Data;

    // Shared GPU variables - one copy per GPU
        
        typedef struct
        {
            
            // projection weights for referencing time in rebinned data
            double *proj_weights_d;

            // affine transform
            float *aff_;

            // array of projection matrices for each projection
            float *pm_;

            // Backprojection filter
            float *filt_;

            // Pointer to mask data on the GPU (z limits header)
            unsigned short *rmask_header_;
            
            // v24: moved to managed memory
            float *norm_vol_; // Allocated only if use_norm_vol = 1. Image domain count normalization for helical data.
            
            // v24: Pointer to mask data on the GPU (volumetric mask)
            unsigned short *rmask_; 
            
            // v24: Data pointers (managed memory, shared between streams)     
            float *vol_;  // Pointer to volume data, managed
            float *proj_; // Pointer to projection data, managed
            
            // v25: Projection weight mask for backprojection (e.g. Tam-Danielson Window)
            //      Assumed to the be the same for all projections.
            float *proj_weight_mask_;

        } Public_GPU_Data;

    // Reconstruction parameters

        typedef struct
        {

            // Input parameters
            
                // v25
                int use_proj_weight_mask;
            
                // new parameter for v22
                // distance in mm, used during backprojection only
                // if 0, do not use
                // if > 0, ignore rays which diverge by more than this absolute distance from the central ray at the detector
                // needed for helical reconstruction
                double limit_angular_range;

                int use_norm_vol; // (0) - do not allocate / use count normalization volume, (1) allocate / use count normalization volume during backprojection

                // new for v20
                int *GPU_indices; // list of GPUs to use for computation
                int nDevices;     // number of GPUs to use for computation

                // flags: new for v15
                int cy_detector; // (0) - detector is flat; (1) detector is cylindrical
                int implicit_mask; // (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
                int explicit_mask;  // (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask
                int use_affine;  // (0) - affine transform is not used (even if one is provided); (1) - affine transform is used
                // unsigned short *rmask;
                double db; // fan angle increment for cylindrical detector

                float *aff; // affine transform
                char *geo_name;  // geometry file name
                char *filt_name; // FDK filter file name
                size_t nx; // number of voxels in x dimension
                size_t ny; // number of voxels in y dimension
                size_t nz; // number of voxels in z dimension
                double du; // horizontal pixel pitch
                double dv; // vertical pixel pitch
                double dx; // voxel size in x dimension
                double dy; // voxel size in y dimension
                double dz; // voxel size in z dimension
                double zo; // initial translation offset on z-axis
                double ao; // initial angle offset on z-axis
                double lsd; // length from source to detector
                double lso; // length from source to object
                double uc; // detector center pixel u value
                double vc; // detector center pixel v value
                double eta; // angle of detector alignment around x-axis
                double sig; // angle of detector alignment around y-axis
                double phi; // angle of detector alignment around z-axis

                size_t np; // number of projections
                size_t nu; // number of pixel columns on detector
                size_t nv; // number of pixel rows on detector
                size_t nu_fft;

                // scaling/weighting flags
                double scale; // scalar volume multiplier
                int filtration; // (1) FDK with specified filter, (0) simple backprojection

            // Derived parameters
                
                // mask limits
                // size_t min_mask;
                // size_t max_mask;

                float *filt; // filter for backprojection
                double np_eff;

                // double scale_factor;
                // double rect_rect_factor; // constant scaling factor for DD operators; does not include angularly-dependent component
                // size_t os, os2;
                // int count;

                // sizes (number of bytes)
                size_t projs_size; // projection stack
                size_t proj_size; // projection
                size_t vol_size; // volume
                size_t slice_size; // slice
                size_t filt_size; // filter 
                size_t pms_size; // projection matrices for all projections
                size_t vecs_size; // vectors for all projections
                size_t fftproj_size; // complex fft of projection
                size_t params_size; // parameter for all projections
                size_t proj3_size; // (x,y,z) for each pixel in projection
                size_t uvs_size; // 1 integer for each projection
                size_t fftslice_size; // complex fft of padded slice

                // system geometry

                int    nx2; // nx/2
                int    ny2; // ny/2
                int    nz2; // nz/2
                size_t nuv; // total number of pixels on detector
                size_t nuvp; // total number of pixels in projection stack
                size_t nxy; // total number of voxels in slice
                size_t nxyz; // total number of voxels in volume
                int3   n3xyz; // {nx,ny,nz}
                int3   n3xyz1; // {nx-1,ny-1,nz-1}
                int3   n3xyz2; // {nx/2,ny/2,nz/2}
                float3 d3xyz; // {dx,dy,dz}

                double *lsds; // array of lsd for all projections
                double *lsos; // array of lso for all projections
                double *ucs; // array of uc for all projections
                double *vcs; // array of vc for all projections
                double *etas; // array of eta for all projections
                double *sigs; // array of sig for all projections
                double *phis; // array of phi for all projections
                double *weights; // array of weights to apply to projections before performing backprojection, also used elsewhere
                double ox; // volume x offset
                double oy; // volume y offset
                double oz; // volume z offset
                double r; // radius of sphere containing volume
                double dstep; // size of voxel step for reprojection
                double nstep; // number of steps along diameter
                int    nstepvox; // number of steps along diameter in terms of voxels
                double *srcs; // array of source vectors for all projections
                double *dtvs; // array of detector center vectors for all projections
                double *puvs; // array of u pixel vectors for all projections
                double *pvvs; // array of v pixel vectors for all projections
                double *pms; // projection matrices for all projections
                float  *angles; // projection angle (including any offset) to decide which DD operator (xz vs. yz) to use
                float  *pmis; // projection matrices for all projections in voxel coordinate system

                dim3 pblock; // block size for projection operations
                dim3 pgrid; // grid size for projection operations
                dim3 pgrid_fft; // grid size for padded FFT operations

                dim3 vblock; // block size for volume operations
                dim3 vgrid; // grid size for volume operations

            // Pinned host memory for fast, asynchronous GPU <=> Host data transfers (sacrifice host memory for speed...)

                float *Pinned_Buffer; // Merge X_p and Y_p into one buffer to save RAM

            // Encapsulated GPU data structures

                Private_GPU_Data *GPU_pri;
                Public_GPU_Data  *GPU_pub;

        } recon_parameters;

#endif // DD_v25_DATA_STRUCTURES