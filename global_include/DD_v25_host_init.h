// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Authors: Sam Johnston, PhD; Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef DD_v25_INIT

    #define DD_v25_INIT

    // Function headers

        void initialize_host();
        void constructGeometry(char *filename);
        void constructProjectionMatrix(double *pm, double *src, double *dtv, double *puv, double *pvv, double uc, double vc);
        void multm(double *a, double *b, int x, int y, int z);
        void invert3(double *m);
        void getFilter(float *filt, char *filename, int fl, float du, int nv);

    // Host initialization
    
        void initialize_host()
        {

            zo = 0;
            ao = 0;

            // Execution configurations

            // ILP magic...!
            vblock = dim3(8,8,1);
            vgrid  = dim3(nx/8,ny/8,1);

            // vblock_zs = dim3(8,8,1);
            // vgrid_zs = dim3(nx/8,ny/8,(nz+ZSTEP-1)/ZSTEP);

            pblock    = dim3(8,8,1);
            pgrid     = dim3((nu+8-1)/8    ,(nv+8-1)/8,1); // dim3(nu/8,nv/8,1); // v25: divisibility by 8 no longer required for projections
            pgrid_fft = dim3((nu_fft+8-1)/8,(nv+8-1)/8,1);

            // Size parameters

            dstep = dx;

            nx2 = nx/2;
            ny2 = ny/2;
            nz2 = nz/2;

            // numbers of elements
            nuv = nu*nv;
            nuvp = nu*nv*np;
            nxy = nx*ny;
            nxyz = nx*ny*nz;

            n3xyz = make_int3(nx,ny,nz);
            n3xyz1 = make_int3(nx-1,ny-1,nz-1); 
            n3xyz2 = make_int3(nx2,ny2,nz2);
            d3xyz = make_float3(dx,dy,dz);

            // array sizes
            vol_size = nxyz*sizeof(float);
            projs_size = nuvp*sizeof(float);
            proj_size = nuv*sizeof(float);
            fftproj_size = nu_fft*nv*sizeof(cufftComplex);
            filt_size = nu_fft*nv*sizeof(float);
            pms_size = 12*np*sizeof(double);
            vecs_size = 3*np*sizeof(double);
            slice_size = nxy*sizeof(float);
            proj3_size = 3*nuv*sizeof(float);
            params_size = np*sizeof(double);
            uvs_size = np*sizeof(int);

            pms     = (double *) malloc(pms_size);
            pmis    = (float  *) malloc(12*np*sizeof(float));
            srcs    = (double *) malloc(vecs_size);
            dtvs    = (double *) malloc(vecs_size);
            puvs    = (double *) malloc(vecs_size);
            pvvs    = (double *) malloc(vecs_size);
            lsds    = (double *) malloc(params_size);
            lsos    = (double *) malloc(params_size);
            ucs     = (double *) malloc(params_size);
            vcs     = (double *) malloc(params_size);
            etas    = (double *) malloc(params_size);
            sigs    = (double *) malloc(params_size);
            phis    = (double *) malloc(params_size);
            weights = (double *) malloc(params_size);
            angles  = (float  *) malloc(np*sizeof(float));

            // geometry
            constructGeometry(geo_name);

            // load in the filter if we are going to perform projection filtration
            // NOTE: The built-in filter is likely not normalized correctly.
            if (filtration) {

                filt = (float *) malloc(filt_size);
                // filt = (float *) calloc(filt_size);

                getFilter(filt,filt_name,nu_fft,du,nv);

            }
            else {

                filt = (float *) malloc( sizeof(float) );

            }

            // Sum up weights to compute the effective number of projections to use for normalization
            np_eff = 0;
            for (int p = 0; p < np; p++) {

                np_eff += weights[p];

            }

        }

        void constructGeometry(char *filename)
        {
            int i, j, p;
            FILE *fp;
            double ang;

            // get vectors describing source, detector center, and detector alignment

            // open geometry file
            if((fp = fopen(filename,"r"))==NULL) {
                // mexErrMsgTxt("Cannot open geometry file!");
                ERR("Cannot open geometry file!");
            }

            for (p=0; p<np; p++) { // for each projection
                i = p*3; // vector index

                // read line from geometry file
                fscanf(fp,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",srcs+i+0,srcs+i+1,srcs+i+2,dtvs+i+0,dtvs+i+1,dtvs+i+2,puvs+i+0,puvs+i+1,puvs+i+2,pvvs+i+0,pvvs+i+1,pvvs+i+2,lsds+p,lsos+p,ucs+p,vcs+p,etas+p,sigs+p,phis+p,weights+p,&ang);

                angles[p] = (float) ang;

            }

            fclose(fp);

            // volume coordinate offsets
            ox = (nx-1)*dx/2.0f; 
            oy = (ny-1)*dy/2.0f;
            oz = (nz-1)*dz/2.0f;

            // radius of sphere containing volume
            r = sqrt(ox*ox+oy*oy+oz*oz); 

            // offset
            //oz += zo;

            // number of steps in ray
            nstep = 2*r/dstep;
            nstepvox = 2*r/dx;

            // construct projection matrices
            for (p=0; p<np; p++) { // for each projection
                i = p*3; // vector index
                j = p*12; // matrix index

                // construct projection matrix from vectors
                constructProjectionMatrix(pms+j,srcs+i,dtvs+i,puvs+i,pvvs+i,ucs[p],vcs[p]);

                // scale for multiplication by int indices
                pmis[j+0] = dx*pms[j+0]; pmis[j+1] = dy*pms[j+1]; pmis[j+2] = dz*pms[j+2]; pmis[j+3] = pms[j+3] - ox*pms[j+0] - oy*pms[j+1] - oz*pms[j+2];
                pmis[j+4] = dx*pms[j+4]; pmis[j+5] = dy*pms[j+5]; pmis[j+6] = dz*pms[j+6]; pmis[j+7] = pms[j+7] - ox*pms[j+4] - oy*pms[j+5] - oz*pms[j+6];
                pmis[j+8] = dx*pms[j+8]; pmis[j+9] = dy*pms[j+9]; pmis[j+10] = dz*pms[j+10]; pmis[j+11] = pms[j+11] - ox*pms[j+8] - oy*pms[j+9] - oz*pms[j+10];

            }

        }

        void constructProjectionMatrix(double *pm, double *src, double *dtv, double *puv, double *pvv, double uc, double vc) 
        {

            double vec[3];
            double k;
            double norm[3];
            int i,j,l;

            // construct detector normal with cross product
            norm[0] = puv[1]*pvv[2] - puv[2]*pvv[1];
            norm[1] = puv[2]*pvv[0] - puv[0]*pvv[2];
            norm[2] = puv[0]*pvv[1] - puv[1]*pvv[0];

            // get scale along source-to-point vector
            k = 0.0f;
            for (i=0;i<3;i++) k += norm[i]*(dtv[i]-src[i]);

            // construct source-to-point vector
            for (i=0;i<3;i++) vec[i] = src[i] - dtv[i] + (uc*puv[i]) + (vc*pvv[i]) + 1;

            // start with identity
            double pm4[] = {
                1,0,0,0,
                0,1,0,0,
                0,0,1,0,
                0,0,0,1};

            // translation 
            double trans[] = {
                1,0,0,-src[0],
                0,1,0,-src[1],
                0,0,1,-src[2],
                0,0,0,1};

            multm(trans,pm4,4,4,4);

            // divide by dot product
            double divdot[] = {
                1,0,0,0,
                0,1,0,0,
                0,0,1,0,
                norm[0],norm[1],norm[2],0};

            multm(divdot,pm4,4,4,4);

            // scale
            double scale[] = {
                k,0,0,0,
                0,k,0,0,
                0,0,k,0,
                0,0,0,1};

            multm(scale,pm4,4,4,4);

            // translation
            double trans2[] = {
                1,0,0,vec[0],
                0,1,0,vec[1],
                0,0,1,vec[2]};

            multm(trans2,pm4,4,4,4);

            // detector misalignment
            double align[] = {
                puv[0],pvv[0],1,
                puv[1],pvv[1],1,
                puv[2],pvv[2],1};

            invert3(align);

            multm(align,pm4,3,3,4);

            // copy temporary matrix to output, scale by value in lower right corner
            for (i=0;i<3;i++) {
                for (j=0;j<4;j++) {
                    l = i*4 + j;
                    pm[l] = pm4[l]/pm4[11];
                }
            }

        }

        void multm(double *a, double *b, int x, int y, int z)
        {
            double *c;
            int i,j,k,m;

            c = (double *) malloc(x*z*sizeof(double));

            // multiply
            for (i=0;i<x; i++) {
                for (j=0; j<z; j++) {
                    m = i*z + j;
                    c[m] = 0.0;
                    for (k=0; k<y; k++) {
                        c[m] += a[i*y + k]*b[k*z + j];
                    }
                }
            }

            // copy
            for (i=0;i<x;i++) {
                for (j=0; j<z; j++) {
                    m = i*z + j;
                    b[m] = c[m];
                }
            }
        }

        void invert3(double *m)
        {
            int i;
            double n[] = {0,0,0,0,0,0,0,0,0};

            // invert and transpose
            double det = m[0]*((m[4]*m[8]) - (m[5]*m[7])) - m[1]*((m[3]*m[8]) - (m[5]*m[6])) + m[2]*((m[3]*m[7]) - (m[4]*m[6]));

            // prevent this from blowing up at multiples of 45 degrees
            if ((float) det == 0) {

                det = 1e-6;

            }

            n[0] =    ((m[4]*m[8]) - (m[5]*m[7]))/det;
            n[1] = -1*((m[1]*m[8]) - (m[2]*m[7]))/det;
            n[2] =    ((m[1]*m[5]) - (m[2]*m[4]))/det;
            n[3] = -1*((m[3]*m[8]) - (m[5]*m[6]))/det;
            n[4] =    ((m[0]*m[8]) - (m[2]*m[6]))/det;
            n[5] = -1*((m[0]*m[5]) - (m[2]*m[3]))/det;
            n[6] =    ((m[3]*m[7]) - (m[4]*m[6]))/det;
            n[7] = -1*((m[0]*m[7]) - (m[1]*m[6]))/det;
            n[8] =    ((m[0]*m[4]) - (m[1]*m[3]))/det;

            // copy
            for (i=0;i<9;i++) m[i] = n[i];
        }

        void getFilter(float *filt, char *filename, int fl, float du, int nv)
        {
            int i, fl2, filtcore_size, v;
            float val;
            FILE *fp;
            float *filtcore;

            fl2 = fl/2;
            filtcore_size = (fl2+1)*sizeof(double);

            filtcore = (float *) malloc(filtcore_size);

            // get filter core (half of filter)
            if (filename != NULL && filename[0]!='0' && filename[0]!=0 ) { // user provided filter core
                // open filter file
                if((fp = fopen(filename,"rb"))==NULL) {
                    ERR("Cannot open filter file.");
                    // mexErrMsgTxt("Cannot open filter file.");
                    // return 1;
                } 

                // read filter file
                for (i=0; i<=fl2; i++) {
                    fread ((float *) &val, sizeof(float), 1, fp);
                    filtcore[i] = val;
                }

                // close filter file
                fclose(fp);

            } else { // build new filter core, ramp filter with Hamming window
                for (i=0; i<=fl2; i++) {
                    filtcore[i] = 2.0f*i/fl2; // ramp
                    filtcore[i] = filtcore[i]*cos(0.5*pi_val*i/fl2); // window
                }
            }

            // build filter from core (both halves)
            for (v=0; v<nv; v++) {
                filt[v*fl] = filtcore[0];
                for (i=1; i<=fl2; i++) {
                    filt[v*fl + i] = filtcore[i];
                    filt[v*fl + fl-i] = filtcore[i];
                }
            }

            free(filtcore);

        }

#endif // DD_v25_INIT
