// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Parameters to be included in the reconstruction parameter structure
// Since cleaning up partial allocations will likely get messy, work with global variables to perform error checking.
// Once we are sure the parameters are valid, allocate and configure a new recon_parameters data structure.

    // new in v25
    // (0) do not use a single precision mask to assign backprojection weights per projection pixel; (1) use a backprojection weight mask (e.g. a Tam-Danielson Window)
    int use_proj_weight_mask;
    float *proj_weight_mask;
    
    // new parameter for v22
    // distance in mm, used during backprojection only
    // if 0, do not use
    // if > 0, ignore rays which diverge by more than this absolute distance from the central ray at the detector
    // needed for helical reconstruction
    double limit_angular_range;
    
    int use_norm_vol; // (0) - do not allocate / use count normalization volume, (1) allocate / use count normalization volume during backprojection

    // new for v20
    int *GPU_indices; // list of GPUs to use for computation
    int nDevices;     // number of GPUs to use for computation

    // flags: new for v15
    int cy_detector; // (0) - detector is flat; (1) detector is cylindrical
    int implicit_mask; // (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
    int explicit_mask;  // (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask
    int use_affine;  // (0) - affine transform is not used (even if one is provided); (1) - affine transform is used
    unsigned short *rmask;
    double db; // fan angle increment for cylindrical detector
    float *aff; // affine transform
    char *geo_name;  // geometry file name
    char *filt_name; // FDK filter file name
    size_t nx; // number of voxels in x dimension
    size_t ny; // number of voxels in y dimension
    size_t nz; // number of voxels in z dimension
    double du; // horizontal pixel pitch
    double dv; // vertical pixel pitch
    double dx; // voxel size in x dimension
    double dy; // voxel size in y dimension
    double dz; // voxel size in z dimension
    double zo = 0.0; // initial translation offset on z-axis
    double ao = 0.0; // initial angle offset on z-axis
    double lsd; // length from source to detector
    double lso; // length from source to object
    double uc; // detector center pixel u value
    double vc; // detector center pixel v value
    double eta; // angle of detector alignment around x-axis
    double sig; // angle of detector alignment around y-axis
    double phi; // angle of detector alignment around z-axis

    size_t np; // number of projections
    size_t nu; // number of pixel columns on detector
    size_t nv; // number of pixel rows on detector
    size_t nu_fft;

    // scaling/weighting flags
    double scale; // scalar volume multiplier
    int filtration; // (1) FDK with specified filter, (0) simple backprojection

    // float rot_angle;
    float *filt; // filter for backprojection
    double np_eff;

    // double scale_factor;
    // double rect_rect_factor; // constant scaling factor for DD operators; does not include angularly-dependent component
    // float mag;
    // unsigned int os, os2;
    // int count;

    // sizes (number of bytes)
    size_t projs_size = 0; // projection stack
    size_t proj_size = 0; // projection
    size_t vol_size = 0; // volume
    size_t slice_size = 0; // slice
    size_t filt_size = 0; // filter 
    size_t pms_size = 0; // projection matrices for all projections
    size_t vecs_size = 0; // vectors for all projections
    size_t fftproj_size = 0; // complex fft of projection
    size_t params_size = 0; // parameter for all projections
    size_t proj3_size = 0; // (x,y,z) for each pixel in projection
    size_t uvs_size = 0; // 1 integer for each projection
    size_t fftslice_size = 0; // complex fft of padded slice

    // system geometry

    int    nx2; // nx/2
    int    ny2; // ny/2
    int    nz2; // nz/2
    size_t nuv; // total number of pixels on detector
    size_t nuvp; // total number of pixels in projection stack
    size_t nxy; // total number of voxels in slice
    size_t nxyz; // total number of voxels in volume
    int3   n3xyz; // {nx,ny,nz}
    int3   n3xyz1; // {nx-1,ny-1,nz-1}
    int3   n3xyz2; // {nx/2,ny/2,nz/2}
    float3 d3xyz; // {dx,dy,dz}

    double *lsds; // array of lsd for all projections
    double *lsos; // array of lso for all projections
    double *ucs; // array of uc for all projections
    double *vcs; // array of vc for all projections
    double *etas; // array of eta for all projections
    double *sigs; // array of sig for all projections
    double *phis; // array of phi for all projections
    double *weights; // array of weights to apply to projections before performing backprojection, also used elsewhere
    double ox; // volume x offset
    double oy; // volume y offset
    double oz; // volume z offset
    double r; // radius of sphere containing volume
    double dstep; // size of voxel step for reprojection
    double nstep; // number of steps along diameter
    int    nstepvox; // number of steps along diameter in terms of voxels
    double *srcs; // array of source vectors for all projections
    double *dtvs; // array of detector center vectors for all projections
    double *puvs; // array of u pixel vectors for all projections
    double *pvvs; // array of v pixel vectors for all projections
    double *pms; // projection matrices for all projections
    float  *angles; // projection angle (including any offset) to decide which DD operator (xz vs. yz) to use
    float  *pmis; // projection matrices for all projections in voxel coordinate system

    // int *minus; // lower bound for u of active field of view in projections
    // int *minvs; // lower bound for v of active field of view in projections
    // int *maxus; // upper bound for u of active field of view in projections
    // int *maxvs; // upper bound for v of active field of view in projections

    dim3 pblock; // block size for projection operations
    dim3 pgrid; // grid size for projection operations
    dim3 pgrid_fft; // grid size for padded FFT operations

    dim3 vblock; // block size for volume operations
    dim3 vgrid; // grid size for volume operations
    