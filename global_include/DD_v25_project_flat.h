// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

    // References

    // 1) "3D forward and back-projection for X-ray CT using separable footprints."
    //    Long, Yong, Jeffrey A. Fessler, and James M. Balter.
    //    IEEE Transactions on Medical Imaging 29.11 (2010): 1839-1850.

    // 2) Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
    //    Cone-beam reprojection using projection-matrices.
    //    IEEE transactions on medical imaging, 22(10), 1202-1214.

// Projection kernels for flat panel detectors
// Supports arbitrary source, detector offsets and detector rotation

#ifndef DD_PROJECT_v25_FLAT
#define DD_PROJECT_v25_FLAT

// Function Headers //
    
    // NOTE: Flat operators use rect approximation along v and the trap approximation along u.


    // cy_detector = 0) flat-panel
    __global__ void DD_project_flat(double *proj, const float *vol, int3 n3xyz, int nz0, float3 d3xyz, int nu, int nv,
                                    const float *aff, const float* pm_, float rect_rect_factor, float3 src,
                                    int use_affine, const unsigned short* mask_header, const unsigned short* mask,
                                    int implicit_mask, int explicit_mask, int z_offset);
    
    // cy_detector = 3) rebinned, flat
    __global__ void DD_project_flatb(double *proj, const float *vol, int3 n3xyz, int nz0, float3 d3xyz, int nu, int nv,
                                     const float *aff, const float* pm_, float rect_rect_factor, float3 src,
                                     float du, float uc, float vc, float dsd,
                                     int use_affine,  const unsigned short* mask_header, const unsigned short* mask,
                                     int implicit_mask, int explicit_mask, int z_offset, float3 puvs);
    

// ***                            ***
// *** DEVICE - MAIN - Flat Panel ***
// ***    cy_detector = 0         ***

__global__ void DD_project_flat(double *proj, const float *vol, int3 n3xyz, int nz0, float3 d3xyz, int nu, int nv,
                                const float *aff, const float* pm_, float rect_rect_factor, float3 src,
                                int use_affine,  const unsigned short* mask_header, const unsigned short* mask,
                                int implicit_mask, int explicit_mask, int z_offset) // float *proj
{

    // Setup
    
        int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
        int iy = (blockIdx.y * blockDim.y) + threadIdx.y;
        
    // Check the z range if we are using an explicit mask
    // when zstart = zend, the kernel does nothing

        // Absolute indices
        int zstart = z_offset;
        int zend   = z_offset + n3xyz.z; // one slice past the end slice

        if (explicit_mask > 0) {

            // restrict the operation to the intersection of our volume chunk and what is inside of the mask
            zstart = max( zstart, mask_header[iy*n3xyz.x + ix]);
            zend   = min( zend, mask_header[n3xyz.x*n3xyz.y + iy*n3xyz.x + ix]);

            // return early if there is no work to do inside of the mask
            if ( zend <= zstart ) return;

        }
    
    // Declare variables
        
        int iz;
        // int iz;
        int i, j;
        float gamma[1];
        float us[4] = {0.0,0.0,0.0,0.0};
        float vs[2] = {0.0,0.0}; //,0.0,0.0};
        float x,y,z,x2,y2,z2,nx,ny,nz,w;
        // unsigned int idx;
        float min_u, max_u, min_v, max_v; // , v0, v1;
        float s1, s2, f1, f2; // r
        int idxu, idxv;
        float C;

        nx = n3xyz.x; ny = n3xyz.y; nz = nz0;
        nx = nx/2; ny = ny/2; nz = nz/2;

        float weight, signy1, signy2, signx1, signx2, signz1, signz2; // sign

        unsigned int nuv = nu*nv;
        
        size_t idx, idx0;
    
    // Check that we are in the implied mask
        
        // All voxels along z will also be outside of the mask, so return.
        // Setting x, y only once assumes no affine.
        x = ix; y = iy;
        
        if (implicit_mask == 1) {
            
            if (sqrtf((x - nx)*(x - nx) + (y - ny)*(y - ny)) > fminf(nx,ny)) return;
            
        }
        
    // Precompute reused quantities
        
        idx0 = (iy*n3xyz.x) + ix;
        
        float pmv2, pmv3;
        float u1, u2, u3, u4, u5, u6, u7, u8;
        if (use_affine == 0) {
         
            pmv2 = pm_[4]*ix + pm_[5]*iy + pm_[7];
            pmv3 = pm_[8]*ix + pm_[9]*iy + pm_[11];
            
            signy1 = iy - 0.5f;
            signy2 = iy + 0.5f;
            signx1 = ix - 0.5f;
            signx2 = ix + 0.5f;

            u1 = pm_[0]*signx1 + pm_[1]*signy1 + pm_[3];
            u2 = pm_[8]*signx1 + pm_[9]*signy1 + pm_[11];

            u3 = pm_[0]*signx2 + pm_[1]*signy1 + pm_[3];
            u4 = pm_[8]*signx2 + pm_[9]*signy1 + pm_[11];

            u5 = pm_[0]*signx1 + pm_[1]*signy2 + pm_[3];
            u6 = pm_[8]*signx1 + pm_[9]*signy2 + pm_[11];

            u7 = pm_[0]*signx2 + pm_[1]*signy2 + pm_[3];
            u8 = pm_[8]*signx2 + pm_[9]*signy2 + pm_[11];
            
        }
        else { // use_affine == 1
         
            x2 = ix - nx + 0.5f;
            y2 = iy - ny + 0.5f;
            
            u1 = x2*aff[0] + y2*aff[3] + aff[9];
            u2 = x2*aff[1] + y2*aff[4] + aff[10];
            u3 = x2*aff[2] + y2*aff[5] + aff[11];
            
        }

    // Use ILP to reduce overhead
    // for (iz = zp; iz < zp+ZSTEP; iz++) {
    for (iz = zstart; iz < zend; iz++) {
        
        // Use the relative z index to do volume lookups, since we are working with a volume buffer
        // NOTE: This may result in illegal indexing if the iz bounds checks are not performed.
        idx = ( (size_t) (iz - z_offset) )*( (size_t) n3xyz.x*n3xyz.y ) + idx0;
        
        // Update z, reset x, y
        
        	x = ix; y = iy; z = iz;

            if (use_affine == 1) {
            
                // ANTs (WIN v2.1.0) compatible affine transform
                // x2 = x - nx + 0.5f;
                // y2 = y - ny + 0.5f;
                z2 = z - nz;

                x = u1 + z2*aff[6];
                y = u2 + z2*aff[7];
                z = u3 + z2*aff[8];

                x = x + nx - 0.5f;
                y = y + ny - 0.5f;
                z = z + nz;

                // Compute v first to make sure we are inside the projection along z
                signz1 = (z-0.5f);
                signz2 = (z+0.5f);

                vs[0] = pm_[4]*x + pm_[5]*y + pm_[6] *signz1 + pm_[7];
                w =     pm_[8]*x + pm_[9]*y + pm_[10]*signz1 + pm_[11];
                vs[0] = vs[0]/w;

                min_v = ceilf(vs[0] - 0.5f);
                if ( min_v >= nv ) return;

                vs[1] = pm_[4]*x + pm_[5]*y + pm_[6] *signz2 + pm_[7];
                w =     pm_[8]*x + pm_[9]*y + pm_[10]*signz2 + pm_[11];
                vs[1] = vs[1]/w;
            
            }
            else {
             
                // Compute v first to make sure we are inside the projection along z
                signz1 = (z-0.5f);
                signz2 = (z+0.5f);

                vs[0] = ( pmv2 + pm_[6] *signz1 ) / ( pmv3 + pm_[10]*signz1 );

                min_v = ceilf(vs[0] - 0.5f);
                if ( min_v >= nv ) return;

                vs[1] = ( pmv2 + pm_[6] *signz2 ) / ( pmv3 + pm_[10]*signz2 );
                
            }
            
            max_v = ceilf(vs[1] - 0.5f);
            if ( max_v < 0 ) continue;
            
            // v24: Use the mask explicitly rather than pre-applying to the volume, since we may
            // only be access a small portion of the data on the GPU.
            if (explicit_mask == 2) {
                
                if (mask[idx] == 0) continue;
                
            }
            
            C = vol[idx];
            if ( (C == 0) ) continue;
            
        // Compute spatial weight

            // lsd included in rect factor
            weight = rsqrtf( (d3xyz.x*(x - nx)-src.x)*(d3xyz.x*(x - nx)-src.x) + (d3xyz.y*(y - ny)-src.y)*(d3xyz.y*(y - ny)-src.y) + (d3xyz.z*(z - nz)-src.z)*(d3xyz.z*(z - nz)-src.z) );
            weight *= weight;
            
        // Compute u
        
            // NOTE: It is possible to reduce the computation / memory use by only computing the used points based on the rotation.
            // However, using the same axis for all rays (i.e. ignoring the fan angles), does not allow the use of different axes
            // within the same projection, creating small errors.
            
            if (use_affine == 1) {
            
                signy1 = y - 0.5f;
                signy2 = y + 0.5f;
                signx1 = x - 0.5f;
                signx2 = x + 0.5f;

                us[0] = pm_[0]*signx1 + pm_[1]*signy1 + pm_[2]*z + pm_[3];
                w =     pm_[8]*signx1 + pm_[9]*signy1 + pm_[10]*z + pm_[11];
                us[0] = us[0]/w;

                us[1] = pm_[0]*signx2 + pm_[1]*signy1 + pm_[2]*z + pm_[3];
                w =     pm_[8]*signx2 + pm_[9]*signy1 + pm_[10]*z + pm_[11];
                us[1] = us[1]/w;

                us[2] = pm_[0]*signx1 + pm_[1]*signy2 + pm_[2]*z + pm_[3];
                w =     pm_[8]*signx1 + pm_[9]*signy2 + pm_[10]*z + pm_[11];
                us[2] = us[2]/w;

                us[3] = pm_[0]*signx2 + pm_[1]*signy2 + pm_[2]*z + pm_[3];
                w =     pm_[8]*signx2 + pm_[9]*signy2 + pm_[10]*z + pm_[11];
                us[3] = us[3]/w;
            
            }
            else {

                us[0] = ( u1 + pm_[2]*z ) / ( u2 + pm_[10]*z );

                us[1] = ( u3 + pm_[2]*z ) / ( u4 + pm_[10]*z );

                us[2] = ( u5 + pm_[2]*z ) / ( u6 + pm_[10]*z );

                us[3] = ( u7 + pm_[2]*z ) / ( u8 + pm_[10]*z );
                
            }

            sort4(us,us+1,us+2,us+3);

            min_u = ceilf(us[0] - 0.5f);
            max_u = ceilf(us[3] - 0.5f);
            
            // continue if we go out of bounds
            if ( ( max_u < 0 ) || ( min_u >= nu ) ) continue;
        
        // Weight and area normalization factor

            // Normalize relative to magnified rect area
            // C *= weight * rect_rect_factor * (1 / ( ( (us[3]-us[0])) * (vs[3]-vs[0]) ) );
        
            // Normalize relative to magnified trap area
            // C *= weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (2 / ( ((vs[3]-vs[0])+(vs[2]-vs[1])) ));

            // Normalize relative to magnified trap-rect area
            C *= weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (1 / ( vs[1] - vs[0] ) );

        // Use weights to perform projection
            
            for (i = 0; i < max_u - min_u + 1; i++) {

                idxu = min_u + i;
                
                // rect
                // s1 = min_u + i + du/2;
                // s2 = min_u + i - du/2;
                // f1 = fmaxf(fminf(s1,us[1]) - fmaxf(s2,us[0]),0);
                // f1 = fmaxf(fminf(s1,us[3]) - fmaxf(s2,us[0]),0);
                
                // trap
                s1 = min_u + i + 0.5f;
                s2 = min_u + i - 0.5f;
                gamma[0] = 0.0f;
                gamma_trap(us, s2, s1, gamma);
                f1 = gamma[0];
                
                for (j = 0; j < max_v - min_v + 1; j++) {

                    idxv = min_v + j;
            
                    // rect
                    s1 = min_v + j + 0.5f;
                    s2 = min_v + j - 0.5f;
                    f2 = C*f1*fmaxf(fminf(s1,vs[1]) - fmaxf(s2,vs[0]),0);

                    // trap
                    // s1 = min_v + j + 0.5f;
                    // s2 = min_v + j - 0.5f;
                    // gamma[0] = 0.0f;
                    // gamma_trap(vs, s2, s1, gamma);
                    // f2 = C*gamma[0]*f1;

                    idxv = idxv*nu + idxu;

                    // check to make sure we have not gone out of bounds before updating the projection
                    // if (idxu < BUFFER*nuv && idxu >= 0) 
                    if (idxv < nuv && idxv >= 0 && f2 == f2) {

                        // proj[idxu] += C*F1[i]*F2[j]; // will fail due to simultaneous read/write operations between threads
                        // atomicAdd(proj+idxv,f2); // *C); // atomic add addresses this problem
                        
                        // Add to double precision projection data to avoid random rounding
                        
                        #if __CUDA_ARCH__ < 600

                           atomicAdd_old(proj+idxv,f2);
                        
                        #else

                           atomicAdd(proj+idxv,f2);
                        
                        #endif

                    }

                }
                
            }

    
    }

}


// ***                               ***
// *** DEVICE - MAIN - Rebinned Flat ***
// *** cy_detector = 3               ***

__global__ void DD_project_flatb(double *proj, const float *vol, int3 n3xyz, int nz0, float3 d3xyz, int nu, int nv,
                                 const float *aff, const float* pm_, float rect_rect_factor, float3 src,
                                 float du, float uc, float vc, float dsd,
                                 int use_affine,  const unsigned short* mask_header, const unsigned short* mask,
                                 int implicit_mask, int explicit_mask, int z_offset, float3 puvs) // float *proj
{

    // A) Obtain x, y, and z coordinates for the current voxel and declare other variables
	int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
	int iy = (blockIdx.y * blockDim.y) + threadIdx.y;
    
    // Check the z range if we are using an explicit mask
    // when zstart = zend, the kernel does nothing

        // Absolute indices
        int zstart = z_offset;
        int zend   = z_offset + n3xyz.z; // one slice past the end slice

        if (explicit_mask > 0) {

            // restrict the operation to the intersection of our volume chunk and what is inside of the mask
            zstart = max( zstart, mask_header[iy*n3xyz.x + ix]);
            zend   = min( zend, mask_header[n3xyz.x*n3xyz.y + iy*n3xyz.x + ix]);

            // return early if there is no work to do inside of the mask
            if ( zend <= zstart ) return;

        }
    
    // Declare variables
        
        int iz;
        // int iz;
        int i, j;
        float gamma[1];
        float us[4] = {0.0,0.0,0.0,0.0};
        float vs[2] = {0.0,0.0}; // ,0.0,0.0};
        float x,y,z,x2,y2,z2,nx,ny,nz,w;
        // unsigned int idx;
        float min_u, max_u, min_v, max_v; //, v0, v1;
        float s1, s2, f1, f2; // r
        int idxu, idxv;
        float C;

        nx = n3xyz.x; ny = n3xyz.y; nz = nz0;
        nx = nx/2; ny = ny/2; nz = nz/2;

        float signy1, signy2, signx1, signx2, signz1, signz2, weight; //, ang; // sign,

        unsigned int nuv = nu*nv;

        size_t idx, idx0;
    
    // check that we are in the mask
    // All voxels along z will also be outside of the mask, so return.
    // Setting x, y only once assumes no affine.
        x = ix; y = iy;
    
        if (implicit_mask == 1) {
            
            if (sqrtf((x - nx)*(x - nx) + (y - ny)*(y - ny)) > fminf(nx,ny)) return;
            
        }
        
    // account for the u offset from the central detector element (du*AM)
    // and the u offset of the source from the source center / isocenter
    // float u_offset = atan2f( -delta_u - du*AM, dsd ) / db;
    
    // Precompute reused quantities
        
        idx0 = (iy*n3xyz.x) + ix;
        
        float pmv1, pmv2, pmv3;
        float u1, u2, u3, u4;
        if (use_affine == 0) {
            
            pmv1 = pm_[0]*ix + pm_[1]*iy + pm_[3];
            pmv2 = pm_[4]*ix + pm_[5]*iy + pm_[7];
            pmv3 = pm_[8]*ix + pm_[9]*iy + pm_[11];
            
            signy1 = iy - 0.5f;
            signy2 = iy + 0.5f;
            signx1 = ix - 0.5f;
            signx2 = ix + 0.5f;
            
            u1 = d3xyz.x*(signx1 - nx) * puvs.x + d3xyz.y*(signy1 - ny) * puvs.y;
            u2 = d3xyz.x*(signx2 - nx) * puvs.x + d3xyz.y*(signy1 - ny) * puvs.y;
            u3 = d3xyz.x*(signx1 - nx) * puvs.x + d3xyz.y*(signy2 - ny) * puvs.y;
            u4 = d3xyz.x*(signx2 - nx) * puvs.x + d3xyz.y*(signy2 - ny) * puvs.y;
            
        }
        else { // use_affine == 1
         
            x2 = ix - nx + 0.5f;
            y2 = iy - ny + 0.5f;
            
            u1 = x2*aff[0] + y2*aff[3] + aff[9];
            u2 = x2*aff[1] + y2*aff[4] + aff[10];
            u3 = x2*aff[2] + y2*aff[5] + aff[11];
            
        }
        
    // convert puvs to a unit vector for parallel beam calculations
        
        float puvs_mag = sqrtf( puvs.x * puvs.x + puvs.y * puvs.y + puvs.z * puvs.z );
        // float r_mag;
        // float v_prime;
        
    // Use ILP to reduce overhead
    // for (iz = zp; iz < zp+ZSTEP; iz++) {
    for (iz = zstart; iz < zend; iz++) {
        
        // Use the relative z index to do volume lookups, since we are working with a volume buffer
        // NOTE: This may result in illegal indexing if the iz bounds checks are not performed.
        idx = ( (size_t) (iz - z_offset) )*( (size_t) n3xyz.x*n3xyz.y ) + idx0;
        
        // Update z, reset x, y
        x = ix; y = iy; z = iz;

            if (use_affine == 1) {
            
                // ANTs (WIN v2.1.0) compatible affine transform
                // x2 = x - nx + 0.5f;
                // y2 = y - ny + 0.5f;
                z2 = z - nz;

                x = u1 + z2*aff[6];
                y = u2 + z2*aff[7];
                z = u3 + z2*aff[8];

                x = x + nx - 0.5f;
                y = y + ny - 0.5f;
                z = z + nz;

                // Compute v first to make sure we are inside the projection along z
                signz1 = (z-0.5f);
                signz2 = (z+0.5f);

                vs[0] = pm_[4]*x + pm_[5]*y + pm_[6] *signz1 + pm_[7];
                w =     pm_[8]*x + pm_[9]*y + pm_[10]*signz1 + pm_[11];
                vs[0] = vs[0]/w;

                min_v = ceilf(vs[0] - 0.5f);
                if ( min_v >= nv ) return;

                vs[1] = pm_[4]*x + pm_[5]*y + pm_[6] *signz2 + pm_[7];
                w =     pm_[8]*x + pm_[9]*y + pm_[10]*signz2 + pm_[11];
                vs[1] = vs[1]/w;
            
            }
            else {
             
                // Compute v first to make sure we are inside the projection along z
                signz1 = (z-0.5f);
                signz2 = (z+0.5f);

                vs[0] = ( pmv2 + pm_[6] *signz1 ) / ( pmv3 + pm_[10]*signz1 );

                min_v = ceilf(vs[0] - 0.5f);
                if ( min_v >= nv ) return;

                vs[1] = ( pmv2 + pm_[6] *signz2 ) / ( pmv3 + pm_[10]*signz2 );
                
            }

            max_v = ceilf(vs[1] - 0.5f);
            if (max_v < 0) continue;
            
           // v24: Use the mask explicitly rather than pre-applying to the volume, since we may
            // only be access a small portion of the data on the GPU.
            if (explicit_mask == 2) {
                
                if (mask[idx] == 0) continue;
                
            }
            
            C = vol[idx];
            if ( (C == 0) ) continue;
            
        // Compute spatial weight
 
            // lsd included in rect factor
            weight = rsqrtf( (d3xyz.x*(x - nx)-src.x)*(d3xyz.x*(x - nx)-src.x) + (d3xyz.y*(y - ny)-src.y)*(d3xyz.y*(y - ny)-src.y) + (d3xyz.z*(z - nz)-src.z)*(d3xyz.z*(z - nz)-src.z) );
            // weight *= weight;
        
        // Trapezoidal approximation along u
            
            if (use_affine == 0) {
                
                us[0] = ( u1 + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[0] = ( us[0] / du + uc );

                us[1] = ( u2 + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[1] = ( us[1] / du + uc );

                us[2] = ( u3 + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[2] = ( us[2] / du + uc );

                us[3] = ( u4 + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[3] = ( us[3] / du + uc );
                
            }
            else {
             
                signy1 = y - 0.5f;
                signy2 = y + 0.5f;
                signx1 = x - 0.5f;
                signx2 = x + 0.5f;

                us[0] = ( d3xyz.x*(signx1 - nx) * puvs.x + d3xyz.y*(signy1 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[0] = ( us[0] / du + uc );

                us[1] = ( d3xyz.x*(signx2 - nx) * puvs.x + d3xyz.y*(signy1 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[1] = ( us[1] / du + uc );

                us[2] = ( d3xyz.x*(signx1 - nx) * puvs.x + d3xyz.y*(signy2 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[2] = ( us[2] / du + uc );

                us[3] = ( d3xyz.x*(signx2 - nx) * puvs.x + d3xyz.y*(signy2 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[3] = ( us[3] / du + uc );
                
            }

            sort4(us,us+1,us+2,us+3);
        
            min_u = ceilf(us[0] - 0.5f);
            max_u = ceilf(us[3] - 0.5f);
            
            // continue if we go out of bounds
            if ( ( max_u < 0 ) || ( min_u >= nu ) ) continue;
            
        // Weight and area normalization factor

            // Normalize relative to magnified rect area
            // C *= weight * rect_rect_factor * (1 / ( ( (us[3]-us[0])) * (vs[3]-vs[0]) ) );
        
            // Normalize relative to magnified trap area
            // C *= weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (2 / ( ((vs[3]-vs[0])+(vs[2]-vs[1])) ));

            // Normalize relative to magnified trap-rect area
            C *= weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (1 / ( vs[1] - vs[0] ) ); // weight *
            // C *= rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (1 / ( vs[1] - vs[0] ) );
            
        // Use weights to perform projection
            
            for (i = 0; i < max_u - min_u + 1; i++) {
                
                idxu = min_u + i;

                // rect
                // s1 = min_u + i + 0.5f;
                // s2 = min_u + i - 0.5f;
                // f1 = fmaxf(fminf(s1,us[1]) - fmaxf(s2,us[0]),0);
                // f1 = fmaxf(fminf(s1,us[3]) - fmaxf(s2,us[0]),0);

                // trap
                s1 = min_u + i + 0.5f;
                s2 = min_u + i - 0.5f;
                gamma[0] = 0.0f;
                gamma_trap(us, s2, s1, gamma);
                // f1 = gamma[0];
                f1 = gamma[0];
                
                for (j = 0; j < max_v - min_v + 1; j++) {
                 
                    idxv = min_v + j;

                    // rect
                    s1 = min_v + j + 0.5f;
                    s2 = min_v + j - 0.5f;
                    f2 = C*f1*fmaxf(fminf(s1,vs[1]) - fmaxf(s2,vs[0]),0);
                    
                    // trap
                    // s1 = min_v + j + 0.5f;
                    // s2 = min_v + j - 0.5f;
                    // gamma[0] = 0.0f;
                    // gamma_trap(vs, s2, s1, gamma);
                    // f2 = gamma[0];
                    
                    idxv = idxv*nu + idxu;
                    
                    // check to make sure we have not gone out of bounds before updating the projection
                    // if (idxu < BUFFER*nuv && idxu >= 0) {
                    if (idxv < nuv && idxv >= 0 && f2 == f2) {

                        // proj[idxv] += C*F1[i]*F2[j]; // will fail due to simultaneous read/write operations between threads
                        // atomicAdd(proj+idxv,f2); // *C); // atomic add addresses this problem
                        
                        // Add to double precision projection data to avoid random rounding
                        
                        #if __CUDA_ARCH__ < 600
                        
                            atomicAdd_old(proj+idxv,f2);
                        
                        #else
                        
                            atomicAdd(proj+idxv,f2);
                        
                        #endif
                        

                    }

                }
                
            }
    
    }

}

#endif // DD_PROJECT_v25_FLAT
