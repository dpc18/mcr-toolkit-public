// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef DD_MACROS

    #define DD_MACROS

    // maximum number of simultaneously allocated reconstruction parameter sets (governs both host and device allocations)

        #define MAX_RECONS 10
    
    // number of streams to use for computation on each GPU (more streams = more memory required for buffers on each GPU)
        
        #define NUMSTREAMS 2

    // maximum number of open MP threads (needed threads = (device count) * NUMSTREAMS )

        #define THREAD_COUNT 8

    // buffer sizes for handling volume and projection data on the GPU
    // TO DO: Consider making these user-defined variables for better performance on sprase / clinical reconstruction problems.

        #define VOL_CHUNK     128 // 64                 // z planes per chunk
        #define PROJ_CHUNK    256 // 30                 // projs per chunk

    #define pi_val 3.141592653589793f

    // square of number
    #define sq( a ) ( (a) * (a) )

    // find maximum of two numbers
    #ifndef max
    #define max( a, b ) ( ((a) > (b)) ? (a) : (b) )
    #endif

    // find minimum of two numbers
    #ifndef min
    #define min( a, b ) ( ((a) < (b)) ? (a) : (b) )
    #endif

#endif //  DD_MACROS