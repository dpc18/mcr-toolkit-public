// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// v3: Modified to handle patch lengths up to 8192 voxels.

#ifndef OMP_SHARED

    #define OMP_SHARED

    #define BLOCK_SZ 64
    #define MAX_ATOMS 96 // 64 // 44 // Hard-wired atom limit (<= 44) // OMP_4D_p assumes this is set to 44...
    // #define NUMSTREAMS 3 // number of streams to use to overlap kernels and memcpys
    // #define ZSTEP 8 // number of z indices to do per backprojection kernel call to reduce overhead

    // ***           ***
    // *** FUNCTIONS ***
    // ***           ***

        __device__ __host__ unsigned int lindx(int3 coord, int3 vsize) {return vsize.x*vsize.y*coord.z + vsize.x*coord.y + coord.x;}; // return linear index
        __device__ __forceinline__ unsigned int ltidx(int col, int row) { return (row*row+row)/2 + col; }; // return compressed lower triangular index

        __global__ void abs_max(const float* __restrict__ A, unsigned int *idx, const int col_len);
        __global__ void update_L(float *L, const float* __restrict__ G, const unsigned int* indices, const int atoms, unsigned int szxy, int na, const float* __restrict__ eps, const float err, const unsigned int realLsz);
        __global__ void update_Gamma(float *Gamma_, const float* __restrict__ eps, const float* __restrict__ L, const unsigned int* __restrict__ indices, const float* __restrict__ A00, const float err, const unsigned int szxy, const int atoms, const int na, const unsigned int realLsz);
        __global__ void update_Beta_v4(const float* __restrict__ G, const float* __restrict__ Gamma_, float* __restrict__ B, const unsigned int* __restrict__ indices, float* __restrict__ A0, float* __restrict__ A00, const int na, const unsigned int szxy, const int atoms, const float* __restrict__ eps, const float err);
        // __global__ void update_Xp_v2(float* __restrict__ Xp, const float* __restrict__ D, const float* __restrict__ Gamma, const unsigned int* __restrict__ indices, const int drows, const int szxy);
        __global__ void update_Xp_v3(float* __restrict__ Xp, const float* __restrict__ D, const float* __restrict__ Gamma, const unsigned int* __restrict__ indices, const int drows, const int szxy, const int* __restrict__ idx_rng);
        __global__ void init_eps(const float* __restrict__ Xp, float* __restrict__ eps, const int drows, const int drows_padded);
        __global__ void update_delta(const float* __restrict__ B, const float* __restrict__ Gamma, const unsigned int* __restrict__ indices, float* __restrict__ delta, const int na, const int atoms1);
        __global__ void update_eps(float* __restrict__ eps_, const float* __restrict__ delta_, float* __restrict__ delta_old_, const unsigned int szxy);
        __global__ void set_float(float* __restrict__ L_, unsigned int stride, float val);

    // ***               ***
    // *** DEVICE - MAIN ***
    // ***               ***

    // Compute the index of the maximum absolute value in each column of A using multiple threads.
    // Requirements:
    // col_len/BLOCK_SZ is an integer
    // na*4 bytes of shared memory
    // # of cols of A = number of blocks in the grid along x
    // TO DO: Add intermediate aggregation steps?
    __global__ void abs_max(const float* __restrict__ A, unsigned int *idx, const int col_len) {

        // indices
        // unsigned int chunck_num = col_len / BLOCK_SZ;
        unsigned int bid = blockIdx.x; // column of A to work with
        unsigned int tid = threadIdx.x; // assigned block within A
        unsigned int aoff = bid*col_len+tid;

        // shared memory
        extern __shared__ float data[]; // na x 1 data vector

        // reuse the shared memory once we are done with A
        float* indexes = data; // index of max value from each chunk
        float* maxes = indexes + blockDim.x; 

        // coalesced global memory reads to shared memory
        for (int i = 0; i < col_len; i+=BLOCK_SZ) {

            data[tid+i] = A[aoff+i];

        }

        // wait until all data is copied
        __syncthreads();

        // find abs max for each chunk of BLOCK_SZ values
        float val = 0;
        float old_val = data[tid];
        float abs_old_val = fabsf(old_val);
        unsigned int id = tid;
        for (int i = 0; i < col_len; i+=BLOCK_SZ) {

            val = data[i+tid];

            if (fabsf(val) > abs_old_val) {

                old_val = val;
                abs_old_val = fabsf(val);
                id = i+tid;

            }

        }

        // store in shared memory for aggregation
        indexes[tid] = id;
        maxes[tid] = old_val;

        // wait until all elements are filled
        __syncthreads();

        // Aggregate results across threads

        if (tid == 0) {

            float val = 0;
            float old_val = maxes[0];
            float abs_old_val = fabsf(old_val);
            float id = indexes[0];

            for (int i = 1; i < blockDim.x; i++) {

                val = maxes[i];

                if (fabsf(val) > abs_old_val) {

                    old_val = val;
                    abs_old_val = fabsf(val);
                    id = indexes[i];

                }

            }

            // max[blockIdx.x] = old_val;
            idx[blockIdx.x] = id;

        }

    }

    // gsL = dim3(sz.x*sz.y,1,1);
    // bsL = dim3(Lsz,1,1);
    // TO DO: How do I make the inner loop faster and/or replace this function entirely?
    __global__ void update_L(float *L, const float* __restrict__ G, const unsigned int* indices, const int atoms, unsigned int szxy, int na, const float* __restrict__ eps, const float err, const unsigned int realLsz) {

        // Check if we should stop early
        if (eps[blockIdx.x] <= err) return;

        extern __shared__ float z[]; 
        float *Ls = z + atoms; // compressed atoms x atoms lower triangular matrix

        unsigned int tid = threadIdx.x;
        unsigned int Loffset = blockIdx.x*(MAX_ATOMS*MAX_ATOMS + MAX_ATOMS)/2;
        unsigned int Ioffset = blockIdx.x;

        float val;
        int i, j;
        unsigned int pos = indices[Ioffset+atoms*szxy];

        // Read L into shared memory
        // Loop in case we have exceeded the thread limit.
        for (i = 0; i < realLsz; i += blockDim.x) {

            if (tid + i < realLsz) {

                Ls[tid + i] = L[Loffset + tid + i];

            }

        }

        // Wait for all data to finish copying to shared memory
        __syncthreads();

        if (tid == 0) {

            // Solve for w: L*z = G_I,k
            for (i = 0; i < atoms; i++) {

                z[i] = G[indices[Ioffset+i*szxy]*na + pos];

                for (j = 0; j < i; j++) {

                    // z[i] = z[i] - Ls[i*MAX_ATOMS + j]*z[j];
                    z[i] -= Ls[ltidx(j, i)]*z[j];

                }

                // val = Ls[i*MAX_ATOMS + i];
                val = Ls[ltidx(j, i)];
                // if ( val > 0 ) {

                //    z[i] /= val;

                //}
                
                z[i] /= (val + 1e-8);

            }

            val = 0;
            for (i = 0; i < atoms; i++) {

                // Ls[h*MAX_ATOMS + i] = z[i];
                Ls[ltidx(i, atoms)] = z[i];
                val += z[i]*z[i];

            }

            val = 1 - val;

            Ls[ltidx(atoms, atoms)] = sqrtf(fmaxf(val,0));

        }

        // Wait on thread 0...ugh!
        __syncthreads();

        // Write back to global memory
        // Loop in case we have exceeded the thread limit.
        for (i = 0; i < realLsz; i += blockDim.x) {

            if (tid + i < realLsz) {

                L[Loffset + tid + i] = Ls[tid + i];

            }

        }

    }

    // Gamma := Solve for c: L*L'*c = A00_I
    // gsL = dim3(sz.x*sz.y,1,1);
    // bsL = dim3(Lsz,1,1);
    // TO DO: How do I make the inner loop faster and/or replace this function entirely?
    // update_Gamma<<<gsL, bsL, (Lsz + 2*(atoms + 1))*sizeof(float)>>>(Gamma_, L_, indices_+atoms*szxy, A00_, atoms);
    __global__ void update_Gamma(float *Gamma_, const float* __restrict__ eps, const float* __restrict__ L, const unsigned int* __restrict__ indices, const float* __restrict__ A00, const float err, const unsigned int szxy, const int atoms, const int na, const unsigned int realLsz) {

        // Check if we should stop early
        if (eps[blockIdx.x] <= err) return;

        extern __shared__ float z[];
        float *gamma = z + atoms + 1;
        float *Ls = gamma + atoms + 1; // compressed atoms x atoms lower triangular matrix

        unsigned int tid = threadIdx.x;
        unsigned int Loffset = blockIdx.x*(MAX_ATOMS*MAX_ATOMS + MAX_ATOMS)/2;
        unsigned int Goffset = blockIdx.x*MAX_ATOMS;
        unsigned int Ioffset = blockIdx.x;
        unsigned int Aoff = blockIdx.x*na;

        float val;
        int i, j;

        // Read L, gamma into shared memory
        // Loop in case we have exceeded the thread limit.
        for (i = 0; i < realLsz; i += blockDim.x) {

            if (tid + i < realLsz) {

                Ls[tid + i] = L[Loffset + tid + i];

            }

        }

        for (i = 0; i < atoms + 1; i += blockDim.x) {

            if (tid + i < atoms + 1) {

                gamma[tid + i] = Gamma_[Goffset + tid + i];

            }

        }

        // Wait for all data to finish copying to shared memory
        __syncthreads();

        if (tid == 0) {

            // z = L \ a00 (forward substitution)
            for (i = 0; i < (atoms+1); i++) {

                z[i] = A00[Aoff + indices[Ioffset+i*szxy]];

                for (j = 0; j < i; j++) {

                    // z[i] -= Ls[i*MAX_ATOMS+j]*z[j];
                    z[i] -= Ls[ltidx(j, i)]*z[j];

                }

                // val = Ls[i*MAX_ATOMS + i];
                val = Ls[ltidx(i, i)];
                // if ( val > 0 ) {

                //    z[i] /= val;

                // }
                
                z[i] /= (val + 1e-8);

            }

            // gamma = L' \ z (back substitution)
            for (i = atoms; i > -1; i--) {

                gamma[i] = z[i];

                for (j = i+1; j < (atoms+1); j++) {

                    // gamma[i] -= Ls[j*MAX_ATOMS+i]*gamma[j];
                    gamma[i] -= Ls[ltidx(i, j)]*gamma[j];

                }

                // val = Ls[i*MAX_ATOMS + i];
                val = Ls[ltidx(i, i)];
                // if ( val > 0 ) {

                //    gamma[i] /= val;

                // }
                
                gamma[i] /= (val + 1e-8);

            }

        }

        // Wait on thread 0...ugh!
        __syncthreads();

        // Write back to global memory
        // Loop in case we have exceeded the thread limit.
        for (i = 0; i < atoms + 1; i += blockDim.x) {

            if (tid + i < atoms + 1) {

                Gamma_[Goffset + tid + i] = gamma[tid + i];

            }

        }

    }

    // gsB = dim3(szxy,1,1);
    // bsB = dim3(BLOCK_SZ,1,1);
    // Also update A0...
    // ILP did not pay off here, presumably because L1 cache has little value
    __global__ void update_Beta_v4(const float* __restrict__ G, const float* __restrict__ Gamma_, float* __restrict__ B, const unsigned int* __restrict__ indices, float* __restrict__ A0, float* __restrict__ A00, const int na, const unsigned int szxy, const int atoms, const float* __restrict__ eps, const float err) {

        // Check if we should stop early
        if (eps[blockIdx.x] <= err) return;

        unsigned int tid = threadIdx.x;
        unsigned int Ioffset = blockIdx.x;
        unsigned int Goffset = blockIdx.x*MAX_ATOMS;
        unsigned int boff = blockIdx.x*na;

        float coeff, betac;
        unsigned int idx;

        extern __shared__ float beta[];

        for (int j = 0; j < na; j+=blockDim.x) {

            // B[boff+j+tid] = 0;
            beta[tid + j] = 0.0f;

        }

        for (int i = 0; i < atoms+1; i++) { // for each index / gamma coefficient

            coeff = Gamma_[Goffset+i];
            idx = indices[Ioffset+i*szxy]*na;

            for (int j = 0; j < na; j+=blockDim.x) {

                // B[boff+j+tid] += G[idx+j+tid] * coeff;
                beta[tid + j] += G[idx+j+tid] * coeff;

            }

        }

        for (int j = 0; j < na; j+=blockDim.x) {

            betac = beta[tid + j];
            A0[boff+j+tid] = A00[boff+j+tid] - betac;
            B[boff+j+tid] = betac;

        }

    }

    // gsB = dim3(szxy,1,1);
    // bsB = dim3(drows,1,1);
//     __global__ void update_Xp_v2(float* __restrict__ Xp, const float* __restrict__ D, const float* __restrict__ Gamma, const unsigned int* __restrict__ indices, const int drows, const int szxy) {
// 
//         int tid = threadIdx.x; // linear offset in 3D
//         int x = blockIdx.x; // x position
//         unsigned int Xpoff = x*drows;
//         unsigned int Ioffset = x;
//         unsigned int Goffset = x*MAX_ATOMS;
//         int idx;
// 
//         Xp[Xpoff + tid] = 0;
// 
//         for (int i = 0; i < MAX_ATOMS; i++) { // for each index / gamma coefficient
// 
//             idx = indices[Ioffset+i*szxy];
// 
//             Xp[Xpoff + tid] += D[drows*idx + tid] * Gamma[Goffset+i];
// 
//         }
// 
//     }
    
    __global__ void update_Xp_v3(float* __restrict__ Xp, const float* __restrict__ D, const float* __restrict__ Gamma, const unsigned int* __restrict__ indices, const int drows, const int szxy, const int* __restrict__ idx_rng) {

        // int tid = threadIdx.x; // linear offset in 3D
        int x = blockIdx.x; // x position
        unsigned int Xpoff = x*drows;
        unsigned int Ioffset = x;
        unsigned int Goffset = x*MAX_ATOMS;
        int idx, idx0, idx1;
        
        idx0 = idx_rng[0];
        idx1 = idx_rng[1];
        
        // Handle patches larger than 1024
        for (int tid = threadIdx.x; tid < drows; tid += 1024) {

            Xp[Xpoff + tid] = 0;

            for (int i = 0; i < MAX_ATOMS; i++) { // for each index / gamma coefficient

                idx = indices[Ioffset+i*szxy];

                if ((idx >= idx0) && (idx <= idx1)) {

                    Xp[Xpoff + tid] += D[drows*idx + tid] * Gamma[Goffset+i];

                }

            }
        
        }

    }

    // Batched inner product computation
    // gsB = dim3(szxy,1,1);
    // bsB = dim3( min(drows_padded/2, 1024) ,1,1); // Unroll the first reduction operation to keep more threads busy.
    // temp has drows padded to next power of 2 elements
    __global__ void init_eps(const float* __restrict__ Xp, float* __restrict__ eps, const int drows, const int drows_padded) {

        int tid = threadIdx.x;
        
        // min(drows_padded,2048)*sizeof(float)
        extern __shared__ float temp[];
        
        // zero-out eps before we add to it
        if (threadIdx.x == 0) {
         
            eps[blockIdx.x] = 0;
            
        }
        
        int os1 = tid + blockDim.x;
        
        // For each chunk of 2048
        // Must keep extra threads alive between drows and drows_padded to maintain powers of two.
        for (int offsetX = threadIdx.x; offsetX < drows_padded; offsetX += 2048) {
        
            int os2 = blockIdx.x*drows + offsetX;

            // transform: temp = Xp[i].*Xp[i]

                // Do the first half
                if (os2 < blockIdx.x*drows + drows) {
            
                    temp[tid] = Xp[os2];
                    temp[tid] *= temp[tid];
                
                }
                else { // zero out padding threads beyond drows

                    temp[tid] = 0.0f;

                }

                // Do the second half
                // tid < drows - blockDim.x && 
                if ( os2 + blockDim.x < blockIdx.x*drows + drows ) { // only for threads with valid indices

                    temp[os1] = Xp[os2 + blockDim.x];
                    temp[os1] *= temp[os1];

                }
                else { // zero out padding threads beyond drows

                    temp[os1] = 0.0f;

                }

            // reduce: eps[i] = sum(temp)

                // Do the first reduction operation before calling syncthreads (since the corresponding elements were handled by the same thread)

                temp[tid] += temp[os1];

                __syncthreads();

                for (unsigned int i = blockDim.x/2; i > 1; i>>=1) { // save the last operation for the output to do one less syncthreads

                    if (tid < i) {

                        temp[tid] += temp[tid + i];

                    }

                    __syncthreads();

                }

                if (threadIdx.x == 0) {

                    eps[blockIdx.x] += temp[0] + temp[1];

                }
            
        }

    }

    // gsB = dim3(szxy,1,1);
    // bsB = dim3(padded_atoms1,1,1);
    // shared: padded_atoms1*sizeof(float)
    __global__ void update_delta(const float* __restrict__ B, const float* __restrict__ Gamma, const unsigned int* __restrict__ indices, float* __restrict__ delta, const int na, const int atoms1) {

        int tid = threadIdx.x;
        unsigned int Goff = MAX_ATOMS*blockIdx.x;
        unsigned int Boff = na*blockIdx.x;

        extern __shared__ float temp[];

        // delta = Gamma'_I * Beta_I

            if (tid < atoms1) { // only for threads with valid incides

                // compute product on each thread
                temp[tid] = Gamma[Goff + tid] * B[Boff + indices[blockIdx.x + gridDim.x*tid]];

            }
            else { // zero out padding threads beyond atoms1

                temp[tid] = 0.0f;

            }

            __syncthreads();

        // reduce (No initial reduction and /2 done here given that this will be applied when there is only 1 atom)

            // further reduction

            for (unsigned int i = blockDim.x/2; i > 0; i>>=1) { // save the last operation for the output to do one less syncthreads

                if (tid < i) {

                    temp[tid] += temp[tid + i];

                }

                __syncthreads();

            }

        // output to global memory

            if (tid == 0) {

                delta[blockIdx.x] = temp[0]; // final reduction

            }

    }

    // dim3((szxy+BLOCK_SZ-1)/BLOCK_SZ,1,1)
    // dim3(BLOCK_SZ,1,1)
    // update_eps_v2<<<dim3((szxy+BLOCK_SZ-1)/BLOCK_SZ,1,1),dim3(BLOCK_SZ,1,1)>>>(eps_, delta_, delta_old_, szxy);
    __global__ void update_eps(float* __restrict__ eps_, const float* __restrict__ delta_, float* __restrict__ delta_old_, const unsigned int szxy) {

        int tid = blockIdx.x*blockDim.x + threadIdx.x;

        if (tid > szxy) return;

        eps_[tid] = eps_[tid] - delta_[tid] + delta_old_[tid];

        delta_old_[tid] = delta_[tid];

    }

    // dim3(szxy,1,1)
    // dim3(1,1,1)
    __global__ void set_float(float* __restrict__ L_, unsigned int stride, float val) {

        L_[blockIdx.x*stride] = val;

    }

#endif // OMP_SHARED