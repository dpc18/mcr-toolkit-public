// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef DD_PROJECT_v25
#define DD_PROJECT_v25

// ***           ***
// *** FUNCTIONS ***
// ***           ***
    
    // NOTE: Cylindrical operators use rect approximation along v and the trap approximation along u.
    //       Flat operators use rect approximation along v and the trap approximation along u.

    /* Supported Geometries and GPU Kernels */

        // NOTE: Cylindrical operators use rect approximation along v and the trap approximation along u.
        //       Flat operators use rect approximation along v and the trap approximation along u.

        // cy_detector = 0) flat-panel
        // cy_detector = 3) rebinned, flat
        #include <DD_v25_project_flat.h>

        // cy_detector = 1) cylindrical
        // cy_detector = 2) rebinned cylindrical
        #include <DD_v25_project_cylindrical.h>

        // cy_detector = 4) array of parallel beams (synchrotron geometry)
        #include <DD_v25_project_parallel.h>


    // Call function

        void DD_project_cuda( Public_GPU_Data *GPUpub, Private_GPU_Data *GPUpri, int proj_start, int proj_end, int z_offset, int z_size, cudaStream_t stream );


// ***      ***
// *** HOST ***
// ***      ***

void DD_project_cuda( Public_GPU_Data *GPUpub, Private_GPU_Data *GPUpri, int proj_start, int proj_end, int z_offset, int z_size, cudaStream_t stream )
{
    
    // NOTE: This will cause problems trying to use nz in geometry calculations!
    // New nz0 parameter added to projection kernels for calculations which require the full volume size (e.g. geo weighting, angular range limitations).
    // nz0 = rp->nz
    int3 n3xyzc = make_int3( rp->nx, rp->ny, z_size );
    
    float rect_rect_factor;
    size_t os, os2;
    
    os2 = z_offset*rp->nxy;
    
    for ( int p = proj_start; p < proj_end; p++ ) { // projections
        
        // ignore this projection if it has zero weight
        if ( weights_sub[p] == 0 ) continue;
        
        // set double precision projection buffer to zero
        cudaMemsetAsync( GPUpri[0].projs_temp_, 0, (rp->proj_size)*2, stream);
        
        os  = p*rp->nuv;

        if ( rp->cy_detector == 1 ) {  // cylindrical detector

            // ASSUMES dy = dx (same for both rect and trap functions)
            rect_rect_factor = ( rp->lsds[p] * rp->lsds[p] ) * ( (rp->d3xyz.y * rp->d3xyz.z)  / (rp->du * rp->dv) );
            
            DD_project_cyl<<<rp->vgrid, rp->vblock, 0, stream>>>( GPUpri[0].projs_temp_, &(GPUpub[0].vol_[os2]), n3xyzc, rp->nz, rp->d3xyz, rp->nu, rp->nv,
                                                                  rect_rect_factor, make_float3( rp->srcs[ p * 3 + 0 ], rp->srcs[ p * 3 + 1 ], rp->srcs[ p * 3 + 2 ] ), rp->du, rp->uc, rp->vc,
                                                                  rp->lsds[ p ], rp->lsos[ p ], rp->db, GPUpub[0].rmask_header_,
                                                                  &(GPUpub[0].rmask_[os2]), rp->implicit_mask, rp->explicit_mask, z_offset, rp->dv,
                                                                  rp->angles[p], rp->dtvs[ p * 3 + 2 ]);

        }
        else if ( rp->cy_detector == 2 ) {  // cylindrical detector, rebinned
            
            // ASSUMES dy = dx (same for both rect and trap functions)
            rect_rect_factor = ( rp->lsds[p] ) * (rp->d3xyz.y * rp->d3xyz.z)  / (rp->du * rp->dv);
            
            DD_project_cylb<<<rp->vgrid, rp->vblock, 0, stream>>>( GPUpri[0].projs_temp_, &(GPUpub[0].vol_[os2]), n3xyzc, rp->nz, rp->d3xyz, rp->nu, rp->nv,
                                                                  rect_rect_factor, make_float3( rp->srcs[ p * 3 + 0 ], rp->srcs[ p * 3 + 1 ], rp->srcs[ p * 3 + 2 ] ), rp->du, rp->uc, rp->vc,
                                                                  rp->lsds[ p ], rp->lsos[ p ], rp->db, GPUpub[0].rmask_header_,
                                                                  &(GPUpub[0].rmask_[os2]), rp->implicit_mask, rp->explicit_mask, z_offset, rp->dv,
                                                                  rp->angles[p], rp->dtvs[ p * 3 + 2 ], make_float3( rp->puvs[(p*3)+0], rp->puvs[(p*3)+1], rp->puvs[(p*3)+2] ) );
            
        }
        else if ( rp->cy_detector == 3 ) {  // flat-panel detector, rebinned

            // ASSUMES dy = dx (same for both rect and trap functions)
            rect_rect_factor = ( rp->lsds[p] ) * (rp->d3xyz.y * rp->d3xyz.z)  / (rp->du * rp->dv);
         
            DD_project_flatb<<<rp->vgrid, rp->vblock, 0, stream>>>( GPUpri[0].projs_temp_, &(GPUpub[0].vol_[os2]), n3xyzc, rp->nz, rp->d3xyz, rp->nu, rp->nv,
                                                                    GPUpub[0].aff_, GPUpub[0].pm_ + p*12, rect_rect_factor, make_float3( rp->srcs[ p * 3 + 0 ], rp->srcs[ p * 3 + 1 ], rp->srcs[ p * 3 + 2 ] ),
                                                                    rp->du, rp->uc, rp->vc, rp->lsds[ p ],
                                                                    rp->use_affine, GPUpub[0].rmask_header_, &(GPUpub[0].rmask_[os2]),
                                                                    rp->implicit_mask, rp->explicit_mask, z_offset,
                                                                    make_float3( rp->puvs[(p*3)+0], rp->puvs[(p*3)+1], rp->puvs[(p*3)+2] ) );
            
        }
        else if ( rp->cy_detector == 4 ) {  // array of parallel beams (synchrotron geometry)

            // ASSUMES dy = dx (same for both rect and trap functions)
            rect_rect_factor = (rp->d3xyz.y * rp->d3xyz.z)  / (rp->du * rp->dv); // ( rp->lsds[p] ) * 

            DD_project_parallel<<<rp->vgrid, rp->vblock, 0, stream>>>( GPUpri[0].projs_temp_, &(GPUpub[0].vol_[os2]), n3xyzc, rp->nz, rp->d3xyz, rp->nu, rp->nv,
                                                                       GPUpub[0].aff_, GPUpub[0].pm_ + p*12, rect_rect_factor, make_float3( rp->srcs[ p * 3 + 0 ], rp->srcs[ p * 3 + 1 ], rp->srcs[ p * 3 + 2 ] ),
                                                                       rp->du, rp->dv, rp->uc, rp->vc, rp->lsds[ p ],
                                                                       rp->use_affine, GPUpub[0].rmask_header_, &(GPUpub[0].rmask_[os2]),
                                                                       rp->implicit_mask, rp->explicit_mask, z_offset,
                                                                       make_float3( rp->puvs[(p*3)+0], rp->puvs[(p*3)+1], rp->puvs[(p*3)+2] ),
                                                                       make_float3( rp->pvvs[(p*3)+0], rp->pvvs[(p*3)+1], rp->pvvs[(p*3)+2] ));

        }
        else { // flat-panel detector
            
            // ASSUMES dy = dx (same for both rect and trap functions)
            rect_rect_factor = ( rp->lsds[p] * rp->lsds[p] ) * ( (rp->d3xyz.y * rp->d3xyz.z)  / (rp->du * rp->dv) );

            DD_project_flat<<<rp->vgrid, rp->vblock, 0, stream>>>( GPUpri[0].projs_temp_, &(GPUpub[0].vol_[os2]), n3xyzc, rp->nz, rp->d3xyz, rp->nu, rp->nv,
                                                                   GPUpub[0].aff_, GPUpub[0].pm_ + p*12, rect_rect_factor, make_float3( rp->srcs[ p * 3 + 0 ], rp->srcs[ p * 3 + 1 ], rp->srcs[ p * 3 + 2 ] ),
                                                                   rp->use_affine, GPUpub[0].rmask_header_, &(GPUpub[0].rmask_[os2]),
                                                                   rp->implicit_mask, rp->explicit_mask, z_offset);

        }
        
        // Add double precision results to single precision buffer
        os = p*rp->nuv;
        add_dp_to_sp<<<rp->pgrid, rp->pblock, 0, stream>>>( &(GPUpub[0].proj_[os]), GPUpri[0].projs_temp_, rp->nu, rp->nv);
    
    }
    
}

#endif // DD_PROJECT_v25
