// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef DD_v25_BACKPROJECT
#define DD_v25_BACKPROJECT

#include <math.h>

/* Supported Geometries and GPU Kernels */

    // NOTE: Cylindrical operators use rect approximation along v and the trap approximation along u.
    //       Flat operators use rect approximation along v and the trap approximation along u.


    // cy_detector = 0) flat-panel
    // cy_detector = 3) rebinned, flat
    #include <DD_v25_backproject_flat.h>


    // cy_detector = 1) cylindrical
    // cy_detector = 2) rebinned cylindrical
    // cy_detector = 5) rebinned cylindrical, more accurate temporal weighting of rebinned data (work in progress)
    #include <DD_v25_backproject_cylindrical.h>


    // cy_detector = 4) array of parallel beams (synchrotron geometry)
    #include <DD_v25_backproject_parallel.h>


// ***      ***
// *** HOST ***
// ***      ***

void DD_backproject_cuda( Public_GPU_Data *GPUpub, Private_GPU_Data *GPUpri, int proj_start, int proj_end, int z_offset, int z_size, cudaStream_t stream, int per_filtration)
{
    
    // NOTE: This will cause problems trying to use nz in geometry calculations!
    // New nz0 parameter added to backprojection kernels for calculations which require the full volume size (e.g. geo weighting, angular range limitations).
    // nz0 = rp->nz
    int3 n3xyzc = make_int3( rp->nx, rp->ny, z_size );
    
    // Weighting factors
    
        float scale_factor = rp->dx * pi_val * rp->scale; // / np_eff_sub;
        
        if ( rp->use_norm_vol == 0 ) {
         
            scale_factor /= np_eff_sub;
            
        }

        if ( ( rp->filtration ) && ( per_filtration ) ) {

            scale_factor /= rp->nu_fft;

        }
        
    // Loop over projections
        
        size_t os, os2;
        float rect_rect_factor;
        
        os2 = z_offset*rp->nxy;

        for (int p = proj_start; p < proj_end; p++) { // for each projection

            if ( weights_sub[p] == 0 && rp->cy_detector != 5 ) continue;

            os  = p*rp->nuv;

            // rect_rect_factor = ( rp->lsds[p] ) * weights_sub[p] * scale_factor;

            if ( rp->cy_detector == 1 ) // cylindrical detector
            {
                
                rect_rect_factor = ( rp->lsds[p] ) * weights_sub[p] * scale_factor;

                DD_backproject_cyl<<<rp->vgrid, rp->vblock, 0, stream>>>( &(GPUpub[0].proj_[os]), &(GPUpub[0].vol_[os2]), n3xyzc, rp->nz, rp->d3xyz, rp->nu, rp->nv,
                                                                          rect_rect_factor, make_float3( rp->srcs[(p*3)+0], rp->srcs[(p*3)+1], rp->srcs[(p*3)+2] ), rp->du, rp->uc, rp->vc,
                                                                          rp->lsds[ p ], rp->lsos[ p ], rp->db, GPUpub[0].rmask_header_,
                                                                          &(GPUpub[0].rmask_[os2]), rp->implicit_mask, rp->explicit_mask, z_offset, rp->dv,
                                                                          rp->limit_angular_range, rp->use_norm_vol, &(GPUpub[0].norm_vol_[os2]), weights_sub[p],
                                                                          rp->angles[p], rp->dtvs[ p * 3 + 2 ] );


            }
            else if ( rp->cy_detector == 2 ) // cylindrical detector, rebinned
            {
                
                rect_rect_factor = weights_sub[p] * scale_factor;

                DD_backproject_cylb<<<rp->vgrid, rp->vblock, 0, stream>>>( &(GPUpub[0].proj_[os]), &(GPUpub[0].vol_[os2]), n3xyzc, rp->nz, rp->d3xyz, rp->nu, rp->nv,
                                                                           rect_rect_factor, make_float3( rp->srcs[(p*3)+0], rp->srcs[(p*3)+1], rp->srcs[(p*3)+2] ), rp->du, rp->uc, rp->vc,
                                                                           rp->lsds[ p ], rp->lsos[ p ], rp->db, GPUpub[0].rmask_header_,
                                                                           &(GPUpub[0].rmask_[os2]), rp->implicit_mask, rp->explicit_mask, z_offset, rp->dv,
                                                                           rp->limit_angular_range, rp->use_norm_vol, &(GPUpub[0].norm_vol_[os2]), weights_sub[p],
                                                                           rp->angles[p], rp->dtvs[ p * 3 + 2 ], make_float3( rp->puvs[(p*3)+0], rp->puvs[(p*3)+1], rp->puvs[(p*3)+2] ) );


            }
            else if ( rp->cy_detector == 3 ) // flat-panel detector, rebinned
            {
                
                rect_rect_factor = weights_sub[p] * scale_factor;

                DD_backproject_flatb<<<rp->vgrid, rp->vblock, 0, stream>>>(&(GPUpub[0].proj_[os]), &(GPUpub[0].vol_[os2]), n3xyzc, rp->nz, rp->d3xyz, rp->nu, rp->nv,
                                                                           GPUpub[0].aff_, GPUpub[0].pm_ + p*12, rect_rect_factor, make_float3( rp->srcs[(p*3)+0], rp->srcs[(p*3)+1], rp->srcs[(p*3)+2] ),
                                                                           rp->du, rp->uc, rp->vc, rp->lsds[ p ],
                                                                           rp->use_affine, GPUpub[0].rmask_header_, &(GPUpub[0].rmask_[os2]), rp->implicit_mask, rp->explicit_mask, z_offset,
                                                                           rp->dv, rp->limit_angular_range, rp->use_norm_vol, &(GPUpub[0].norm_vol_[os2]), 
                                                                           make_float3( rp->puvs[(p*3)+0], rp->puvs[(p*3)+1], rp->puvs[(p*3)+2] ), weights_sub[p] );


            }
            else if ( rp->cy_detector == 4 ) // array of parallel beams (synchrotron geometry)
            {

                rect_rect_factor = weights_sub[p] * scale_factor;

                DD_backproject_parallel<<<rp->vgrid, rp->vblock, 0, stream>>>(&(GPUpub[0].proj_[os]), &(GPUpub[0].vol_[os2]), n3xyzc, rp->nz, rp->d3xyz, rp->nu, rp->nv,
                                                                              GPUpub[0].aff_, GPUpub[0].pm_ + p*12, rect_rect_factor, make_float3( rp->srcs[(p*3)+0], rp->srcs[(p*3)+1], rp->srcs[(p*3)+2] ),
                                                                              rp->du, rp->uc, rp->vc, rp->lsds[ p ],
                                                                              rp->use_affine, GPUpub[0].rmask_header_, &(GPUpub[0].rmask_[os2]), rp->implicit_mask, rp->explicit_mask, z_offset,
                                                                              rp->dv, rp->limit_angular_range, rp->use_norm_vol, &(GPUpub[0].norm_vol_[os2]),
                                                                              make_float3( rp->puvs[(p*3)+0], rp->puvs[(p*3)+1], rp->puvs[(p*3)+2] ),
                                                                              make_float3( rp->pvvs[(p*3)+0], rp->pvvs[(p*3)+1], rp->pvvs[(p*3)+2] ), weights_sub[p]);


            }
            else if ( rp->cy_detector == 5 ) // rebinned cylindrical, more accurate temporal weighting of rebinned data (work in progress)
            {

                // Compute the source projection to assign the correct time to each rebinned detector pixel
                // Directly incorporate Gaussian weight computations to avoid discretization
                rect_rect_factor = scale_factor;

                DD_backproject_cylbt<<<rp->vgrid, rp->vblock, 0, stream>>>(&(GPUpub[0].proj_[os]), &(GPUpub[0].vol_[os2]), n3xyzc, rp->nz, rp->d3xyz, rp->nu, rp->nv,
                                                                           GPUpub[0].aff_, GPUpub[0].pm_ + p*12, rect_rect_factor, make_float3( rp->srcs[(p*3)+0], rp->srcs[(p*3)+1], rp->srcs[(p*3)+2] ),
                                                                           rp->du, rp->uc, rp->vc, rp->lsds[ p ], rp->db,
                                                                           rp->use_affine, GPUpub[0].rmask_header_, &(GPUpub[0].rmask_[os2]), rp->implicit_mask, rp->explicit_mask, z_offset,
                                                                           rp->dv, rp->limit_angular_range, rp->use_norm_vol, &(GPUpub[0].norm_vol_[os2]), 
                                                                           make_float3( rp->puvs[(p*3)+0], rp->puvs[(p*3)+1], rp->puvs[(p*3)+2] ), GPUpub[0].proj_weights_d, p );



            }
            else // flat-panel detector
            {
                
                rect_rect_factor = ( rp->lsds[p] ) * weights_sub[p] * scale_factor;
                // rect_rect_factor = weights_sub[p] * scale_factor;

                DD_backproject_flat<<<rp->vgrid, rp->vblock, 0, stream>>>(&(GPUpub[0].proj_[os]), &(GPUpub[0].vol_[os2]), n3xyzc, rp->nz, rp->d3xyz, rp->nu, rp->nv,
                                                                          GPUpub[0].aff_, GPUpub[0].pm_ + p*12, rect_rect_factor, make_float3( rp->srcs[(p*3)+0], rp->srcs[(p*3)+1], rp->srcs[(p*3)+2] ),
                                                                          rp->use_affine, GPUpub[0].rmask_header_, &(GPUpub[0].rmask_[os2]), rp->implicit_mask, rp->explicit_mask, z_offset,
                                                                          rp->dv, rp->vc, rp->limit_angular_range, rp->use_norm_vol, &(GPUpub[0].norm_vol_[os2]), weights_sub[p] );

            }

        }
    
}

#endif // DD_v25_BACKPROJECT