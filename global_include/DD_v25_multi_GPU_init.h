// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Support variables / functions for multi GPU parallelization

#include "cuda_runtime.h"

#ifndef MULTI_GPU

    #define MULTI_GPU
  
    // *** Declare functions ***
        
        // Check to make sure the user made valid selections for which GPUs to use
        void validate_GPUs();

        // initialize GPU variables which are shared between GPU streams (assumes the current device is set)
        void initialize_GPU_public( Public_GPU_Data *GPUpub  );
        
        // initialize GPU variables which are assoicated with individual GPU streams (assumes the current device is set)
        void initialize_GPU_private( Private_GPU_Data *GPUpri, int proj_increment, int GPUid );
    
    // *** Functions ***
        
        // Check to make sure the user made valid selections for which GPUs to use
        void validate_GPUs() {

            size_t total_gmem    = 0;
            size_t major_version = 0;
            cudaDeviceProp prop;
            int nDevices_found;

            // Total number of GPUs visible on the system
            cudaGetDeviceCount(&nDevices_found);

            if (nDevices_found == 0) {

                // mexErrMsgTxt("No GPUs found.");
                ERR("No GPUs found.");

            }

            // make sure all indices are legal
            // NOTE: nDevices has been set to match the length of GPU_indices.
            for (int i = 0; i < nDevices; i++) {

                if ( GPU_indices[i] < 0 || GPU_indices[i] > nDevices_found-1 ) {

                    ERR("At least one GPU specified for use does not exist or is not visible. (NOTE: GPU indexing starts from 0.)");
                    // mexErrMsgTxt("At least one GPU specified for use does not exist or is not visible. (NOTE: GPU indexing starts from 0.)");

                }

            }

            // make sure all specified GPUs are very similar (global memory, compute capability)

            for (int i = 0; i < nDevices; i++) {

                cudaGetDeviceProperties(&prop, GPU_indices[i]);

                if ( total_gmem != 0 && ( prop.totalGlobalMem > 1.02*total_gmem || prop.totalGlobalMem < 0.98*total_gmem ) )
                {
                    
                    ERR("Multi-GPU parallelization model assumes similar amounts of global memory between all specified GPUs.");

                    // mexErrMsgTxt("Multi-GPU parallelization model assumes similar amounts of global memory between all specified GPUs.");

                }

                total_gmem = prop.totalGlobalMem;

                // if ( major_version != 0 && prop.major != major_version )
                // {
                // 
                //     ERR("Multi-GPU parallelization model assumes similar compute capabilities.");
                // 
                //     // mexErrMsgTxt("Multi-GPU parallelization model assumes similar compute capabilities.");
                // 
                // }

                major_version = prop.major;

            }
            
            // Check to make sure we will not exceed thread count limits
        
            if ( THREAD_COUNT < NUMSTREAMS*nDevices  ) {

                ERR("Insufficient CPU thread count. Reduce device count, stream count product, or, increase THREAD_COUNT environment variable and recompile.");
                
                // mexErrMsgTxt("Insufficient CPU thread count. Reduce device count, stream count product, or, increase THREAD_COUNT environment variable and recompile.");

            }

        }
        
        // initialize GPU variables which are shared between GPU streams (assumes the current device is set)
        void initialize_GPU_public( Public_GPU_Data *GPUpub  )
        {

            // Allocate affine transform matrix
            if (use_affine == 1) {

                cudaMalloc( (void **) &GPUpub[0].aff_, 12*sizeof(float) );
                cudaMemcpy( GPUpub[0].aff_, aff, 12*sizeof(float), cudaMemcpyHostToDevice);

            }
            else {
                
                cudaMalloc( (void **) &GPUpub[0].aff_, sizeof(float) );
                
            }

            // Allocate FFT variables
            if ( filtration ) {

                cudaMalloc( (void **) &GPUpub[0].filt_, filt_size );

                // use this as a safeguard in case the projection sizes ever get mixed up
                cudaMemset( GPUpub[0].filt_, 0, filt_size);

                cudaMemcpy( GPUpub[0].filt_, filt, filt_size, cudaMemcpyHostToDevice);

            }
            else {
             
                cudaMalloc( (void **) &GPUpub[0].filt_, sizeof(float) );
                
            }

            // allocate projection matrices
            cudaMalloc( (void **) &GPUpub[0].pm_, 12*np*sizeof(float) );
            cudaMemcpy( GPUpub[0].pm_, pmis, 12*np*sizeof(float), cudaMemcpyHostToDevice );

            // If used, copy over the static portion of the rmask one time
            if ( explicit_mask > 0 ) {

                cudaMalloc( (void **) &GPUpub[0].rmask_header_, 2*nx*ny*sizeof(unsigned short) );
                cudaMemcpy( GPUpub[0].rmask_header_, rmask, 2*nx*ny*sizeof(unsigned short), cudaMemcpyHostToDevice);

            }
            else {
             
                cudaMalloc( (void **) &GPUpub[0].rmask_header_, sizeof(unsigned short) );
                
            }
            
            // v24: Normalization volume for helical reconstruction.
            // This shouldn't really be needed given proper vetrical cropping and z masking.
            if ( use_norm_vol == 1 ) {

                cudaMallocManaged( (void **) &GPUpub[0].norm_vol_, nxyz*sizeof(float), cudaMemAttachGlobal );

            }
            else {

                cudaMalloc( (void **) &GPUpub[0].norm_vol_, sizeof(float) );

            }
            
            // v24: share explicit mask between streams on the same GPU
            if ( explicit_mask == 2 ) {

                cudaMallocManaged( (void **) &GPUpub[0].rmask_, nxyz*sizeof(unsigned short), cudaMemAttachGlobal );
                cudaMemcpy( GPUpub[0].rmask_, &(rmask[2*nx*ny]), nxyz*sizeof(unsigned short), cudaMemcpyDefault );

            }
            else {
                
                cudaMalloc( (void **) &GPUpub[0].rmask_, sizeof(unsigned short) );

            }
            
            // v24: share volume and projection data between streams on the same GPU
            // cudaMemAttachGlobal: memory accessible from all streams and devices
            // NOTE: Managed memory defaults to (extremely slow) zero-copy memory under Windows drivers!
            
            cudaMallocManaged(  (void **) &(GPUpub[0].vol_),   vol_size, cudaMemAttachGlobal );
            
            cudaMallocManaged( (void **) &(GPUpub[0].proj_), projs_size, cudaMemAttachGlobal );
            
            // For temporal reconstruction with rebinned data we need to be able to look up temporal weights
            // from other projections (reverse the rebinning process).
            // Must be accessible from all streams. (cudaMemAttachGlobal)
            if (cy_detector == 5) {
             
                cudaMallocManaged( (void **) &(GPUpub[0].proj_weights_d), np * sizeof(double), cudaMemAttachGlobal );
                cudaMemcpy( GPUpub[0].proj_weights_d, weights, np * sizeof(double), cudaMemcpyDefault );
                
            }
            else { // dummy allocation
             
                cudaMallocManaged( (void **) &(GPUpub[0].proj_weights_d), sizeof(double), cudaMemAttachGlobal );
                
            }
            
            // v25: projection weight mask for backprojection is read only, so allocate one copy per GPU
            // cudaMemAttachGlobal: memory accessible from all streams and devices
            // NOTE: Managed memory defaults to (extremely slow) zero-copy memory under Windows drivers!
            if ( use_proj_weight_mask == 1 ){
                
                cudaMallocManaged( (void **) &(GPUpub[0].proj_weight_mask_), nuv * sizeof(float), cudaMemAttachGlobal );
                cudaMemcpy( GPUpub[0].proj_weight_mask_, proj_weight_mask, nuv * sizeof(float), cudaMemcpyDefault );
                
            }
            else { // dummy allocation
                
                cudaMallocManaged( (void **) &(GPUpub[0].proj_weight_mask_), sizeof(float), cudaMemAttachGlobal );
                
            }

        }
    
        // initialize GPU variables which are assoicated with individual GPU streams (assumes the current device is set)
        void initialize_GPU_private( Private_GPU_Data *GPUpri, int proj_increment, int GPUid )
        {
            
            char err_string[200];
            
            if ( cudaStreamCreate( &(GPUpri[0].mystream) ) != 0 ) {

                sprintf(err_string, "Stream allocation error on device %d!", GPUid);

                ERR(err_string);
                
                // mexErrMsgTxt(err_string);

            }

            // Allocate FFT variables
            if ( filtration ) {

                cudaMalloc( (void **) &GPUpri[0].fftproj_, fftproj_size );
                
            }
            else {
             
                cudaMalloc( (void **) &GPUpri[0].fftproj_, sizeof(float) );
                
            }

            cufftPlan1d(&GPUpri[0].fftplan,  nu_fft, CUFFT_C2C, nv);
            cufftPlan1d(&GPUpri[0].ifftplan, nu_fft, CUFFT_C2C, nv);

            cufftSetStream(GPUpri[0].fftplan , GPUpri[0].mystream);
            cufftSetStream(GPUpri[0].ifftplan, GPUpri[0].mystream);

            // Double precision projection buffer to achieve reproducible results with the projection operator,
            // which uses atomic add operations.
            cudaMalloc( (void **) &GPUpri[0].projs_temp_, proj_size * 2 ); // double projections 2x the size of single projections

        }

#endif // MULTI_GPU
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                