% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
%% References
 
    % Long, Y., Fessler, J. A., & Balter, J. M. (2010).
    % 3D forward and back-projection for X-ray CT using separable footprints.
    % IEEE transactions on medical imaging, 29(11), 1839-1850.
    
    % Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
    % Weighted FBPâ€”a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
    % Physics in Medicine & Biology, 49(11), 2209.
    
    % Van der Vorst, H. A. (1992).
    % Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
    % SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
    
%%
 
%%%%%%
 
% Build the "cyrebin" function 
% Rebin the rows of an ideal helical, cone-beam geometry + cylindrical detector to allow row-wise filtration of parallel rays
 
%%%%%%
        
%%
 
    disp(sprintf(['\n\nTesting cone-parallel rebinning and WFBP reconstruction in a third-generation CT scanner geometry...\n\n']));
    
    X = load_nii(proc_string('Test_Data/SL3D_ph_new_sph0.nii.gz'));
    X = single(reshape(X.img,[256 256 256]));
 
    % geometric parameters (Duke1 Geometry)
    
        lsd = 1050; % mm
        lso = 575;  % mm
        
        pitch = 1.0; % 1.0; % 1.9; % 1.0;
 
        mag = lsd/lso;
 
        % sampling angles
        np_rot = 1000;
        rotations = 8;
        np = rotations*np_rot;
        ap = 360 / np_rot;
        angles = single(0:ap:(360*rotations-ap));
 
        % geometry
 
        rotdir = 1;
        
        du = 1.0; % mm
        dv = 1.0; % mm
        nu = 900; % pixels
        nv =  64; % pixels
        
        dx = 0.6;
        dx0 = dx;
        dy = 0.6;
        dz = 0.6;
        
        nx = 256; % MUST be divisible by 8
        ny = 256; % MUST be divisible by 8
        nz = 256;
        
        AM = 0.0; % 1.75; % central ray offset from center of detector (pixels)
        uc = ( nu - 1 ) / 2; % - AM;
        vc = ( nv - 1 ) / 2;
        
        eta = 0;
        sig = 0;
        phi = 0;  
 
        % preview slice
        slice_num = round(nz/2);
        
        % angular offset for coarse alignment of each chain (radians)
        ao = 0;
 
        % z offset
        slicewidth  = dv / mag;
        mm_per_proj = pitch * ( 1 / np_rot ) * nv * slicewidth;
 
        % zo_ = 50*dz;
        zo  = mm_per_proj*(0:(np-1));
        zo_ = (np/2)*mm_per_proj;
        
        detector_collimation = (dv / mag) * nv;
        
    % Helical CT
    
        % travel of the source position on the detector per rotation (mm)
        % this parameter has three modes:
        % limit_angular_range == 0: not used
        % limit_angular_range > 0, <= 1.0: limit based on fractions of the detector height (uses a cosine^2 window)
        % limit_angular_range > 1.00001: specified range to use from detector center in each direction (mm)
        short_scan = 1;
        
        if short_scan == 1
        
            % Short scan (minimum necessary after rebinning)
            limit_angular_range = detector_collimation * pitch  * 180/360 / 2;
            
            % Allow overlap between rotations to reduce transition artifacts.
            % TO DO: Is there a way to fix this by modifying the computation of area integrals?
            limit_multiplier = 1.10;
        
        else
        
            % full scan (minimum necessary after rebinning)
            limit_angular_range = detector_collimation * pitch  * 360/360 / 2;
        
        end
        
        % limit_angular_range = 0;
        
        % limit_angular_range = 0.82; % 0.82; % 0.8; % 0.6; % 0.82; % 0.71; % 0.7; % 0.9; % 1.0;  % (0) do not use; (>0) ignore rays which diverge by more than this absolute distance (in mm) from the central ray at the detector
        scale = 1.0;                   % scalar volume multiplier
 
        % cylindrical detector
        % cy_detector = 2;      % (0) - detector is flat; (1) detector is cylindrical; (2) detector is cylindrical, backproject rebinned projections
        db = atan2( du, lsd ); % fan angle increment (radians)
 
    % create geometry file
    
        a2r = pi/180;
 
        trans = @(x) ...
            [1 0 0 x(1);
            0 1 0 x(2);
            0 0 1 x(3);
            0 0 0 1];
 
        rotx3 = @(x) ...
            [1 0 0;
            0 cos(x) -sin(x);
            0 sin(x) cos(x)];
 
        roty3 = @(x) ...
            [cos(x) 0 sin(x);
            0 1 0;
            -sin(x) 0 cos(x)];
 
        rotz3 = @(x) ...
            [cos(x) -sin(x) 0;
            sin(x) cos(x) 0;
            0 0 1];
 
        rotz = @(x) ...
            [cos(x) -sin(x) 0 0;
            sin(x) cos(x) 0 0;
            0 0 1 0;
            0 0 0 1];
 
        geolines = zeros(np,21,'double');
 
        rot = rotx3(-eta)*roty3(-sig)*rotz3(-phi);
        puv = rot*[0; du; 0];
        pvv = rot*[0; 0; dv];
        
        for i=1:np % for each projection, rotate system
            
            angle = angles(i); % ap*(i-1);
            
            % prevent singular projection matrices
            % if (mod(angle,45) == 0) % can sometimes fail
            if abs(rem(angle,45)) < 1e-6 % seems to work reliably, assuming angles is double precision
 
                angle = angle+1e-4;
 
            end
 
            src = [-lso;      0; zo(i)-zo_] + [0; lsd * sin( AM * db ); 0];
            dtv = [(lsd-lso); 0; zo(i)-zo_];
 
            rsrc = rotz3(rotdir*angle*a2r)*src;
            rdtv = rotz3(rotdir*angle*a2r)*dtv;
            rpuv = rotz3(rotdir*angle*a2r)*puv;
            rpvv = rotz3(rotdir*angle*a2r)*pvv;
            
            % line in geofile
            geolines(i,:) = [rsrc' rdtv' rpuv' rpvv' lsd lso uc vc eta sig phi 1 angle];
            
        end
 
    % Save the geometry to a file
    
        geofile = proc_string('Test_Data/geofile.geo');
        delete(geofile); % delete a pre-existing geo file, if there is one by the same name
        save(geofile,'geolines','-ASCII'); % save the geometry file
 
    % Additional flags
    
        implicit_mask = 1; % (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
        explicit_mask = 0; % (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask
        use_affine = 0;    % (0) - affine transform is not used (even if one is provided); (1) - affine transform is used
 
    % Dummy parameters for no affine, minimal computation
    
        Rt_aff = single([1 0 0 0 1 0 0 0 1 0 0 0]);
 
    % Explicit mask - image domain reconstruction mask
    
        if (explicit_mask == 0) % no explicit mask
 
            rmask = uint16(0);
 
        elseif (explicit_mask == 1)  % used z range explicit mask (good for boxes)
 
            % Test explicit mask
            % Reproduces implicit mask
            rmask = zeros(nx,ny,'single');
            [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            rmask = repmat(rmask,[1 1 nz]);
 
            % rmask2 = zeros(nx,ny,nz,'single');
            % rmask2(:,:,385:end-384) = repmat(rmask,[1 1 256]);
            % rmask = rmask2;
            % clear rmask2;
 
            % rmask = rmask(:);
            clear x1 x2 x3;
 
            rmask = mask_zrange( rmask );
 
            rmask = uint16(rmask(:));
 
        else % use for arbitrary shapes... slower
 
            % Test explicit mask
            % Reproduces implicit mask
            rmask = zeros(nx,ny,'single');
            [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            rmask = repmat(rmask,[1 1 nz]);
            % rmask = rmask(:);
            clear x1 x2 x3;
 
            zmask = mask_zrange( rmask );
 
            rmask = cat(3,zmask,rmask);
 
            rmask = uint16(rmask(:));
 
        end
 
    % Parameter vectors for reconstruction operators
    
        % (1) FBP, (0) simple backprojection
        FBP = 1;           
        simple_back_projection = 0;
    
        cy_detector     = 1; % (0) - detector is flat; (1) detector is cylindrical; (2) cylindrical, cone-parallel rebinning; (3) flat panel, cone-parallel rebinning; (4) array of parallel beams (synchrotron geometry)
        cy_detector_FBP = 2;
        
        use_norm_vol     = 0; % (0) - do not allocate / use count normalization volume, (1) allocate / use count normalization volume during backprojection
        use_norm_vol_FBP = 1;
        
        int_params    = int32([nu nv np nx ny nz simple_back_projection use_affine implicit_mask explicit_mask cy_detector     use_norm_vol    ]);
        % int_params_f  = int32([nu nv np nx ny nz FBP                    use_affine implicit_mask explicit_mask cy_detector_FBP use_norm_vol_FBP]);
        double_params  = [du dv dx dy dz zo_ ao lsd lso uc vc eta sig phi scale db limit_angular_range];
        double_params2 = [du dv dx dy dz zo_ ao lsd lso uc vc eta sig phi scale db limit_multiplier*limit_angular_range];
        
    % Make a Ram-Lak filter for cone-beam backprojection
        
        lenu = 2^(ceil(log(nu-1.0)/log(2.0))); % next power of 2
        filt_name = proc_string(['Test_Data/Ram-Lak_' num2str(lenu) '.txt']);
        make_FBP_filter( filt_name, du, lenu, 'ram-lak', 1.0 );
        
%% Construct a TD window for WFBP
% Construct a weight map to limit the data used for reconstruction.
 
    r_os   = 1;               % row oversampling factor for cone-parallel rebinning
    du_out = du / r_os / mag;  % oversample the output to better preserve spatial resolution
 
    % window_type = 0; % 3rd generation CT scanner after cone-parallel rebinning
    % buffer_fraction = 0.0; % fraction of the detector collimation by which to smoothly extend the TD window
    % 
    % TD_window = make_TD_window( lsd, lso, db, dv, nu, nv, uc, vc, AM, 0, pitch, np_rot, ...
    %                               0, window_type, du_out, mag, buffer_fraction, rotdir );
 
    % TD_window = TD_window(end:-1:1,:);
    % TD_window = TD_window(:,end:-1:1);
                              
    % imtool(mat2gray(TD_window));
    
    % This feature has been removed, and it not used even if a projection mask is provided.
    % Set it to zero so it is not used.
    % TD_window = single(0);
 
%% Initialize reconstruction operators
% Be sure to "clear DD_init" before clearing recon. allocations or resetting the GPU(s)!
% Note: Any outstanding recon allocations will be properly deallocated when Matlab is closed.
 
    clear DD_init;
 
    tic;
 
    recon_alloc   = DD_init(int_params(:)  , double_params2(:), filt_name, geofile, Rt_aff(:), rmask, gpu_list); % , TD_window);
    % recon_alloc_f = DD_init(int_params_f(:), double_params(:), filt_name, geofile, Rt_aff(:), rmask, gpu_list); % , TD_window);
    
    toc;
    
%% Reconstruction operators
 
    W0  = ones(1,np);
    R   = @(x,w) DD_project(x,recon_alloc,w);
    Rt  = @(x,w) DD_backproject(x,recon_alloc,int32(0),w);
    % Rtf = @(x,w) DD_backproject(x,recon_alloc_f,int32(1),w);
 
%% Create original projections
 
    tic;
    Y = R(X(:),W0);
    toc;
 
    if isempty(javachk('awt'))
        Y_temp = reshape(Y,[nu nv np]);
        imtool(mat2gray(reshape(Y_temp(:,:,round(linspace(1,np,5))),[nu  nv*5])));
        clear Y_temp;
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end

    cd(sprintf(proc_string('DD_operators_v%i/CyRebin_v%i/'),op_version_number,rebin_number));
    
    % save_nii(make_nii(reshape(Y,[nu nv np])),'Yb_v3.nii');
    
%% Compile cyrebin CUDA code
 
    cd(sprintf(proc_string('DD_operators_v%i/CyRebin_v%i/'),op_version_number,rebin_number));
 
    %   1) use more L1 cache (vs. shared memory)
    %   2) ptxas compiler info for CUDA kernels
    %   3) use "fast math" (lower precision, hardware-based basic math functions)
    %   4) include OMP
    %   5) debugging flags
 
    cmd = compile_nvcc( 'cyrebin_cuda', c_compiler, GPU_compute_capability, [1 1 1 0 0], {}, {mex_includes, operator_includes} );
 
%% Compile C program w/ mex interface
 
    clear cyrebin;
 
    cd(sprintf(proc_string('DD_operators_v%i/CyRebin_v%i/'),op_version_number,rebin_number));
 
    %   1) use large array dimension varaibles (size_t, mwsize => 64 bit unsigned integers)
    %   2) include CUDA libraries
    %   3) include -g flag for on-line debugging of the C/C++ portion of the code in Visual Studio
 
    cmd = compile_mex( mex_path, c_compiler, [1 1 0], 'cyrebin', 'cyrebin_cuda', {cuda_libraries}, {cuda_includes, operator_includes} );
    
%% Rebinning parameter vectors
    
    Y_B      = single(0);
    GPU_idx  = int32(gpu_list(1));
    det_type = int32(1); % (0) flat-panel, (1) cylindrical
 
    Rf   = lsd; % i.e. lso * mag, needed to match the expected  du_out = du / r_os / mag for Siemens Recon CT rebinned results
    zrot = mm_per_proj / ap; % z travel in mm / degree
    nu_A = nu;
    nu_B = 0;
    % r_os = 1;
 
    int_params_b = int32([ nu_A, nu_B, nv, np, r_os, rotdir ]);
 
    delta_theta = ap;                % assumes input angular increment = output angular increment
    delta_beta  = atan2d( du, lsd ); % degrees
    uc_A        = uc;
    uc_B        = 0;
    % du_out      = du / r_os / mag;   % oversample the output to better preserve spatial resolution      
    theta_B     = 0;                 % not used
    scale_A_B   = 1;                 % not used
 
    double_params_b = double([ Rf, zrot, delta_theta, delta_beta, dv, uc_A, uc_B, du_out, theta_B, scale_A_B, mag, AM ]);
    
%% Perform projection rebinning
 
    clear cyrebin;
 
    tic;
    
    Yb = cyrebin( Y, Y_B, int_params_b, double_params_b, GPU_idx, det_type );
    
    toc;
    
    Yb = reshape(Yb,[nu * r_os, nv, np]);

    if isempty(javachk('awt'))
        imtool(mat2gray(reshape(Yb(:,:,round(linspace(1,np,5))),[nu*r_os  nv*5])));
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end
    
    % save_nii(make_nii(Yb),['/home/x-ray/Documents/GPU_Recon_Toolkit_v16/DD_operators_v24/CyRebin/Yb.nii']);
    
%% Update operator for WFBP using new, smaller pixel size
 
    du_new = du_out;
    nu_new = nu * r_os;
    uc_new = uc * r_os;
    
    % Compensate for the smaller pixel size at the same pixel magnitude
    scale = r_os * mag;
 
    use_norm_vol        = 1;
    cy_detector         = 2; % (0) - detector is flat; (1) detector is cylindrical; (2) detector is cylindrical, backproject rebinned projections
 
    int_params    = int32([nu_new nv np nx ny nz simple_back_projection use_affine implicit_mask explicit_mask cy_detector use_norm_vol    ]);
    int_params_f  = int32([nu_new nv np nx ny nz FBP                    use_affine implicit_mask explicit_mask cy_detector use_norm_vol_FBP]);
 
    double_params = double([du_new dv dx dy dz zo_ ao lsd lso uc_new vc eta sig phi scale db limit_angular_range]);
 
    % Update geometry file
    
        a2r = pi/180;
 
        trans = @(x) ...
            [1 0 0 x(1);
            0 1 0 x(2);
            0 0 1 x(3);
            0 0 0 1];
 
        rotx3 = @(x) ...
            [1 0 0;
            0 cos(x) -sin(x);
            0 sin(x) cos(x)];
 
        roty3 = @(x) ...
            [cos(x) 0 sin(x);
            0 1 0;
            -sin(x) 0 cos(x)];
 
        rotz3 = @(x) ...
            [cos(x) -sin(x) 0;
            sin(x) cos(x) 0;
            0 0 1];
 
        geolines = zeros(np,21,'double');
 
        rot = rotx3(-eta)*roty3(-sig)*rotz3(-phi);
        puv = rot*[0; du; 0];
        pvv = rot*[0; 0; dv];
        
        for i=1:np % for each projection, rotate system
            
            angle = angles(i); % ap*(i-1);
            
            % prevent singular projection matrices
            % if (mod(angle,45) == 0) % can sometimes fail
            if abs(rem(angle,45)) < 1e-6 % seems to work reliably, assuming angles is double precision
 
                angle = angle+1e-4;
 
            end
 
            src = [-lso;      0; zo(i)-zo_] + [0; lsd * sin( AM * db ); 0];
            dtv = [(lsd-lso); 0; zo(i)-zo_];
 
            rsrc = rotz3(rotdir*angle*a2r)*src;
            rdtv = rotz3(rotdir*angle*a2r)*dtv;
            rpuv = rotz3(rotdir*angle*a2r)*puv;
            rpvv = rotz3(rotdir*angle*a2r)*pvv;
            
            % line in geofile
            geolines(i,:) = [rsrc' rdtv' rpuv' rpvv' lsd lso uc vc eta sig phi 1 angle];
            
        end
 
    % Make a Ram-Lak filter for cone-beam backprojection
        
        lenu = 2^(ceil(log(nu_new-1.0)/log(2.0))); % next power of 2
        filt_name = proc_string(['Test_Data/Ram-Lak_' num2str(lenu) '.txt']);
        make_FBP_filter( filt_name, du, lenu, 'ram-lak', 1.0 );
        
%% Update reconstruction operators
% Be sure to "clear DD_init" before clearing recon. allocations or resetting the GPU(s)!
% Note: Any outstanding recon allocations will be properly deallocated when Matlab is closed.
 
    % clear DD_init;
 
    tic;
 
    % recon_alloc_2  = DD_init(int_params(:)  , double_params(:), filt_name, geofile, Rt_aff(:), rmask, gpu_list);
    recon_alloc_f2 = DD_init(int_params_f(:), double_params(:), filt_name, geofile, Rt_aff(:), rmask, gpu_list);
 
    toc;
 
    W0   = ones(np,1);
    % R2   = @(x,w) DD_project(x,recon_alloc_f2,w);
    % Rt2  = @(x,w) DD_backproject(x,recon_alloc_f2,int32(0),w);
    Rtf2 = @(x,w) DD_backproject(x,recon_alloc_f2,int32(1),w);
    
%% Perform weighted filtered backprojection in cone-parallel geometry
% RMSE: 0.0424262 (Duke1 geometry, r_os = 1), short-scan reconstruction, ~1.1 seconds, 1x RTX8000 GPU
 
    tic;
    X_out = Rtf2(Yb(:), W0);
    toc;

    if isempty(javachk('awt'))
        X_out = reshape(X_out,[nx ny nz]);
        slices = round(linspace(50,nz-50,5));
        figure(2);
        imagesc([reshape(X(:,:,slices),[nx ny*5]); reshape(X_out(:,:,slices),[nx ny*5]); abs(reshape(X(:,:,slices),[nx ny*5])-reshape(X_out(:,:,slices),[nx ny*5]))],[0 1]);
        title('Recon. w/ WFBP (Expected, WFBP, |Residuals|)');
        axis image off; colormap gray; colorbar;
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end
 
    RMSE = sqrt(sum((X_out(:) - X(:)).^2)./(nx*ny*nz))
   
    X_WFBP = X_out;
    
%% Iterate to resolve short-scan, high pitch artifacts from the approximate short-scan WFBP reconstruction
% TO DO: Do the updates relative to the original projections instead of relative to
%        the rebinned projections?
% short scan: ~75 seconds
% resvec = 100 *  1.592094421386719   0.533774414062500   0.143264226913452   0.077238287925720   0.048760762214661
%                                 0   0.277868843078613   0.114632310867310   0.065730829238892   0.042205038070679
%                                 0   0.191618614196777   0.092618560791016   0.056234679222107   0.039183480739594
% RMSE = 0.0226435

    num_iter = 4;
    snum = 3;
    ell = 2;
    
    subsets = int32(bitrevorder(1:2^ceil(log2(snum))));
    subsets = subsets(subsets <= snum);
    
    W = zeros(np,snum);
    
    for i = 1:snum
    
        W(i:snum:end,i) = 1;
        
    end
    
    % W = W0;
    
    const = Rt( Y(:), sum(W,2));
 
    fun = @(x,w) Rt(R(x,w),w);
    
    tic;
    [ X_out, resvec ] = bicgstabl_os( fun, const, X_WFBP(:), num_iter, W, ell );
    toc;
 
    disp(['L2 Residuals: ']);
    resvec

    if isempty(javachk('awt'))
        X_out = reshape(X_out,[nx ny nz]);
        slices = round(linspace(50,nz-50,5));
        figure(3);
        imagesc([reshape(X(:,:,slices),[nx ny*5]); reshape(X_out(:,:,slices),[nx ny*5]); abs(reshape(X(:,:,slices),[nx ny*5])-reshape(X_out(:,:,slices),[nx ny*5]))],[0 1]);
        title('Recon. w/ L2 Minimization (Expected, L2-min Recon., |Residuals|)');
        axis image off; colormap gray; colorbar;
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end
 
    
 
    RMSE = sqrt(sum((X_out(:) - X(:)).^2)./(nx*ny*nz))
    
%% Clear allocations and add to path
 
    % Clear allocated reconstruction objects using mexAtExit() within the source code
    clear DD_init;
    clear recon_alloc;
    clear recon_alloc_f;
    
    % Add rebinning operator to the path
    path(sprintf(proc_string('DD_operators_v%i/CyRebin_v%i/'),op_version_number,rebin_number),path);
    
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
