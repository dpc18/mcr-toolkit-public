% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % Long, Y., Fessler, J. A., & Balter, J. M. (2010).
    % 3D forward and back-projection for X-ray CT using separable footprints.
    % IEEE transactions on medical imaging, 29(11), 1839-1850.
    
    % Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
    % Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
    % Physics in Medicine & Biology, 49(11), 2209.
    
    % Van der Vorst, H. A. (1992).
    % Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
    % SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
    
    % Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
    % Cone-beam reprojection using projection-matrices.
    % IEEE transactions on medical imaging, 22(10), 1202-1214.
    
    % Sleijpen, G. L., & Van Gijzen, M. B. (2010).
    % Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
    % SIAM journal on scientific computing, 32(5), 2687-2709.
    % Algorithm 3.1    

%%

%%%%%%

% Build the "DD_init" function

%%%%%%

%% Compile DD_init operator

    clear DD_init;

    cd(sprintf(proc_string('DD_operators_v%i/DD_3DCB_Initialization_v%i'),op_version_number,op_version_number));

    cmd = compile_mex( mex_path, c_compiler, [1 1 0], 'DD_init', '', {cuda_libraries}, {cuda_includes, operator_includes} );

%% add init operator to path

    path(sprintf(proc_string('DD_operators_v%i/DD_3DCB_Initialization_v%i'),op_version_number,op_version_number),path);
   
%%

%%%%%%

% Test the "project" function

%%%%%%

%% Phase 1, 3D Shepp-Logan phantom

    % geometric parameters
    
        volname = 'SL3D_ph_new_sph0.nii.gz';

        scaler = 2; % 3

        nsets = 1;
        nx = 256; % MUST be divisible by 8
        ny = 256; % MUST be divisible by 8
        nz = 256;
        nu = scaler*400-3; % v25: no long needs to be divisible by 8
        nv = scaler*272-3; % v25: no long needs to be divisible by 8
        dx = 0.088;
        dy = 0.088;
        dz = 0.088;
        du = 0.044;
        dv = 0.044;
        lsd = 800;
        lso = 700;
        uc = scaler*190;
        vc = scaler*135;
        eta = 0;
        sig = 0;
        phi = 0;
        ap = 1;
        % ap = 0.36;
        rotdir = -1; % 1: clockwise, -1: counterclockwise
        nphot = 50;
        ao = 0;
        zo = 0;

        np=360/ap;
        angles = single(0:ap:360-ap);
        
    % Phantom Data
    
        X = load_nii(proc_string(['Test_Data/' volname]));
        X = single(X.img);
    
    % Helical CT
        
        limit_angular_range = 0;  % (0) do not use; (>0) ignore rays which diverge by more than this absolute distance (in mm) from the central ray at the detector
        scale = 1;                % scalar volume multiplier

        % cylindrical detector
        cy_detector = 0;          % (0) - detector is flat; (1) detector is cylindrical; (2) cylindrical, cone-parallel rebinning; (3) flat panel, cone-parallel rebinning; (4) array of parallel beams (synchrotron geometry)
        db = 0; % fan angle increment (radians)

    % create geometry file
    
        a2r = pi/180;

        trans = @(x) ...
            [1 0 0 x(1);
            0 1 0 x(2);
            0 0 1 x(3);
            0 0 0 1];

        rotx3 = @(x) ...
            [1 0 0;
            0 cos(x) -sin(x);
            0 sin(x) cos(x)];

        roty3 = @(x) ...
            [cos(x) 0 sin(x);
            0 1 0;
            -sin(x) 0 cos(x)];

        rotz3 = @(x) ...
            [cos(x) -sin(x) 0;
            sin(x) cos(x) 0;
            0 0 1];

        rotz = @(x) ...
            [cos(x) -sin(x) 0 0;
            sin(x) cos(x) 0 0;
            0 0 1 0;
            0 0 0 1];

        geolines = zeros(np,21,'double');

        rot = rotx3(-eta)*roty3(-sig)*rotz3(-phi);
        puv = rot*[0; du; 0];
        pvv = rot*[0; 0; dv];
        
        for i=1:np % for each projection, rotate system
            
            angle = angles(i); % ap*(i-1);

            if ( mod(angle,45) == 0)

                angle = angle + 1e-4;

            end

            src = [-lso;      0; zo];
            dtv = [(lsd-lso); 0; zo];

            rsrc = rotz3(rotdir*angle*a2r)*src;
            rdtv = rotz3(rotdir*angle*a2r)*dtv;
            rpuv = rotz3(rotdir*angle*a2r)*puv;
            rpvv = rotz3(rotdir*angle*a2r)*pvv;
            
            % line in geofile
            geolines(i,:) = [rsrc' rdtv' rpuv' rpvv' lsd lso uc vc eta sig phi 1 angle];
            
        end

    % Save the geometry to a file
    
        geofile = proc_string('Test_Data/geofile.geo');
        delete(geofile); % delete a pre-existing geo file, if there is one by the same name
        save(geofile,'geolines','-ASCII'); % save the geometry file

    % Additional flags
    
        FBP = 1;                    % (1) FBP, (0) simple backprojection
        simple_back_projection = 0;
        implicit_mask = 1;          % (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
        explicit_mask = 0;          % (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask; (2) use volumetric reconstruction mask
        use_affine = 0;             % (0) - affine transform is not used (even if one is provided); (1) - affine transform is used
        use_norm_vol = 0;           % (0) - do not allocate / use count normalization volume, (1) allocate / use count normalization volume during backprojection

    % Dummy parameters for no affine, minimal computation
    
        % NOTE: When the affine causes a volume change (i.e. it is non-rigid), the resultant FBP will not be properly normalized.
        %       Also, inversion of the z axis is not supported.
        % Rt_aff   = single([0 -1 0 1 0 0 0 0 1 -5 3 11]);
        % Rt_aff   = single([0 1 0 1 0 0 0 0 1 -5 3 11]);
        % Rt_aff = single([1 0 0 0 1 0 0 0 1 -5 7 5]);
        Rt_aff = single([1 0 0 0 1 0 0 0 1 0 0 0]);

    % Explicit mask - image domain reconstruction mask
    
        if (explicit_mask == 0) % no explicit mask

            rmask = uint16(0);

        elseif (explicit_mask == 1)  % used z range explicit mask (good for boxes)

            % Test explicit mask
            % Reproduces implicit mask
            rmask = zeros(nx,ny,'single');
            % [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            [x1, x2] = ndgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            rmask = repmat(rmask,[1 1 nz]);

            % rmask2 = zeros(nx,ny,nz,'single');
            % rmask2(:,:,385:end-384) = repmat(rmask,[1 1 256]);
            % rmask = rmask2;
            % clear rmask2;

            % rmask = rmask(:);
            clear x1 x2 x3;

            rmask = mask_zrange( rmask );

            rmask = uint16(rmask(:));

        else % use for arbitrary shapes... slower

            % Test explicit mask
            % Reproduces implicit mask
            rmask = zeros(nx,ny,'single');
            % [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            [x1, x2] = ndgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            rmask = repmat(rmask,[1 1 nz]);
            % rmask = rmask(:);
            clear x1 x2 x3;

            zmask = mask_zrange( rmask );

            rmask = cat(3,zmask,rmask);

            rmask = uint16(rmask(:));

        end
        
    % Backprojection weight mask
    
        proj_weight_mask = single(0); % not used

    % Parameter vectors
    
        % int_params
        % int_params_f = int32([nu nv np nx ny nz FBP cy_detector implicit_mask explicit_mask use_affine use_norm_vol]);
        % double_params = [du dv dx dy dz zo ao lsd lso uc vc eta sig phi scale db limit_angular_range];

        % Required parameters
        int_params    = int32([nu nv np nx ny nz simple_back_projection use_affine]);
        int_params_f  = int32([nu nv np nx ny nz FBP                    use_affine]);
        double_params = [du dv dx dy dz zo ao lsd lso uc vc eta sig phi];
        
        % Add optional parameters
        int_params    = cat(2,    int_params, int32([implicit_mask explicit_mask cy_detector use_norm_vol]));
        int_params_f  = cat(2,  int_params_f, int32([implicit_mask explicit_mask cy_detector use_norm_vol]));
        double_params = cat(2, double_params,       [scale db limit_angular_range]);
        
    % Make a Ram-Lak filter for cone-beam backprojection
        
        lenu = 2^(ceil(log(nu-1.0)/log(2.0))); % next power of 2
        filt_name = proc_string(['Test_Data/Ram-Lak_' num2str(lenu) '.txt']);
        make_FBP_filter( filt_name, du, lenu, 'ram-lak', 1.0 );

%% Initialize reconstruction operators
% Be sure to "clear DD_init" before clearing recon. allocations or resetting the GPU(s)!
% Note: Any outstanding recon allocations will be properly deallocated when Matlab is closed.

    clear DD_init;

    tic;

    recon_alloc = DD_init(int_params_f(:), double_params(:), filt_name, geofile, Rt_aff(:), rmask, gpu_list); % , proj_weight_mask );
    
    toc;
    
    %%

%%%%%%

% Build the "DD_project" function which uses an initialized reconstruction problem

%%%%%%
    
%% Build the project operator object code

    cd(sprintf(proc_string('DD_operators_v%i/DD_3DCB_Project_v%i'),op_version_number,op_version_number));

    %   1) use more L1 cache (vs. shared memory)
    %   2) ptxas compiler info for CUDA kernels
    %   3) use "fast math" (lower precision, hardware-based basic math functions)
    %   4) include OMP
    %   5) debugging flags

    cmd = compile_nvcc( 'DD_project_cuda', c_compiler, GPU_compute_capability, [1 1 1 1 0], {}, {mex_includes, operator_includes} );

%% Compile C program w/ mex interface

    % required to recompile after calling the function
    clear DD_project;

    cd(sprintf(proc_string('DD_operators_v%i/DD_3DCB_Project_v%i'),op_version_number,op_version_number));

    cmd = compile_mex( mex_path, c_compiler, [1 1 0], 'DD_project', 'DD_project_cuda', {cuda_libraries}, {cuda_includes, operator_includes} );
    
%% Reconstruction operators

    W0 = ones(1,np);
    R = @(x,w) DD_project(x,recon_alloc,w);
    Rt = @(x,w) DD_backproject(x,recon_alloc,int32(0),w);
    Rtf = @(x,w) DD_backproject(x,recon_alloc,int32(1),w);

%% Test projection operation w/ prior initialization
% BB4 - 2 GPUs (SM_61, Ubuntu Linux, CUDA 9.0): ~0.572241 sec.

disp(sprintf(['\n\nCreating projections from the SL phantom...\n\n']));

    tic;
    % Y = DD_project(X(:), recon_alloc);
    Y = R(X(:),W0);
    toc;

    if isempty(javachk('awt'))
        Y_temp = reshape(Y,[nu nv np]);
        imtool(mat2gray(reshape(Y_temp(:,:,round(linspace(1,np,5))),[nu  nv*5])));
        clear Y_temp;
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end

    % Y_test = reshape(Y,[nu nv np]);  
    % Y_test = reshape(Y,[nu*nv np]);
    % temp = sqrt(sum(Y_test.^2,1));
    % plot(temp)
    % clear Y_test temp;

%% Compile backprojection CUDA code

    cd(sprintf(proc_string('DD_operators_v%i/DD_3DCB_Backproject_v%i/'),op_version_number,op_version_number));

    %   1) use more L1 cache (vs. shared memory)
    %   2) ptxas compiler info for CUDA kernels
    %   3) use "fast math" (lower precision, hardware-based basic math functions)
    %   4) include OMP
    %   5) debugging flags

    cmd = compile_nvcc( 'DD_backproject_cuda', c_compiler, GPU_compute_capability, [1 1 1 1 0], {}, {mex_includes, operator_includes} );

%% Compile C program w/ mex interface

    clear DD_backproject;

    cd(sprintf(proc_string('DD_operators_v%i/DD_3DCB_Backproject_v%i/'),op_version_number,op_version_number));

    % obj_in = {'DD_backproject_cuda','GPU_support_functions'};

    cmd = compile_mex( mex_path, c_compiler, [1 1 0], 'DD_backproject', 'DD_backproject_cuda', {cuda_libraries}, {cuda_includes, operator_includes} );
    
%% Perform filtered backprojection
% RMSE: ~0.0201279

    tic;
    % X_out = DD_backproject(Y(:), recon_alloc);
    X_out = Rtf(Y(:), W0);
    toc;

    if isempty(javachk('awt'))
        X_temp = reshape(X_out,[nx ny nz]);
        imtool(mat2gray(reshape(X_temp(:,:,round(linspace(1,nz,5))),[nx  ny*5])));
        clear X_temp;
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end

    RMSE = @(a,b) sqrt(mean((a(:)-b(:)).^2));

    RMSE(X,X_out)

%% Update path

    path(sprintf(proc_string('DD_operators_v%i/DD_3DCB_Backproject_v%i/'),op_version_number,op_version_number),path);
    path(sprintf(proc_string('DD_operators_v%i/DD_3DCB_Project_v%i/'),op_version_number,op_version_number),path);

%% Algebraic reconstruction with subsets
% Expected RMSE: ~0.0238934

    num_iter = 3;
    snum = 3;
    ell = 2;
    
    subsets = int32(bitrevorder(1:2^ceil(log2(snum))));
    subsets = subsets(subsets <= snum);
    
    W = zeros(np,snum);
    
    for i = 1:snum
    
        W(i:snum:end,i) = 1;
        
    end
    
    const = Rt( Y(:), sum(W,2));

    fun = @(x,w) Rt(R(x,w),w);
    
    tic;
    [ X_out, resvec ] = bicgstabl_os( fun, const, const*0, num_iter, W, ell );
    toc;

    disp(['L2 Residuals: ']);
    resvec

    if isempty(javachk('awt'))
        X_out = reshape(X_out,[nx ny nz]);
        slices = round(linspace(50,nz-50,5));
        figure(2);
        imagesc([reshape(X(:,:,slices),[nx ny*5]); reshape(X_out(:,:,slices),[nx ny*5]); abs(reshape(X(:,:,slices),[nx ny*5])-reshape(X_out(:,:,slices),[nx ny*5]))],[0 1]);
        title('Recon. w/ L2 Minimization (Expected, L2-min Recon., |Residuals|)');
        axis image off; colormap gray; colorbar;
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end

    RMSE = sqrt(sum((X_out(:) - X(:)).^2)./(nx*ny*nz))
    
%% Try to delete existing allocations
% NOTE: Be sure to deallocate (clear) recon. allocations before resetting GPU devices.

    % Clear allocated reconstruction objects using mexAtExit() within the source code
    clear DD_init;
    clear recon_alloc;
    
    % Reset to make sure we are done on the GPU
    reset_GPUs(gpu_list);
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    