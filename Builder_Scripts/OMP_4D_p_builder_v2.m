% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % Rubinstein, R., Zibulevsky, M., & Elad, M. (2008).
    % Efficient implementation of the K-SVD algorithm using batch orthogonal matching pursuit (No. CS Technion report CS-2008-08).
    % Computer Science Department, Technion.

%% Build the OMP_4D_p object code
% adds separable resampling kernel

    cd(proc_string(['Regularization/OMP/OMP_4D_p' OMP_p_version]));

    cmd = compile_nvcc( 'OMP_4D_p_cuda_96', c_compiler, GPU_compute_capability, [1 1 1 0 0], {}, {mex_includes,OMP_includes} );

%% Compile C program w/ mex interface and ignored libraries

    clear mex;

    cmd = compile_mex( mex_path, c_compiler, [1 1 0], 'OMP_4D_p_96', 'OMP_4D_p_cuda_96', {cuda_libraries}, {cuda_includes} );

%% add OMP to path

    path(fullfile(toolkit_path,['Regularization/OMP/OMP_4D_p' OMP_p_version]),path);
