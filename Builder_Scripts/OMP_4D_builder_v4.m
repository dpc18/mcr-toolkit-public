% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % Rubinstein, R., Zibulevsky, M., & Elad, M. (2008).
    % Efficient implementation of the K-SVD algorithm using batch orthogonal matching pursuit (No. CS Technion report CS-2008-08).
    % Computer Science Department, Technion.

%% Build the OMP_4D object code

    cd(proc_string(['Regularization/OMP/OMP_4D' OMP_version]));
   
    cmd = compile_nvcc( 'OMP_4D_cuda', c_compiler, GPU_compute_capability, [1 1 1 0 0], {}, {mex_includes,OMP_includes} );

%%

    clear mex;

    cmd = compile_mex( mex_path, c_compiler, [1 1 0], 'OMP_4D', 'OMP_4D_cuda', {cuda_libraries}, {cuda_includes} );

%% add OMP to path

    path(proc_string(['Regularization/OMP/OMP_4D' OMP_version]),path);

%% Create noisy phantom

    rng('default'); % make sure the noise is always the same for comparison
    rng(1);

    phant = load_nii(fullfile(toolkit_path,'Test_Data/contrast_res_phantom.nii.gz'));
    phant = phant.img;

    sigma = 0.001; % 0.01

    phantn = phant + sigma*randn(size(phant));

%% Load a learned dictionary

% Dictionary file

    dict_params.outname = 'test_v4_4.mat'; % 9.4848852e-05, 9x9x9, Bregman, 20 iterations, gain = 1.02, 10e6/5e6 patches, 2048 atoms, mu = 0.35
    
% Bregman parameters

    dict_params.delta_mu = 0.35;
    
% Dictionary parameters / initialization

    dict_params.blocksize = [9 9 9];
    prad = (dict_params.blocksize(1)-1)/2;
    dict_params.nvols = 1;

    sz = [size(phantn)];
    p = length(dict_params.blocksize); % dimensionality

    samp = 4; % revised version supports 1, 4, and 8 only to ensure reliable results

    dict_params.trainnum = 10e6;
    dict_params.maxusenum = 5e6;
    dict_params.gain = 1.02;
    dict_params.samp_gain = 0;
    dict_params.iternum = 20;

    % radial mask
    [x,y,z] = meshgrid(-prad:prad,-prad:prad,-prad:prad);
    dist = sqrt(x.^2 + y.^2 + z.^2);
    pmask = dist < 1.01*prad;
    pmask = repmat(pmask,[1 1 1 dict_params.nvols]);
    dict_params.prad = prad;
    dict_params.pmask = pmask;
    dict_params.maxatoms = round(sum(pmask(:))); % enforce an expected level of sparsity; legacy parameter
    
    dict_params.dictsize = 2048; % divisible by 64

%% If we are using an old dictionary

    load(dict_params.outname);
    
    dict_params.dict = D;
    
    temp = view_radial_dictionary( dict_params.dict, pmask, 1 );

    if isempty(javachk('awt'))
        imtool(mat2gray(temp));
    else
        disp('Java awt not available. Figure display suppressed.');
    end

%% Estimate phantom with OMP_4D and learned dictionary

    disp(sprintf('\n\nOMP applied to a single volume with a learned dictionary (expected RMSE: ~7.8453e-05)...\n\n'));

    dict_params.Edata = sqrt(sum(pmask(:))) * sigma * dict_params.gain;
    err = single(dict_params.Edata);

    szX = int32(size(phantn));
    X = single(phantn);
    D =  single(dict_params.dict);

    prad_in = int32(prad);
    na = int32(size(D,2));

    samp_in = int32(1);
    nvol_in = int32(dict_params.nvols);
    idx_rng = int32([0 na-1]);

    tic;

    d = OMP_4D(X, D, err, na, prad_in, szX, nvol_in, samp_in, idx_rng); % , single(whMat), single(invMat), single(mu));

    toc;

    d = reshape(d,szX);

    if isempty(javachk('awt'))
        imtool(mat2gray([phantn(:,:,83) abs(phant(:,:,83)-phantn(:,:,83)) d(:,:,83) abs(phant(:,:,83)-d(:,:,83))]));
    else
        disp('Java awt not available. Figure display suppressed.');
    end

    RMSE = @(a,b) sqrt(mean((a(:)-b(:)).^2));

    RMSE(d,phant)

%% Estimate phantom with OMP_4D and learned dictionary

    disp(sprintf('\n\nOMP applied to two volumes with a learned dictionary (expected RMSEs: ~6.1823e-05)...\n\n'));

    rng('default'); % make sure the noise is always the same for comparison
    rng(1);

    phantn1 = phant + sigma*randn(size(phant));
    phantn2 = phant + sigma*randn(size(phant));
    phantn_in = cat(4,phantn1,phantn2);

    szX = int32(size(phantn_in));
    X = single(phantn_in);
    D = single([dict_params.dict; dict_params.dict]);
    D = bsxfun(@minus,D,mean(D,1));
    D = bsxfun(@rdivide,D,sqrt(sum(D.^2)));

    dict_params.Edata = sqrt(sum(2*pmask(:))) * sigma * dict_params.gain; 
    err = single(dict_params.Edata);

    prad_in = int32(prad);
    na = int32(size(D,2));

    samp_in = int32(1);
    nvol_in = int32(szX(4));
    idx_rng = int32([0 na-1]);

    tic;

    d = OMP_4D(X, D, err, na, prad_in, szX, nvol_in, samp_in, idx_rng);

    toc;

    d = reshape(d,szX);

    if isempty(javachk('awt'))
        imtool(mat2gray([phantn_in(:,:,83,1) abs(phant(:,:,83)-phantn_in(:,:,83,1)) d(:,:,83,1) abs(phant(:,:,83)-d(:,:,83,1)); ...
                         phantn_in(:,:,83,2) abs(phant(:,:,83)-phantn_in(:,:,83,2)) d(:,:,83,2) abs(phant(:,:,83)-d(:,:,83,2))]));
    else
        disp('Java awt not available. Figure display suppressed.');
    end
    

    RMSE = @(a,b) sqrt(mean((a(:)-b(:)).^2));

    RMSE(d(:,:,:,1),phant)
    RMSE(d(:,:,:,2),phant)
