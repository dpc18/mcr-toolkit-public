% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % Tomasi, C., & Manduchi, R. (1998, January).
    % Bilateral filtering for gray and color images.
    % In Sixth international conference on computer vision (IEEE Cat. No. 98CH36271) (pp. 839-846). IEEE.
    
    % Takeda, H., Farsiu, S., & Milanfar, P. (2007).
    % Kernel regression for image processing and reconstruction.
    % IEEE Transactions on image processing, 16(2), 349-366.
    
    % Clark, D. P., Ghaghada, K., Moding, E. J., Kirsch, D. G., & Badea, C. T. (2013).
    % In vivo characterization of tumor vasculature using iodine and gold nanoparticles and dual energy micro-CT.
    % Physics in Medicine & Biology, 58(6), 1683.
    
    % Clark, D. P., & Badea, C. T. (2017).
    % Hybrid spectral CT reconstruction.
    % PloS one, 12(7), e0180324.

%% // Mex Interface with Matlab
% // v5: Use spatially adaptive noise estimation.
% 
% // [ Xf, noise_sig ] = jointBF4D( X, nvols, ntimes, A, At, size, radius, multiplier, gpu_idx, return_noise_map, Xr )
% // INPUTS:
% //                  X: (single*) vectorized data to be filtered; 3D volume x volumes to be jointly filter x time points to extend the filtration domain
% //              nvols: (int32)   number of volumes to jointly filter
% //             ntimes: (int32)   number of time points along which to extend the filtration domain
% //                  A: (single*) x,y,z factored resampling kernel (expected size: radius x 3)
% //                 At: (single*) temporal resampling weights (vector with ntimes entries; used to combine resampling results between timepoints; sums to 1)
% //               size: (int32*)  volume size in voxels (x,y,z)
% //             radius: (int32)   filter radius (width = 2*radius + 1)
% //         multiplier: (single*) scales filtration strength as this multiple of the estimated noise standard deviation (vector with nvols entries)
% //            gpu_idx: (optional) (int32)   hardware index pointing to the GPU to be used for BF (defaults to the system default)
% //   return_noise_map: (optional) (int32)   (0) do not return noise map; (1) return noise map (defaults to 0 to reduce memory transfers)
% //                 Xr: (optional) (single*) if size(Xr) == nx*ny*nz*nvols, take resampled values from these volumes; otherwise, compute resampled values with the A, At kernels (defaults to computing resampled values)
% //
% // OUTPUTS:
% //                 Xf: (single*) vectorized, filtered volumes ( prod(size) x nvols )
% //          noise_sig: (single*) returned if return_noise_map == 1; estimated noise standard deviation ( prod(size) x nvols )

% Constants declared within the code:
% __device__ const int MAX_NVOLS = 10;       // maximum number of volumes which can be jointly filtered
% __device__ const int KERNEL_DIAMETER = 13; // currently, 2*w+1 must equal this expected kernel diameter (i.e. w = 6)
% const int B_SIZE_2D = 16;                  // 2D slice operations
% const int B_SIZE_3D = 8;                   // 3D buffer operations
% const int B_SIZE_3D_2D = 8;                // Expensive 3D operations which output a 2D slice (e.g. BF)
% __device__ const float EPS = 1e-12;        // prevent division by 0

%% Compile bilateral filter v5

    fprintf('\nCompiling v5 of the BF...\n\n');

    cd(BF_v5);

    cmd = compile_nvcc( 'jointBF4D_cuda', c_compiler, GPU_compute_capability, [0 1 0 0 0], {}, {mex_includes,BF_includes} );

    clear mex;

    cmd = compile_mex( mex_path, c_compiler, [1 1 0], 'jointBF4D', 'jointBF4D_cuda', {cuda_libraries}, {cuda_includes,BF_includes} );

%%
%%%%%%

% Test jointBF4D v5: filter a single volume

%%%%%%

    cd(BF_v5);
    
%% Test code

    At = [1];

    sz = [256 256 256];
    w = 6;
    w2 = 0.8;
    q = 2*w;
    m = [1.5];
    sig = 0.1;

    A_ = single(make_A_approx_3D_v2(w,w2));

    X = zeros(sz,'single'); % GPU 1D texture size limit: 2^27 (i.e. 512^3)
    X1 = X; X2 = X;

    X1(128:end,:,:) = 0.1;
    X2(128:end,:,:) = 0.5;

    X1 = X1 + sig*randn(size(X1));
    X2 = X2 + sig*randn(size(X2));

%% Test 3D BF on a single, low contrast volume (X1)
% Perform kernel resampling on the GPU

    fprintf('\nTesting BF v5 (filter a single volume)...\n\n');

    X       = single(X1(:));
    nvols   = int32(1);
    ntimes  = int32(1);
    A_in    = single(A_);
    At_in   = single(At);
    sz_in   = int32(sz);
    w_in    = int32(w);
    m_in    = single(m);
    gpu_idx = int32(gpu_list(1));    % optional; if not specified, the system default is used
    return_noise_map = int32(1);     % optional; when 1, adds an additional output argument
    Xr      = zeros([1,1],'single'); % optional, does nothing when specified like this

    tic;
    [Xf,noise] = jointBF4D(X,nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map,Xr);
    toc;
    
    Xf = reshape(Xf,[sz(1) sz(2) sz(3)]);
    X_ = reshape(X,[sz(1) sz(2) sz(3)]);
    % imtool(mat2gray(Xf(:,:,w+1)));

    if isempty(javachk('awt'))
        figure(1); imagesc([X_(:,:,w+1) Xf(:,:,w+1)],[-0.1,0.5]);
        title('3D BF Results: Before vs. After');
        axis image off;
        colormap gray;
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end

    disp(['Expected noise coefficient: ' num2str(sig) ' vs. measured noise coefficient: ' num2str(mean(noise))]);
    
%% Test 3D BF on a single, low contrast volume (X1)
%  Pass in a resampled volume.
%  Expected results: 'GPU resampling vs. CPU resampling (% error): 0.03382'

    fprintf('\nTesting BF v5 (CPU vs. GPU resampling)...\n\n');

% Perform resampling

    d = X1;

    d  = reshape(d,[sz 1]);
    
    d = cat(1,d(w+1:-1:2,:,:,:),d,d(end-1:-1:end-w,:,:,:));
    d = cat(2,d(:,w+1:-1:2,:,:),d,d(:,end-1:-1:end-w,:,:));
    d = cat(3,d(:,:,w+1:-1:2,:),d,d(:,:,end-1:-1:end-w,:));
            
    d_in = d;

    d =     convn(convn(convn(d_in,A_(:,1),'same'),A_(:,1)','same'),reshape(A_(:,1),[1 1 2*w+1]),'same');
    d = d + convn(convn(convn(d_in,A_(:,2),'same'),A_(:,2)','same'),reshape(A_(:,2),[1 1 2*w+1]),'same');
    d = d + convn(convn(convn(d_in,A_(:,3),'same'),A_(:,3)','same'),reshape(A_(:,3),[1 1 2*w+1]),'same');

    d    = d(w+1:end-w,w+1:end-w,w+1:end-w,:);
    X1_r = reshape(d,[prod(sz) 1]);
    
    clear d d_in;
    
% Pass the resampled volume to the GPU and filter

    X       = single(X1(:));
    nvols   = int32(1);
    ntimes  = int32(1);
    A_in    = single(A_);
    At_in   = single(At);
    sz_in   = int32(sz);
    w_in    = int32(w);
    m_in    = single(m);
    gpu_idx = int32(gpu_list(1));
    return_noise_map = int32(1);
    Xr      = single(X1_r(:));

    [Xf_1,~] = jointBF4D(X,nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map,Xr);
    
    disp(['GPU resampling vs. CPU resampling (% error): ' num2str(100*norm(Xf_1(:)-Xf(:))/norm(Xf_1(:)))]);
    
%% Demonstrate adaptive noise estimation in BF v5
% NOTE: Adaptation is hard-wired to use 32x32 pixel patches with 16 pixel overlap in x and y.
    
    fprintf('\nDemonstrating local noise estimation with BF v5...\n\n');
    
    nvols   = int32(1);
    ntimes  = int32(1);
    A_in    = single(A_);
    At_in   = single(At);
    sz_in   = int32(sz);
    w_in    = int32(w);
    m_in    = single(m);
    gpu_idx = int32(gpu_list(1));
    return_noise_map = int32(1);
    Xr      = zeros([1,1],'single');
    
    Xp = repmat(reshape(phantom(256),[256,256,1]),[1,1,256]);
    N1 = 0.0*randn([256,256,256]);
    N2 = 0.3*randn([256,256,256]);
    
    weight = linspace(0,1,256);
    N = weight.*N2+(1-weight).*N1;
    
    Xpn = single(Xp + N);
    
    tic;
    [Xf_v5,noise_v5] = jointBF4D(Xpn(:),nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map,Xr);
    toc;
    
    disp(['Expected noise coefficient (?): ' num2str(0.15) ' vs. measured noise coefficient ' num2str(mean(noise_v5))]);
    
    Xf_v5 = reshape(Xf_v5,sz);
    noise_v5 = reshape(noise_v5,sz);

    if isempty(javachk('awt'))

        figure(2); imagesc([Xp(64:192,:,w+1) Xpn(64:192,:,w+1) Xf_v5(64:192,:,w+1) abs(Xp(64:192,:,w+1)-Xf_v5(64:192,:,w+1)) ],[0,0.8]);
        title('3D BF Results: Expected, Input, Output BFv5, Resid BFv5');
        axis image off;
        colormap gray;
        drawnow;

        figure(3); imagesc([0.3*repmat(reshape(weight,[1,256,1]),[129,1,1]),noise_v5(64:192,:,w+1)],[0,0.3]);
        title('Noise Mapping (Standard Deviation): Expected Noise Map, Measured Noise Map');
        axis image off;
        colorbar;
        drawnow;

    else
        disp('Java awt not available. Figure display suppressed.');
    end


%% Test Joint, 3D BF (3D + Energy).
% Filtration between a low contrast volume (X1) and a high contrast volume (X2) with the same noise level.
% Constructing a joint kernel will transfer image structure from the high contrast volume to the low contrast volume.

    fprintf('\nTesting BF v5 (joint filtration of two volumes)...\n\n');
    
    m = 2.5;

    X      = single(cat(2,X1(:),X2(:)));
    Xr     = zeros([1,1],'single');
    ntimes = int32(1);
    nvols  = int32(1);
    m_in   = single([m]);
    
    tic;
    [Xf_1,noise_1] = jointBF4D(single(X1(:)),nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map,Xr);
    toc;
    
    nvols  = int32(2);
    m_in   = single([m,m]);

    tic;
    [Xf_2,noise_2] = jointBF4D(X,nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map,Xr);
    toc;
    
    X_      = reshape(X1,sz);
    Xf_1    = reshape(Xf_1   ,sz);
    noise_1 = reshape(noise_1,sz);
    Xf_2    = reshape(Xf_2   ,[sz 2]);
    noise_2 = reshape(noise_2,[sz 2]);

    if isempty(javachk('awt'))
        figure(4); imagesc([X_(:,:,w+1) Xf_1(:,:,w+1) Xf_2(:,:,w+1,1) Xf_2(:,:,w+1,2)],[-0.1 0.6]);
        title('3D Joint BF Results: Original - 3D BF (low cont.) - Joint BF (low cont.) - Joint BF (high cont.)');
        axis image off;
        colormap gray;
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end

    disp(['Expected noise coefficients: ' num2str([sig sig]) ' vs. measured noise coefficient: ' num2str(squeeze(mean(noise_2,[1,2,3]))')]);

%% Test Joint, 3D BF (3D + Energy) w/ Rank-Sparse Kernel Regression (RSKR).
% Filtration between a low contrast volume (X1) and a high contrast volume (x2) with the same noise level.
% Performing RSKR allows stronger filtration of the difference between the two input images.
    
    fprintf('\nComparing joint BF with RSKR...\n\n');
    
    stride        = 1;
    lambda0       = 1.2;
    attn_water    = ones([1,2],'single');
    g             = 0.5;
    nE            = 2;
    expected_rank = 2;
    X             = single(cat(2,X1(:),X2(:)));
    
    tic;
    Xf_jBF = jointBF4D(X,nvols,ntimes,A_in,At_in,sz_in,w_in,single([lambda0 lambda0]),gpu_idx);
    toc;
    
    tic;
    Xf_v5 = RSKR_5D_v14_BF5( X, stride, w, w2, lambda0, sz, attn_water, g, nE, expected_rank, BF_v5, gpu_idx );
    toc;
    
    Xf_v5  = reshape(Xf_v5,[sz 2]);
    Xf_jBF = reshape(Xf_jBF,[sz 2]);
    X_     = reshape(X,[sz 2]);

    if isempty(javachk('awt'))
        figure(5); imagesc([X_(:,:,w+1) Xf_jBF(:,:,w+1,1) Xf_v5(:,:,w+1,1)],[-0.1 0.6]);
        title('3D Joint BF Results: Original (low cont.) - jBF - RSKR BFv5');
        axis image off;
        colormap gray;
        colorbar;
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end

    

%% Test 4D BF (3D + Time)
% - Extend the filtration domain by 1 phase before and after the phase to be filtered.
% - This cell demonstrates that spatio-temporal resampling correctly averages along the temporal dimension.

    fprintf('\nTesting temporal resampling and 4D filtration...\n\n');

    sig1 = 0.05; sig2 = 0.05; sig3 = 0.05;
    X1 = zeros(sz) + 0.2 + 0.05*randn(sz);
    X2 = zeros(sz) + 1   + 0.05*randn(sz);
    X3 = zeros(sz) + 0.7 + 0.05*randn(sz);

    % Weighted averaging coefficients for combining resampled intensities at each time point
    % within the filtration domain.
    % Must sum to 1.
    At = [0.2 0.6 0.2];

    X = single(cat(2,X1(:),X2(:),X3(:)));
    nvols = int32(1);
    ntimes = int32(3);
    At_in = single(At);
    m_in = single([m]);
    gpu_idx = int32(gpu_list(1));
    return_noise_map = int32(1);

    tic;
    [Xf_joint,noise] = jointBF4D(X,nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map);
    toc;

    Xf_joint = reshape(Xf_joint,sz);
    X1 = reshape(X1,sz);
    X2 = reshape(X2,sz);
    X3 = reshape(X3,sz);

    if isempty(javachk('awt'))
        figure(7); imagesc([X1(:,:,w+1) X2(:,:,w+1) Xf_joint(:,:,w+1) X3(:,:,w+1)],[-0.1 1.1]);
        title('4D BF Results: Time Point 1 - TP 2 - TP 2 Filtered - TP 3');
        axis image off;
        colormap gray;
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end    

    disp(['Expected noise coefficients: ' num2str([sig1 sig2 sig3])]);
    disp(['vs. measured noise coefficient: ' num2str(mean(noise))]);

    % Compute expected result

    a = sum([0.2 1 0.7].*[0.2 0.6 0.2]);

    R1 = exp(-(X1(:)-a).^2./(2*m*m*0.05*0.05));
    R2 = exp(-(X2(:)-a).^2./(2*m*m*0.05*0.05));
    R3 = exp(-(X3(:)-a).^2./(2*m*m*0.05*0.05));

    expected_val = sum(X1(:).*R1+X2(:).*R2+X3(:).*R3)./sum(R1+R2+R3);

    disp(['Expected value in the filtered volume (approx.): ' num2str(expected_val)]);
    disp(['Measured value in the filtered volume: ' num2str(mean(Xf_joint(:)))]);

%% Load test volumes for temporal filtration

    X = load_nii(proc_string('Test_Data/retro.nii.gz'));
    X = X.img;
    X = single(X);
    X = reshape(X,[256^3 3]);
    clear X2;
    
    Xref = load_nii(proc_string('Test_Data/SL3D_ph_new_sph1.nii.gz'));
    Xref = single(reshape(Xref.img,sz)); 

%% Test 4D, joint BF. (3D + time + energy)
% This is the most general filtration case for combined time and energy data.
% All other filtration cases are special cases of 4D, joint BF.
% 4D: extending the filtration domain along the time dimension
% joint: multiplying range kernels computed at different energies

% input: [ X_e1_t-1(:),X_e2_t-1(:)   X_e1_t(:),X_e2_t(:)   X_e1_t+1(:),X_e2_t+1(:) ]
% returns: [Xf_e1_t(:) Xf_e2_t(:)]

% in lieu of energy data, this example uses a temporal average which is jointly filtered with each phase

    % Weighted averaging coefficients for combining resampled intensities at each time point
    % within the filtration domain. This weighted, linear combination of the resampled
    % intensities yields the mean value used for all range kernels within the filtration domain.
    % Must sum to 1.
    At = [0.2 0.6 0.2];

    % 3 time point window ([X1 X2 X3]) - e.g. temporal contrast images (noisy images with artifacts representing what we are trying to recover)
    % 1 template (X0) - e.g. spectro-temporal average
    % columns [[X0 X1] [X0 X2] [X0 X3]] => yields the following joint filtration kernels within the filtration domain: [R(X0)*R(X1)*X1 R(X0)*R(X2)*X2 R(X0)*R(X3)*X3].
    % All spatial and temporal positions within the domain contribute equally.

    % 4D, joint BF of retrospective data
    X0 = mean(X,2);
    X_in = single([X0 X(:,1) X0 X(:,2) X0 X(:,3)]);

    nvols = int32(2); % two volumes to be jointly filtered per time point
    ntimes = int32(3); % three time points
    A_in = single(A_);
    At_in = single(At);
    sz_in = int32(sz);
    w_in = int32(w);
    m_in = single([m,m]);

    tic;
    X_out = jointBF4D(X_in,nvols,ntimes,A_in,At_in,sz_in,w_in,m_in);
    toc;
    
    X_out = reshape(X_out(:,2),sz);
    X_ = reshape(X(:,2),sz);

    if isempty(javachk('awt'))
        figure(8); imagesc([Xref(:,:,129) X_(:,:,129) abs(Xref(:,:,129)-X_(:,:,129)) X_out(:,:,129) abs(Xref(:,:,129)-X_out(:,:,129))],[0 1.1]);
        title('4D, Joint BF: Expected - Input - Residuals Before BF - Output - Residuals After BF');
        axis image off;
        colormap gray;
        drawnow;
    else
        disp('Java awt not available. Figure display suppressed.');
    end

%% Add relevant functions to Matlab path

    close all force;

    path(BF_v5,path);
