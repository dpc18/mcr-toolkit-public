% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % Kim, K., Ye, J. C., Worstell, W., Ouyang, J., Rakvongthai, Y., El Fakhri, G., & Li, Q. (2014).
    % Sparse-view spectral CT reconstruction using spectral patch-based low-rank penalty.
    % IEEE transactions on medical imaging, 34(3), 748-760.
    
    % Teukolsky, S. A., Flannery, B. P., Press, W. H., & Vetterling, W. T. (1992).
    % Numerical recipes in C: The Art of Scientific Computing Second Edition.
    % Cambridge University Press.

%% Build the pSVT object code

    cd(fullfile(toolkit_path,'Regularization/pSVT'));

    cmd = compile_nvcc( 'pSVT_cuda', c_compiler, GPU_compute_capability, [0 1 0 0 0], {}, {mex_includes} );

%% Compile C program w/ mex interface and ignored libraries

    clear mex;

    cmd = compile_mex( mex_path, c_compiler, [1 1 0], 'pSVT', 'pSVT_cuda', {cuda_libraries}, {cuda_includes} );

%% add pSVT to path

    path(fullfile(toolkit_path,'Regularization/pSVT'),path);
