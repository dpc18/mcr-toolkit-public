% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% Minimum Recommended System Specifications

    % => 1x NVIDIA GPU with 16 GB of GPU memory and support for compute capability 6.1 (Pascal) or higher
    %    When using multiple GPUs, the Toolkit currently expects all GPUs used to be similar. 
    % => NVIDIA CUDA Toolkit 9.0 or newer (https://developer.nvidia.com/cuda-toolkit)
    % => MATLAB version 2018a or newer with compatible C/C++ mex compiler (gcc for Linux; Visual C++ for Windows)
    %    Mex compiler settings: https://www.mathworks.com/help/matlab/matlab_external/changing-default-compiler.html
    %    Compatible compilers (Linux): https://www.mathworks.com/support/requirements/supported-compilers-linux.html
    %    Compatible compilers (Windows): https://www.mathworks.com/support/requirements/supported-compilers.html
    % => Linux operating system with NVIDIA driver support (highly recommended)
    %    -OR-
    %    Windows operating system (see environment variables below)
    % => 128 GB of system RAM (TR and METR iterative reconstruction)
    
%% External Dependencies:

    % TO DO: Consider adding support for nifti files included in MATLAB's Image Processing Toolbox.

    % Tools for NIfTI and ANALYZE image (mathworks.com/matlabcentral/fileexchange/8797-tools-for-nifti-and-analyze-image)
    
%% Versions / headers / libraries / dependencies
% Edit me as needed...

    if ispc

        % Windows
        
        % Force cudaMallocManaged to allocate GPU memory (behave like cudaMalloc) instead of zero copying from the host RAM.
        % This must be set before the CUDA context is established by calling CUDA code. (otherwise it has no effect)
        % This setting prevents oversubscription of GPU memory.
        % Streams can only be allocated on ONE GPU when this is set to a non-zero value. (unless peer-to-peer communication is available between those GPUs?)
        % Otherwise, there will be stream allocation errors.
        % For multi-gpu systems without peer-to-peer communication, use the 'CUDA_VISIBLE_DEVICES' environment variable so that MATLAB only
        % sees one GPU.
        %
        % The environment variables below can be added to a startup.m file in the [userpath] directory to have them
        % instantiated automatically when MATLAB is opened.
        % i.e. run: edit(fullfile(userpath,'startup.m'))
        % and copy in: setenv('CUDA_MANAGED_FORCE_DEVICE_ALLOC','1');
        %              setenv('CUDA_VISIBLE_DEVICES',num2str(0));
        
        gpu_list = int32([0]);
        setenv('CUDA_MANAGED_FORCE_DEVICE_ALLOC','1'); % replace cudaMallocManaged with cudaMalloc
        setenv('CUDA_VISIBLE_DEVICES',num2str(gpu_list(1))); % make sure only 1 GPU is visible
        
        % Update the GPU list to reflect the fact that only the selected GPU is visible.
        % (i.e. the originally selected GPU will always be seen at index 0)
        gpu_list = int32([0]);

        % code directories
        mex_includes = 'C:\Program Files\MATLAB\R2019a\extern\include';
        toolkit_path = 'F:\xray\Documents\MCR_Toolkit';

        % CUDA
        GPU_compute_capability = '61'; % hardware compute capability to compile for
        cuda_libraries = 'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.4\lib\x64';
        cuda_includes = 'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.4\include';

        % Compiler
        c_compiler = 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.29.30133\bin\Hostx64\x64';

        % Mex
        mex_path = 'mex';

        % Nifti toolkit
        nifti_toolkit = 'F:\Toolkits\NIfTI_20140122';

    else

        % Linux

        % code directories
        mex_includes = '/home/x-ray/Matlab_2021a/extern/include/';
        toolkit_path = '/home/x-ray/Documents/MCR_Toolkit_Public/';

        % CUDA
        GPU_compute_capability = '61'; % hardware compute capability to compile for
        cuda_libraries = '/usr/local/cuda/targets/x86_64-linux/lib/';
        cuda_includes  = '/usr/local/cuda/targets/x86_64-linux/include/';

        % Compiler
        c_compiler = '/usr/bin/gcc';

        % Mex
        mex_path = '/home/x-ray/Matlab_2021a/bin/mex';

        gpu_list = int32([0]);         % Use 1 GPU
        % gpu_list = int32([0,1]);     % Use 2 GPUs
        % gpu_list = int32([0,1,2,3]); % Use 4 GPUs

        % Nifti toolkit
        nifti_toolkit = '/media/x-ray/bbarchive/Toolkits/NIfTI_20140122';

    end

% Prevent escape sequences under Windows

    proc_string = @(s) strrep(fullfile(toolkit_path,s),'\','/');

% Code versions to compile / add to path

    % DD projection / backprojection operators
        
        op_version_number = 25;
        
    % Cylindrical projection rebinning
    
        rebin_number = 1;

    % Reconstruction headers
    
        includes = fullfile(toolkit_path,'global_include/');
        operator_includes = includes;

    % Orthogonal matching pursuit on the GPU
    
        OMP_version = '_v4'; % version 4 - allow patches sizes up to 8192...?!
        OMP_p_version = '_v2_96'; % inputs / outputs patches

        % OMP headers
        OMP_includes = includes;

    % Bilateral filtration (BF)
    
        BF_v5 = fullfile(toolkit_path,'Regularization/jointBF_4D_v5');
        BF_includes = includes;
        
%% External toolkits
% These should be set up manually as needed.
    
    % 1) Nifti toolkit for working with *.nii files
    % source: https://www.mathworks.com/matlabcentral/fileexchange/8797-tools-for-nifti-and-analyze-image
    path(nifti_toolkit,path);

%% Navigate to toolkit root directory

    cd(toolkit_path);

%% Add matlab support functions to path

    % Convex optimization functions
    path('Functions/Optimizers',path);

    % Various cardio-resp. gating functions
    path('Functions/Gating',path);

    % Frequency filters for analytical reconstruction
    path('Functions/FBP_Filters',path);

    % Separable approximations for 3D resampling kernels
    % A surrogate for Fourier domain FBP windows when preforming iterative reconstruction.
    path('Functions/Resampling_kernels',path);

    % Non-negative material decomposition (non-negative least-squares)
    path('Functions/Mat_decomp',path);

    % Make geometry files for GPU-based reconstruction
    path('Functions/Make_geo_files',path);
    
    % Helper functions for fast compiling
    path('Functions/Compile_templates',path);
    
    % pSVT regularization (patch-based singular value thresholding)
    % Combined RSKR + pSVT denoising functions
    path('Functions/pSVT',path);
    
    % Rank-sparse kernel regression (scaled SVD + iterative regularization w/ joint bilateral filtration)
    path('Functions/RSKR',path);
    
    % Faster and/or more accurate singular value decomposition
    path('Functions/SVD',path);
    
    % Support functions for KSVD
    path('Functions/OMP_KSVD',path);
    
    % Create compact masks for reconstructing on restricted FoVs
    path('Functions/Reconstruction_mask',path);
    
    % Utility functions for WFBP reconstruction
    path('Functions/WFBP',path);
    
    % Utility functions for Tam-Danielsson window construction
    path('Functions/TD_Window',path);
    
    % Other utility functions
    path('Functions/Misc',path);
    
    % Calibrate system geometry
    path('Functions/Geo_cal',path);
    
    % Projection pre-processing
    path('Functions/Projection_normalization',path);
    
    % Ring artifact correction code applied to projection
    path('Functions/Ring_correction',path);
    
    % Functions for working with DukeSim data
    path('Functions/DukeSim_Functions',path);
    
    % Functions for performing iterative reconstruction with the split Bregman method
    path('Iter_Recon_Functions',path);
    
    % Functions for parsing DukeSim parameter files for analytical or iterative reconstruction 
    path('DukeSim_Templates',path);
    
%% Compile support functions written in C

% Compute a double precision dot product from single precision inputs.

    cd(fullfile(toolkit_path,'Functions/Double_precision_dot_product'));
    
    path(fullfile(toolkit_path,'Functions/Double_precision_dot_product'),path);
    
    [ cmd ] = compile_mex( mex_path, c_compiler, [1 0 0], 'dds', '', {}, {} );
    
% Mutual information metric (used for geometric calibration)

    cd(fullfile(toolkit_path,'Functions/Mutual_information'));
    
    path(fullfile(toolkit_path,'Functions/Mutual_information'),path);
    
    [ cmd ] = compile_mex( mex_path, c_compiler, [1 0 0], 'MI', '', {}, {} );

%% Compile and test basic reconstruction operators

    cd(toolkit_path);
    
    run( sprintf('Builder_Scripts/DD_Init_v%i_Builder.m',op_version_number) );
    
%% Compile and test projection rebinning for cylindrical detector reconstruction

    cd(toolkit_path);
    
    run( sprintf('Builder_Scripts/CyRebin_Builder_v%i.m',rebin_number)  );

%% GPU-based Regularization - Bilateral filtration
% handles 3D, 3D + energy, 3D + time, and 3D + time + energy regularization
% v5: Instead of estimating noise globally using the central slice, estimates noise locally using partially overlapping
%     32x32 image patches. Given the same regularization parameters, this version will tend to perform slightly more regularization
%     than older versions of the filter.
% NOTE: Compiled limit of 10 jointly filtered volumes

    cd(toolkit_path);

    run('Builder_Scripts/JointBF4D_v5_Builder.m');
    
%% GPU-based Regularization - Orthogonal Matching Pursuit (OMP)

    cd(toolkit_path);
    
    % Volume => Patches => Dict. Encoded Patches => Volume
    run( sprintf(['Builder_Scripts/OMP_4D_builder' OMP_version '.m']) );
    
    cd(toolkit_path);
    
    % Patches => Dict. Encoded Patches
    % NOTE: This code does not run an associated unit test...
    run( sprintf(['Builder_Scripts/OMP_4D_p_builder' OMP_p_version(1:3) '.m']) );
    
%% GPU-based Regularization - Patch-based singular value thresholding

    cd(toolkit_path);

    % NOTE: This code does not run an assoicated unit test...
    run( 'Builder_Scripts/pSVT_builder.m' );

%% Save the modified pathdef file

    close all force;
    
    savepath
    