% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%%

    % Reconstruct helical, cone-beam CT data with 3 spatial dimensions,
    % up to 10 cardiac phases, and an arbitrary number of spectral channels.
    % Regularize the energy dimension using rank-sparse kernel regression (RSKR).
    % Regularize the time dimension using patch-based singular value thresholding (pSVT).

function [ X, RMSEs ] = Recon_5D_Data_v2( recon_template )

%% Unpack reconstruction template

    disp('Unpacking reconstruction template...');

    % Input / output

        run_name         = recon_template.run_name;       % identifier string for specific results

        outpath          = recon_template.outpath;        % location where results are saved

        computeRMSE      = recon_template.computeRMSE;    % (0) do not comptue RMSE (i.e. for in vivo data), (1) compute RMSE each time the reconstruction is updated (simulations)
        X_ref            = recon_template.X_ref;          % reference volume string
        
        do_save          = recon_template.do_save;        % (0) return final results only, (1) save final results only, (2) save intermediate results and final results

        Y                = recon_template.Y;              % projection data string, nifti file

        breg_iter        = recon_template.breg_iter;      % total number of times to update the reconstructed results (initialization + regularized iterations)
        
        bicgstabl_iter   = recon_template.bicgstabl_iter; % data fidelity update iterations (bicgstabl with l = 2)

        preview_slice    = recon_template.preview_slice;  % z slice used for previewing reconstructed results
        display_range    = recon_template.display_range;  % minimum and maximum fractions of the dynamic range of CT attenuation values for consistent display purposes
        
    % Projection / Backprojection
    
        int_params_f  = recon_template.int_params_f;  % int32 parameters
        % int_params    = recon_template.int_params;    % int32 parameters
        double_params = recon_template.double_params; % double precision parameters used for geometric calculations
        filt_name     = recon_template.filt_name;     % FDK filter
        geo_all       = recon_template.geo_all;       % geometry file, global geometry file...individual temporal geometry files no longer used
        Rt_aff        = recon_template.Rt_aff;        % affine transform
        rmask         = recon_template.rmask;         % volumetric masking
        gpu_list      = recon_template.gpu_list;      % GPU(s) to use for reconstruction ( Matlab getDevice(#) indices - 1), int32             
        
        Wt            = recon_template.Wt;            % temporal weights
        % snum          = recon_template.snum;        % number of subsets to use for reconstruction
        
        geo_lines     = recon_template.geo_lines;     % We need the geometry file for all projections to allow us to sub-select rows for temporal reconstruction

    % Sizes

        p   = recon_template.p;   % number of temporal phases
        e   = recon_template.e;   % number of spectral samples
        sz  = recon_template.sz;  % full volume size
        szy = recon_template.szy; % projection data size

    % Regularization
    
        mu = recon_template.mu; % regularization, fidelity
    
        % regularization, RSKR (energy)
        stride        = recon_template.stride;      % subsampling to deal with correlated noise
        w             = recon_template.w;           % filtration kernel diameter
        w2            = recon_template.w2;          % kernel resampling bandwidth
        lambda0       = recon_template.lambda0;     % regularization strength
        attn_water    = recon_template.attn_water;  % attempt to equalize the noise level between spectral samples, following water normalization
        g             = recon_template.g;           % exponential regularization scaling with singular value ratio ( lambda0*(E(1,1)./diag(E)').^(g) )
    
        % directory of the version of BF to use for RSKR
        BF_path       = recon_template.BF_path; 
    
        % regularization, 4D BF (time)
        % At     = recon_template.At;     % temporal resampling weights (sums to 1)
        % nvols  = recon_template.nvols;  % number of volumes to be jointly filtered
        % ntimes = recon_template.ntimes; % number of timepoints to include within the filter kernel
        
        % regularization, pSVT (time)
        apply_pSVT      = recon_template.apply_pSVT;      % (0) do not apply pSVT; (1) Apply pSVT when there is a time dimension
        w_pSVT          = recon_template.w_pSVT;          % kernel diameter, pSVT
        nu_pSVT         = recon_template.nu_pSVT;         % non-convex singular value exponent
        multiplier_pSVT = recon_template.multiplier_pSVT; % scalar multiplier for singular value threshold
        
        if e == 1 && p == 1
           
            A_r = single(make_A_approx_3D_v2( double(w), double(w2) ));
            
        end
        
    % clear template
        
        clear recon_template;
    
    % Bicgstabl parameters
    
        ell = 2;
        
%% Load projection data

    disp('Loading projection data...');

    Y = load_nii(Y); Y = Y.img;
    
    Y = single(Y);
    
    if length(size(Y)) == 4 && size(Y,4) > e
       
        disp('Based on the energy settings and the projection data dimensions, it is assumed you intend to reconstruct the first energy only...');
        
        Y = Y(:,:,:,1);
        
        attn_water = attn_water(1);
        
    end
    
    Y = reshape(Y,[prod(szy) e]);
    
%% Bregman variables

    disp('Initializing Bregman variables...');

    X = zeros([prod(sz) p e],'single');
    
    D1 = X * 1.0;
    V1 = X * 1.0;
    
    % D2 = X;
    % V2 = X;
    
    sigma    = zeros(1,e);
    
    mu_scale = zeros(1,e);
    
    RMSEs    = zeros(1,breg_iter+1,'single');
    
%% Setting up projection data and reconstruction operators for temporal reconstruction
% Problem: Reconstructions with mostly zero projection weights result in large amounts of unused data being copied to the GPU.
% Bad solution: Create subsets of projections with non-zero weights and re-initialize the reconstruction operators before each time, energy update.
% Better solution: Given enough system RAM, create a copy of the projections with non-zero weights for each time point and corresponding reconstruction
%                  operators once and reuse them. Helical data will require separate analytical (count normalized) and iterative reconstruction operator
%                  initializations.
% NOTE: Only 10 reconstruction allocations can exist at once! (if >10 timepoints, the code will have to be recompiled)

    Y_             = cell(1,p);
    szy_           = cell(1,p);
    recon_allocs_f = zeros([1,p],'uint64');
    Rtfs           = cell(1,p);
    geo_files_time = cell(1,p);
    Wts = cell(1,p);

    if p > 1  % we are reconstructing multiple time points

        disp('Pre-allocating projection data and reconstruction allocations for temporal reconstruction...');
        disp('(This uses significant memory resources, but provides the best performance.)');

        Y = reshape(Y,[szy e]);

        for t = 1:p

        % Find projections used for this time point

            idx = Wt(:,t) > 0;

            Wts{t} = Wt(idx,t);

        % Time point specific geometry file

            geo_files_time{t} = fullfile(outpath,[run_name '_geo' num2str(t) '.geo']);

            geo_lines_ = geo_lines(idx,:);

            save(geo_files_time{t},'geo_lines_','-ASCII'); % save the geometry file

        % Time point specific projections

            Y_{t}   = Y(:,:,idx,:);
            szy_{t} = size(Y_{t});
            Y_{t}   = reshape(Y_{t},[prod(szy_{t}(1:3)) e]);

        % Reconstruction operator(s)

            % Use int_params_f for analytical recon. to count normalize helical data.
            int_params_    = int_params_f;
            int_params_(3) = int32(szy_{t}(3));

            recon_allocs_f(t) = DD_init(int_params_(:), double_params(:), filt_name, geo_files_time{t}, Rt_aff(:), rmask, gpu_list);

            Rtfs{t} = @(y,w) DD_backproject(y,recon_allocs_f(t),int32(1),w);

        end

        clear Y;
    
    else % we are only reconstructing one time point
        
        Y     = reshape(Y,[prod(szy) e]);
        Y_{1} = Y;
        
        clear Y;
 
        szy_{1} = szy;
        
        geo_files_time{1} = geo_all;
        
        recon_allocs_f(1) = DD_init(int_params_f, double_params(:), filt_name, geo_files_time{1}, Rt_aff(:), rmask, gpu_list);

        Rtfs{1} = @(y,w) DD_backproject(y,recon_allocs_f(1),int32(1),w);
        
        Wts{1} = ones([szy(3),1]);

    end
    
%% Initialization

    disp('Initializing with analytical reconstruction...');

    for s = 1:e

        for t = 1:p

            X(:,t,s) = Rtfs{t}( Y_{t}(:,s), Wts{t} );

            disp(['Done with WFBP for energy ' num2str(s) ', timepoint ' num2str(t)]);

        end

    end

    % Save analytical initialization
    if ( do_save == 2 )

        save_nii(make_nii(squeeze(reshape(X,[sz p e])),double_params(3:5)),fullfile(outpath,[run_name '_FBP.nii']));

    end
    
    % Reconstruction mask (for regularization)
    mask = zeros(sz(1:2),'single');
    [x2, x1] = meshgrid((0:sz(1)-1)-sz(1)/2,(0:sz(2)-1)-sz(2)/2);
    mask(sqrt(x1.^2 + x2.^2) <= min(sz(1)/2,sz(2)/2)) = 1;
    mask = repmat(mask,[1 1 sz(3)]);
    mask = mask(:);
    clear x1 x2;
    
%% Reset reconstruction allocations for unfiltered backprojection, no count normalization

    disp('Updating reconstruction allocations for unfiltered backprojection...');
    
    clear DD_init recon_allocs_f;
    
    Rts = cell(1,p);
    Rs  = cell(1,p);
    
    recon_allocs = zeros([1,p],'uint64');
    
    for t = 1:p
        
        int_params_     = int_params_f;
        int_params_(3)  = int32(szy_{t}(3));
        int_params_(12) = 0; % ensure count normalization is disabled to algebraic reconstruction
        
        recon_allocs(t) = DD_init(int_params_(:), double_params(:), filt_name, geo_files_time{t}, Rt_aff(:), rmask, gpu_list);
        
        Rts{t} = @(y,w) DD_backproject(y,recon_allocs(t),int32(0),w);
        Rs{t}  = @(x,w) DD_project(x,recon_allocs(t),w);
        
    end
    
%% RMSE

    if (computeRMSE)
        
        RMSE = @(a,b) sqrt(mean((a(:)-b(:)).^2));
        
        disp('Loading reference data for RMSE computations...');
        X_ref = load_nii(X_ref);
        X_ref = single(X_ref.img);
        
        if e == 1
           
            disp('Using only the first energy of the reference data...');
            
            X_ref = X_ref(:,:,:,:,1);
            
        end
        
        if p == 1
           
            disp('Using the temporal average of the reference data...');
            
            X_ref = mean(X_ref,4);
            
        end
        
        disp('Computing RMSE values...');

        RMSEs(1) = RMSE(X_ref(:),X(:));
        disp(['iter 0, log10(RMSE), X: ' num2str(log10(RMSEs(1)))]);
        
    else
        
        RMSEs = 0;
        
    end
    
%% Open and dock figures

    set(0,'DefaultFigureWindowStyle','docked');
    for i = 1:breg_iter

        figure(i);

    end

%% Iterative reconstruction

    start_time = tic;

    for breg = 1:breg_iter
        
        %% Data fidelity updates
        
            disp(sprintf('\n\n'));
            disp(['Starting iteration ' num2str(breg)]);

            % initialization and regularization parameters

            if (breg == 1)

                mu_reg = zeros(1,e);

            end

            % Update X
            
            disp('Data fidelity updates...');

            for s = 1:e

                for t = 1:p
                    
                % Data fidelity update

                    b = Rts{t}( Y_{t}(:,s), Wts{t} ) + mu_reg(s) * ( D1(:,t,s) - V1(:,t,s) );

                    A = @(x,w) Rts{t}(Rs{t}(x,w),w) + mu_reg(s) * x;

                    tic;
                    [X(:,t,s), resvec] = bicgstabl_os(A, b, X(:,t,s), bicgstabl_iter, Wts{t}, ell);
                    toc;

                    disp(['Done with energy ' num2str(s) ', timepoint ' num2str(t) ': ']);
                    resvec
                    
                % Regularization strength scaling based on data norm
                % assumes mu_reg == 0 when breg == 1
                
                    if breg == 1 && t == 1
                
                        mu_scale(s) = norm( Rts{t}( Y_{t}(:,s), Wts{t} ) ) / norm( X(:,t,s) );
                    
                    end

                end

            end
            
            clear b;
            
        %% Save results

            if (do_save == 2 || ( (do_save == 1) && (breg == breg_iter) ))

                disp(['Saving iteration ' num2str(breg) ' results...']);

                save_nii(make_nii(reshape(X,[sz p e]),double_params(3:5)),fullfile(outpath,[run_name '_iter' num2str(breg) '.nii']));

            end
            
        %% Data-adaptive regularization
        
            if (breg == 1)

                disp('Scaling regularization parameters...');
                
                for s = 1:e
                    
                    % Compute noise level using the MAD (assumed Gaussian)
                    
                    HP = reshape(X(:,1,s),sz);
                    HP = 0.5*convn(convn(HP(:,:,round(sz(3)/2))./attn_water(s),[1; -1],'same'),[1 -1],'same');
                    
                    mask_ = reshape(mask,sz)==1;

                    sigma(s) = median(abs(HP(mask_(:,:,round(sz(3)/2))))) / 0.6745;
                    
                end
                
                sigma = sigma ./ min(sigma);

                disp('Mu regularization parameters: ');
                mu_reg = mu .* mu_scale .* sigma
                
                RSKR_weights = attn_water .* sigma;

            end

        %% Compute RMSE

            if (computeRMSE == 1)

                disp('Computing RMSE values...');

                RMSEs(breg+1) = RMSE(X_ref(:),X(:));
                disp(['iter ' num2str(breg) ', log10(RMSE), X: ' num2str(log10(RMSEs(breg+1)))]);

            end

        %% Preview results

            disp('Previewing results...');
            
            % Use the same windowing for each iteration
            if breg == 1
                
                X  = reshape(X,[sz p e]);
                X_ = X(:,:,preview_slice,1,1);
                X  = reshape(X,[prod(sz) p e]);
                
                min_int = min(X_(:));
                max_int = max(X_(:));
                
                clear X_;
                
                display_range = display_range .* [min_int max_int];
                
            end
            
            if p > 1 && e > 1
            
                phase1 = 1;
                phase2 = max(2,round(p/2));
                energy1 = 1;
                energy2 = max(2,round(e/2));
                title_string = ['Phases ' num2str(phase1) ', ' num2str(phase2) ' (rows) - Energies ' num2str(energy1) ', ' num2str(energy2) ' (cols)'];

                slice1 = X(:,phase1,energy1); slice1 = reshape(slice1,sz); slice1 = slice1(:,:,preview_slice);
                slice2 = X(:,phase2,energy1); slice2 = reshape(slice2,sz); slice2 = slice2(:,:,preview_slice);
                slice3 = X(:,phase1,energy2); slice3 = reshape(slice3,sz); slice3 = slice3(:,:,preview_slice);
                slice4 = X(:,phase2,energy2); slice4 = reshape(slice4,sz); slice4 = slice4(:,:,preview_slice);

                preview = cat(1,cat(2,slice1,slice2),cat(2,slice3,slice4));

                figure(breg);
                title(title_string);
                imagesc(preview,display_range); axis image off; colormap gray;

                drawnow;

                pause(0.5);

                clear slice1 slice2 slice3 slice4 preview;
            
            elseif p > 1 && e == 1
                
                phase1 = 1;
                phase2 = max(2,round(p/2));

                title_string = ['Phases ' num2str(phase1) ', ' num2str(phase2)];

                slice1 = X(:,phase1,1); slice1 = reshape(slice1,sz); slice1 = slice1(:,:,preview_slice);
                slice2 = X(:,phase2,1); slice2 = reshape(slice2,sz); slice2 = slice2(:,:,preview_slice);

                preview = cat(2,slice1,slice2);

                figure(breg);
                title(title_string);
                imagesc(preview,display_range); axis image off; colormap gray;

                drawnow;

                pause(0.5);

                clear slice1 slice2 preview;
                
            elseif p == 1 && e > 1
                
                energy1 = 1;
                energy2 = max(2,round(e/2));
                title_string = ['Energies ' num2str(energy1) ', ' num2str(energy2)];

                slice1 = X(:,1,energy1); slice1 = reshape(slice1,sz); slice1 = slice1(:,:,preview_slice);
                slice2 = X(:,1,energy2); slice2 = reshape(slice2,sz); slice2 = slice2(:,:,preview_slice);

                preview = cat(2,slice1,slice2);

                figure(breg);
                title(title_string);
                imagesc(preview,display_range); axis image off; colormap gray;

                drawnow;

                pause(0.5);

                clear slice1 slice2 preview;
                
            else % 3D
                
                slice1 = X(:,1,1); slice1 = reshape(slice1,sz); slice1 = slice1(:,:,preview_slice);
                
                title_string = '3D Reconstruction';
                
                figure(breg);
                title(title_string);
                imagesc(slice1,display_range); axis image off; colormap gray;

                drawnow;

                pause(0.5);
                
                clear slice1 preview;
                
            end

        %% Regularization...
        % Skip on the last iteration.

        if breg ~= breg_iter
            
        % Add residuals back

            D1 = X + V1;

        % Add noise outside of the reconstruction mask to avoid underestimating local noise at the edges of the reconstruction mask

            sigma0 = zeros(1,e);

            for s = 1:e

                HP = reshape(D1(:,1,s),sz);
                HP = 0.5*convn(convn(HP(:,:,round(sz(3)/2))./attn_water(s),[1; -1],'same'),[1 -1],'same');

                mask_ = reshape(mask,sz)==1;

                sigma0(s) = median(abs(HP(mask_(:,:,round(sz(3)/2))))) / 0.6745;

            end

            N  = bsxfun(@times,randn([prod(sz)*p e]),sigma0.*attn_water);
            D1 = bsxfun(@times,reshape(D1,[prod(sz) p*e]),mask) + bsxfun(@times,reshape(N,[prod(sz) p*e]),1-mask);
            clear N;
            
        % Apply a different regularizer based on the reconstruction dimensions
            
            if p > 1 % Apply RSKR to energy dimension, pSVT along the time dimension

                disp('RSKR + pSVT (3D + time or 3D + time + energy problem)...');

            else % Apply RSKR to the energy dimension
                
                disp('RSKR (3D or 3D + energy problem)...');
                
            end
            
            D1 = reshape(D1,[prod(sz)*p e]);
            
            D1 = RSKR_pSVT_5D_v4( D1, [sz(1:3) p], stride, w, w2, lambda0, g, RSKR_weights, apply_pSVT, reshape(mask,sz(1:3)), w_pSVT, nu_pSVT, multiplier_pSVT, gpu_list(1) );
            
            % Apply mask to remove added noise, set masked values to 0
            D1 = reshape(bsxfun(@times,reshape(D1,[prod(sz) p*e]),mask),[prod(sz) p e]);
            
        % Update residuals
                
            V1 = X + V1 - D1;
            
        % Save itermediate results for debugging, parameter calibration
            
            if do_save == 2

                disp(['Saving iteration ' num2str(breg) ' RSKR results...']);

                save_nii(make_nii(reshape(X ,[sz p e]),double_params(3:5)),fullfile(outpath,[run_name '_iter' num2str(breg) '_RSKR_X.nii'] ));
                save_nii(make_nii(reshape(V1,[sz p e]),double_params(3:5)),fullfile(outpath,[run_name '_iter' num2str(breg) '_RSKR_V.nii'] ));
                save_nii(make_nii(reshape(D1,[sz p e]),double_params(3:5)),fullfile(outpath,[run_name '_iter' num2str(breg) '_RSKR_D.nii']));

            end
                
        end

    end

    time = toc(start_time);
    
    disp(['Total reconstruction time for ' run_name ': ' num2str(time) ' seconds'])

end



