% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ X ] = RSKR_5D_v14( X, stride, w, w2, lambda0, sz0, attn_water, g, nE, expected_rank, BF_path )
% function [ U, E, V, U0 ] = RSKR_5D_v11( X, stride, w, w2, lambda0, sz0, attn_water, g, nE, expected_rank, BF_path )
% Make sense of data with something to hide...
% Includes post-filtration resampling.

    % Handle cases where multiple volumes are stacked along the z axis
    
        sz_z = sz0(3);

        if length(sz0) == 4

            sz0 = [sz0(1:2) sz0(3)*sz0(4)];

        end

    cdir = pwd;

    mo = @(x) mosaic(x,stride,sz0);
    demo = @(x) demosaic(x,stride,sz0);
    
    sz = size(X);
    X = bsxfun(@rdivide,X,attn_water);
    % X = double(X);
    
    % SVD
        
    % [U,E,V] = svd(X,0);
    
    [ U, E, V ] = eig_SVD( X, 1, 1 );

    % Prevent ill-conditioning
    sub_sig = sum(diag(E) < 1e-4*E(1,1));  
    range = 1:min((size(X,2)-sub_sig),nE); % truncate to the number of energies
    U = U(:,range);
    E = E(range,range);
    V = V(:,range);
    sz(2) = length(range);

    U = single(U);
    E = single(E);
    V = single(V);
    
    clear X;

    % Regularization stength

    mu = lambda0*(E(1,1)./diag(E)').^(g)

    % BF setup

    A_r = single(make_A_approx_3D_v2( double(6), double(w2) ));
    cd(BF_path);
        
    % to regularize strided volumes (center of first strided volume)
    
        measure_z_slice1 = int32( sz_z / ( 2 * stride ) );
        
    % to regularize normal volumes (center of first volume)
    
        measure_z_slice2 = int32( sz_z / 2 );

    BF1 = @(x, xr) jointBF4D(single(x),single(xr),int32(sz(2)),int32(1),A_r,single(1),int32(sz0),int32(w),single(mu),measure_z_slice1);
    BF2 = @(x, xr) jointBF4D(single(x),single(xr),int32(sz(2)),int32(1),A_r,single(1),int32(sz0),int32(w),single(mu),measure_z_slice2);
    
    d = U*0;
    v = d;
    delta_norm = 1;
    U0 = U;
    iter = 0;
    
    % double precision dot product
    % dd = @(x,y) single(double(x)'*double(y)); % Matlab version (fast, but uses tons of RAM)
    dd = @(x,y) ddm(single(x),single(y)); % CPU version (slower, but uses almost no additional RAM)
    
    % while (delta_norm >= 0.01) % Hybrid paper
    while iter < 3
        
        iter = iter + 1;

        if (stride == 1)
            
            d = BF2(U+v,U+v);

        else
            
            disp('Performing BF1...');
            d  = mo(U+v);
            d  = BF1(d,d);
            d  = demo(d);
            disp('Performing BF2...');
            d2 = BF2(U+v,U+v);
            disp('Averaging BFs...');
            d  = ( d + d2 )./2;
            clear d2;

        end

        if (w2 > 0.01)
            
            disp('Performing resampling...');
            % inline to save memory...
        
            % d = resamp(d,A_r,sz0);
            
            e_ = size(d,2);
            d  = reshape(d,[sz0 e_]);
            d  = padarray(d,[6 6 6 0],'symmetric');
            
            for s = 1:e_
                
                d_in = d(:,:,:,s);
                
                d(:,:,:,s) =              convn(convn(convn(d_in,A_r(:,1),'same'),A_r(:,1)','same'),reshape(A_r(:,1),[1 1 2*w+1]),'same');
                d(:,:,:,s) = d(:,:,:,s) + convn(convn(convn(d_in,A_r(:,2),'same'),A_r(:,2)','same'),reshape(A_r(:,2),[1 1 2*w+1]),'same');
                d(:,:,:,s) = d(:,:,:,s) + convn(convn(convn(d_in,A_r(:,3),'same'),A_r(:,3)','same'),reshape(A_r(:,3),[1 1 2*w+1]),'same');
                
            end
            
            clear d_in;
            
            d = d(w+1:end-w,w+1:end-w,w+1:end-w,:);
            d = reshape(d,[prod(sz0) e_]);
        
        end
        
        disp('Updating residuals...');
        
        d(~isfinite(d)) = 0;

        v = v + U - d;
        
        % norm_Ax_b = norm( reshape( U - bsxfun(@rdivide, U0 + (d-v)*diag(mu), 1 + mu), [prod(sz0)*sz(2) 1]) );
        % norm_b = norm( reshape(bsxfun(@rdivide, U0 + (d-v)*diag(mu), 1 + mu), [prod(sz0)*sz(2) 1]) );
        
        % delta_norm = norm_Ax_b ./ norm_b;
        
        disp('Updating solution...');
        
        U = bsxfun(@rdivide, U0 + (d-v)*diag(mu), 1 + mu);
        
        % disp(['Iter ' num2str(iter) ', Convergence criterion: ' num2str(delta_norm)]); % ' (vs. 0.01)']);
        
        disp('The convergence criterion is commented out to save memory...');
        
    end
    
    clear U0 d v;
    
    X = bsxfun(@times,U(:,1:expected_rank)*E(1:expected_rank,1:expected_rank)*V(:,1:expected_rank)',attn_water);
    
    cd(cdir);
    
end

function [U] = resamp(U,A,sz0)

    w = 6;

    e = size(U,2);
    U = reshape(U,[sz0 e]);
    U = padarray(U,[w w w 0],'symmetric');
    
    for s = 1:e
        
        U(:,:,:,s) = convn(convn(convn(U(:,:,:,s),A(:,1),'same'),A(:,1)','same'),reshape(A(:,1),[1 1 2*w+1]),'same') + ...
                     convn(convn(convn(U(:,:,:,s),A(:,2),'same'),A(:,2)','same'),reshape(A(:,2),[1 1 2*w+1]),'same') + ...
                     convn(convn(convn(U(:,:,:,s),A(:,3),'same'),A(:,3)','same'),reshape(A(:,3),[1 1 2*w+1]),'same');

    end
    
    U = U(w+1:end-w,w+1:end-w,w+1:end-w,:);
    U = reshape(U,[prod(sz0) e]);

end

function [ u ] = nan_check(u)

    u(~isfinite(u)) = 0;

end

function [ X ] = mosaic(X_,stride,sz)

    if (stride == 1)
       
        X = X_;
        
        return;
        
    end

    sz2 = [sz size(X_,2)];
    
    X_ = reshape(X_,sz2);
    
    X = X_;
    
    x = 1; y = 1; z = 1;
    
    for x_ = 1:stride
        
        for y_ = 1:stride
            
            for z_ = 1:stride
                
                lenx = length(x_:stride:sz(1));
                leny = length(y_:stride:sz(2));
                lenz = length(z_:stride:sz(3));
                
                X(x:(x+lenx-1),y:(y+leny-1),z:(z+lenz-1),:) = X_(x_:stride:sz(1),y_:stride:sz(2),z_:stride:sz(3),:);
                
                z = z + lenz;
                
            end
            
            z = 1;
            y = y + leny;
            
        end
        
        y = 1;
        x = x + lenx;
        
    end
    
    X = reshape(X,[prod(sz) sz2(end)]);

end

function [ X ] = demosaic(X_,stride,sz)

    if (stride == 1)
       
        X = X_;
        
        return;
        
    end

    sz2 = [sz size(X_,2)];
    
    X_ = reshape(X_,sz2);
    
    X = X_;
    
    x = 1; y = 1; z = 1;
    
    for x_ = 1:stride
        
        for y_ = 1:stride
            
            for z_ = 1:stride
                
                lenx = length(x_:stride:sz(1));
                leny = length(y_:stride:sz(2));
                lenz = length(z_:stride:sz(3));
                
                % X(x:(x+lenx-1),y:(y+leny-1),z:(z+lenz-1),:) = X_(x_:stride:sz(1),y_:stride:sz(2),z_:stride:sz(3),:);
                X(x_:stride:sz(1),y_:stride:sz(2),z_:stride:sz(3),:) = X_(x:(x+lenx-1),y:(y+leny-1),z:(z+lenz-1),:);
                
                z = z + lenz;
                
            end
            
            z = 1;
            y = y + leny;
            
        end
        
        y = 1;
        x = x + lenx;
        
    end
    
    X = reshape(X,[prod(sz) sz2(end)]);

end

function [ M ] = ddm(a,b)
 % compute a'*b with matrices 
% accuracy of single(double(a)'*double(b)) at a fraction of the memory usage, but ~2x execution time

    M = zeros(size(a,2),size(b,2));

    for i = 1:size(a,2)
        
        for j = 1:size(b,2)
            
            M(i,j) = dds(a(:,i),b(:,j));
            
        end
        
    end

end