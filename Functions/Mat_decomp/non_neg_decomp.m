% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ C ] = non_neg_decomp(X, M)
% Pass the arguments in as single precision to save memory. Only the needed data gets converted to double.

    X = single(X);
    C = zeros(size(X,1),size(M,1),'single');
    M = double(M);
    
    % idx = find(min(C,[],2) < 0 & max(C,[],2) > 0);
    % M = double(M);
    step = 50e6;
    N = size(X,1);
    
    for j = 1:ceil(N/step)
        
        % initial decomposition
        idx = ((j-1)*step + 1):min(N,j*step);
        C(idx,:) = single(double(X(idx,:)) / M);
        
        subidx = idx(min(C(idx,:),[],2) < 0 & max(C(idx,:),[],2) > 0);
        
        if ~isempty(subidx)
    
            C0 = double(C(subidx,:));
            X0 = double(X(subidx,:));

            parfor i = 1:length(subidx)

                v = C0(i,:);
                q = X0(i,:);

                while( min(v) < 0 )

                    temp = v > 0;

                    v(temp) = q / M(temp,:);

                    v(~temp) = 0;

                end

                C0(i,:) = v;

            end

            C(subidx,:) = single(C0);
        
        end
        
        disp([num2str(100*min(N,j*step)/N) '%']);
    
    end
    
    C(C < 0) = 0;

end