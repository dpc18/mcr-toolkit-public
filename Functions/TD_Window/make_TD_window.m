% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % Danielsson, P. E., Edholm, P., Eriksson, J., & Seger, M. M. (1997, June).
    % Towards exact reconstruction for helical cone-beam scanning of long objects.
    % A new detector arrangement and a new completeness condition.
    % In Proc. 1997 Meeting on Fully 3D Image Reconstruction in Radiology and Nuclear Medicine (pp. 141-144). Pittsburgh.
    
    % Tam, K. C., Samarasekera, S., & Sauer, F. (1998).
    % Exact cone beam CT with a spiral scan.
    % Physics in Medicine & Biology, 43(4), 1015.
    
%%

function [TD_window] = make_TD_window( dsd, dso, db, dv, nu, nv, uc, vc, AMu, AMv, pitch, np_rot, ...
                                       zo, window_type, du_out, mag, buffer_fraction, rotdir )
                                   
    rotz = @(x) [cos(x) -sin(x) 0;
                 sin(x)  cos(x) 0;
                 0       0      1];
             
    if window_type == 0 % window along the z axis only
        
        limit = dv * nv * pitch  * 180/360 / 2.0;
        
        g_v = (0:nv-1) - (vc+AMv);
        
        TD_window = zeros(nu,nv,'single');
        
        idx = floor(abs(g_v)) <= limit;
        
        TD_window(:,idx) = 1.0;
        
        % idx = abs(g_v) > limit & abs(g_v) <= (limit+dv);
        % TD_window(:,idx) = repmat( 1.0 - (abs(g_v(idx)) - limit)/(dv), [nu 1] );
        
        idx = abs(g_v) > limit & abs(g_v) <= ((1+buffer_fraction)*limit);
        
        TD_window(:,idx) = repmat( 1.0 - (abs(g_v(idx)) - limit)/(buffer_fraction*limit), [nu 1] );
        
    end

    if window_type == 1 || window_type==2  % 3rd generation CT scanner geometry

    % Source offset from central ray defined by the detector array element of intersection

        s_offset = [ 0; dsd * sin( AMu * db ); AMv * dv ]; % convert from detector element offset to x,y,z offset

    % Construct a grid of x,y,z points on the detector at theta = 0

        % reference source position
        s_0  = [0;0;0];
        c_0  = [dsd;0;0];
        iso0 = [dso;0;0];
        
    % Equate rotation around the source position to rotation around the isocenter
    
        g_u = (0:nu-1) - (uc+AMu);
        
        theta = atan2( dsd * sin( g_u * db ), dsd * cos( g_u * db ) - dso );
        
    % Compute the TD window at theta = 0, z = 0
    
        g_u = (0:nu-1) - (uc+AMu);
        g_v = (0:nv-1) - (vc+AMv);
        
        gamma_p =  pi + theta;
        gamma_m = -pi + theta;
        
        s_ = (s_0 - iso0 + s_offset + [0;0;zo]);
        
        mm_per_proj   = pitch * ( 1 / np_rot ) * nv * dv / mag;
        mm_per_radian = mm_per_proj * np_rot / (2 * pi);
        
        s_gp  = arrayfun( @(d) rotz(d) * s_, gamma_p, 'UniformOutput', false);
        s_gp = cell2mat(s_gp) + [0;0;1] * mm_per_radian .* gamma_p;
        
        s_gm = arrayfun( @(d) rotz(d) * s_, gamma_m, 'UniformOutput', false);
        s_gm = cell2mat(s_gm) + [0;0;1] * mm_per_radian .* gamma_m;

        xp = dsd .* cos( g_u .* db ) - dso;
        yp = dsd .* sin( g_u .* db );
        
        % a_p = 1 - ( xp ./ s_gp(1,:) );
        % a_m = 1 - ( xp ./ s_gm(1,:) );
        
        a_p = ( xp - s_(1,:) ) ./ ( s_gp(1,:) - s_(1,:) );
        a_m = ( xp - s_(1,:) ) ./ ( s_gm(1,:) - s_(1,:) );
        
        % Why does this calculation come out wrong?
        % a_p2 = ( yp - s_(2,:) ) ./ ( s_gp(2,:) - s_(2,:) );
        % a_m2 = ( yp - s_(2,:) ) ./ ( s_gm(2,:) - s_(2,:) );
        
        % disp(['Slope consistency: ' num2str(norm(a_p-a_p2)+norm(a_m-a_m2))]);
        
        v_p = (a_p .* s_gp(3,:) + (1.0 - a_p) .* s_(3) - c_0(3))./dv;
        v_m = (a_m .* s_gm(3,:) + (1.0 - a_m) .* s_(3) - c_0(3))./dv;
        
    % Construct TD window
        
        TD_window = zeros(nu,nv);
        
        for i = 1:nu
        
            end_floor   = floor(v_p(i) + vc);
            start_floor = floor(v_m(i) + vc);
        
            end_ceil   = ceil(v_p(i) + vc);
            start_ceil = ceil(v_m(i) + vc);
        
            TD_window(i,start_ceil:end_floor) = 1.0;
        
            TD_window(i,end_ceil)    = ( v_p(i) + vc - end_floor   ) / min(end_ceil   - end_floor,1.0);
            TD_window(i,start_floor) = ( start_ceil - (v_m(i) + vc)) / min(start_ceil - start_floor,1.0);
        
        end
        
        % first_idx = repmat(v_m',[1,nv]);
        % last_idx  = repmat(v_p',[1,nv]);
        % 
        % g_v_ = repmat(g_v,[nu,1]);
        % 
        % idx = (g_v_ >= first_idx) & (g_v_ <= last_idx);
        % TD_window(idx) = 1.0;
        % 
        % collimation = dv * nv / mag;
        % buffer = buffer_fraction * collimation * pitch / dv;
        % 
        % idx            = (g_v_ < first_idx) & (g_v_ >= first_idx - buffer);
        % TD_window(idx) = (g_v_(idx) - ( first_idx(idx) - buffer ) ) ./ buffer ;
        % 
        % idx            = (g_v_ > last_idx) & (g_v_ <= last_idx + buffer);
        % TD_window(idx) = (last_idx(idx) + buffer - g_v_(idx)) ./ buffer;
        
        % TD_window = TD_window .* cos( ( pi / 2 ) .* abs( g_v ) ./ max(abs(g_v)) ).^2;
     
    end
        
    if window_type == 2  % adjust for cone-parallel rebinning
        
            delta_theta = 360 / np_rot; % degrees / projection

        % Step 1: Since all rebinning operations are relative to the angle of the input projection,
        %         set up a virtual detector at theta = 0.

            u_out = repmat( du_out * ((0:nu-1)'-uc-AMu) , [1 , nv] );
            v_out = repmat( dv     * ((0:nv-1) -vc) + zo, [nu, 1 ] );

            virt_det      = zeros(3,nu*nv);
            % x axis is zero at theta = 0
            virt_det(2,:) = u_out(:); % virtual u axis aligned along +y
            virt_det(3,:) = v_out(:); % virtual v axis aligned along +z

        % Step 2: Compute coordinates to interpolate from

            p    = u_out * mag ;
            beta = asin( p / dsd );
            beta = reshape(beta,[1,nu*nv]);

            us = beta ./ db + uc;

            vidx = (virt_det(3,:)-zo)./dv;
            vs   = vidx - rotdir * mm_per_proj * beta ./ ( delta_theta * pi / 180 ) ./ dv + vc;

        % Step 3: Interpolate from the TD window

            [a,b] = meshgrid(0:nu-1,0:nv-1);

            TD_window = interp2( a, b, TD_window', reshape(us,[nu,nv])', reshape(vs,[nu,nv])','linear', 0 )';
        
    end
        
    if ~(window_type == 0 || window_type == 1 || window_type==2)
        
        TD_window = -1;
        
    end
    
    TD_window = single(TD_window);

end

