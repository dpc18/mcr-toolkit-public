% %     Copyright (C) 2023, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.


function [Y, np] = load_nii_sliced( projection_file, nu, nv, idx )
% Inputs
% 1) projection_file: binary file of projection data to read
% 2) nu: number of detector columns
% 3) nv: number of detector rows
% 4) idx: array of indices corresponding to projections to load from a nifti file
%
% Outputs:
% 1) Y: loaded projections (0 if no projections were loaded)
% 2) np: number of projections in the specified projection_file based on the file size

    hdr = load_nii_hdr(projection_file);
    
    offset = hdr.dime.vox_offset;
    
    dims = hdr.dime.dim(2:4);
    
    if (nu ~= dims(1)) || (nv ~= dims(2))
        
        error('Specified nifti file projection dimensions do not match nifti header dimensions.');
        
    end
    
    np = dims(3);
    
    if max(idx) > np
           
        error(['Requested projection index (' num2str(max(idx)) ') exceeds number of available projections (' num2str(np) ').']);
            
    end
        
    if min(idx) < 1

        error('All requested projection indices must be greater than or equal to one.');

    end
    
    Y = zeros(nu,nv,length(idx));
        
    c = 1;
    
    fid = fopen(projection_file,'r','l');

    for i = 1:length(idx)

        fseek(fid, (idx(i)-1)*nu*nv*4 + offset, 'bof');

        Y(:,:,c) = fread(fid,[nu nv],'float=>float');

        c = c + 1;

    end

    np = length(idx);
    
end

