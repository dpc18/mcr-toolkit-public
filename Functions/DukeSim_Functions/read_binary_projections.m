% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [ Y, np ] = read_binary_projections(projection_file,nu,nv,get_projs)
% Inputs
% 1) projection_file: binary file of projection data to read
% 2) nu: number of detector columns
% 3) nv: number of detector rows
% 4) get_projs: (0) only get the number of projections based on the file size
%               (1) load all projections and return the number of projections
%               (array of indices) only load specified projections
%
% Outputs:
% 1) Y: loaded projections (0 if no projections were loaded)
% 2) np: number of projections in the specified projection_file based on the file size

    fid = fopen(projection_file,'r','l');

    fseek(fid, 0, 'eof');
    filesize = ftell(fid);

    np = round(filesize/(nu*nv*4));

    if length(get_projs) > 1 % load specific projections
        
        if max(get_projs) > np
           
            error(['Requested projection index (' num2str(max(get_projs)) ') exceeds number of available projections (' num2str(np) ').']);
            
        end
        
        if min(get_projs) < 1
           
            error('All requested projection indices must be greater than or equal to one.');
            
        end
        
        fseek(fid, 0, 'bof');
        
        Y = zeros(nu,nv,length(get_projs));
        
        c = 1;
        
        for i = 1:length(get_projs)
           
            fseek(fid, (get_projs(i)-1)*nu*nv*4, 'bof');
            
            Y(:,:,c) = fread(fid,[nu nv],'float=>float');
            
            c = c + 1;
            
        end
        
        np = length(get_projs);
    
    elseif get_projs == 1 % load in all projections

        fseek(fid, 0, 'bof');
        Y = fread(fid,filesize,'float=>float');

        fclose(fid);

        Y = reshape(Y,[nu,nv,np]);
    
    else % load in no projections
        
        Y = 0;
        
    end

end

