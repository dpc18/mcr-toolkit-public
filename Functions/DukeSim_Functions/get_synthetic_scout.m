% %     Copyright (C) 2023, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [ scout ] = get_synthetic_scout( Y, scout_size, z_pos, slicewidth0, table_feed )
% Generate something like a scout scan from a series of projections
% acquired at the same angle in consecutive rotations.
% Probably only works well for smaller pitch values.

% Inputs
% 1) Y: MDCT projection data from consecutive rotations to use for scout scan generation
% 2) scout_size: number of rows in the scout scan
% 3) z_pos: z position at which each projection was acquired
% 4) slicewidth0: detector row height at isocenter
% 5) table_feed: +/- 1
%
% Outputs:
% 1) scout: synthetic scout image

    nu = size(Y,1);
    nv = size(Y,2);
    np = size(Y,3);

    scout = zeros( [nu,scout_size-1], 'single' );
    N     = zeros( [nu,scout_size-1], 'single' );

    % Assumes even number of detector elements
    % slices = z_det:table_feed*slicewidth0:-z_det;
    slices = -nv/2:nv/2;
    scout_idx = 0:scout_size-1;

    row_weight = cos(linspace(-pi/2,pi/2,nv+2));
    row_weight = reshape(row_weight(2:end-1),[1,nv]);

    for i = 1:np

        Yc = Y(:,:,i);

        % offsets by half the detector size assuming the focal spot is centered
        % on the detector rows
        zp = table_feed * z_pos(i)/slicewidth0 + nv/2;

        z_proj = slices + zp;

        start_idx = find( scout_idx <= z_proj(1), 1, 'last' );
        end_idx   = start_idx + nv;

        overlap_left  = scout_idx(start_idx+1)-z_proj(1);
        overlap_right = z_proj(end) - scout_idx(end_idx);

        scout(:,start_idx:end_idx) = scout(:,start_idx:end_idx) + overlap_left  .* cat(2,zeros(nu,1),Yc .* row_weight);
        scout(:,start_idx:end_idx) = scout(:,start_idx:end_idx) + overlap_right .* cat(2,Yc.* row_weight,zeros(nu,1));

        N(:,start_idx:end_idx) = N(:,start_idx:end_idx) + overlap_left  .* [0 row_weight];
        N(:,start_idx:end_idx) = N(:,start_idx:end_idx) + overlap_right .* [row_weight 0];

    end

    N = max(N,1);
    scout = scout ./ N;

end

