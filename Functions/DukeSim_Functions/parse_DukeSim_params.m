function [DukeSim_params] = parse_DukeSim_params( params_in )

    % read file
    filestr = fileread(params_in);
    
    % break it into lines
    filebyline = regexp(filestr, '\n', 'split');
    
    % remove empty lines
    filebyline( cellfun(@isempty,filebyline) ) = [];
    
    % split by fields
    filebyfield = regexp(filebyline, '\: ', 'split');
    
    % Create structure array from parameters
    rm = logical(ones(length(filebyfield),1));
    for i = 1:length(filebyfield)
       
        if length(filebyfield{i}) == 1
           
            rm(i) = false;
            
        end
        
    end
    
    filebyfield = filebyfield(rm);
    
    for i = 1:length(filebyfield)
       
        DukeSim_params.(filebyfield{i}{1}) = filebyfield{i}{2};
        
    end

end

