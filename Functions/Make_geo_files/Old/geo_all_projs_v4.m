% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [  ] = geo_all_projs_v4( geo_all, params, du, dv, rotdir, np, angles, offset_ang, nsets, nprojset, weights )
% v3 vs. v4: v4 adds the rotation angle onto the end of the geometry file vectors to make sure distance driven operators work correctly given angular offsets

    % transformation matrices
    trans = @(x) [1 0 0 x(1); 0 1 0 x(2); 0 0 1 x(3); 0 0 0 1];
    rotx3 = @(x) [1 0 0; 0 cos(x) -sin(x); 0 sin(x) cos(x)];
    roty3 = @(x) [cos(x) 0 sin(x); 0 1 0; -sin(x) 0 cos(x)];
    rotz3 = @(x) [cos(x) -sin(x) 0; sin(x) cos(x) 0; 0 0 1];
    rotz = @(x) [cos(x) -sin(x) 0 0; sin(x) cos(x) 0 0; 0 0 1 0; 0 0 0 1];
    
    a2r = pi/180; % angles to radians
    r2a = 180/pi; % radians to angles
    
    % Optimized chain 1 parameters
    lsd = params(1);
    lso = params(2);
    uc = params(3);
    vc = params(4); 
    eta = params(5);
    sig = params(6);
	phi = params(7);
    zz = params(8);
    aa = params(9)*r2a;
    zo = 0; % added to zz

    % angles of projections
    offsets = repmat(offset_ang*(0:nsets-1),[nprojset 1]); % no offsets used in this study?
    angles = repmat(angles,[1 nsets])'+offsets(:);

    % get the vectors at each projection
    geolines = zeros(np,21);
    j=0;
    for chain = 1 % :nch % for each chain
        rot = rotx3(-eta)*roty3(-sig)*rotz3(-phi);
        src = [-lso; 0; zo-zz];
        dtv = [(lsd-lso); 0; zo-zz];
        puv = rot*[0; du; 0];
        pvv = rot*[0; 0; dv]; 
        for i=1:np % for each projection, rotate system
            % angle = angles(i)-aa;
            angle = angles(i)+aa;
            
            % prevent singular projection matrices
            if ( mod(angle,45) == 0 )
                
                disp('Offsetting 45 degree angle to avoid potential errors (1e-4 degree offset).');
                
                angle = angle + 1e-4;
                
            end
            
            rsrc = rotz3(rotdir*angle*a2r)*src;
            rdtv = rotz3(rotdir*angle*a2r)*dtv;
            rpuv = rotz3(rotdir*angle*a2r)*puv;
            rpvv = rotz3(rotdir*angle*a2r)*pvv;
            % line in geofile
            j=j+1;
            
            % projection weighting
            if (nargin > 10)
                
                geolines(j,:) = [rsrc' rdtv' rpuv' rpvv' lsd lso uc vc eta sig phi weights(i) angle];
                
            else
            
                geolines(j,:) = [rsrc' rdtv' rpuv' rpvv' lsd lso uc vc eta sig phi 1 angle];
            
            end
            
        end
    end

    delete(geo_all);
    save(geo_all,'geolines','-ASCII'); % save the geometry file

end

