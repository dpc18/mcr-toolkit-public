% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ geolines ] = make_geo_file_cylindrical( geo_name, params, du, dv, rotdir, np, angles, AMu, AMv, z_pos, db )
% Initializes all weights to 1.0. Override as needed when calling the reconstruction operators.

    % Transforms

        a2r = pi/180;
        r2a = 180/pi;

        rotx3 = @(x) ...
            [1 0 0;
            0 cos(x) -sin(x);
            0 sin(x) cos(x)];

        roty3 = @(x) ...
            [cos(x) 0 sin(x);
            0 1 0;
            -sin(x) 0 cos(x)];

        rotz3 = @(x) ...
            [cos(x) -sin(x) 0;
            sin(x) cos(x) 0;
            0 0 1];
        
        % Optimized chain 1 parameters
        lsd = params(1);
        lso = params(2);
        uc  = params(3);
        vc  = params(4); 
        eta = params(5);
        sig = params(6);
        phi = params(7);
        zo  = params(8);
        ao  = params(9)*r2a;

    % Make geometry files using all projections

        geolines = zeros(np,21,'double');

        rot = rotx3(-eta)*roty3(-sig)*rotz3(-phi);
        puv = rot*[0; du; 0];
        pvv = rot*[0; 0; dv]; 

        for i = 1:np % for each projection, rotate system

            angle = angles(i) + ao;

            % prevent singular projection matrices
            % if (mod(angle,45) == 0) % can sometimes fail
            if abs(rem(angle,45)) < 1e-6 % seems to work reliably, assuming angles is double precision

                angle = angle+1e-4;

            end

            src = rotz3(rotdir*angle*a2r) * ( [-lso;0;0]    - [0; 0; z_pos(i) - zo] + [ 0; lsd * sin( AMu * db ); AMv * dv ] );
            dtv = rotz3(rotdir*angle*a2r) * ( [lsd-lso;0;0] - [0; 0; z_pos(i) - zo] );

            rpuv  = rotz3(rotdir*angle*a2r)*puv;
            rpvv  = rotz3(rotdir*angle*a2r)*pvv;

            % line in geofile
            geolines(i,:) = [src' dtv' rpuv' rpvv' lsd lso uc vc eta sig phi 1 angle];

        end
        
    % Save a geometry file

        delete(geo_name);
        save(geo_name,'geolines','-ASCII'); % save the geometry file

end

