% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ cmd ] = compile_nvcc( name_string, c_compiler, GPU_compute_capability, flags, libs, includes )
% Compile basic (i.e. single file) CUDA code for PC or Linux
% (binary) Flags:
%   1) use more L1 cache (vs. shared memory)
%   2) ptxas compiler info for CUDA kernels
%   3) use "fast math" (lower precision, hardware-based basic math functions)
%   4) include OpenMP
%   5) debugging flags

% TO DO: Check if MEX_COMPILE_FLAG is properly defined on PCs.

    % Always use forward slashes in paths to avoid errors
    name_string = strrep(name_string,'\','/');
    c_compiler  = strrep(c_compiler,'\','/');
    libs        = strrep(libs,'\','/');
    includes    = strrep(includes,'\','/');
    
    if iscell(name_string)
    
        disp(sprintf(['\n\nCompiling ' name_string{1} '...\n\n']));
    
    else
        
        disp(sprintf(['\n\nCompiling ' name_string '...\n\n']));
        
    end

    if ispc
        
        cmd = ['nvcc '];
        
        % round small divisors to zero (?)
        cmd = [cmd '-ftz=true '];
        
        % cmd = [cmd '--default-stream per-thread '];
        
        % debugging
        if ( flags(5) == 1 )
            
            cmd = [cmd '--debug --device-debug '];
            
        end
        
        % fast math
        if flags(3) == 1
            
            cmd = [cmd '--use_fast_math '];
            
        end
        
        if flags(4) == 1
            
            cmd = [cmd '--compiler-options -openmp '];
            
        end
        
        % L1 cache vs. shared memory
        if flags(1) == 1
           
            cmd = [cmd '-Xptxas -dlcm=ca '];
            
        end
        
        % code file name
        if iscell(name_string)
            
            cmd = [cmd '-c '];
            
            for i = 1:length(name_string)
               
                cmd = [cmd name_string{i} '.cu '];
                
            end
            
            % cmd = [cmd '-o ' name_string{1} '.obj '];
            
        else
            
            cmd = [cmd '-c ' name_string '.cu '];
            
        end
 
        % C compiler for C portion of the code
        cmd = [cmd '-ccbin "' c_compiler '" '];
        
        % Does this work when compiling under windows w/ visual studio???
        cmd = [cmd '--compiler-options="/DMEX_COMPILE_FLAG" '];
        
        % GPU compute capability (balances backwards compatibility against use of new features)
        cmd = [cmd '-arch=sm_' GPU_compute_capability ' '];
        
        for i = 1:length(includes)
            
            cmd = [cmd '-I"' includes{i} '" '];
            
        end
        
        for i = 1:length(libs)
            
            cmd = [cmd '-L"' libs{i} '" '];
            
        end
        
        % Ptxas compiler output (registers / kernel type info)
        if flags(2) == 1
           
            cmd = [cmd '--ptxas-options=-v'];
            
        end
        
        chk = system(cmd);
        
    else % assume linux
        
        cmd = ['nvcc '];
        
        % round small divisors to zero (?)
        cmd = [cmd '-ftz=true '];
        
        % fast math
        if flags(3) == 1
            
            cmd = [cmd '--use_fast_math '];
            
        end
        
        % L1 cache vs. shared memory
        if flags(1) == 1
           
            cmd = [cmd '-Xptxas -dlcm=ca '];
            
        end
        
        % code file name
        if iscell(name_string)
            
            cmd = [cmd '-c '];
            
            for i = 1:length(name_string)
               
                cmd = [cmd name_string{i} '.cu '];
                
            end
            
            % cmd = [cmd '-o ' name_string{1} '.o '];
            
        else
            
            cmd = [cmd '-c ' name_string '.cu '];
            
        end
        
        % C compiler for C portion of the code
        cmd = [cmd '-ccbin "' c_compiler '" '];
        
        % use Position Independent Code
        % "all your address references are relative to the global offset table"
        % almost always used?
        % required to integrate this object code into a mex function...
        cmd = [cmd '--compiler-options="-fPIC -DMEX_COMPILE_FLAG" '];
        
        % GPU compute capability (balances backwards compatibility against use of new features)
        cmd = [cmd '-arch=sm_' GPU_compute_capability ' '];
        
        for i = 1:length(includes)
            
            cmd = [cmd '-I"' includes{i} '" '];
            
        end
        
        for i = 1:length(libs)
            
            cmd = [cmd '-L"' libs{i} '" '];
            
        end
        
        if flags(4) == 1
            
            cmd = [cmd '--compiler-options -fopenmp '];
            
        end
        
        % Ptxas compiler output (registers / kernel type info)
        if flags(2) == 1
           
            cmd = [cmd '--ptxas-options=-v'];
            
        end
        
        chk = system(cmd);
        
    end
    
    if chk ~= 0
       
        error(['Compile operation on ' name_string ' failed']);
        
    end

end
