% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ cmd ] = compile_mex( mex_path, c_compiler, flags, name_string, cuda_obj_name, libs, includes )
% Compile basic (i.e. single file) mex code for PC or Linux
% (Binary) flags:
%   1) use large array dimension varaibles (size_t, mwsize => 64 bit unsigned integers)
%   2) include CUDA libraries
%   3) include -g flag for on-line debugging of the C/C++ portion of the code in Visual Studio

    % Always use forward slashes in paths to avoid errors
    mex_path      = strrep(mex_path,'\','/');
    c_compiler    = strrep(c_compiler,'\','/');
    cuda_obj_name = strrep(cuda_obj_name,'\','/');
    libs          = strrep(libs,'\','/');
    includes      = strrep(includes,'\','/');
    
    disp(sprintf(['\n\nCompiling ' name_string '...\n\n']));
    
    if ispc
        
        cmd = [mex_path ' '];
        
        if flags(3) == 1
           
            cmd = [cmd '-g '];
            
        end
        
        if flags(1) == 1
            
            v = version('-release');
            v = str2double(v(1:4));
        
            % 64 bit integers
            if (v >= 2018)
                
                cmd = [cmd '-R2018a ']; % implied 64-bit integers
                
            else
               
                cmd = [cmd '-largeArrayDims ']; % specify 64-bit integers
                
            end
        
        end
        
        if flags(2) == 1
           
            % library include required for incorporating CUDA code
            % overkill in most cases, but makes things less complicated
            cmd = [cmd '-lcudart ']; % cuda runtime library
            cmd = [cmd '-lcufft '];  % cuda FFT / iFFT
            cmd = [cmd '-lcublas ']; % cuda BLAS (linear algebra)
            
        end
        
        % cpp file
        cmd = [cmd name_string '.cpp '];
        
        % provision so we can compile functions which do not incorporate additional object code
        if ~isempty(cuda_obj_name)
            
            if iscell(cuda_obj_name) % include multiple object files
                
                for i = 1:length(cuda_obj_name)
                    
                    cmd = [cmd cuda_obj_name{i} '.obj '];
                    
                end
                
            else
        
                % pre-compiled cuda obj code
                cmd = [cmd cuda_obj_name '.obj '];
                
            end
 
        end
        
        % additional includes
        for i = 1:length(includes)
            
            cmd = [cmd '-I"' includes{i} '" '];
            
        end
        
        % additional libraries
        for i = 1:length(libs)
            
            cmd = [cmd '-L"' libs{i} '" '];
            
        end
        
        % Flag for compiling the mex interface
        cmd =  [cmd ' -DMEX_COMPILE_FLAG '];
        
        chk = system(cmd);
        
    else % assume linux
        
        cmd = [mex_path ' '];
        
        % specify gcc version matlab is looking for...
        cmd = [cmd '-v GCC="' c_compiler '" '];
        
        if flags(1) == 1
        
            v = version('-release');
            v = str2double(v(1:4));
        
            % 64 bit integers
            if (v >= 2018)
                
                cmd = [cmd '-R2018a ']; % implied 64-bit integers
                
            else
               
                cmd = [cmd '-largeArrayDims ']; % specify 64-bit integers
                
            end
        
        end
        
        if flags(2) == 1
           
            % library include required for incorporating CUDA code
            % overkill in most cases, but makes things less complicated
            cmd = [cmd '-lcudart ']; % cuda runtime library
            cmd = [cmd '-lcufft '];  % cuda FFT / iFFT
            cmd = [cmd '-lcublas ']; % cuda BLAS (linear algebra)
            cmd = [cmd '-lgomp ']; % cuda BLAS (linear algebra)
            
        end
        
        % cpp file
        cmd = [cmd name_string '.cpp '];
        
        % provision so we can compile functions which do not incorporate additional object code
        if ~isempty(cuda_obj_name)
            
            if iscell(cuda_obj_name) % include multiple object files
                
                for i = 1:length(cuda_obj_name)
                    
                    cmd = [cmd cuda_obj_name{i} '.o '];
                    
                end
                
            else
        
                % pre-compiled cuda obj code
                cmd = [cmd cuda_obj_name '.o '];
                
            end
 
        end
        
        % cmd = [cmd '-g COMPFLAGS="-m64" '];
        
        % additional includes
        for i = 1:length(includes)
            
            cmd = [cmd '-I"' includes{i} '" '];
            
        end
        
        % additional libraries
        for i = 1:length(libs)
            
            cmd = [cmd '-L"' libs{i} '" '];
            
        end
        
        % Flag for compiling the mex interface
        cmd =  [cmd ' -DMEX_COMPILE_FLAG '];
        
        chk = system(cmd);
        
    end
    
    if chk ~= 0
       
        error(['Compile operation on ' name_string ' failed']);
        
    end

end
