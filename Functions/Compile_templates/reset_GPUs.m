% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ ] = reset_GPUs( GPU_idxs )
% Properly free GPU reconstruction allocations and reset used GPUs prior
% to performing CUDA profiling.

    % Clear allocated reconstruction objects using mexAtExit() within the source code
    % clear DD_init;
    
    % Reset GPUs
    for i = 1:length(GPU_idxs)
    
        % assumes zero indexing
        g = gpuDevice( double(GPU_idxs(i)) + 1 );
        
        % resets the device
        reset(g); 
        
    end

end

