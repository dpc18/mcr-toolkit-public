% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ filter ] = make_FBP_filter( name, du, nu, window_type, cutoff )
% Make frequnecy domain filter
% A. C. Kak and Malcolm Slaney, Principles of Computerized Tomographic Imaging, IEEE Press, 1988.

    if abs(round(log2(nu)) - log2(nu)) > 1e-5
       
        error('Number of filter elements (nu) must be set to the next highest power of 2 above the number of detector elements.');
        
    end
    
    % Chapter 3, Eq. 61
    filter = get_half_ramp(nu, du);

    filter = apply_window( filter, window_type, cutoff, nu );
    
    fid = fopen(name,'w');
    
    if (fid < 1)
       
        error('File could not be created.');
        
    end
    
    fwrite(fid,single(filter),'single');
    fclose(fid);

end

function [ filter ] = get_half_ramp(nu, du)

    filter = zeros([nu,1]);
    N      = -(nu/2):(nu/2-1);
    
    filter(nu/2+1) = 1 / ( 4 * du^2 );
    
    idx = mod(N,2) == 1;
    
    filter(idx) = -1.0 ./ ( pi .* N(idx) .* du ).^2;
    
    filter = du * real(fft(fftshift(filter)));
    
    filter = filter(1:(nu/2+1))';
    
end

function [ filter ] = apply_window( filter, window, cutoff, nu )
% https://en.wikipedia.org/wiki/Window_function

    w = linspace(0,pi,nu/2+1);

    if strcmpi(window, 'shepp-logan')
        
        P = sin(w(2:end)/(2*cutoff)) .* (2*cutoff) ./ w(2:end);
        
    elseif strcmpi(window, 'cosine')
            
        P = cos(w(2:end)/(2*cutoff));
        
    elseif strcmpi(window, 'hamming')
        
        P = 0.53836 + 0.46164 * cos(w(2:end)/cutoff);
        
    elseif strcmpi(window, 'hann')
        
        P = (1+cos(w(2:end)./cutoff)) / 2;
        
    elseif strcmpi(window, 'ram-lak')
       
        P = 1;
        
    else
       
        error(['FBP apodization window ' window ' not recognized.']);
        
    end
    
    filter(2:end) = filter(2:end) .* P;
    
    filter((ceil(cutoff*nu/2) + 2):end) = 0;

end
