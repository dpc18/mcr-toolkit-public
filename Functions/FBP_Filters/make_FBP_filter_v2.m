% %     Copyright (C) 2023, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ filter ] = make_FBP_filter_v2( name, du, nu, window_type, mod50 )
% Make frequnecy domain filter
% A. C. Kak and Malcolm Slaney, Principles of Computerized Tomographic Imaging, IEEE Press, 1988.
%
% Inputs:
% 1) name: complete file name (*.txt) and path where the filter will be saved
% 2) du: detector pixel size along the u axis
% 3) nu: number of detector pixels (rounded up to the next highest power of 2)
% 4) window_type: function to use for ramp filter windowing
% 5) mod50: frequency in cycles / mm at which the window function reaches 50%
%
% Outputs:
% filter: frequency filter from 0 to 0.5 cycles / pixel


    if abs(round(log2(nu)) - log2(nu)) > 1e-5
       
        error('Number of filter elements (nu) must be set to the next highest power of 2 above the number of detector elements.');
        
    end
    
    % Chapter 3, Eq. 61
    filter = get_half_ramp(nu, du);
    
    f    = (0:nu/2)./(nu)./(du);
    fmax = f(end);
    
    if mod50 > 2*fmax
    
        error(['Target frequency for 50% modulation (' num2str(mod50) ' cycles/mm) exceeds 2 x Nyquist frequency (' num2str(2*fmax) ' cycles/mm).']);
    
    end
    
    % Use a higher maximum frequency in case mod50 is beyond Nyquist.
    fmax = 1./(du);
    f    = (0:nu)./(nu)./(du);
    
    f_   = abs(f-mod50);
    f50  = find( f_ == min(f_), 1, 'first');
    
    % plot(f,filter);
    % xlabel('Frequency (cycles/mm)');

    filter = apply_window( filter, window_type, f50, fmax, nu );
    
    fid = fopen(name,'w');
    
    if (fid < 1)
       
        error('File could not be created.');
        
    end
    
    fwrite(fid,single(filter),'single');
    fclose(fid);

end

function [ filter ] = get_half_ramp(nu, du)

    filter = zeros([nu,1]);
    N      = -(nu/2):(nu/2-1);
    
    filter(nu/2+1) = 1 / ( 4 * du^2 );
    
    idx = mod(N,2) == 1;
    
    filter(idx) = -1.0 ./ ( pi .* N(idx) .* du ).^2;
    
    filter = du * real(fft(fftshift(filter)));
    
    filter = filter(1:(nu/2+1))';
    
end

function [ filter ] = apply_window( filter, window, f50, fmax, nu )
% https://en.wikipedia.org/wiki/Window_function

    % w = linspace(0,pi,nu/2+1);
    w = linspace(0,2*pi,nu+1);

    if strcmpi(window, 'shepp-logan')
        
        c50 = fminbnd( @(c) abs( sin( w(f50)/(2*c)) .* (2*c) ./ w(f50) - 0.5 ), 0, fmax );
        
        P = sin(w(2:end)/(2*c50)) .* (2*c50) ./ w(2:end);  
        
    elseif strcmpi(window, 'cosine')
        
        c50 = w(f50) / ( 2 * acos(0.5) );
            
        P = cos( min( w(2:end)/(2*c50), pi/2 ) );
        
    elseif strcmpi(window, 'hamming')
        
        c50 = w(f50) ./ acos( (0.5 - 0.53836) / 0.46164 );
        
        P = 0.53836 + 0.46164 * cos(w(2:end)/c50);
        
    elseif strcmpi(window, 'hann')
        
        c50 = w(f50) / acos(0);
        
        P = (1+cos(w(2:end)./c50)) / 2;
        
    elseif strcmpi(window, 'ram-lak')
       
        P = w(2:end).*0 + 1;
        
    else
       
        error(['FBP apodization window ' window ' not recognized.']);
        
    end
    
    P = P(2:nu/2+1); 
    
    idx = find( diff(P) <= 0, 1, 'last' );
    P(idx:end) = P(idx);
    
    P(P < 0) = 0;
    
    filter(2:end) = filter(2:end) .* P;
    
    % filter((ceil(c50*nu/2) + 2):end) = 0;

end
