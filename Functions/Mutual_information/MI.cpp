// Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// Original Author: Darin Clark, PhD
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


#include "mex.h"
#include <math.h>
#include <memory.h>
#include <stdint.h>

// ***     ***
// *** CPU ***
// ***     ***

float MI(float *imgA, float *imgB, uint64_t img_len, uint64_t bin_x, uint64_t bin_y)
{	
    
    // Construct the 2D histogram

        uint64_t *hist = (uint64_t *) malloc( bin_x * bin_y * sizeof(uint64_t) );
        memset(hist, 0, bin_x * bin_y * sizeof(uint64_t) );
        
        uint64_t xi, yi;
    
        for (uint64_t i = 0; i < img_len; i++)
        {
            // Assumes the imgA, imgB values are between [0,1]
            // Add 0.5f to round to the nearest integer value when casting to an unsigned integer
            xi = (uint64_t) ( imgA[i] * ( ( (float) bin_x ) - 1.0f) + 0.5f );
            yi = (uint64_t) ( imgB[i] * ( ( (float) bin_y ) - 1.0f) + 0.5f );

            // Make sure we don't go out of bounds on either axis of the 2D histogram
            if (xi >= bin_x) xi = bin_x - 1;
            if (yi >= bin_y) yi = bin_y - 1;

            // Add a count to the appropriate bin
            hist[bin_x * yi + xi]++;
        }
        
    // Compute mutual information
    // MI = H(x) + H(y) - H(x,y)
        
        float H_x  = 0.0f; // marginal entropy x
        float H_y  = 0.0f; // marginal entropy y
        float H_xy = 0.0f; // joint entropy x,y
        
        float prob0, prob;
        
        for (xi = 0; xi < bin_x; xi++) { // for each x
            
            prob = 0.0f;
         
            // integrate along y
            for (yi = 0; yi < bin_y; yi++) {
                
                prob0 = ( (float) hist[bin_x * yi + xi] ) / img_len;
                
                if (prob0 > 0.0f) { // avoid adding log(0) = -inf
                    
                    H_xy -= prob0 * log(prob0);
                    prob += prob0;
                    
                }
                

            }
            
            if (prob > 0.0f) {
                
                H_x -= prob * log(prob);
            
            }
            
        }
        
        for (yi = 0; yi < bin_y; yi++) { // for each y
            
            prob = 0.0f;
         
            // integrate along x
            for (xi = 0; xi < bin_x; xi++) {
                
                prob += ( (float) hist[bin_x * yi + xi] ) / img_len;
                
            }
            
            if (prob > 0.0f) { // avoid adding log(0) = -inf
                
                H_y -= prob * log(prob);
            
            }
            
        }
        
        float MI = H_x + H_y - H_xy;
        
    // Free memory
    free(hist);	
        
    // Return result
    return MI;
    
}

// ***     ***
// *** MEX ***
// ***     ***

// Matlab interface
// MI_val = MI(float *image1, float *image2, uint64_t image_length, uint64_t number of bins along x axis, uint64_t number of bins along y axis)
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
    
    // Check input arguments
    
        if (nrhs != 5)
            mexErrMsgTxt("Five input arguments required! (MI_val = MI(imgA,imgB,img_len,bin_x,bin_y)");

        if (nlhs != 1)
            mexErrMsgTxt("One output argument required! (MI_val = MI(imgA,imgB,img_len,bin_x,bin_y)");

        if( !mxIsSingle(prhs[0]) || !mxIsSingle(prhs[1]) )
            mexErrMsgTxt("Arguments 1 and 2 must be single precision.");

         if( !mxIsClass(prhs[2],"uint64")  || !mxIsClass(prhs[3],"uint64") || !mxIsClass(prhs[4],"uint64"))
            mexErrMsgTxt("Arguments 3, 4, and 5 must be uint64.");
        
    // Parse Matlab data
    
        float* imgA = (float*) mxGetData(prhs[0]);
        float* imgB = (float*) mxGetData(prhs[1]);
        uint64_t img_len = *((uint64_t*) mxGetData(prhs[2]));
        uint64_t bin_x = *((uint64_t*) mxGetData(prhs[3]));
        uint64_t bin_y = *((uint64_t*) mxGetData(prhs[4]));
    
        const uint64_t *imgA_size = mxGetDimensions(prhs[0]);
        const uint64_t *imgB_size = mxGetDimensions(prhs[1]);

        if ((imgA_size[0] != imgB_size[0]) || (imgA_size[1] != imgB_size[1])) {

            mexErrMsgTxt("Dimensions of imgA and imgB must match.");

        }
        
    // Compute Mutual Information (MI)
    
        float MI_out;

        MI_out = MI(imgA, imgB, img_len, bin_x, bin_y);
        
    // Make the answer available in the Matlab workspace
    
        const mwSize siz[2] = {1,1};
        plhs[0] = mxCreateNumericArray(2,siz,mxSINGLE_CLASS,mxREAL);
        float* c = (float*) mxGetData(plhs[0]);

        c[0] = MI_out;
    
}
