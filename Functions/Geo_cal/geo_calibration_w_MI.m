function [ optim_params ] = geo_calibration_w_MI( Y, filt_name, szy, int_params, double_params, initial_params, geo_name, angles, rotdir, deltas, a_usamp, disp_rng, zo, zo_, gpu_list, display_results )
% optimize geometric parameters based on FDK recon + reprojection
% include least-squares weighting
    
    clear opt;

    % [1   2   3  4  5   6   7  ]
    % [SDD SOD uc vc eta sig phi]
    optim_params = initial_params;

    maxiter = 50;
    fun_tol = 1e-4;
    
    oc = @(p) opt([optim_params(1:2) p(1:5)], Y, geo_name, angles, szy, int_params, double_params, a_usamp, filt_name, rotdir, disp_rng, zo, zo_, gpu_list, display_results);
    p_in = optim_params([3:7]);
    
    [p_out,f_out] = Nelder_Mead( p_in, deltas(3:7), oc, maxiter, fun_tol );
    
    optim_params([3:7]) = p_out;
    
    fprintf(['\nOptimal solution -- Cost: ' num2str(f_out) ', Evaluated: ' num2str(optim_params) '\n']);
    
end

function [ cost ] = opt(params, Y, geo_name, angles, szy, int_params, double_params, a_usamp, filt_name, rotdir, disp_rng, zo, zo_, gpu_list, display_results)

    % Variable which persist between optimization function calls
    persistent count norm minval maxval
    
    % a_usamp > 1: subsample angles for speed
    
        aidx = 1:round(length(angles));
        aidx = aidx(1:a_usamp:end);
    
    % Update the double parameters with the current parameter values
    
        double_params(8:14) = params(1:7);
    
    % int_params_f  = int32([nu nv np nx ny nz FBP use_affine]);
    
        int_params(3) = length(aidx);
        int_params(7) = 1; % Make sure we are doing analytical reconstruction.
    
    % Make a new geometry files using the current parameters, subsampled angles
    % The reconstruction operators read the geometry directly from the file this function saves.
    
        make_new_geo_file(params, geo_name, angles(aidx), double_params(1:2), single(rotdir), zo(aidx), zo_);
    
    % Allocate reconstruction operators using the current parameters
    
        clear DD_init;
        Rt_aff      = single([1 0 0 0 1 0 0 0 1 0 0 0]);
        rmask       = uint16(0);
        recon_alloc = DD_init(int_params(:), double_params(:), filt_name, geo_name, Rt_aff(:), rmask, gpu_list);
        W0          = ones(1,length(aidx));
        R           = @(x,w) DD_project(x,recon_alloc,w);
        Rtf         = @(y,w) DD_backproject(y,recon_alloc,int32(1),w);
        
    % WFBP + Reprojection

        Y_ = reshape(Y,szy);
        Y_ = Y_(:,:,aidx);
        Y = Y_(:);
        szy(end) = length(aidx);

        X = Rtf(Y_(:), W0(:));
        Y_ = R(X, W0(:));

        Y_ = reshape(Y_,szy); % reprojections
        Y  = reshape(Y,szy);  % original projections
    
    % Assuming the initial geometry is sub-optimal, establish normalization factors for the MI calculations.
    
        if isempty(count)

            minval = -1.1*max(abs(Y_(:)-Y(:)));
            maxval =  1.1*max(abs(Y_(:)-Y(:)));

        end
    
    % Call a C function for computing mutual information between the original projections and the reprojections.
    % Inputs must between 0 and 1, inclusive.
    % The MI calculation uses 512 histogram bins for each input.

        input1 = ( Y_(:) - minval)/(maxval - minval); input1(input1 < 0) = 0; input1(input1 > 1) = 1;
        input2 = ( Y(:)  - minval)/(maxval - minval); input2(input2 < 0) = 0; input2(input2 > 1) = 1;

        cost = MI(single(input1(:)), single(input2(:)), uint64(length(input1(:))), uint64(512), uint64(512));
        
    % If this is our first evaluation, make the cost 1. All future cost values will be relative to this initial cost.
    
        if isempty(count)

            count = 1;

            disp('Normalizing cost...');

            norm = cost;

        end
    
    % We evaluate the reciprocal here since the optimizer finds the minimum cost,
    % but we want to maximize MI between the original projections and the
    % reprojections.
        
        cost = norm/cost;
        
    % Display intermediate results if we so choose

        if isempty(javachk('awt')) % We need Java to display images.
        
            if display_results == 1

                X = reshape(X,int_params(4:6));
    
                samp = round(linspace(1,length(aidx),6));
                temp = imc({Y(:,:,samp(2:5)),Y_(:,:,samp(2:5))},szy(1:2).*[1 4],1,0);

                if size(temp,1) > size(temp,2)

                    figure(1); imagesc(mat2gray(temp'),disp_rng); axis image off; colormap gray; drawnow;

                else
    
                    figure(1); imagesc(mat2gray(temp),disp_rng); axis image off; colormap gray; drawnow;
    
                end
    
                figure(2); imagesc(mat2gray(X(:,:,round(int_params(6)/2))),disp_rng); axis image off; colormap gray; drawnow;

            end

        end

end

function [ ] = make_new_geo_file(params, geo_name, angles, du_dv, rotdir, zo, zo_)

    % [1   2   3  4  5   6   7  ]
    % [SDD SOD uc vc eta sig phi]

    % transformation matrices
    rotx3 = @(x) [1 0 0; 0 cos(x) -sin(x); 0 sin(x) cos(x)];
    roty3 = @(x) [cos(x) 0 sin(x); 0 1 0; -sin(x) 0 cos(x)];
    rotz3 = @(x) [cos(x) -sin(x) 0; sin(x) cos(x) 0; 0 0 1];
    
    a2r = pi/180; % degrees to radians

    np = length(angles);
    
    lsd = params(1);
    lso = params(2);
    uc = params(3);
    vc = params(4); 
    eta = params(5);
    sig = params(6);
	phi = params(7);
    
    du = du_dv(1);
    dv = du_dv(2);
    
    aa = 0  ; % Even if an angular offset is specified, it is not important for geometry calibration.
    zz = zo_; % z coordinate of the center of the reconstructed volume.

    % get the vectors at each projection
    geolines = zeros(np,21);
    j = 0;
    
    for chain = 1 % :nch % for each chain
        
        rot = rotx3(-eta)*roty3(-sig)*rotz3(-phi);
        puv = rot*[0; du; 0];
        pvv = rot*[0; 0; dv];
        
        for i = 1:np % for each projection, rotate system

            src = [-lso;      0; zo(i)-zz];
            dtv = [(lsd-lso); 0; zo(i)-zz];
            
            angle = angles(i)-aa;
            
            % prevent singular projection matrices
            if abs(rem(angle,45)) < 1e-6 % seems to work reliably, assuming angles is double precision

                angle = angle + 1e-4;

            end
            
            rsrc = rotz3(rotdir*angle*a2r)*src;
            rdtv = rotz3(rotdir*angle*a2r)*dtv;
            rpuv = rotz3(rotdir*angle*a2r)*puv;
            rpvv = rotz3(rotdir*angle*a2r)*pvv;
            
            j = j+1;
            
            geolines(j,:) = [rsrc' rdtv' rpuv' rpvv' lsd lso uc vc eta sig phi 1 angle];
            
        end
        
    end

    delete(geo_name);
    save(geo_name,'geolines','-ASCII'); % save the geometry file

end
