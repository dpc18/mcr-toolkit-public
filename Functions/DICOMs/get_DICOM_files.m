% %     Copyright (C) 2023, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [ DICOM_files ] = get_DICOM_files( sort_string, illegal_string )
% Return a cell array of full paths to DICOM files sorted with a string.
% Inputs:
%   sort_string: Sample DICOM path with one or more %d wildcards, number(s) to use for sorting the files.
%                Use '*' for substrings to be ignored.
%                Use '%d' for substrings to be sorted.
%   illegal_string: substring which must be removed before calling sscanf.
% Outputs:
%   DICOM_files: Cell array of full paths to DICOM files (sorted)
% Note: Currently assumes that the files are sorted by the first number first and any subsequent numbers
%       second.


    % Get all matching files within the target directory
    dir_string = replace(sort_string,'%d','*');
    files      = dir(dir_string); 
    
    % Sort files by %d wildcards.
    current_DICOM_files = cellfun( @(x,y) fullfile( x, y ), {files.folder}, {files.name}, 'UniformOutput', false);
    
    try % try a method based on multiple keys sorted in the same direction
    
        % Assume the root directories are in the right order and only sort the files within each DICOM directory
        strs    = cellfun(@(s) sscanf(replace(s,illegal_string,''),replace(sort_string,illegal_string,'')), current_DICOM_files, 'UniformOutput', false);
        [~,idx] = sortrows(cell2mat(strs)',[1,2]);
    
    catch % try a method based on a single key
    
        strs    = cellfun(@(s) sscanf(replace(s,illegal_string,''),replace(sort_string,illegal_string,'')), current_DICOM_files, 'UniformOutput', false);
        [~,idx] = sort(cell2mat(strs)');
    
    end

    DICOM_files = current_DICOM_files(idx);
    
end

