% %     Copyright (C) 2023, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [] = merge_and_save_DICOMs( sorted_DICOMs, X, output_dir, mat_size )

    % Load processed image data
    % X = load_nii(nifti_source);
    % X = X.img;
    
    % Check for size inconsistencies.
    if size(X,1) ~= mat_size || size(X,2) ~= mat_size || size(X,3) ~= length(sorted_DICOMs)
       
        error('Image volume sizes does not match.');
        
    end
    
    if ~exist(output_dir,'dir')
        
        mkdir(output_dir);
        
    end

    for i = 1:length(sorted_DICOMs)
       
        cfile = sorted_DICOMs{i};
        
        s = dir(cfile); 
        
        cslice = X(:,:,i)';
        
        info = dicominfo(cfile);
        
        cslice = uint16( ( cslice - info.RescaleIntercept ) / info.RescaleSlope );
        
        fid_source_file = fopen(cfile,'r');
        
        [~,b,c]       = fileparts(cfile);
        new_file_path = fullfile(output_dir,[b c]);
        fid_new_file  = fopen(new_file_path,'w');
        
        header_bytes = fread( fid_source_file, s.bytes - 2 * mat_size^2, 'uint8=>uint8' );
        fwrite(fid_new_file, header_bytes, 'uint8');
        fwrite(fid_new_file, cslice(:), 'uint16');
        
        fclose(fid_new_file);
        fclose(fid_source_file);
        
    end
    

end

