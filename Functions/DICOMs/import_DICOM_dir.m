% %     Copyright (C) 2023, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [ vol, T ] = import_DICOM_dir( sort_string, mat_size, illegal_string )
% Inputs:
% (1) sort_string: string to use for sorting DICOMs
%                  (e.g. 'Case_1.CT.CT_CHEST_ANGIOG.%d.%d' where %d is the energy, %d is slice number)
%                  Can also include '*' for substrings to be ignored.
% (2) mat_size: matrix size (e.g. 512, 1024)
% (3) illegal_string: substring which must be removed before calling sscanf.
% 
% Outputs:
% (1) vol: volume data
% (2) T: table of DICOM header information

%% Parse / sort DICOM files
   
    % cd(source_dir);
    % 
    % files = dir;
    % files = files(3:end);
    % 
    % current_DICOM_files = cellfun( @(x) x, {files.name}, 'UniformOutput', false);
    % 
    % try % try a method based on multiple keys sorted in the same direction
    % 
    %     % Assume the root directories are in the right order and only sort the files within each DICOM directory
    %     strs    = cellfun(@(s) sscanf(s,sort_string), current_DICOM_files, 'UniformOutput', false);
    %     [~,idx] = sortrows(cell2mat(strs)',[1,2]);
    % 
    % catch % try a method based on a single key
    % 
    %     strs    = cellfun(@(s) sscanf(s,sort_string), current_DICOM_files, 'UniformOutput', false);
    %     [~,idx] = sort(cell2mat(strs)');
    % 
    % end
    
%% Parse / sort DICOM files

    DICOM_files = get_DICOM_files( sort_string, illegal_string );
    
%%  Create a new table for header information

    N = length(DICOM_files);
            
    PatientID              = cell(N,1);
    FileName               = cell(N,1);
    % PatientAge             = {''};
    ImageType              = cell(N,1);
    SeriesDescription      = cell(N,1);
    SliceThickness         = zeros([N,1],'single');
    KVP                    = zeros([N,1],'single');
    ConvolutionKernel      = cell(N,1);
    SpiralPitchFactor      = zeros([N,1],'single');
    Rows                   = zeros([N,1],'single');
    Columns                = zeros([N,1],'single');
    InstanceNumber         = zeros([N,1],'single');
    PixelSpacing_x         = zeros([N,1],'single');
    PixelSpacing_y         = zeros([N,1],'single');
    RescaleIntercept       = zeros([N,1],'single');
    RescaleSlope           = zeros([N,1],'single');
    
    T = table(PatientID,FileName,ImageType,SeriesDescription,SliceThickness,KVP,... % PatientAge,
                      ConvolutionKernel, SpiralPitchFactor, Rows, Columns, InstanceNumber, PixelSpacing_x,...
                      PixelSpacing_y, RescaleIntercept, RescaleSlope);

    table_row = 1; 
    
%% Read DICOM info and image data
    
    vol = zeros([mat_size mat_size N],'single');
    
    % for i = 1:length(current_DICOM_files)
    % 
    %     cfile = fullfile(files(idx(i)).folder,files(idx(i)).name);
        
    for i = 1:length(DICOM_files)
        
        cfile = DICOM_files{i};
        
        info = dicominfo(cfile);
        
        if isempty(info.KVP)
            
            info.KVP = 0;
            
        end
        
        new_row = {{info.PatientID},{info.ImageType},{info.PatientAge},...
                   {info.SeriesDescription},info.SliceThickness,info.KVP,...
                   {info.ConvolutionKernel},info.SpiralPitchFactor...
                   info.Rows, info.Columns, info.InstanceNumber,...
                   info.PixelSpacing(1), info.PixelSpacing(2), info.RescaleIntercept, info.RescaleSlope};

        T(table_row,:) = new_row;
        
        table_row = table_row + 1;
        
        vol(:,:,i) = single(dicomread(cfile)) * info.RescaleSlope + info.RescaleIntercept;
        
    end

end

