% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % Takeda, H., Farsiu, S., & Milanfar, P. (2007).
    % Kernel regression for image processing and reconstruction.
    % IEEE Transactions on image processing, 16(2), 349-366.
    
%%

function [ A_ ] = make_A_approx_3D_v2( w, w2 )
% Approximates the w1*Gaussian + w2*LoG resampling kernel A
% as a sum of three separable Gaussian
% w: kernel radius
% w2: kernel bandwidth w2 = 2*sig^2

    %% Compute the dense, 3D resampling kernel

    q = w*2;

    [x2, x1, x3] = meshgrid(-w:w,-w:w,-w:w);
    Xx = [ones((2*w+1)^3,1), x1(:), x2(:), x3(:), ...
      x1(:).^2, x2(:).^2, x3(:).^2, ...
      x1(:).*x2(:), x2(:).*x3(:), x3(:).*x1(:)];
    tt = x1.^2 + x2.^2 + x3.^2;
    W = exp(-(1/w2) * tt); % 1/6 for 44 micron data? 1/2 for 88 micron data?
    Xw = [Xx(:,1).*W(:), Xx(:,2).*W(:), Xx(:,3).*W(:), ...
      Xx(:,4).*W(:), Xx(:,5).*W(:), Xx(:,6).*W(:), ...
      Xx(:,7).*W(:), Xx(:,8).*W(:), Xx(:,9).*W(:), Xx(:,10).*W(:)];
    A = inv(Xx' * Xw + eye(10)*1e-6) * (Xw');
    A = reshape(A(1,:),[q+1 q+1 q+1]);
    A = A./sum(A(:));
    
    %% Solve for the dense approximation

    sigma_k = sqrt(w2/2);

    options = optimset('TolX',2*eps,'TolFun',2*eps,'MaxFunEvals',1e6);

    % Create normalized kernels based on e0
    e0 = exp(-tt/(2*sigma_k^2));  factor = sum(e0(:)); e0 = e0./factor;
    % e1 = (-(x1 + x2 + x3)/sigma_k^2).*e0; 
    % e2 = ((tt-sigma_k^2)./sigma_k^4).*e0;

    e0_1 = exp(-tt/(2*(sigma_k/sqrt(1.0654))^2));  e0_1 = e0_1./sum(e0_1(:));
    e0_2 = exp(-tt/(2*(sigma_k*sqrt(1.0654))^2));  e0_2 = e0_2./sum(e0_2(:));
    % e0_2 = @(sig) exp(-tt/(2*(sig)^2))./sum(sum(sum(exp(-tt/(2*(sig)^2)))));  % e0_2 = e0_2./sum(e0_2(:));

    % Include the normalization in the coefficients
    % e0 = exp(-tt/(2*sigma_k^2));
    % e2 = ((tt-sigma_k^2)./sigma_k^4).*exp(-tt/(2*sigma_k^2)); 

    profile = @(w_0,w_1,w_2) (w_0.*e0 + w_1.*e0_1 + w_2.*e0_2)./(w_0+w_1+w_2);
    % profile = @(w_0,w_1) (w_0.*e0 + e0_2(w_1))./(w_0+1);

    % MSE = @(w) norm(reshape(A - profile(w(1),w(2)),[13^3 1]));
    MSE = @(w) norm(reshape(A - profile(w(1),w(2),w(3)),[(q+1)^3 1]));

    % output = fminsearch(MSE,[1 1],options);
    output = fminsearch(MSE,[0.1 0.05 0.05],options);

    % t = profile(output(1),output(2),output(3));
    % t = profile(output(1),output(2),output(3));
    % MSE(output)

    % close all force;
    % figure(1); hold on; plot(-w:w,t(7,:,7),'r'); plot(-w:w,A(7,:,7),'k');

    % t = t./sum(t(:));

    % t2 = t(x1(:)+w+1).*t(x2(:)+w+1).*t(x3(:)+w+1);

    % t2 = reshape(t2./sum(t2),[13 13 13]);
    
    %% Separate the dense Gaussian kernels to 1D kernels
    
    diam = q+1;

    % Find the 3D diagonal starting from the upper left
    G0 = e0;
    G0_diag = G0(diam^2*((1:diam)-1)+diam*((1:diam)-1)+(1:diam));
    G0_basis = G0_diag.^(1/3);

    G0_1 = e0_1;
    G0_1_diag = G0_1(diam^2*((1:diam)-1)+diam*((1:diam)-1)+(1:diam));
    G0_1_basis = G0_1_diag.^(1/3);

    % G0_2 = e0_2(output(4));
    G0_2 = e0_2;
    G0_2_diag = G0_2(diam^2*((1:diam)-1)+diam*((1:diam)-1)+(1:diam));
    G0_2_basis = G0_2_diag.^(1/3);

    % Check results
    % norm(reshape(convn(convn(G0_basis,G0_basis'),reshape(G0_basis,[1 1 diam]))-G0,[1 diam^3]))
    % norm(reshape(convn(convn(G0_1_basis,G0_1_basis'),reshape(G0_1_basis,[1 1 diam]))-G0_1,[1 diam^3]))
    % norm(reshape(convn(convn(G0_2_basis,G0_2_basis'),reshape(G0_2_basis,[1 1 diam]))-G0_2,[1 diam^3]))
    
    %% Construct the approximation
    
    A_ = bsxfun(@times,sign(output(1:3)).*(abs(output(1:3))./sum(output(1:3))).^(1/3),[G0_basis(:) G0_1_basis(:) G0_2_basis(:)]);
    
    %% Check the accuracy

    part1 = convn(convn(A_(:,1),A_(:,1)'),reshape(A_(:,1),[1 1 diam]));
    part2 = convn(convn(A_(:,2),A_(:,2)'),reshape(A_(:,2),[1 1 diam]));
    part3 = convn(convn(A_(:,3),A_(:,3)'),reshape(A_(:,3),[1 1 diam]));

    part1 = part1+part2+part3;

    % disp(['Approximation error (%): ' num2str(norm(A(:)-part1(:))*100)]);

end

