% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ output ] = view_radial_dictionary( D, pmask, sortD )
% View patches from a radial 3D dictionary.
% D: dictionary (# voxels x # patches)
% pmask: Cubic radial patch mask.

    % sort dictionary if requested to expose redundancy
    if (sortD == 1)
       
        szD = size(D);
    
        % computes distances between all patches (all atoms in dictionary x subtracted atom)
        % dists = squeeze(sqrt(sum(bsxfun(@minus,D,reshape(D,[szD(1) 1 szD(2)])).^2)));
    
        idx = randperm(szD(2));
        idx = idx(1);
        
        D0 = D;                 % dictionary of unused atoms
        Dc = D(:,idx);          % current atom
        idxs = zeros(1,szD(2)); % list of indexes to reverse sorting after regularization
        
        D(:,1) = Dc;
        D0(:,idx) = [];         % remove atoms after they are used
        idxs(1) = idx;
        
        idxlist = 1:szD(2);     % list of unused indexes
        idxlist(idx) = [];
        
        for i = 2:szD(2)

            % distance from remaining atoms
            dist = sqrt(sum(bsxfun(@minus,Dc,D0).^2));
            
            % id = rand(1);
            
            % if (id < 0.33 && size(D0,2) > 1) % perturbation
                
                % [~,idx_] = min(dist);
                % dist(idx_) = inf;
                % [~,idx] = min(dist);
                
            % else
                
                % next patch index
                [~,idx] = min(dist);
                
            % end

            % update list of used indexes relative to the original ordering
            idxs(i) = idxlist(idx);

            % write sorted atom to dictionary
            D(:,i) = D0(:,idx);

            % update current atom
            Dc = D0(:,idx);

            % remove current atom from working lists
            D0(:,idx) = [];
            idxlist(idx) = [];

        end
        
        clear D0 Dc idx;
        
    end

    pmask_ = double(repmat(pmask,[1 1 1 size(D,2)]));
    
    pmask_(pmask_ > 0) = D;
    
    cols = ceil(sqrt(size(D,2)));
    
    ps = size(pmask);
    
    output = zeros(ps(1)*cols,ps(1)*cols);
    
    counter = 0;
    for i = 1:cols
        
       atoms = min(cols, size(D,2)-counter);
       
       output(((i-1)*ps(1)+1):(i*ps(1)),1:(atoms*ps(2))) = reshape(pmask_(:,:,ceil(size(pmask,3)/2),(counter+1):min(counter + cols,size(D,2))),ps(1:2).*[1 min(cols, size(D,2)-counter)]);
       
       counter = min(counter + cols,size(D,2));
        
    end

end

