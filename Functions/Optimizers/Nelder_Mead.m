% %     Copyright (C) 2022 Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % Lagarias, J. C., Reeds, J. A., Wright, M. H., & Wright, P. E. (1998). 
    % Convergence properties of the Nelder--Mead simplex method in low dimensions.
    % SIAM Journal on optimization, 9(1), 112-147.
    
%%

function [ opt_params, opt_fval ] = Nelder_Mead( params0, offsets0, eval_fun, maxiter, fun_tol )

    % --Inputs--
    % params0 : initial values for n parameters
    % offsets0: offset from each of n parameters implying the expected viable deviation from params0 to opt_params
    % eval_fun: function handle which returns the cost value for a given set of n parameters
    % maxiter : maximum number of Nelder-Mead iterations allowed
    % fun_tol : exit early if the best and worst function values are within this tolerance
    
    % --Outputs--
    % opt_params: parameters which yield the lowest function value found
    % opt_fval  : lowest function value found

    % optimization parameters
    rho   = 1;
    chi   = 2;
    gamma = 0.5;
    sigma = 0.5;
    
    n = length(params0);
    
    % n + 1 vertices x n params
    offsets = diag([0 offsets0(:)']);
    offsets = double(offsets(:,2:end));
    
    % n + 1 vertices x n params
    params = repmat(params0,[n+1,1]);
    params = double(params);
    
    params = params + offsets;
    
    % n + 1 function values
    fvals = zeros([n+1,1]);
    
    % Initialization: compute the function values for the initial simplex,
    %                 params0 and params0 + one offset at a time
    fprintf('\nInitialization...\n');
    for i = 1:n+1
       
        fprintf(['Evaluating parameters: ' num2str(params(i,:)) '\n']);
        
        fvals(i) = eval_fun( params(i,:) );
        
        fprintf(['Function value: ' num2str(fvals(i)) '\n']);
        
    end
    
    % Optimization
    for k = 1:maxiter
        
        fprintf(['\nIteration ' num2str(k) '...\n']);
       
        % 1. Order: sort parameter vertices by function values
        
            % NOTE: Sorting after each iteration may result in tie-break inconsistencies vs. those described in the referenced paper.

            % sort by increasing function values
            [fvals,idx] = sort(fvals);

            params = params(idx,:);

        % Check for convergence

            if abs(fvals(end) - fvals(1)) < fun_tol

                fprintf(['\nConverged on iteration ' num2str(k) ' to within tolerance ' num2str(fun_tol) '.\n']);

                opt_params = params(1,:);
                opt_fval   = fvals(1);

                return;

            end
            
        % 2. Reflect
        
            x_ = mean(params(1:n,:),1);
        
            x_r = (1.0 + rho) * x_ - rho * params(n+1,:);
            
            fprintf(['Evaluating parameters: ' num2str(x_r) '\n']);
            
            f_r = eval_fun( x_r );
            
            fprintf(['Function value: ' num2str(f_r) ' (best: ' num2str(fvals(1)) ')\n']);
            
            if (f_r < fvals(n)) && (f_r >= fvals(1))
                
                fprintf(['Reflect step for iteration ' num2str(k) '.\n']);
                
                fvals(n+1)    = f_r;
                params(n+1,:) = x_r;
                
                continue;
                
            end
            
        % 3. Expand
        
            if ( f_r < fvals(1) )
               
                x_e = ( 1.0 + rho * chi ) * x_ - rho * chi * params(n+1,:);
                
                fprintf(['Evaluating parameters: ' num2str(x_e) '\n']);
                
                f_e = eval_fun( x_e );
                
                fprintf(['Function value: ' num2str(f_e) ' (best: ' num2str(fvals(1)) ')\n']);
                
                if ( f_e < f_r )
                   
                    f_out = f_e;
                    x_out = x_e;
                    
                else
                   
                    f_out = f_r;
                    x_out = x_r;
                    
                end
                
                fprintf(['Expand step for iteration ' num2str(k) '.\n']);
                
                fvals(n+1)    = f_out;
                params(n+1,:) = x_out;
                
                continue;
                
            end
            
        % 4. Contract
        
            if f_r >= fvals(n)
                
                % 4.a Outside contraction
                
                    if (f_r < fvals(n+1)) % && (f_r >= fvals(n))
                        
                        x_c = (1.0 + rho * gamma) * x_ - rho * gamma * params(n+1,:);
                        
                        fprintf(['Evaluating parameters: ' num2str(x_c) '\n']);
                
                        f_c = eval_fun( x_c );

                        fprintf(['Function value: ' num2str(f_c) ' (best: ' num2str(fvals(1)) ')\n']);
                        
                        if f_c <= f_r
                            
                            fprintf(['Contract outside step for iteration ' num2str(k) '.\n']);
                
                            fvals(n+1)    = f_c;
                            params(n+1,:) = x_c;

                            continue;
                            
                        end
                        
                    end
                
                % 4.b Inside contraction
                
                    if ( f_r >= fvals(n+1) )
                        
                        x_cc = (1.0 - gamma) * x_ + gamma * params(n+1,:);
                        
                        fprintf(['Evaluating parameters: ' num2str(x_cc) '\n']);
                
                        f_cc = eval_fun( x_cc );

                        fprintf(['Function value: ' num2str(f_cc) ' (best: ' num2str(fvals(1)) ')\n']);
                        
                        if f_cc < fvals(n+1)
                           
                            fprintf(['Contract inside step for iteration ' num2str(k) '.\n']);
                
                            fvals(n+1)    = f_cc;
                            params(n+1,:) = x_cc;

                            continue;
                            
                        end

                    end
                
            end
            
        % 5. Shrink
        
            fprintf(['Performing a shrink step for iteration ' num2str(k) '.\n']);
        
            for i = 2:n+1
               
                v_i = params(1,:) + sigma * ( params(i,:) - params(1,:) );
                
                fprintf(['Evaluating parameters: ' num2str(v_i) '\n']);
                
                f_i = eval_fun( v_i );

                fprintf(['Function value: ' num2str(f_i) ' (best: ' num2str(fvals(1)) ')\n']);
                
                fvals(i)    = f_i;
                params(i,:) = v_i;
                
            end
        
        
    end
    
    % If we hit the maximum number of iterations, return the parameters with the lowest function value.
    
        fprintf(['\nMaximum number of iterations reached (' num2str(maxiter) ' iterations).\n']);

        [fvals,idx] = sort(fvals);
        
        params = params(idx,:);
        
        opt_params = params(1,:);
        opt_fval   = fvals(1);

end
































