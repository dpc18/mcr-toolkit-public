% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % Van der Vorst, H. A. (1992).
    % Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
    % SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
    
    % http://en.wikipedia.org/wiki/Biconjugate_gradient_stabilized_method

%%

function [x, resvec] = bicgstab_os(A, b, x, maxit, W)
% Avoid storing x0, ph, and sh to save RAM. Write over x in place, if possible.

    % resvec = zeros(maxit+1,1);
    snum = size(W,2);
    if (snum ~= 1 && mod(snum,2) ~= 0) 
       error('Even number of subsets expected.'); 
    end
    resvec = inf+zeros(snum/2,maxit+1);

    % n2b = norm(b);
    
    % 1) residual
    r = b - A(x, sum(W,2));
    resvec(1,1) = norm(r);
    
    % 2) previous iteration init
    rt = r;
   
    % 3) rate parameters
    rho = 1;
    omega = 1;
    alpha = 1;
    
    % store the minimum result
    xmin = x;
    
    for ii = 1:maxit
        
        for j = 1:2:snum
            
            % counter = counter + 1;
            
            rho1 = rho;

            % 1)
            rho = rt' * r;

            if ii == 1
                p = r;
            else
                % 2)
                beta = (rho/rho1)*(alpha/omega);
                % 3) 
                p = r + beta * (p - omega * v);
            end

            % 4)
            v = A(p, W(:,j));

            % 5)
            alpha = rho / (rt' * v);

            % 6)
            s = r - alpha * v;

            % 7)
            t = A(s, W(:,j + (snum > 1)));

            % 8)
            omega = (t' * s) / (t' * t);

            % 9)
            x = x + alpha*p + omega*s;

            % 10)
            r = s - omega * t;

            resvec((j+1)/2,ii+1) = norm(r);

            if (resvec((j+1)/2,ii+1) == min(resvec(:)))

                xmin = x;

            end

        end

    end                                % for ii = 1 : maxit
    
    resvec(~isfinite(resvec)) = 0;
    
    x = xmin;

end