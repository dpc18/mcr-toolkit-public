% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
%% References

    % Sleijpen, G. L., & Van Gijzen, M. B. (2010).
    % Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
    % SIAM journal on scientific computing, 32(5), 2687-2709.
    % Algorithm 3.1

%% 

function [ xmin, resvec ] = bicgstabl_os( A, b, x, maxit, W, ell )

    % avoid warnings when ell is set too high for single precision calculations
    warning('off','MATLAB:nearlySingularMatrix');

    snum = size(W,2);

    r0 = b - A(x,sum(W,2));
    
    n = numel(x);
    r = zeros([n,ell+1],'single');
    u = zeros([n,ell+2],'single');
    
    resvec = zeros(snum, maxit+1) + inf;
    resvec(1,1) = norm(r0);
    
    r(:,1) = r0;
    u(:,1:2) = [r0, A(r0,W(:,1))];
    
    r0t = r0;
    xmin = x;
    
    clear r0;
    
    for i = 1:maxit
        
        for s = 1:snum
        
            % The Bi-CG step
            for j = 1:ell

                sigma = r0t' * u(:,j+1);
                alpha = (r0t' * r(:,j)) ./ sigma;

                x        = x + u(:,1) * alpha;
                r(:,1:j) = r(:,1:j) - u(:,2:j+1) * alpha;
                r(:,j+1) = A(r(:,j),W(:,s));

                beta       = (r0t' * r(:,j+1)) ./ sigma;
                u(:,1:j+1) = r(:,1:j+1) - u(:,1:j+1) * beta;
                u(:,j+2)   = A(u(:,j+1),W(:,s));

            end

            % The polynomial step
            [Q,R] = qr(r(:,2:end),0);
            gamma = (R'*R + eye(ell)*1e-6) \ ( R' * Q' * r(:,1) );
            
            % alternative solutions
            % gamma = single( double( r(:,2:end) ) \ double( r(:,1) ) );
            % gamma = r(:,2:end) \ r(:,1);
            % [ U, E, V ] = eig_SVD( r(:,2:end), 1, 1 );
            % gamma = ( V * diag(1./(diag(E)+1e-6)) * U' ) * r(:,1);

            x      = x + r(:,1:ell) * gamma;
            r(:,1) = r(:,1) - r(:,2:end) * gamma;

            u(:,1:2) = [u(:,1) - u(:,2:end-1) * gamma, u(:,2) - u(:,3:end)   * gamma];

            resvec(s,i+1) = norm(r(:,1));

            if resvec(s,i+1) == min(resvec(:))

                xmin = x;

            end
        
        end
            
    end
    
    warning('on','MATLAB:nearlySingularMatrix');
    
    resvec(~isfinite(resvec)) = 0;
    
end

