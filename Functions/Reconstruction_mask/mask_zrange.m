function [ zmask ] = mask_zrange( mask )
% Input: 3D GPU projection / backprojection mask
% Output: 2D z index mask

    zmask = mask(:,:,1:2)*0;
    
    for i = 1:size(mask,3)
        
        temp = zmask(:,:,1);
       
        temp(zmask(:,:,1) == 0 & mask(:,:,i) ~= 0) = i;
        
        zmask(:,:,1) = temp;
        
    end
    
    for i = size(mask,3):-1:1
        
        temp = zmask(:,:,2);
       
        temp(zmask(:,:,2) == 0 & mask(:,:,i) ~= 0) = i;
        
        zmask(:,:,2) = temp;
        
    end
    
    % prepare for looping in C (zero based indexing)
    zmask(:,:,1) = max(zmask(:,:,1)-1,0);
    
end

