% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [ Y_ ] = transform_projections_v1( data, air, fix_proj, enforce_non_negativity )

    % read projection data
    
        Y = load_nii(data);
        Y = single(Y.img);
        
        szy = size(Y);
        nu = szy(1);
        nv = szy(2);
        np = szy(3);

    % read air raw data

        I0 = load_nii(air);
        I0 = I0.img;
        
        I0 = single(median(I0,3));

    % make gap map

        cols = [129 258 387];
        rows = [129];

        gaps = zeros(nu,nv,'single');

        for c = 1:length(cols)

            gaps(cols(c),:) = 1;

        end

        for r = 1:length(rows)

            gaps(:,rows(r)) = 1;

        end
        
        % Add some extra problem points
        gaps(305+1,(142:143)+1) = 1;
        gaps((313:317)+1,140+1) = 1;
        gaps((315:316)+1,141+1) = 1;
        gaps(357+1,(129:130)+1) = 1;
        gaps(358+1,129+1) = 1;
        
    % Exposure correction
        
        if ( ~isempty(fix_proj) )

            for e = 1:4

                Y_sums = squeeze(sum(sum(Y(:,:,:,e),1),2));

                Y(:,:,fix_proj,e) = bsxfun(@times,Y(:,:,fix_proj,e),reshape(median(Y_sums)./Y_sums(fix_proj),[1 1 length(fix_proj)]));

            end

        end

    % Air normalization

        % suppress the contribution of spurious values
        I0_ = median(I0,3);
        clear I0;
        
        temp = I0_(:,:,:,1);
        med_val = median(temp(:));
        
        gaps(I0_(:,:,1,1) > 1.2*med_val | I0_(:,:,1,1) < 0.8*med_val) = 1;

        Y_ = -log( bsxfun(@rdivide, Y, I0_) );
        
        if enforce_non_negativity == 1
        
            Y_(Y_ < 0) = 0;
        
        end
        
        Y_(~isfinite(Y_)) = 0;
        
       clear Y I0_;

    % convolve and fill gaps

        % Gaussian with 2 pixel FWHM
        sig = 2 / (2 * sqrt( 2* log(2) ) );
        G = exp(-(-5:5).^2 ./ ( 2 * sig^2 ));
        G = G./sum(G);
        G = G'*G;

        % Normalization
        norm0 = convn(~logical(gaps),G,'same');
        norm0 = max(~logical(gaps),norm0);

        for th = 1:4

            for i = 1:np

                temp = Y_(:,:,i,th);

                temp(logical(gaps)) = 0;

                temp2 = convn(temp, G, 'same');

                temp2(~logical(gaps)) = temp(~logical(gaps));

                temp2 = temp2 ./ norm0;

                Y_(:,:,i,th) = temp2;
                
                if ( mod(i,np/4) == 0 )
                    
                    disp(['Y' num2str(th) ': ' num2str(100*i/np) '%']);
                    
                end

            end

        end

end