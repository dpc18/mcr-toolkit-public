% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ Ws, percent_R_R ] = Gaussian_weights_trunc_v2( phases, subphases, SDs )

%% Compute Guassian/Sinc Temporal Basis Functions

    phase = round(phases/2);
    bw = phases;
    tp = subphases; % subphases
    bw2 = round(subphases/phases);

    % Gaussian temporal sigmas (sig*2*sqrt(2*log(2)) = FWHM)
    sig = bw2./(2*sqrt(2*log(2)));

    bins = 1:bw2:tp;
    sel = find(bins < bw2*phase,1,'last');
    mu = (bins(sel)-1) + (bw2-1)/2+1;

    Ws = zeros(tp,phases);

    % Compute circular Gaussian weights

    d = min([abs(mu-(1:tp)); abs(mu+bw2*bw-(1:tp)); abs(mu-bw2*bw-(1:tp))]);

    Ws(:,1) = exp(-(d.^2)./(2*sig^2));
    
    % threshold at +/- SDs stds. (3 = 99.7% density)
    temp = Ws(:,1);
    temp(d > sig*SDs) = 0;
    Ws(:,1) = temp;

    Ws(:,1) = Ws(:,1)./sum(Ws(:,1));

    % figure(i); plot(Ws(:,i));

    % Compute correction to make a uniform distribution
    % err = (W1+W2+W3+W4-mean(W1+W2+W3+W4));
    % tot = Ws(:,1);
    temp = Ws(:,1);
    for j = 1:phases-1

        temp = circshift(temp,bw2);
        Ws(:,1+j) = temp;
        % tot = tot + temp;

    end

    percent_R_R = zeros([1,phases]);
    
    for j = 1:phases
        
        t = find(Ws(:,j) == max(Ws(:,j)));
    
        percent_R_R(j) = t(1);
    
    end
    
    idx = find(percent_R_R == min(percent_R_R));
    
    Ws          = circshift(Ws,-idx+1,2);
    percent_R_R = circshift(percent_R_R,-idx+1,2);


end

