% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ Ws ] = Gaussian_weights_trunc( phases, subphases )

%% Compute Guassian/Sinc Temporal Basis Functions

    phase = round(phases/2);
    bw = phases;
    tp = subphases; % subphases
    bw2 = subphases/phases;

    % Gaussian temporal sigmas (sig*2*sqrt(2*log(2)) = FWHM)
    sig = bw2./(2*sqrt(2*log(2)));

    bins = 1:bw2:tp;
    sel = find(bins < bw2*phase,1,'last');
    mu = (bins(sel)-1) + (bw2-1)/2+1;

    Ws = zeros(tp,phases);

    % Compute circular Gaussian weights

    d = min([abs(mu-(1:tp)); abs(mu+bw2*bw-(1:tp)); abs(mu-bw2*bw-(1:tp))]);

    Ws(:,1) = exp(-(d.^2)./(2*sig^2));
    
    % threshold at +/- 3 std. (99.7% density)
    temp = Ws(:,1);
    temp(d > sig*3) = 0;
    Ws(:,1) = temp;

    Ws(:,1) = Ws(:,1)./sum(Ws(:,1));

    % figure(i); plot(Ws(:,i));

    % Compute correction to make a uniform distribution
    % err = (W1+W2+W3+W4-mean(W1+W2+W3+W4));
    tot = Ws(:,1);
    temp = Ws(:,1);
    for j = 1:phases-1

        temp = circshift(temp,bw2);
        Ws(:,1+j) = temp;
        tot = tot + temp;

    end

    % Correct to sum to a uniform distribution
    % Ws = bsxfun(@minus,Ws,(tot-mean(tot))./length(bins));

        % Check summation

    %     tot2 = Ws(:,i);
    %     temp = Ws(:,i);
    %     for j = 1:length(bins{i})-1
    %         
    %         temp = circshift(temp,bw2(i));
    %         tot2 = tot2 + temp;
    %     
    %     end

        % figure(i); plot(tot2);

    % end
    
    %% Convert to step-function weights
    
%     for i = 1:length(Wsc)
%        
%         for j = 1:size(Wsc{i},2)
%            
%             Wsc{i}(:,j) = Wsc{i}(:,j) == max(Wsc{i},[],2);
%             Wsc{i}(:,j) = Wsc{i}(:,j)./sum(Wsc{i}(:,j));
%             
%         end
%         
%         % Check summation
% 
%         % figure(i); plot(sum(Wsc{i},2));
%         
%     end
    
    %% Visualize temporal and spectral sampling
    
    % rot = 360;

    % channels = zeros(tp,rot,3);
    
%     for v = 1:3
%     
%         for t = (v-1)*rot + 1:v*rot
%        
%             g = (ang_list(angles(t)) + 0.5)./0.5;
%             channels(ts(t),g,v) = channels(ts(t),g,v) + 1;
%             
%         end
%         
%     end
% 
% %     for t = 1:tproj
% % 
% %         for v = 1:3
% % 
% %             channels(ts(t),round(ang_list(angles(t)))+1,v) = channels(ts(t),round(ang_list(angles(t))./2)+1,v) + 1;
% % 
% %         end
% % 
% %     end
% 
%     channels(:,:,1) = mat2gray(channels(:,:,1));
%     channels(:,:,2) = mat2gray(channels(:,:,2));
%     channels(:,:,3) = mat2gray(channels(:,:,3));
% 
%     figure(9); image(channels); axis tight; xlabel('angle'); ylabel('time (ms)');
%     title('Energy vs. Time vs. Angle');


end

