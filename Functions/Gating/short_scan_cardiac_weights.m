% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ Ws, percent_R_R ] = short_scan_cardiac_weights( phase, num_phases, np_rot, fan_angle, pad_z, limit_multiplier )

%% Decide on the R-R interval percents for reconstruction

    percent_R_R = linspace(0,100,num_phases+1) + 100 / (2 * num_phases);
    
    percent_R_R = percent_R_R(1:end-1);
    
%% Contiguous, short-scan basis functions

    np = length(phase);

    short_proj_num = ceil( limit_multiplier * ( np_rot * (180 + fan_angle) / 360 ) );
    
    [~,intervals] = find(phase(2:end) - phase(1:end-1) < 0);
    
    Ws = zeros( [np num_phases], 'double' );
      
    for i = 1:length(intervals)+1

        if i == 1

            current_interval = 1:intervals(1);

        elseif i == length(intervals)+1
        
            current_interval = intervals(end):np;
                
        else

            current_interval = intervals(i-1)+1:intervals(i);

        end
        
        phase_ = phase(current_interval);

        for t = 1:num_phases
            
            dist = abs(phase_ - percent_R_R(t)/100);

            dist = min(dist,1.0-dist);
            
            if min(dist) > 1/num_phases
               
                continue;
                
            end

            [~,mid] = find( dist == min(dist) );
            
            interval = (current_interval(1) + mid - 1);
            interval = floor(interval - short_proj_num/2):ceil(interval + short_proj_num/2);
            interval = interval(1:short_proj_num);
            
            if interval(1) < 0
               
                interval = interval - interval(1) + 1;
                
            end
            
            if interval(end) > np
               
                interval = interval + ( np - interval(end) );
                
            end
            
            Ws(interval,t) = 1;

        end

    end
    
%% Override the temporal weights to unify the reconstructed z FoV across phases

    if pad_z == 1
       
        Ws(1:short_proj_num,:) = Ws(1:short_proj_num,:) + 1;
        
        Ws(end-short_proj_num+1:end,:) = Ws(end-short_proj_num+1:end,:) + 1;
        
    end
    
    Ws = min(Ws,1.0);

end

