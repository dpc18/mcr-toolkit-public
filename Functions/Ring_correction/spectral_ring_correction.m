% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % Candès, E. J., Li, X., Ma, Y., & Wright, J. (2011).
    % Robust principal component analysis?.
    % Journal of the ACM (JACM), 58(3), 1-37.

%%

function [Y_out] = spectral_ring_correction( Y, sinogram_rank, summarize_results, return_LP, limit_rank )

    sz = size(Y);
    nu = sz(1);
    nv = sz(2);
    np = sz(3);
    ne = sz(4);
    
    tic;

    [ U, E, V ] = eig_SVD( reshape(single(Y),[nu*nv*np ne]), 1, 1 );

    U     = reshape(U,[nu nv np ne]);
    U_out = U;

    n = nu*nv; % assume more rows than columns
    p = 1;
    e = np;
    tol = 1e-4;   % 5e-5; % 3e-4; % 5e-4;

% Fix the average projections
    
    for c = 1

    % Extract ?

        disp(['Working on component ' num2str(c) '...']);

        chunk = U(:,:,:,c);

    % Low pass filter

        G = exp( -(-30:30).^2 / (2 * 8^2 ) );
        G = G./sum(G);

        chunkG = cat(1,chunk(30:-1:1,:,:),chunk,chunk(end:-1:end-30+1,:,:));

        chunkG = convn(chunkG,G','same');

        chunkG = chunkG(31:end-30,:,:);
        
        chunkG = cat(2,chunkG(:,30:-1:1,:),chunkG,chunkG(:,end:-1:end-30+1,:));

        chunkG = convn(chunkG,G,'same');

        chunkG = chunkG(:,31:end-30,:);

        diff = chunk-chunkG;

    % Rank reduction

        % Limit the rank of XL more to prevent valid sinogram data from being assigned to XL
        [ XL, XS ] = RPCA(reshape(diff,[nu*nv np]),n,p,e,tol,'max_rank',sinogram_rank);

        XL = reshape(XL,[nu nv np]);
        XS = reshape(XS,[nu nv np]);
        
    % Filter XL along columns to return signal
    
        if return_LP > 0
    
            XLG = cat(2,XL(:,30:-1:1,:),XL,XL(:,end:-1:end-30+1,:));

            XLG = convn(XLG,G,'same');

            XLG = XLG(:,31:end-30,:);
        
        else
            
            XLG = 0;
            
        end

    % Report residual for QA purposes
    
        disp(['Relative sinogram residual: ' num2str( norm( diff(:)-XS(:)-XLG(:) ) / norm( diff(:) ) )]);

    % Put the deringed result into the output

        U_out(:,:,:,c) = XS + chunkG + XLG;

    end

    clear chunkG XLG diff XL XS;
    
% Fix the spectral components

    for c = 2:ne
        
        if c > limit_rank
            
            U_out(:,:,:,c) = U(:,:,:,c);
            
        else

        % Extract XS

            disp(['Working on component ' num2str(c) '...']);

            chunk = U(:,:,:,c);

        % Rank reduction

            % Limit the rank of XL more to prevent valid sinogram data from being assigned to XL
            [ ~, XS ] = RPCA(reshape(chunk,[nu*nv np]),n,p,e,tol,'max_rank',sinogram_rank);

            % XL = reshape(XL,[nu nv np]);
            XS = reshape(XS,[nu nv np]);

        % Report residual for QA purposes

            disp(['Relative sinogram residual: ' num2str( norm( chunk(:)-XS(:) ) / norm( chunk(:) ) )]);

        % Put the deringed result into the output

            U_out(:,:,:,c) = XS;
            
        end

    end
    
    clear U XS chunk;

    toc;
    
%% Reconstruction output

    Y_out = single(reshape(U_out,[nu*nv*np ne])*E*V');
    
    Y_out = reshape(Y_out,[nu nv np ne]);
    
    if summarize_results > 0
       
        imc({ Y(:,round(nv/2),:,:), Y_out(:,round(nv/2),:,:) },[ nu np*ne ],1,1);
        
    end
    
end

function [ XL, XS ] = RPCA(X,n,p,e,tol,flag,varargin)

    % Check the flag to make sure we made a conscious choice about which
    % form of RPCA to use...
    if (~strcmp('classic',flag) && ~strcmp('zero',flag) && ~strcmp('restrict_magnitude',flag) && ~strcmp('max_rank',flag) )
       
        error(['RPCA flag must be either "classic" (Candes et al.), ' ... 
               ' "zero" (zero-mean XS for temporal recon.),' ...
               ' "restrict_magnitude" ( |XS| cannot be larger than |X| ),' ...
               ' or "max_rank" (hard limit on the rank of XL).']);
        
    end
    
    if strcmp('max_rank',flag) && nargin > 0
       
        r = varargin{1};
        
    end

    X = reshape(double(X),[n p*e]);
    XL = X;
    XS = X*0;
    mu = e*n*p./(4*norm(X(:),1));
    lambda = 1./sqrt(n);
    threshL = 1./mu;
    threshS = lambda/mu;
    f = 0;
    
    miniter = 30;
    
    [U,E,V] = svds(XL,1);
    XL = U*E*V';
    XS = X - XL;
    
    % convc = 2e-5*norm(X,'fro');
    convc = tol*norm(X,'fro');
    fid = 0;
    iter = 0;
    
    while iter < miniter || fid > convc
        
        iter = iter + 1;
        
    % Update XL
    
        XL = X - XS + f./mu;
        
        if strcmp('max_rank',flag)
            
            [U,E,V] = svds(XL,r);
            E = diag(E);
            E = diag(max(E-threshL,0));
            
            XL = U*E*V';
            
        else
        
            [U,E,V] = svd(XL,'econ');
            E = diag(max(diag(E)-threshL,0));
            XL = U*E*V';
        
        end

    % Update XS
    
        XS = X - XL + f./mu;
        
        if strcmp('classic',flag) || strcmp('max_rank',flag) % 'classic' RPCA (Candes et al.) with soft thresholding
            
            XS = sign(XS).*max(bsxfun(@minus,abs(XS),threshS),0);
        
        elseif strcmp('zero',flag) % zero mean XS for temporal reconstruction
            
            XS = bsxfun(@minus,XS,mean(XS,2));
            
        elseif strcmp('restrict_magnitude',flag)
            
            XS = sign(XS).*max(bsxfun(@minus,abs(XS),threshS),0);
            
            XS = sign(XS).* min( abs(XS), abs(X) );
            
        end

    % Update Residuals
    
        f = f + mu*(X - XL - XS);
        
        fid = norm(X-XL-XS,'fro');
        
        if (iter < miniter || mod(iter,10) == 0)
        
            disp(['Resid ' num2str(fid) ' vs. ' num2str(convc)]);
            
        end
            
    end
    
    disp(['Number of RPCA iterations: ' num2str(iter)]);
    
    % XS = X - XL; % prevent the loss of data fidelity due to incomplete convergence
    
    XL = reshape(XL,[n p e]);
    XS = reshape(XS,[n p e]);

end