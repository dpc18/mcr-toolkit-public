% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [ Y ] = suppress_rings( Y, batch_size )

    np = size(Y,3);
    e  = size(Y,4);

    % Y = load_nii(proj_file); Y = Y.img;

    for i = 1:e

        rad = 7; % 5;

        Ymv = convn(Y(:,:,:,i),ones(1,1,rad),'same')./rad;
        % Ymr = convn(Y(:,:,:,i),ones(rad,1,1),'same')./rad;
        Ymr = Y(:,:,:,i);
        
        for range = 1:batch_size:np
            
            rng = range:(range+batch_size-1);
            
            Y_ = Ymr(:,:,rng);
        
            parfor j = 1:batch_size % np

                slice = Y_(:,:,j);

                slice = medfilt2(slice,[rad rad]);

                Y_(:,:,j) = slice;

            end
            
            Ymr(:,:,rng) = Y_;

            diff = Ymv(:,:,rng) - Ymr(:,:,rng);
            diff = median(diff(:,:,rad:end-rad),3);

            Y_ = Y(:,:,rng,i);

            parfor j = 1:batch_size % np

                diff2 = diff;

                diff2 = sign(diff2).*min(abs(diff2),abs(Y_(:,:,j)));

                Y_(:,:,j) = Y_(:,:,j) - diff2;

            end 

            Y(:,:,rng,i) = Y_;
            
            disp(['Done with th ' num2str(i) ', ' num2str(range+batch_size-1) ' / ' num2str(np)]);
        
        end
        
        clear Y_;

    end

end

