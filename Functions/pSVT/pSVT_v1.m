% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ d ] = pSVT_v1( X, mask, sz, w, nu, multiplier )
% Performs pSVT in the image domain (i.e. c = 1 sub-band).

        c = 1;
        e = size(X,2);
            
        disp('Threshold estimation...');
        
            sig_X = zeros(1,e);
        
            % convert back to image domain
            
            for s = 1:e
                
                sig_X(s) = measure_noise( X(:,s), 1, mask, sz);
            
            end
            
            Xc = bsxfun(@times,randn([prod(sz) e]),sig_X);
            
            szs = sz;
            
            threshs = measure_thresholds( reshape(Xc,[sz e]), e, w ); % expected variability from Gaussian noise < 1% (125 test patches)
            
        disp('Patch-based singular value thresholding...');
            
            tic;
            [d,~] = pSVT(single( X ), int32(c), int32(szs), int32(e), single(threshs), int32(w), single(nu), single(multiplier));
            toc;

end

function [ sig_X ] = measure_noise( X, attn_water, mask, szb)

    MAD = @(x) median(abs(x(:))) / 0.6745;

    e = size(X,2);
    
    % Xc = D(X);
    % maskc = D(mask);
    sig_X = zeros(1,e);
    
    maskc = reshape(mask, szb);
    maskc = maskc(:,:,round(szb(3)/2));
    
    for s = 1:e
       
        grad = reshape(X(:,s)./attn_water(s),szb);
        grad = grad(:,:,round(szb(3)/2));
        grad = convn(convn(grad,[1 -1],'same'),[1; -1],'same');
        
        sig_X(s) = MAD(0.5*grad(maskc(:) > 0));
        
    end

end

function [ threshs ] = measure_thresholds( X, e, w )

    threshs = zeros(e,1);
    
    lbx = round(size(X,1)/2-size(X,1)/4);
    ubx = round(size(X,1)/2+size(X,1)/4);
    
    lby = round(size(X,2)/2-size(X,2)/4);
    uby = round(size(X,2)/2+size(X,2)/4);
    
    lbz = round(size(X,3)/2-size(X,3)/4);
    ubz = round(size(X,3)/2+size(X,3)/4);
    
    x = round(linspace(lbx,ubx,5));
    y = round(linspace(lby,uby,5));
    z = round(linspace(lbz,ubz,5));
    
    for pz = 1:5
    
        for py = 1:5
    
            for px = 1:5

                % extract patch
                patch = X((x(px)-w):(x(px)+w),(y(py)-w):(y(py)+w),(z(pz)-w):(z(pz)+w),:);

                % perform svd
                patch = reshape(patch,[(2*w+1)^3 e]);

                E = svd(patch,0);

                % measure singular values
                threshs = threshs + (1/5^3)*E;

            end
        
        end
    
    end

end