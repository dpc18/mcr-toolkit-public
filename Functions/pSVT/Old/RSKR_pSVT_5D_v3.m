% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ X ] = RSKR_pSVT_5D_v3( X, stride, w, w2, lambda0, sz0, attn_water, g, nE, expected_rank, BF_path, mask, w_pSVT, nu_pSVT, multiplier_pSVT, gpu_idx )
% Make sense of data with something to hide...
% Instead of resampling, applies pSVT along the time dimension.
% Updated to work with v5 of the BF.
% TO DO: It doesn't make sense to estimate the noise level for pSVT after applying BF.
% TO DO: Application of pSVT should not be gated behind the use of resampling.

    % Handle cases where multiple volumes are stacked along the z axis
    
        sz_z = sz0(3);
        szp  = sz0;

        if length(sz0) == 4

            sz0 = [sz0(1:2) sz0(3)*sz0(4)];

        end
        
    % Since we will be changing to the BF path, keep track of our initial working directory.

        cdir = pwd;
        
    % Shuffled/strided sampling to decrease (mo), increase (demo) resolution

        mo = @(x) mosaic(x,stride,sz0);
        demo = @(x) demosaic(x,stride,sz0);
        
    % Water and noise level normalization to reweight singular vectors
    
        sz = size(X);
        X = bsxfun(@rdivide,X,attn_water);
        
    % Efficient SVD
    
        [ U, E, V ] = eig_SVD( X, 1, 1 ); % Use an efficient and accurate shortcut for tall, skinny matrices

        % Prevent ill-conditioning
        sub_sig = sum(diag(E) < 1e-4*E(1,1));  
        range = 1:min((size(X,2)-sub_sig),nE); % truncate to the number of energies
        U = U(:,range);
        E = E(range,range);
        V = V(:,range);
        sz(2) = length(range);

        U = single(U);
        E = single(E);
        V = single(V);

        clear X;

    % Calibrate regularization stength vs. the first singular vector

        mu = lambda0*(E(1,1)./diag(E)').^(g)

    % BF setup

        A_r = single(make_A_approx_3D_v2( double(6), double(w2) ));
        cd(BF_path);

        BFr  = @(x, xr) jointBF4D(single(x),int32(sz(2)),int32(1),A_r,single(1),int32(sz0),int32(w),single(mu),int32(gpu_idx),int32(0),single(xr));
        pSVT = @(x)     pSVT_v1( x, mask, [sz0(1:2) sz_z], w_pSVT, nu_pSVT, multiplier_pSVT );
        
    % Bregman initialization
    
        d = U*0;
        v = d;
        U0 = U;
        iter = 0;
    
    % double precision dot product
    
        % dd = @(x,y) single(double(x)'*double(y)); % Matlab version (fast, but uses tons of RAM)
        dd = @(x,y) ddm(single(x),single(y)); % CPU version (slower, but uses almost no additional RAM)
    
    % Bregman loop
    % while iter < 3
    while iter < 4
        
        iter = iter + 1;

        if (stride == 1)
            
            d = BFr(U+v,U+v);

        else
            
            d = mo(U+v);
            d = BFr(d,d);
            d = demo(d) + BFr(U+v,U+v);
            d = d./2;

        end

        if (w2 > 0.01)
            
            disp('Performing resampling with pSVT...');
            
            % d = resamp(d,A_r,sz0);
            
        % Perform pSVT along another dimension (e.g. time)
            
            d = reshape(d,[prod(szp(1:3)) szp(4) sz(2)]);
            
            for s = 1:sz(2)
               
                d(:,:,s) = pSVT(d(:,:,s));
                
                disp(['pSVT: Done with energy ' num2str(s) '...']);
                
            end
            
            d = reshape(d,[prod(szp(1:3))*szp(4) sz(2)]);

        % Perform pSVT along the same dimension (e.g. energy)

            % d = pSVT(d);
            % 
            
            d = resamp(d,A_r,sz0);
        
        end
        
        d(~isfinite(d)) = 0;

        v = v + U - d;
        
        norm_Ax_b = norm( reshape( U - bsxfun(@rdivide, U0 + (d-v)*diag(mu), 1 + mu), [prod(sz0)*sz(2) 1]) );
        norm_b = norm( reshape(bsxfun(@rdivide, U0 + (d-v)*diag(mu), 1 + mu), [prod(sz0)*sz(2) 1]) );
        
        delta_norm = norm_Ax_b ./ norm_b;
        
        U = bsxfun(@rdivide, U0 + (d-v)*diag(mu), 1 + mu);
        
        disp(['Iter ' num2str(iter) ', Convergence criterion: ' num2str(delta_norm)]); % ' (vs. 0.01)']);
        
    end
    
    X = bsxfun(@times,U(:,1:expected_rank)*E(1:expected_rank,1:expected_rank)*V(:,1:expected_rank)',attn_water);
    
    cd(cdir);
    
end

function [U] = resamp(U,A,sz0)

    w = 6;

    e = size(U,2);
    U = reshape(U,[sz0 e]);
    U = padarray(U,[w w w 0],'symmetric');
    
    for s = 1:e
       
        U(:,:,:,s) = convn(convn(convn(U(:,:,:,s),A(:,1),'same'),A(:,1)','same'),reshape(A(:,1),[1 1 2*w+1]),'same') + ...
                     convn(convn(convn(U(:,:,:,s),A(:,2),'same'),A(:,2)','same'),reshape(A(:,2),[1 1 2*w+1]),'same') + ...
                     convn(convn(convn(U(:,:,:,s),A(:,3),'same'),A(:,3)','same'),reshape(A(:,3),[1 1 2*w+1]),'same');
        
    end
    
    U = U(w+1:end-w,w+1:end-w,w+1:end-w,:);
    U = reshape(U,[prod(sz0) e]);

end

function [ u ] = nan_check(u)

    u(~isfinite(u)) = 0;

end

function [ X ] = mosaic(X_,stride,sz)

    if (stride == 1)
       
        X = X_;
        
        return;
        
    end

    sz2 = [sz size(X_,2)];
    
    X_ = reshape(X_,sz2);
    
    X = X_;
    
    x = 1; y = 1; z = 1;
    
    for x_ = 1:stride
        
        for y_ = 1:stride
            
            for z_ = 1:stride
                
                lenx = length(x_:stride:sz(1));
                leny = length(y_:stride:sz(2));
                lenz = length(z_:stride:sz(3));
                
                X(x:(x+lenx-1),y:(y+leny-1),z:(z+lenz-1),:) = X_(x_:stride:sz(1),y_:stride:sz(2),z_:stride:sz(3),:);
                
                z = z + lenz;
                
            end
            
            z = 1;
            y = y + leny;
            
        end
        
        y = 1;
        x = x + lenx;
        
    end
    
    X = reshape(X,[prod(sz) sz2(end)]);

end

function [ X ] = demosaic(X_,stride,sz)

    if (stride == 1)
       
        X = X_;
        
        return;
        
    end

    sz2 = [sz size(X_,2)];
    
    X_ = reshape(X_,sz2);
    
    X = X_;
    
    x = 1; y = 1; z = 1;
    
    for x_ = 1:stride
        
        for y_ = 1:stride
            
            for z_ = 1:stride
                
                lenx = length(x_:stride:sz(1));
                leny = length(y_:stride:sz(2));
                lenz = length(z_:stride:sz(3));
                
                % X(x:(x+lenx-1),y:(y+leny-1),z:(z+lenz-1),:) = X_(x_:stride:sz(1),y_:stride:sz(2),z_:stride:sz(3),:);
                X(x_:stride:sz(1),y_:stride:sz(2),z_:stride:sz(3),:) = X_(x:(x+lenx-1),y:(y+leny-1),z:(z+lenz-1),:);
                
                z = z + lenz;
                
            end
            
            z = 1;
            y = y + leny;
            
        end
        
        y = 1;
        x = x + lenx;
        
    end
    
    X = reshape(X,[prod(sz) sz2(end)]);

end

function [ M ] = ddm(a,b)
 % compute a'*b with matrices 
% accuracy of single(double(a)'*double(b)) at a fraction of the memory usage, but ~2x execution time

    M = zeros(size(a,2),size(b,2));

    for i = 1:size(a,2)
        
        for j = 1:size(b,2)
            
            M(i,j) = dds(a(:,i),b(:,j));
            
        end
        
    end

end