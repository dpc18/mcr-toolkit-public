% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

% TO DO: Update BF and pSVT to use multiple GPUs.
% TO DO: Modify the pSVT code to allow GPU selection by GPU index.
    
function [ X ] = RSKR_pSVT_5D_v5( X, sz0, stride, w, w2, lambda0, g, attn_scaling, apply_pSVT, mask, w_pSVT, nu_pSVT, multiplier_pSVT, gpu_idx )
% Make sense of data with something to hide...
% When the resampling bandwidth, w2, is zero, apply pSVT along the time dimension.
% When the resampling bandwidth is greater than 0, apply kernel resampling.
% Updated to work with v5 of the BF.
%
% X                : input data to filter, size: [x*y*z*t e]
% sz0              : [sz(1:3) p]
% stride           : filter data at a coarser resolution level to address correlated noise;
%                    average with nominal resolution to control bias (two BF operations)
% w                : kernel radius (diameter = 2*raidus+1; currently restricted to w = 6)
% w2               : kernel resampling factor ( w2 = 0, use pSVT if there is a time dimension; w2 > 0, use kernel resampling )
% lambda0, g       : regularization strength scaling based on singular values, E; mu = lambda0*(E(1,1)./diag(E)').^(g)
% attn_scaling     : pre-SVD per-energy scaling; typically attn_scaling(e) = mu_water(e) * rel_sigma(e)
% gpu_idx          : the index of the GPU to use for filtration (system indexing starts from 0)
%                    typically, set this to zero to use the strongest GPU available on the system

% pSVT variables            : parameters only used when pSVT is used (w2 = 0, time dimension > 1)
%   apply_pSVT              : (0) override, don't apply pSVT even if there is a time dimension
%                             (1) apply pSVT when there is a time dimension
%   mask                    : single volume of size [x y z] with 1.0 indicating voxels to use for
%                             global noise estimation
%   w_pSVT                  : kernel diameter (values larger than 3 will result in very high computational cost)
%      
%   nu_pSVT, multiplier_pSVT: given patch matrix singular values E0,
%                             and data-adapative thresholds per singular value, "thresholds"
%                             E_out(i) = max( E0(i) - thresholds(i) * multiplier_pSVT * ( E0(i) / E0(1) ) ^ ( nu_pSVT - 1.0 ), 0.0 )

    % Handle cases where multiple volumes are stacked along the z axis
    
        sz_z = sz0(3);
        szp  = sz0;

        sz0 = [sz0(1:2) sz0(3)*sz0(4)];
            
        sig_t = measure_noise( reshape(X(:,1), [prod(szp(1:3)) szp(4)]), ones([1,szp(4)]), mask, size(mask));
        sig_t = sig_t / min(sig_t);
        
    % Since we will be changing to the BF path, keep track of our initial working directory.

        cdir = pwd;
        BF_path = fileparts(which('jointBF4D')); % assume the jointBF4D function on the path is v5
        
    % Shuffled/strided sampling to decrease (mo), increase (demo) resolution

        mo   = @(x) mosaic  (x,stride,sz0);
        demo = @(x) demosaic(x,stride,sz0);
        
    % Water and noise level normalization to approximately arrange singular vectors by SNR.
    % Water normalization roughly equalizes denoising performnce after reconstructed data is converted to HU.
    
        sz = size(X);
        X = bsxfun(@rdivide,X,attn_scaling);
        
    % Efficient SVD
    
        [ U, E, V ] = eig_SVD( X, 1, 1 ); % Use an efficient and accurate shortcut for tall, skinny matrices

        % Prevent ill-conditioning
        sub_sig = sum(diag(E) < 1e-4*E(1,1));  
        % range = 1:min((size(X,2)-sub_sig),nE); % truncate to the number of energies
        range = 1:(size(X,2)-sub_sig);
        U = U(:,range);
        E = E(range,range);
        V = V(:,range);
        sz(2) = length(range);

        U = single(U);
        E = single(E);
        V = single(V);

        clear X;

    % Calibrate regularization stength vs. the first singular vector

        disp('Regularization strength scaling by singular vector...');
        mu = lambda0*(E(1,1)./diag(E)').^(g)
        
    % Adjust regularization strength to variable noise level by time point
    % Broadcast to a time,energy matrix.
    
        disp('Regularization strength scaling by time point and singular vector...');
        mu2 = sig_t(:) * mu

    % BF setup
    
        if (w2 == 0)
            
            % Don't evaluate this function with w2 = 0
            A_r = single(make_A_approx_3D_v2( double(6), double(0.01) ));
        
        else
            
            A_r = single(make_A_approx_3D_v2( double(6), double(w2) ));
            
        end
        
        cd(BF_path);

        BFr  = @(x, xr) jointBF4D(single(x),int32(sz(2)),int32(1),A_r,single(1),int32(sz0),int32(w),single(mu),int32(gpu_idx),int32(0),single(xr));
        pSVT = @(x)     pSVT_v1( x, mask, [sz0(1:2) sz_z], w_pSVT, nu_pSVT, multiplier_pSVT );
        
    % Bregman initialization
    
        d = U*0;
        v = d;
        U0 = U;
        iter = 0;
        
        U0  = reshape(U0,[prod(szp(1:3)) szp(4) sz(2)]);
        mu2 = reshape(mu2,[1,szp(4),sz(2)]);
    
    % Bregman loop
    % while iter < 3
    while iter < 4
        
        iter = iter + 1;

        if (stride == 1)
            
            disp('Performing BF at nominal resolution...');
            
            d = BFr(U+v,U+v);

        else
            
            disp(['Performing BF at nominal resolution and coarse resolution (stride = ' num2str(stride) ')...']);
            
            d = mo(U+v);
            d = BFr(d,d);
            d = demo(d) + BFr(U+v,U+v);
            d = d./2;

        end

        if ( szp(4) > 1 && apply_pSVT == 1 )
            
        % Perform pSVT along the time dimension
            
            disp('Performing resampling with pSVT...');
            
            d  = reshape(d,[szp(1:3) szp(4) sz(2)]);
            
            N  = 1.0 / w_pSVT^3;
            dm = N * convn(convn(convn( d, ones( [w_pSVT,1,1], 'single' ), 'same' ), ...
                                           ones( [1,w_pSVT,1], 'single' ), 'same' ), ...
                                           ones( [1,1,w_pSVT], 'single' ), 'same' );
            dm = mean(dm, 4);
            
            d  = reshape(d ,[prod(szp(1:3)) szp(4) sz(2)]);
            dm = reshape(dm,[prod(szp(1:3)) 1      sz(2)]);
            
            d = d - dm;
            
            for s = 1:sz(2)
               
                d(:,:,s) = pSVT(d(:,:,s));
                
                disp(['pSVT: Done with energy ' num2str(s) '...']);
                
            end
            
            d = d + dm;
            
            clear dm;
            
            d = reshape(d,[prod(szp(1:3))*szp(4) sz(2)]);
        
        end
            
        disp('Performing kernel resampling...');
           
        d = resamp(d,A_r,sz0);
        
        d(~isfinite(d)) = 0;

        v = v + U - d;
        
        U = reshape(U,[prod(szp(1:3)) szp(4) sz(2)]);
        d = reshape(d,[prod(szp(1:3)) szp(4) sz(2)]);
        v = reshape(v,[prod(szp(1:3)) szp(4) sz(2)]);
        
        b = ( U0 + mu2 .* ( d - v ) ) ./ ( 1 + mu2 );
        delta_norm = norm( U(:) - b(:) ) ./ norm( b(:) );
        U = b;
        
        clear norm_b;
        
        disp(['Iter ' num2str(iter) ', Convergence criterion: ' num2str(delta_norm)]);
        
        U = reshape(U,[prod(szp(1:3))*szp(4) sz(2)]);
        v = reshape(v,[prod(szp(1:3))*szp(4) sz(2)]);
        
    end
    
    % X = bsxfun(@times,U(:,1:expected_rank)*E(1:expected_rank,1:expected_rank)*V(:,1:expected_rank)',attn_scaling);
    
    X = bsxfun(@times,U*E*V',attn_scaling);
    
    cd(cdir);
    
end

function [U] = resamp(U,A,sz0)

    w = 6;

    e = size(U,2);
    U = reshape(U,[sz0 e]);
    U = padarray(U,[w w w 0],'symmetric');
    
    for s = 1:e
       
        U(:,:,:,s) = convn(convn(convn(U(:,:,:,s),A(:,1),'same'),A(:,1)','same'),reshape(A(:,1),[1 1 2*w+1]),'same') + ...
                     convn(convn(convn(U(:,:,:,s),A(:,2),'same'),A(:,2)','same'),reshape(A(:,2),[1 1 2*w+1]),'same') + ...
                     convn(convn(convn(U(:,:,:,s),A(:,3),'same'),A(:,3)','same'),reshape(A(:,3),[1 1 2*w+1]),'same');
        
    end
    
    U = U(w+1:end-w,w+1:end-w,w+1:end-w,:);
    U = reshape(U,[prod(sz0) e]);

end

function [ sig_X ] = measure_noise( X, attn_water, mask, szb )

    MAD = @(x) median(abs(x(:))) / 0.6745;

    e = size(X,2);
    
    % Xc = D(X);
    % maskc = D(mask);
    sig_X = zeros(1,e);
    
    maskc = reshape(mask, szb);
    maskc = maskc(:,:,round(szb(3)/2));
    
    for s = 1:e
       
        grad = reshape(X(:,s)./attn_water(s),szb);
        grad = grad(:,:,round(szb(3)/2));
        grad = convn(convn(grad,[1 -1],'same'),[1; -1],'same');
        
        sig_X(s) = MAD(0.5*grad(maskc(:) > 0));
        
    end

end