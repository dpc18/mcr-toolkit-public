% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [ images_out ] = imc( images, sz, gamma, show )
% Compare two or more sets of images with an absolute difference image
% images: {reference image, test image 1, test image 2, ...}
% sz: dimensions of a single row of the output
% gamma: intensity rescaling exponent
% show: (0) do not show output; (1) show output 

    % Insert absolute difference images into the cell array
    for i = 2:2:2*(length(images)-1)
        
        if i < length(images)
           
            images = [images(1:i) {abs(reshape(images{1},sz)-reshape(images{i},sz))} images(i+1:end)];
            
        else
            
            images = [images(1:i) {abs(reshape(images{1},sz)-reshape(images{i},sz))}];
            
        end
        
    end
    
    % Display the results as an image
    images_out = []; % zeros(sz(1)*length(images),sz(2));
    
    for i = 1:length(images)
       
        images_out = [images_out; reshape(images{i},sz)];
        
    end
    
    if (size(images_out,1) > size(images_out,2))
       
        images_out = images_out';
        
    end
    
    min_val = min(images_out(:));
    
    images_out = images_out - min_val;
    
    images_out = images_out.^(gamma);
    
    images_out = images_out + min_val;
    
    if show
    
        % figure(fignum);
        % imagesc(images_out);
        % axis image off; colormap gray;
        % colorbar;
        imtool(mat2gray(images_out),'InitialMagnification',100*ceil(1080/max(size(images_out))));
        drawnow;
    
    end
    
end

