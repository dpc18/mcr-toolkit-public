% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [ X ] = mosaic(X_,stride,sz)

    if (stride == 1)
       
        X = X_;
        
        return;
        
    end

    sz2 = [sz size(X_,2)];
    
    X_ = reshape(X_,sz2);
    
    X = X_;
    
    x = 1; y = 1; z = 1;
    
    for x_ = 1:stride
        
        for y_ = 1:stride
            
            for z_ = 1:stride
                
                lenx = length(x_:stride:sz(1));
                leny = length(y_:stride:sz(2));
                lenz = length(z_:stride:sz(3));
                
                X(x:(x+lenx-1),y:(y+leny-1),z:(z+lenz-1),:) = X_(x_:stride:sz(1),y_:stride:sz(2),z_:stride:sz(3),:);
                
                z = z + lenz;
                
            end
            
            z = 1;
            y = y + leny;
            
        end
        
        y = 1;
        x = x + lenx;
        
    end
    
    X = reshape(X,[prod(sz) sz2(end)]);

end