% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [ limit_angular_ranges ] = variable_angular_range( collimation, pitch, zo, zo_, nz, dz )
% Assumes the center of the reconstruction volume is assigned to half the z range of the total travel.
% Assumes the pitch is <= 1.0.

    % minimum range required for reconstruction with cos^2 weights
    range0    = collimation * pitch / 2.0;
    max_range = collimation/2;
    
    z_vol  = dz*(0:nz-1) - dz*(nz/2);
    z_proj = zo-zo_;
    
    difference = z_vol' - z_proj;
    divergence = min(abs(difference),[],2);

    limit_angular_ranges = min(range0 + divergence,max_range);

end

