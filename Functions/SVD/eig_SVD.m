% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
function [ U, E, V ] = eig_SVD( X, more_accurate, limited_memory )
% Faster / more accurate SVD for assymetric matrices
% Input:
%   X: matrix to compute SVD from
%   more_accurate: anything other than 0, uses C implementation of single/double precision inner products or double precision if not limited_memory
%   limited_memory: anything other than 0, keep memory usage in check

    if (size(X,2) < size(X,1)) % tall and skinny matrix

        if (limited_memory && ~(more_accurate)) % use single precision

            X = single(X);

            [V,E2] = eig(X'*X);

        elseif (limited_memory && more_accurate) % used mixed precision inner product calculations

            X = single(X);

            [V,E2] = eig(ddm(X,X));

        elseif (~(limited_memory) && more_accurate) % convert to double if needed

            if isa(X,'double')

                [V,E2] = eig(X'*X);

            else

                X = double(X);

                [V,E2] = eig(X'*X);

            end

        else % use native precision

            [V,E2] = eig(X'*X);

        end

        % E2 can sometimes come out negative
        E2 = max(E2,0);

        E = sqrt(E2);

        % not sorted like SVD...
        % idx = find(diag(E) >= 1e-6);

        E = diag(E)';

        [~,idx] = sort(E,'descend');

        E = diag(E(idx));
        V = V(:,idx);

        rank = sum(diag(E) >= 1e-6);
        idx = 1:rank;

        V = V(:,idx);
        E = E(idx,idx);

        % Does U ever need to be normalized after this calculation?
        U = X * (V / E);
    
    else % narrow and fat matrix or square matrix
        
        if (limited_memory && ~(more_accurate)) % use single precision

            X = single(X);

            [U,E2] = eig(X*X');

        elseif (limited_memory && more_accurate) % used mixed precision inner product calculations

            X = single(X);

            [U,E2] = eig(ddmt(X,X));

        elseif (~(limited_memory) && more_accurate) % convert to double if needed

            if isa(X,'double')

                [U,E2] = eig(X*X');

            else

                X = double(X);

                [U,E2] = eig(X*X');

            end

        else % use native precision

            [U,E2] = eig(X*X');

        end

        % E2 can sometimes come out negative
        E2 = max(E2,0);

        E = sqrt(E2);

        % not sorted like SVD...
        % idx = find(diag(E) >= 1e-6);

        E = diag(E)';

        [~,idx] = sort(E,'descend');

        E = diag(E(idx));
        U = U(:,idx);

        rank = sum(diag(E) >= 1e-6);
        idx = 1:rank;

        U = U(:,idx);
        E = E(idx,idx);

        % Does U ever need to be normalized after this calculation?
        V = E \ (U' * X);
        
        V = V';
        
    end

end

function [ M ] = ddm(A, B)
% compute A'*B with matrices 
% accuracy of single(double(A)'*double(B)) at a fraction of the memory usage, but ~2x execution time

    M = zeros(size(A,2),size(B,2));

    for i = 1:size(A,2)
        
        for j = 1:size(B,2)
            
            M(i,j) = dds(A(:,i),B(:,j));
            
        end
        
    end

end

function [ M ] = ddmt(A, B)
% compute A*B' with matrices 
% accuracy of single(double(A)*double(B)') at a fraction of the memory usage, but ~2x execution time

    M = zeros(size(A,1),size(B,1));

    for i = 1:size(A,1)
        
        for j = 1:size(B,1)
            
            M(i,j) = dds(A(i,:),B(j,:));
            
        end
        
    end

end