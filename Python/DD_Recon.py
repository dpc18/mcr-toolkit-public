
# Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
# Original Author: Darin Clark, PhD
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# References

# Long, Y., Fessler, J. A., & Balter, J. M. (2010).
# 3D forward and back-projection for X-ray CT using separable footprints.
# IEEE transactions on medical imaging, 29(11), 1839-1850.
#
# Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
# Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
# Physics in Medicine & Biology, 49(11), 2209.
#
# Van der Vorst, H. A. (1992).
# Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
# SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
#
# Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
# Cone-beam reprojection using projection-matrices.
# IEEE transactions on medical imaging, 22(10), 1202-1214.
#
# Sleijpen, G. L., & Van Gijzen, M. B. (2010).
# Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
# SIAM journal on scientific computing, 32(5), 2687-2709.
# Algorithm 3.1


# WARNING: Numpy (1.19.2) assumes C ordering when calling np.reshape()! 'F' Fortran ordering must be specified for compatbility with the GPU
#          reconstruction operators.
# NOTE: np.copy(x) uses the memory ordering of x by default...


import ctypes
import numpy as np
import time
import sys
import os.path
import math
import csv
import matplotlib
import matplotlib.pyplot as plt

# from PIL import Image
import scipy.signal

matplotlib.use('Qt5Agg')

patcher_path = '/home/x-ray/Documents/MCR_Toolkit/Regularization/PyPatches/make_patches.so'
patcher      = ctypes.CDLL(patcher_path)
patch_maker    = patcher.make_patches
patch_returner = patcher.return_patches

patch_maker.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32), ctypes.c_size_t,
                         ctypes.c_size_t, ctypes.c_size_t, ctypes.c_size_t,
                         ctypes.c_size_t, ctypes.c_size_t, ctypes.c_size_t,
                         ctypes.c_size_t, ctypes.c_size_t, ctypes.c_size_t, ctypes.c_size_t]

patch_returner.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32), ctypes.c_size_t,
                            ctypes.c_size_t, ctypes.c_size_t, ctypes.c_size_t,
                            ctypes.c_size_t, ctypes.c_size_t, ctypes.c_size_t,
                            ctypes.c_size_t, ctypes.c_size_t, ctypes.c_size_t, ctypes.c_size_t]


PI = 3.14159265358979


class Recon():


    def __init__(self, recon_lib_path ):

        self.recon_lib = ctypes.CDLL(recon_lib_path)

    # Flags to make sure all parameters are set before calling operators.

        # [ int_params, double_params, filt_name, geofile, Rt_aff, rmask, gpu_list ]
        self._flags = np.zeros(  (7,), dtype=np.int32 )

        self._op_flag = 0

    # "Private" _variables which are designed to be set through setter functions to
    # allow error checking. Should be treated as read-only by the end-user.

        # [ nu, nv, np, nx, ny, nz, filtration, use_affine, [implicit_mask], [explicit_mask], [cy_detector], [use_norm_vol] ]
        self._int_params    = np.zeros( (12,), dtype=np.int32 )

        # [ du, dv, dx, dy, dz, zo, ao, lsd, lso, uc, vc, eta, sig, phi, [scale], [db], [limit_angular_range] ]
        self._double_params = np.zeros( (17,), dtype=np.double )

        # FDK filter file
        self._filt_name = ''

        # Text file with angle-by-angle geometry information
        self._geofile = ''

        # Image-domain affine transform relative to the specified geometry
        self._Rt_aff = np.zeros( (12,), dtype=np.float32 )

        # Explicit reconstruction mask
        self._rmask = np.zeros( (1,), dtype=np.uint16 )

        # GPUs to use for reconstruction
        self._gpu_list = np.zeros( (1,), dtype=np.int32 )

    # Private variables which should never be directly modified.

        self._recon_alloc       = np.zeros( (1,), dtype=np.uint64 )
        self._int_params_len    = 0
        self._double_params_len = 0
        self._row_oversampling_factor = 1

        self._cone_parallel = False
        self._chain_B       = False
        self._rot_dir = 1
        self._detector_type = 0


    def set_int_params(self, int_params_in, rot_dir):

    # Error checking - int_params_in

        if ( self._op_flag == 1 ):

            print( 'Cannot change int_params associated with a current recon allocation.' )
            return -1

        if ( type(int_params_in) != np.ndarray ):

            print( 'Numpy ndarray is expected.' )
            return -1

        if ( len( int_params_in.shape ) > 1 ):

            print( 'One dimensional array expected.' )
            return -1

        if ( int_params_in.dtype != np.int32 ):

            print( 'np.int32 dtype expected.' )
            return -1

        if ( int_params_in.size < 8 ):

            print( 'Too few int_params specified.' )
            return -1

        if ( int_params_in.size > 12 ):

            print( 'Too many int_params specified.' )
            return -1

        # Check if we are reconstructing cylindrical, cone-parallel data ( int_params_in[10] = 2 ).
        if ( int_params_in.size >= 11 and int_params_in[10] == 2 ):

            if ( int_params_in.size != 12 ):

                print( 'All 12 int_params are required for cylindrical, cone-parallel data.' )
                return -1

            self._cone_parallel = True

    # Error checking - rot_dir

        if ( not isinstance(rot_dir, int) ):

             print( 'Rotation direction must be an integer.' )
             return -1

        if ( rot_dir != -1 and rot_dir != 1 ):

             print( 'Invalid rotation direction: possible values {1, -1}.' )
             return -1

    # Set the internal int_params variable.

        self._int_params[ 0:int_params_in.size ] = int_params_in

        self._int_params_len = int_params_in.size
        self._rot_dir = rot_dir

        self._flags[0] = 1

        return 0


    def set_double_params(self, double_params_in):

        if ( self._op_flag == 1 ):

            print( 'Cannot change double_params associated with a current recon allocation.' )
            return -1

        if ( self._flags[0] == 0 ):

            print( 'Please set integer parameters before setting double parameters.' )
            return -1

    # Error checking

        if ( type(double_params_in) != np.ndarray ):

            print( 'Numpy ndarray is expected.' )
            return -1

        if ( len( double_params_in.shape ) > 1 ):

            print( 'One dimensional array expected.' )
            return -1

        if ( double_params_in.dtype != np.double ):

            print( 'np.double dtype expected.' )
            return -1

        if ( double_params_in.size < 14 ):

            print( 'Too few double_params specified.' )
            return -1

        if ( double_params_in.size > 17 ):

            print( 'Too many double_params specified.' )
            return -1

        if ( self._cone_parallel == True and double_params_in.size != 17 ):

            print( 'All 17 double parameters are required cylindrical, cone-parallel data.' )
            return -1


    # Set the internal double_params variable.

        self._double_params[ 0:double_params_in.size ] = double_params_in

        self._double_params_len = double_params_in.size

        self._flags[1] = 1

        return 0


    def make_and_set_filter(self, out_name, filter_type='ram-lak', detector_type=0, cutoff=1.0 ):
    # detector_type: (0) - detector is flat; (1) detector is cylindrical; (2) cylindrical, cone-parallel rebinning; (3) flat panel, cone-parallel rebinning; (4) array of parallel beams (synchrotron geometry)

    # Error checking

        if ( self._op_flag == 1 ):

            print( 'Cannot change filter associated with a current recon allocation.' )
            return -1

        if ( self._flags[0] == 0 ):

            print( 'Integer parameters must be specified before creating a filter file.' )
            return -1

        if ( self._flags[1] == 0 ):

            print( 'Double parameters must be specified before creating a filter file.' )
            return -1

        if ( self._cone_parallel == True and self._flags[7] == 0 ):

            print( 'For WFBP objects, rebinning parameters must be specified before creating a filter file.' )
            return -1

        if ( ( cutoff > 1.0 ) | ( cutoff <= 0.0 ) ) :

            print( 'Relative cutoff frequency should be in range (0,1].' )
            return -1

        if ( detector_type < 0 | detector_type > 4 ):

            print( 'Invalid detector type: (0) - detector is flat; (1) detector is cylindrical; (2) cylindrical, cone-parallel rebinning; (3) flat panel, cone-parallel rebinning; (4) array of parallel beams (synchrotron geometry).' )
            return -1

    # Parse parameters, considering whether or not we have performed rebinning.

        if ( self._chain_B ): # If we are going to use rebinned and completed projections for reconstruction, we need to use the new size for the filter.

            nu  = self._int_params_b[0] * self._row_oversampling_factor

        else:

            nu  = self._int_params[0] * self._row_oversampling_factor

        # lsd = self._double_params[7]

        if ( self._cone_parallel == True ):

            du = self._double_params[2] / self._row_oversampling_factor # dx / r_os

        else:

            du  = self._double_params[0]

        lenu = ( 2 ** ( np.ceil( np.log2( nu ) ) ) ).astype(np.int32)

    # Make filter

        out_name = out_name % str(lenu)

        make_FBP_filter( out_name, du, lenu, filter_type, cutoff )

    # Make sure we made the output file...

        if ( os.path.isfile(out_name) == False ):

             sys.exit("Failed to produce filter file.")

    # Set filter and filter flag

        self._filt_name = out_name

        self._flags[2] = 1

        self._detector_type = detector_type

        return 0


    def make_and_set_geofile(self, geofile, angles, z_pos, proj_weights):

    # Error checking - flags

        if ( self._op_flag == 1 ):

            print( 'Cannot change geo file associated with a current recon allocation.' )
            return -1

        if ( self._flags[0] == 0 ):

            print( 'Integer parameters must be specified before creating a geometry file.' )
            return -1

        if ( self._flags[1] == 0 ):

            print( 'Double parameters must be specified before creating a geometry file.' )
            return -1

        if ( self._cone_parallel == True and self._flags[7] == 0 ):

            print( 'For WFBP objects, rebinning parameters must be specified before creating a geometry file.' )
            return -1

    # Error checking - geofile

        if not isinstance(geofile, str):

            print( 'Geometry file output directory must be a string (geofile).' )
            return -1

    # Error checking - angles

        if ( type(angles) != np.ndarray ):

            print( 'Numpy ndarray is expected (angles).' )
            return -1

        if ( len( angles.shape ) > 1 ):

            print( 'One dimensional array expected (angles).' )
            return -1

        if ( angles.dtype != np.double ):

            print( 'np.double dtype expected (angles).' )
            return -1

        np_ = self._int_params[2]

        if ( angles.size != np_ ):

            print( 'Angles vector must have np elements (' + str(np_) + '). np was specified in int_params.' )
            return -1

    # Error checking - z position

        if ( type(z_pos) != np.ndarray ):

            print( 'Numpy ndarray is expected (z_pos).' )
            return -1

        if ( len( z_pos.shape ) > 1 ):

            print( 'One dimensional array expected (z_pos).' )
            return -1

        if ( z_pos.dtype != np.double ):

            print( 'np.double dtype expected (z_pos).' )
            return -1

        if ( z_pos.size != 1 and z_pos.size != np_ ):

            print( 'Z_pos vector must have 1 or np elements (' + str(np_) + '). np was specified in int_params.' )
            return -1

    # Error checking - projection weights

        if ( type(proj_weights) != np.ndarray ):

            print( 'Numpy ndarray is expected (proj_weights).' )
            return -1

        if ( len( proj_weights.shape ) != 1 and len( proj_weights.shape ) != 2 ):

            print( 'One dimensional array expected (proj_weights).' )
            return -1

        if ( proj_weights.dtype != np.double ):

            print( 'np.double dtype expected (proj_weights).' )
            return -1

        if ( proj_weights.size != np_ ):

            print( 'Proj_weights vector must have np elements (' + str(np_) + '). np was specified in int_params.' )
            return -1

    # Override relevant int_params and double_params if we will be backprojecting rebinned projections

        int_params    = np.copy( self._int_params )
        double_params = np.copy( self._double_params )

        if ( self._detector_type == 2 and self._cone_parallel == True ):

            int_params[0]    = self._int_params_b[0] * self._row_oversampling_factor # nu_A * r_os
            double_params[0] = self._double_params_b[7]                              # du

            # NOTE: This implies uc_B = uc_A following data completion...
            #       This is not a problem, unless the alignment relative to the center of each detector is different between the two chains...
            double_params[9] = self._double_params_b[5] * self._row_oversampling_factor # uc

    # prevent 45 degree angle increments which cause numerical instabilities

        temp = angles % 45 == 0
        angles[temp] = angles[temp] + 1e-4

    # Produce geometry file

        make_geo_file( geofile, int_params, double_params, self._rot_dir, angles, proj_weights, z_pos )

    # Make sure we made the output file...

        if ( os.path.isfile(geofile) == False ):

             print("Failed to produce geometry file.")
             return -1

    # Set the geofile and geofile flag

        self._geofile = geofile

        self._flags[3] = 1

        return 0


    def set_affine(self, aff_in):

    # Error checking - flags

        if ( self._op_flag == 1 ):

            print( 'Cannot change affine associated with a current recon allocation.' )
            return -1

        if ( self._flags[0] == 0 ):

            print( 'Integer parameters must be specified before creating a affine transform.' )
            return -1

        use_affine = self._int_params[7]

        if ( use_affine == 0 ):

            print('use_affine = 0. No affine transform will be specified.')

            self._Rt_aff = np.array( [1,0,0,0,1,0,0,0,1,0,0,0], dtype=np.float32 )

            self._flags[4] = 1

            return 0

        if ( type(aff_in) != np.ndarray ):

            print( 'Numpy ndarray is expected (aff_in).' )
            return -1

        if ( len( aff_in.shape ) > 2 ):

            print( 'One dimensional array expected (aff_in).' )
            return -1

        if ( aff_in.dtype != np.float32 ):

            print( 'np.float32 dtype expected (aff_in).' )
            return -1

        if ( aff_in.size != 12 ):

            print( 'Incorrect affine transform size. (9-parameter 3D rotation / shear / scale; 3-parameter xyz translation.)' )
            return -1

    # Set the affine transform and the affine transform flag.

        self._flags[4] = 1

        self._Rt_aff = aff_in

        return 0


    def set_rmask(self, rmask_in):

    # Error checking - flags

        if ( self._op_flag == 1 ):

            print( 'Cannot change rmask associated with a current recon allocation.' )
            return -1

        if ( self._flags[0] == 0 ):

            print( 'Integer parameters must be specified before creating a mask.' )
            return -1

    # Check for the base case

        if ( self._int_params_len < 10 ):

            print('Explicit mask type not specified. Proceeding without an explicit mask.')

            self._flags[5] = 1

            # Set this again, in case it is set and then changed.
            self._rmask = np.zeros( (1,), dtype=np.uint16 )

            return 0

        nx            = self._int_params[3]
        ny            = self._int_params[4]
        nz            = self._int_params[5]
        explicit_mask = self._int_params[9]

        if ( explicit_mask == 0 ):

            print('Explicit mask type is 0. Proceeding without an explicit mask.')

            self._flags[5] = 1

            # Set this again, in case it is set and then changed.
            self._rmask = np.zeros( (1,), dtype=np.uint16 )

            return 0

    # Error checking - rmask_in

        if ( explicit_mask < 0 | explicit_mask > 2 ):

            print('Unknown explicit mask type in int_params vector ( index 9 ). Valid values: 0 (no explicit mask), \
                   1 (z projected explicit mask), 2 (z projected explicit mask and volumetric explicit mask).')

            return -1

        if ( type(rmask_in) != np.ndarray ):

            print( 'Numpy ndarray is expected (rmask).' )
            return -1

        if ( rmask_in.dtype != np.uint16 ):

            print( 'np.uint16 dtype expected (rmask).' )
            return -1

        if ( ( explicit_mask == 1 | explicit_mask == 2 ) and ( rmask_in.shape != (nx,ny,nz) ) ):

            print('Explicit mask type is 1 or 2, but the input rmask shape is not (nx,ny,nz). \
                   Consult the make_default_rmask(...) function for an example.')
            return -1

    # Decide which type of mask to make

        if ( explicit_mask == 1 ):

            rmask = mask_zrange( rmask_in )

            self._flags[5] = 1

            self._rmask = rmask

            return 0

        elif ( explicit_mask == 2 ):

            rmask_in = np.asfortranarray( rmask_in )

            rmask = mask_zrange( rmask_in )

            rmask = np.concatenate( (rmask, rmask_in), axis=2 )

            self._flags[5] = 1

            self._rmask = rmask

            return 0

        else:

            print('Something went wrong. How did we get here?')
            return -1


    def set_GPU_list(self, gpu_list_in):

    # Error checking - flags

        if ( self._op_flag == 1 ):

            print( 'Cannot change GPU list associated with a current recon allocation.' )
            return -1

        if ( type(gpu_list_in) != np.ndarray ):

            print( 'Numpy ndarray is expected (gpu_list_in).' )
            return -1

        if ( len( gpu_list_in.shape ) > 2 ):

            print( 'One dimensional array expected (gpu_list_in).' )
            return -1

        if ( gpu_list_in.dtype != np.int32 ):

            print( 'np.int32 dtype expected (gpu_list_in).' )
            return -1

        if ( ( gpu_list_in < 0 ).any() ):

            print( 'Valid GPU indices start from 0 (gpu_list_in).' )
            return -1

        if ( np.amin( gpu_list_in ) > 0 ):

            print( 'GPU index 0 not included. Is this intentional? This is not an error, but be aware that GPU indexing starts from 0.' )

    # Set gpu_indices
    # NOTE: Additional GPU validation checks are performed during initialization.

        self._gpu_list = gpu_list_in

        self._flags[6] = 1

        return 0


    def DD_init(self):

    # Make sure all parameters are initialized

        if ( ( self._flags != 1 ).any() ):

            print('Cannot initialize reconstruction. One or more parameters are unset.')
            return -1

        if ( self._op_flag == 1 ):

            print('Current reconstruction initialization / allocations must be cleared ( reset_allocation() ) prior to performing a new initialization.')
            return -1

    # Override relevant int_params and double_params if we will be backprojecting rebinned projections

        int_params    = np.copy( self._int_params )
        double_params = np.copy( self._double_params )

        if ( self._detector_type == 2 and self._cone_parallel == True ):

            int_params[0]    = self._int_params_b[0] * self._row_oversampling_factor # nu_A * r_os
            double_params[0] = self._double_params_b[7]                              # du

            # NOTE: This implies uc_B = uc_A following data completion...
            #       This is not a problem, unless the alignment relative to the center of each detector is different between the two chains...
            double_params[9] = self._double_params_b[5] * self._row_oversampling_factor # uc

    # Statically initialize a new recon_parameters structure on the host and GPU(s).

        # [ int *int_params, int int_params_len, double *double_params, int double_params_len, char *filt_name_in, char *geo_name_in, float32 *aff_in,
        #   unsigned short* rmask_in, int *GPU_indices_in, int GPU_counter ]

        DD_init = self.recon_lib.DD_init

        DD_init.argtypes = [ np.ctypeslib.ndpointer(dtype=np.int32),  ctypes.c_int32,
                             np.ctypeslib.ndpointer(dtype=np.double), ctypes.c_int32,
                             ctypes.c_char_p, ctypes.c_char_p, np.ctypeslib.ndpointer(dtype=np.float32),
                             np.ctypeslib.ndpointer(dtype=np.uint16), np.ctypeslib.ndpointer(dtype=np.int32), ctypes.c_int32 ]

        DD_init.restype = ctypes.c_uint64

        # create byte objects from the strings
        filt_p = self._filt_name.encode('utf-8')
        geo_p  = self._geofile.encode('utf-8')

        self._recon_alloc[0] = DD_init( int_params,    self._int_params_len,
                                        double_params, self._double_params_len,
                                        filt_p, geo_p, self._Rt_aff, self._rmask, self._gpu_list, len(self._gpu_list) )

        print( 'Return value: ' + str( self._recon_alloc[0] ) )

    # Set a flag saying it is now safe to call the operators

        self._op_flag = 1

        return 0


    def R(self, X, W): # Y,
    # X: volume to be projected
    # Y: output projection data
    # W: projection weights

    # Make sure all parameters and the GPU/Host are initialized

        if ( ( self._flags != 1 ).any() ):

            print('Cannot perform projection. One or more parameters are unset.')
            return -1

        if ( self._op_flag != 1 ):

            print('Cannot perform projection. Reconstruction parameters have not been initialized (call DD_init()).')
            return -1

    # Check to make sure the inputs are valid

        nu = self._int_params[0]
        nv = self._int_params[1]
        np_ = self._int_params[2]
        nx = self._int_params[3]
        ny = self._int_params[4]
        nz = self._int_params[5]

        if ( type(X) != np.ndarray ):

            print( 'Numpy ndarray is expected (X).' )
            return -1

        if ( X.size != nx*ny*nz ):

            print( 'Input array size must be nx*ny*nz (X).' )
            return -1

        if ( X.dtype != np.float32 ):

            print( 'np.float32 dtype expected (X).' )
            return -1

        if ( type(W) != np.ndarray ):

            print( 'Numpy ndarray is expected (W).' )
            return -1

        if ( W.size != np_ ):

            print( 'Input array size must be np (W).' )
            return -1

        if ( W.dtype != np.double ):

            print( 'np.double dtype expected (W).' )
            return -1

    # Handle (extremely annoying) Fortran / C convention conversion

        change_X = False
        # change_W = False

        if not np.isfortran(X):

            X = np.copy( X, 'F' )

            change_X = True

        if not np.isfortran(W):

            W = np.copy( W, 'F' )

    # Perform projection


        DD_project = self.recon_lib.DD_project

        # float32 *X, float32 *Y, uint64_t rp_in, double *weights_in

        DD_project.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32),
                                ctypes.c_uint64, np.ctypeslib.ndpointer(dtype=np.double) ]

        Y = np.zeros( (nu,nv,np_), dtype=np.float32, order='F')

        DD_project( X, Y, self._recon_alloc[0], W )


    # Handle (extremely annoying) Fortran / C convention conversion

        if change_X:

            Y = np.copy( Y, 'C' )

        #    X = np.ascontiguousarray( X )

        #if change_Y:

        #    Y = np.ascontiguousarray( Y )

        #if change_W:

        #    W = np.ascontiguousarray( W )

        return Y


    def Rt(self, Y, W): # X
    # Y: input projection data
    # X: output backprojected volume
    # W: projection weights

    # Make sure all parameters and the GPU/Host are initialized

        if ( ( self._flags != 1 ).any() ):

            print('Cannot perform backprojection. One or more parameters are unset.')
            return -1

        if ( self._op_flag != 1 ):

            print('Cannot perform backprojection. Reconstruction parameters have not been initialized (call DD_init()).')
            return -1

    # Check to make sure the inputs are valid

        nu = self._int_params[0]
        nv = self._int_params[1]
        np_ = self._int_params[2]
        nx = self._int_params[3]
        ny = self._int_params[4]
        nz = self._int_params[5]

        if ( type(Y) != np.ndarray ):

            print( 'Numpy ndarray is expected (Y).' )
            return -1

        if ( Y.size != nu*nv*np_ ):

            print( 'Input array size must be nu*nv*np (Y).' )
            return -1

        if ( Y.dtype != np.float32 ):

            print( 'np.float32 dtype expected (Y).' )
            return -1

        if ( type(W) != np.ndarray ):

            print( 'Numpy ndarray is expected (W).' )
            return -1

        # if ( W.size != np_ ):
        if ( W.shape[0] != np_ ):

            print( 'Input array size must be np (W).' )
            return -1

        if ( W.dtype != np.double ):

            print( 'np.double dtype expected (W).' )
            return -1

    # Handle (extremely annoying) Fortran / C convention conversion

        change_Y = False

        if not np.isfortran(Y):

            change_Y = True
            Y = np.copy( Y, 'F' )

        if not np.isfortran(W):

            W = np.copy( W, 'F' )

    # Perform projection

        DD_backproject = self.recon_lib.DD_backproject

        # float32 *Y, float32 *X, uint64_t rp_in, int perform_filtration_in, double *weights_in

        DD_backproject.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32),
                                    ctypes.c_uint64, ctypes.c_int32, np.ctypeslib.ndpointer(dtype=np.double) ]

        X = np.zeros( (nx,ny,nz), dtype=np.float32, order='F' )

        DD_backproject( Y, X, self._recon_alloc[0], int(0), W )

    # Handle (extremely annoying) Fortran / C convention conversion

        if change_Y:

            X = np.copy(X, 'C')

        return X


    def Rtf(self, Y, W): # X
    # Y: input projection data
    # X: output backprojected volume
    # W: projection weights

    # Make sure all parameters and the GPU/Host are initialized

        if ( ( self._flags != 1 ).any() ):

            print('Cannot perform backprojection. One or more parameters are unset.')
            return -1

        if ( self._op_flag != 1 ):

            print('Cannot perform backprojection. Reconstruction parameters have not been initialized (call DD_init()).')
            return -1

    # Check to make sure the inputs are valid

        nu = self._int_params[0]
        nv = self._int_params[1]
        np_ = self._int_params[2]
        nx = self._int_params[3]
        ny = self._int_params[4]
        nz = self._int_params[5]
        filtration = self._int_params[6]

        if ( type(Y) != np.ndarray ):

            print( 'Numpy ndarray is expected (Y).' )
            return -1

        if ( Y.size != nu*nv*np_ ):

            print( 'Input array size must be nu*nv*np (Y).' )
            return -1

        if ( Y.dtype != np.float32 ):

            print( 'np.float32 dtype expected (Y).' )
            return -1

        if ( type(W) != np.ndarray ):

            print( 'Numpy ndarray is expected (W).' )
            return -1

        if ( W.size != np_ ):

            print( 'Input array size must be np (W).' )
            return -1

        if ( W.dtype != np.double ):

            print( 'np.double dtype expected (W).' )
            return -1

        if ( filtration == 0 ):

            print( 'Cannot perform filtered backprojection because a filter was not specified or prepared during initialization ( int_params[6] = 0 ).' )
            return -1

    # Handle (extremely annoying) Fortran / C convention conversion

        change_Y = False

        if not np.isfortran(Y):

            change_Y = True
            Y = np.copy( Y, 'F' )

        if not np.isfortran(W):

            W = np.copy( W, 'F' )

    # Perform projection

        DD_backproject = self.recon_lib.DD_backproject

        # float32 *Y, float32 *X, uint64_t rp_in, int perform_filtration_in, double *weights_in

        DD_backproject.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32),
                                    ctypes.c_uint64, ctypes.c_int32, np.ctypeslib.ndpointer(dtype=np.double) ]

        X = np.zeros( (nx,ny,nz), dtype=np.float32, order='F' )

        DD_backproject( Y, X, self._recon_alloc[0], int(1), W )

    # Handle (extremely annoying) Fortran / C convention conversion
        if change_Y:

            X = np.copy(X, 'C')

        return X


    def _reset_flags(self):
    # Flags to make sure all parameters are set before calling the operators again.

        # [ int_params, double_params, filt_name, geofile, Rt_aff, rmask, gpu_list ]
        # self._flags = np.zeros(  (7,), dtype=np.int32 )
        self._flags = self._flags * 0

        self._op_flag = 0

        return 0


    def reset_all_GPUs(self):
    # Allows recovery when GPUs are in an error state.
    # Currently, this resets >ALL< visible GPUs and will invalidate allocations associated
    # with reconstruction operators as well as programs such as tensorflow.

        # We can only clear resources if we have allocated them...
        # Check this before resetting the GPU since host allocations are also involved.
        # Will this work when the GPU is in an error state?
        if ( ( self._flags != 0 ).all() & self._op_flag == 1 ):

            clear = self.recon_lib.clear

            clear()

        self._reset_flags()

        reset_devices = self.recon_lib.reset_devices

        reset_devices()

        self._op_flag = 0

        return 0


    def reset_allocation(self):
    # Keeps the object, but resets the GPU/Host allocations.

        # We can only clear resources if we have allocated them...
        if ( ( self._flags != 0 ).all() & self._op_flag == 1 ):

            clear = self.recon_lib.clear

            clear()

        self._reset_flags()

        self._op_flag = 0

        return 0


    def __del__(self):
    # Imply deallocation of resources when the last reference to this object is cleared (i.e. during garbage collection).

        # We can only clear resources if we have allocated them...
        if ( ( self._flags != 0 ).all() & self._op_flag == 1 ):

            clear = self.recon_lib.clear

            clear()

        return 0


# Approximate, analytical reconstruction of helical data acquired with a cylindrical detector.
# "Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch"
# Karl Stierstorfer, Annabella Rauscher, Jan Boese, Herbert Bruder, Stefan Schaller and Thomas Flohr
# Phys. Med. Biol. 49 (2004) 2209–2218
# "Image reconstruction and image quality evaluation for a dual source CT scanner"
# T. G. Flohr, H. Bruder, K. Stierstorfer, M. Petersilka, B. Schmidt, C. H. McCollough
# Medical Physics, 35(12), 5882-5897.
# ASSUMES / IMPLIES
#   1) The angular increment between projections is equal between chains and before and after rebinning.
#   2) The alignment in chain B is the same as the alignment in chain A, following rebinning of chain B.
#      alignment: detector element offset of the central ray
class WFBP( Recon ):

    def __init__(self, recon_lib_path, row_oversampling_factor ):

    # Oversampling factor to use for rebinning

        if ( not isinstance( row_oversampling_factor, int ) ):

            print( 'Integer data type expected for row_oversampling_factor.' )
            return -1

        if ( row_oversampling_factor < 1 ):

            print( 'row_oversampling_factor must be at least 1.' )
            return -1

    # Recon object initialization

        super(WFBP, self).__init__(recon_lib_path)

    # Set these after calling the Recon __init__ function to override values.

        self._row_oversampling_factor = row_oversampling_factor

        # Add extra flag for the rebinning parameter vectors.
        # [ int_params, double_params, filt_name, geofile, Rt_aff, rmask, gpu_list, params_b ]
        self._flags = np.zeros(  (8,), dtype=np.int32 )

        # self._int_params_b = np.zeros( (7,), dtype=np.int32 )
        # self._double_params_b = np.zeros( (9,), dtype=np.double )

        # int32([ nu_A, nu_B, nv, np, r_os, rotdir ]);
        self._int_params_b = np.zeros( (6,), dtype=np.int32 )

        # [ Rf, zrot, delta_theta, delta_beta, dv, uc_A, uc_B, dx0 / r_os, theta_B, scale_A_B ]
        self._double_params_b = np.zeros( (10,), dtype=np.double )

    def set_rebin_params(self, zrot, delta_theta, uc_A, nu_A, nu_B=0, uc_B=0.0, theta_B=0.0, scale_A_B=0.0):
    # NOTE: Assigning nu_B > 0 implies that this WFBP object is associated with "chain B" and that
    # the chain B projection data must be completed with chain A.

        if ( self._cone_parallel == False ):

            print( 'Rebinning parameters are for cylindrical, cone-parallel data only ( int_params[10] = 2 ).' )
            return -1

        if ( self._flags[0] == 0 or self._flags[1] == 0 ):

            print( 'Please set integer and double parameters before setting rebin parameters.' )
            return -1

    # Error checking - zrot

        if ( zrot <= 0 ):

             print( 'zrot (z translation in mm / degree) must be > 0.' )
             return -1

    # Error checking - delta_theta

        if ( delta_theta <= 0 ):

             print( 'delta_theta (rotation in degrees / projection) must be > 0.' )
             return -1

    # Errors / Warnings - nu_B

        if ( not isinstance( nu_B, int ) ):

            print( 'Integer data type expected for nu_B (detector elements per row, chain B).' )
            return -1

        if ( nu_B == 0 ):

             print( 'nu_B = 0: chain B will not be used.' )

        if ( nu_B < 0 ):

            print( 'nu_B (detector elements per row, chain B) must be >= 0.' )
            return -1

        if ( ( nu_B > 0 and uc_B != self._double_params[9] ) or ( nu_B == 0 and uc_A != self._double_params[9] )  ):

            print( 'Specified uc_A (chain A) or uc_B (chain B) parameter does not match previously specified uc value ( double_params[9] ).' )
            return -1

        if ( nu_B == 0 and nu_A != self._int_params[0] ):

            print( 'Specified nu_A parameter does not match previously specified value ( int_params[0] ).' )
            return -1

        if ( nu_B > 0 ):

            self._chain_B = True

    # Set rebinning parameter vectors

        os_ = self._row_oversampling_factor

        self._int_params_b[0] = nu_A
        self._int_params_b[1] = nu_B
        self._int_params_b[2] = self._int_params[1]           # nv
        self._int_params_b[3] = self._int_params[2]           # np
        self._int_params_b[4] = self._row_oversampling_factor
        self._int_params_b[5] = self._rot_dir                 # rotation direction

        self._double_params_b[0] = self._double_params[8]               # Rf
        self._double_params_b[1] = zrot                                 # zrot (z translation in mm / degree)
        self._double_params_b[2] = delta_theta                          # delta_theta, angular increment in degrees => assumed output angular increment
        self._double_params_b[3] = self._double_params[15] * 180.0 / PI # delta_beta (radians => degrees), fan angle increment between detector elements
        self._double_params_b[4] = self._double_params[1]               # dv
        self._double_params_b[5] = uc_A
        self._double_params_b[6] = uc_B
        self._double_params_b[7] = self._double_params[2] / os_         # du / os_ => dx (output)
        self._double_params_b[8] = theta_B                              # angular offset from chain A to chain B
        self._double_params_b[9] = scale_A_B                            # mu_water_B / mu_water_A or similar A to B attenuation scaling factor

    # Flag that the rebinning parameters have been successfully set.

        self._flags[7] = 1

        return 0

    def R(self, X, W):

        print("Cannot perform projection (R operator) with a WFBP object.")

        return -1

    def Rt(self, Y, W):

        print("Cannot perform backprojection (Rt operator) with a WFBP object.")

        return -1

    def Rtf(self, Y, W):

        print("Cannot perform filtered backprojection (Rtf operator) without rebinning. Call rebin(...) and then Rtfb(...) instead.")

        return -1

    def rebin(self, Y_A, Y_B, GPU_idx ):
    # Y_A: input chain A projection data.
    # Y_B: input chain B projection data.
    # GPU_idx: index of the GPU to use to perform rebinning. (indexing starts from zero)

    # Make sure the rebinning parameters have been specified

        if ( self._flags[0] == 0 or self._flags[1] == 0 or self._flags[7] == 0 ):

            print('int_params, double_params, and rebin_params must be set before projection rebinning.')
            return -1

        # if ( self._op_flag == 1 ):
        #
        #     print('Rebinning must be performed before reconstruction parameter initialization ( DD_init() ) so that the initialization reflects the rebinned parameters.')
        #     return -1

    # Check to make sure the inputs are valid

        nu_A   = self._int_params_b[0]
        nu_B   = self._int_params_b[1]
        nu_out = nu_A * self._row_oversampling_factor
        nv     = self._int_params[1]
        np_    = self._int_params[2]

        if ( type(Y_A) != np.ndarray ):

            print( 'Numpy ndarray is expected (Y_A).' )
            return -1

        if ( Y_A.size != nu_A*nv*np_ ):

            print( 'Input array size must be nu_A*nv*np (Y_A).' )
            return -1

        if ( Y_A.dtype != np.float32 ):

            print( 'np.float32 dtype expected (Y_A).' )
            return -1

        if ( self._chain_B ):

            if ( type(Y_B) != np.ndarray ):

                print( 'Numpy ndarray is expected (Y_B).' )
                return -1

            if ( Y_B.size != nu_B*nv*np_ ):

                print( 'Input array size must be nu_B*nv*np (Y_B).' )
                return -1

            if ( Y_B.dtype != np.float32 ):

                print( 'np.float32 dtype expected (Y_B).' )
                return -1

        if ( not isinstance( GPU_idx, int ) ):

            print( 'Integer data type expected for GPU_idx.' )
            return -1

        if ( GPU_idx < 0 ):

            print( 'GPU_idx expected to be >= 0.' )
            return -1

    # Handle (extremely annoying) Fortran / C convention conversion

        change_Y = False

        if not np.isfortran(Y_A):

            change_Y = True
            Y_A = np.copy( Y_A, 'F' )

            if ( self._chain_B ):

                Y_B = np.copy( Y_B, 'F' )

    # Perform rebinning

        rebin = self.recon_lib.rebin

        # void rebin( const float *Y_A, const float *Y_B, float *Y_b, int *int_params, double *double_params, int GPU_idx );

        rebin.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32),
                           np.ctypeslib.ndpointer(dtype=np.int32),   np.ctypeslib.ndpointer(dtype=np.double), ctypes.c_int32 ]

        Y_b = np.zeros( (nu_out,nv,np_), dtype=np.float32, order='F' )

        if ( not self._chain_B ):

            Y_B = np.zeros( (1,), dtype=np.float32, order='F' )

        rebin( Y_A, Y_B, Y_b, self._int_params_b, self._double_params_b, GPU_idx )

        # Handle (extremely annoying) Fortran / C convention conversion
        if change_Y:

            Y_b = np.copy(Y_b, 'C')

        return Y_b

    def Rtfb(self, Y_b, W):
    # WFBP reconstruction of cylindrical, helical, cone-parallel projection data
    # Y_b: input, rebinned projection data
    # X: output backprojected volume
    # W: projection weights

    # Make sure all parameters and the GPU/Host are initialized

        if ( ( self._flags != 1 ).any() ):

            print('Cannot perform WFBP (Rtfb). One or more parameters are unset.')
            return -1

        if ( self._op_flag != 1 ):

            print('Cannot perform backprojection. Reconstruction parameters have not been initialized (call DD_init()).')
            return -1

        if ( self._detector_type != 2 or self._cone_parallel == False ):

            print('Cone-parallel data required for WFBP (Rtfb).')
            return -1

    # Check to make sure the inputs are valid

        nu  = self._int_params_b[0] * self._row_oversampling_factor
        nv  = self._int_params[1]
        np_ = self._int_params[2]
        nx  = self._int_params[3]
        ny  = self._int_params[4]
        nz  = self._int_params[5]
        filtration = self._int_params[6]

        if ( type(Y_b) != np.ndarray ):

            print( 'Numpy ndarray is expected (Y_b).' )
            return -1

        if ( Y_b.size != nu*nv*np_ ):

            print( 'Input array size must be nu_out*nv*np (Y_b).' )
            return -1

        if ( Y_b.dtype != np.float32 ):

            print( 'np.float32 dtype expected (Y_b).' )
            return -1

        if ( type(W) != np.ndarray ):

            print( 'Numpy ndarray is expected (W).' )
            return -1

        if ( W.size != np_ ):

            print( 'Input array size must be np (W).' )
            return -1

        if ( W.dtype != np.double ):

            print( 'np.double dtype expected (W).' )
            return -1

        if ( filtration == 0 ):

            print( 'Cannot perform WFBP because a filter was not specified or prepared during initialization ( int_params[6] = 0 ).' )
            return -1

    # Handle (extremely annoying) Fortran / C convention conversion

        change_Y_b = False

        if not np.isfortran(Y_b):

            change_Y_b = True
            Y_b = np.copy( Y_b, 'F' )

        if not np.isfortran(W):

            W = np.copy( W, 'F' )

    # Perform projection

        DD_backproject = self.recon_lib.DD_backproject

        # float32 *Y, float32 *X, uint64_t rp_in, int perform_filtration_in, double *weights_in

        DD_backproject.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32),
                                    ctypes.c_uint64, ctypes.c_int32, np.ctypeslib.ndpointer(dtype=np.double) ]

        X = np.zeros( (nx,ny,nz), dtype=np.float32, order='F' )

        DD_backproject( Y_b, X, self._recon_alloc[0], int(1), W )

    # Handle (extremely annoying) Fortran / C convention conversion
        if change_Y_b:

            X = np.copy(X, 'C')

        return X

'''
L2 Solvers
'''

# Sleijpen, G. L., & Van Gijzen, M. B. (2010).
# Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
# SIAM journal on scientific computing, 32(5), 2687-2709.
# Algorithm 3.1
def bicgstabl_os(A, b, x, maxit, W, ell, mat_ord='F'):
# Needs more testing with varying numbers of subsets, varying A operators...

    b = np.reshape(b,[-1,1],order=mat_ord)
    x = np.reshape(x,[-1,1],order=mat_ord)

    if ( len(W.shape) == 1):

        print('If there is only one subset, W (proj_weight) shape should be (#,1).')
        sys.exit()

    if ( b.dtype != np.float32 or x.dtype != np.float32 ):

        print( 'b and x should be numpy arrays of dtype np.float32.' )
        sys.exit()

    # If there is only one subset, shape should be [#,1].
    snum = W.shape[1]

    # Initial residuals
    r0 = b - A(x, np.sum( W, 1, keepdims=True ) )

    n = x.size;
    r = np.zeros( (n,ell+1), dtype=np.float32, order=mat_ord )
    u = np.zeros( (n,ell+2), dtype=np.float32, order=mat_ord )

    resvec      = np.nan_to_num( np.zeros( [snum, maxit+1], dtype=np.float32, order=mat_ord ) + math.inf )
    resvec[0,0] = np.sqrt( np.sum( r0**2 ) )

    r[:,0:1] = r0
    u[:,0:2] = np.concatenate( ( r0, A(r0,W[:,:1]) ), axis=1 )

    r0t  = np.copy( r0 )
    xmin = np.copy( x )

    del r0

    for i in range(0,maxit):

        for s in range(0,snum):

            # The Bi-CG step
            for j in range(0,ell):

                sigma = np.sum( r0t * u[:,j+1:j+2] )         # sigma = r0t' * u(:,j+1);
                alpha = np.sum( r0t * r[:,j:j+1]   ) / sigma # alpha = (r0t' * r(:,j)) ./ sigma;

                x            = x + u[:,:1] * alpha             # x        = x + u(:,1) * alpha;
                r[:,0:j+1]   = r[:,0:j+1] - u[:,1:j+2] * alpha # r(:,1:j) = r(:,1:j) - u(:,2:j+1) * alpha;
                r[:,j+1:j+2] = A(r[:,j:j+1],W[:,s:s+1])        # r(:,j+1) = A(r(:,j),W(:,s));

                beta         = np.sum( r0t * r[:,j+1:j+2] ) / sigma # beta       = (r0t' * r(:,j+1)) ./ sigma;
                u[:,0:j+2]   = r[:,0:j+2] - u[:,0:j+2] * beta       # u(:,1:j+1) = r(:,1:j+1) - u(:,1:j+1) * beta;
                u[:,j+2:j+3] = A(u[:,j+1:j+2],W[:,s:s+1])           # u(:,j+2)   = A(u(:,j+1),W(:,s));


            # The polynomial step
            Q,R   = np.linalg.qr( r[:,1:], mode='reduced' )                                      # [Q,R] = qr(r(:,2:end),0);
            gamma = np.linalg.inv( R.T @ R + np.eye(ell) * 1e-6 ) @ ( R.T @ ( Q.T @ r[:,0:1] ) ) # gamma = (R'*R + eye(ell)*1e-6) \ ( R' * Q' * r(:,1) );

            # gamma = np.linalg.pinv( r[:,1:], rcond=1e-6 ) @ r[:,0:1]

            x       = x + r[:,0:ell] @ gamma    # x = x + r(:,1:ell) * gamma;
            r[:,:1] = r[:,:1] - r[:,1:] @ gamma # r(:,1) = r(:,1) - r(:,2:end) * gamma;


            # u(:,1:2) = [u(:,1) - u(:,2:end-1) * gamma, u(:,2) - u(:,3:end)   * gamma];
            u[:,0:2] = np.concatenate( ( u[:,0:1] - u[:,1:-1] @ gamma, u[:,1:2] - u[:,2:] @ gamma ), axis=1 )


            resvec[s,i+1] = np.linalg.norm( r[:,:1], 'fro' )

            if resvec[s,i+1] == np.amin( resvec ):

                xmin = np.copy( x )


    resvec[resvec>1e38] = 0

    return xmin, resvec


def RtR_op(RC, mu=0, mat_ord='F'):

    def A(X, W):

        Y     = RC.R( X, W )
        X_out = RC.Rt( Y, W )

        X_out = np.reshape(X_out,[-1,1],order=mat_ord)

        if mu > 0:

            X_out = X_out + mu * X

        return X_out

    return A


def get_subset_weights( W, np_, snum ):

    # Bit reverse order
    if snum == 1:

        sets = [1]

    elif snum == 2:

        sets = [1,2]

    elif snum == 3:

        sets = [1, 3, 2]

    elif snum == 4:

        sets = [1, 3, 2, 4]

    elif snum == 8:

        sets = [1, 5, 3, 7, 2, 6, 4, 8]

    elif snum == 16:

        sets = [1, 9, 5, 13, 3, 11, 7, 15, 2, 10, 6, 14, 4, 12, 8, 16]

    else:

        assert('Specified number of subsets not supported (snum = 1, 2, 3, 4, 8, or 16).')

    Ws = np.zeros([np_,3],order='F')
    for s in range(0,snum):

        idx = sets[s] - 1

        Ws[idx::snum,s:s+1] = W[idx::snum,:]

    return Ws


'''
Regularization
'''




'''
Compare images
'''

def compare_img(X1,X2,szx,zslice,fig,mat_ord):

    plt.figure(fig)

    diff = np.abs(np.subtract( X1, X2 ))

    plt.imshow( np.concatenate( ( X1.reshape(szx,order=mat_ord)[:,:,zslice], X2.reshape(szx,order=mat_ord)[:,:,zslice], diff.reshape(szx,order=mat_ord)[:,:,zslice] ), axis=1 ), cmap='gray' )

    plt.pause(0.5)

    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()

    plt.pause(0.5)

    plt.draw()


'''
Project a volumetric reconstruction mask along z for efficient masking on the GPU.
'''

def mask_zrange( mask ):
# Input: 3D GPU projection / backprojection mask
# Output: 2D z index mask

    zmask = 0 * mask[:,:,0:2];

    sh = mask.shape

    temp = zmask[:,:,0]

    for i in range( 1, sh[2]+1 ):

        temp[ np.logical_and( temp == 0, mask[:,:,i-1] != 0 ) ] = i;

    zmask[:,:,0] = temp

    temp = zmask[:,:,1]

    for i in range( sh[2], 0, -1 ):

        temp[ np.logical_and( temp == 0, mask[:,:,i-1] != 0 ) ] = i;

    zmask[:,:,1] = temp

    zmask = zmask.astype(np.int32)

    zmask[:,:,0] = np.maximum( zmask[:,:,0] - 1, 0 );

    zmask = zmask.astype(np.uint16)

    zmask = np.asfortranarray(zmask)

    return zmask


# Reproduces the implicit mask as an explicit mask
def make_default_rmask( nx, ny, nz ):

    rmask = np.zeros( (nx,ny), dtype=np.float )

    x1, x2 = np.meshgrid( np.arange( 0. - nx / 2., nx - 1. - nx / 2. + 1 ),
                          np.arange( 0. - ny / 2., ny - 1. - ny / 2. + 1), indexing='ij' )

    rmask[ np.sqrt( np.add(x1**2, x2**2) ) <= min(nx/2,ny/2) ] = 1

    rmask = np.repeat( rmask[:,:,np.newaxis], nz, axis=2 )

    rmask = rmask.astype( np.uint16 );

    rmask = np.asfortranarray(rmask)

    return rmask


def make_diam_rmask( nx, ny, nz, diam ):

    rmask = np.zeros( (nx,ny), dtype=np.float )

    x1, x2 = np.meshgrid( np.arange( 0. - nx / 2., nx - 1. - nx / 2. + 1 ),
                          np.arange( 0. - ny / 2., ny - 1. - ny / 2. + 1), indexing='ij' )

    rmask[ np.sqrt( np.add(x1**2, x2**2) ) <= diam/2.0 ] = 1

    rmask = np.repeat( rmask[:,:,np.newaxis], nz, axis=2 )

    rmask = rmask.astype( np.uint16 );

    rmask = np.asfortranarray(rmask)

    return rmask


'''
Make a geometry file
'''

def make_geo_file( geofile, int_params, double_params, rot_dir, angles, proj_weights, z_pos ):

    a2r = PI / 180.
    r2a = 180. / PI

    # int_params:    [ nu, nv, np, nx, ny, nz, filtration, use_affine, [implicit_mask], [explicit_mask], [cy_detector], [use_norm_vol] ]
    # double_params: [ du, dv, dx, dy, dz, zo, ao, lsd, lso, uc, vc, eta, sig, phi, [scale], [db], [limit_angular_range] ]

    np_ = int_params[2]

    lsd = double_params[7]
    lso = double_params[8]
    uc  = double_params[9]
    vc  = double_params[10]
    eta = double_params[11]
    sig = double_params[12]
    phi = double_params[13]
    zz  = double_params[5]
    aa  = double_params[6]
    du  = double_params[0]
    dv  = double_params[1]

    geo_lines = np.zeros( ( np_, 21), dtype=np.double )

    # j = -1

    rot = np.matmul( rotx3(-eta), np.matmul( roty3(-sig), rotz3(-phi) ) )
    puv = np.matmul( rot, [[0.],[du],[0.]] )
    pvv = np.matmul( rot, [[0.],[0.],[dv]] )

    if ( z_pos.size == 1 ): # circular geometry

        src = [   [-1.*lso], [ 0. ], [ -zz ] ]
        dtv = [ [ lsd-lso ], [ 0. ], [ -zz ] ]

    for i in range(0,np_):

        # j = j + 1

        if ( z_pos.size > 1 ): # helical geometry

            src = [   [-1.*lso], [ 0. ], [ z_pos[i]-zz ] ]
            dtv = [ [ lsd-lso ], [ 0. ], [ z_pos[i]-zz ] ]

        angle = angles[i] + aa

        # prevent singular projection matrices
        # if ( np.mod( angle, 45.0 ) == 0 ):

        #    angle = angle + 1e-4

        rsrc = np.matmul( rotz3( rot_dir * angle * a2r) , src )
        rdtv = np.matmul( rotz3( rot_dir * angle * a2r) , dtv )
        rpuv = np.matmul( rotz3( rot_dir * angle * a2r) , puv )
        rpvv = np.matmul( rotz3( rot_dir * angle * a2r) , pvv )

        geo_lines[i,:] = np.concatenate( ( np.reshape( np.concatenate( ( rsrc, rdtv, rpuv, rpvv ), axis = 0 ), (1, 12) ),
                                           np.reshape( np.array( [lsd, lso, uc, vc, eta, sig, phi, proj_weights[i], angle] ), (1,9) ) ), axis=1 )

    # geo_lines.tofile( geofile )

    np.savetxt( geofile, geo_lines, fmt='%1.7e' )

    return 0


def rotx3(x):

    return np.array( [ [1, 0, 0], [0, np.cos(x), -1. * np.sin(x)], [ 0, np.sin(x), np.cos(x)] ], dtype=np.double )

def roty3(x):

    return np.array( [ [np.cos(x), 0, np.sin(x)], [0, 1, 0], [-1. * np.sin(x), 0, np.cos(x)] ], dtype=np.double )

def rotz3(x):

    return np.array( [ [np.cos(x), -1. * np.sin(x), 0], [ np.sin(x), np.cos(x), 0 ], [0, 0, 1] ], dtype=np.double )


'''
Gating
'''



'''
Projection truncation
'''




''' 
Frequency filters for analytical reconstruction
'''

def make_FBP_filter( name, du, nu, window_type, cutoff ):
# Make frequnecy domain filter
# A. C. Kak and Malcolm Slaney, Principles of Computerized Tomographic Imaging, IEEE Press, 1988.

    # Number of detector elements along a (padded) row, must be a power of 2
    assert np.round(np.log2(nu)) == np.log2(nu)

    # Chapter 3, Eq. 61
    filter = get_half_ramp(nu,du)

    filter = apply_window( filter, window_type, cutoff, nu )

    filter = filter.astype( np.float32 )

    # NOTE: np.savetxt does not encode in the expected way
    filter.tofile(name)

    return filter


def get_half_ramp(nu, du):

    filter = np.zeros((nu,))
    N = np.arange(-(nu//2),(nu//2))

    filter[nu//2] = 1.0 / ( 4.0 * du**2 )

    idx = np.mod( N, 2 ).astype('bool')

    filter[idx] = -1.0 / ( PI * N[idx] * du )**2

    filter = du * np.real( np.fft.fft( np.fft.fftshift( filter ) ) )

    filter = filter[0:(nu//2+1)]

    return filter


def apply_window( filter, window, cutoff, nu ):
# https://en.wikipedia.org/wiki/Window_function

    w = np.linspace(0,PI,num=nu//2+1)

    if window == 'shepp-logan':

        P = np.sin( w[1:] / (2*cutoff) ) * (2.0*cutoff) / w[1:]

    elif window == 'cosine':

        P = np.cos( w[1:] / (2.0*cutoff) )

    elif window == 'hamming':

        P = 0.53836 + 0.46164 * np.cos( w[1:] / cutoff )

    elif window == 'hann':

        P = ( 1.0 + np.cos( w[1:] / cutoff ) ) / 2.0

    else: # Otherwise, assume Ram-Lak

        P = 1

    filter[1:] *= P

    filter[(np.ceil(cutoff*nu/2) + 1).astype(np.int32):] = 0

    return filter
