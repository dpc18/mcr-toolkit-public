// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef JOINTBF4D
#define JOINTBF4D

// Global Variables //

    float *X, *Xr, *Xf, *Xn, *A, *At, *multiplier;
    int *size;
    int perform_resampling, radius, return_noise_map, gpu_idx;
    uint64_t nvols, ntimes, sx, sy, sz;
    
    float **Xpts, **Xfpts, **Xnpts, **Xrpts;
    
// Function Declarations
    
    void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]);                // mex interface
    void check_len(const mxArray* prhs[], const int arg_num, const int idx, const uint64_t len); // check the validity of arguments based on their length
    void validate_GPU( int gpu_idx );                                                            // Make sure the requested GPU is valid before setting the active device
    
// External Functions //

    extern void jointBF4D_cuda(float **X, float **Xr, float **Xf, float **Xn,
                               const float *A, const float *At, const float* m, 
                               uint64_t sx, uint64_t sy, uint64_t sz,
                               int w, uint64_t nvols, uint64_t ntimes,
                               int return_noise_map, int perform_resampling);

#endif // JOINTBF4D