// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cuda_runtime.h>
#include "mex.h"
#include <stdlib.h> // allows use of size_t on host code?
#include <stdio.h> 
#include <stdint.h>

#include "error_chk.h"
#include "jointBF4D_cuda.h"

//***                         ***
//*** CUDA Kernels - Joint BF ***
//***                         ***
        
__global__ void jointBF_4D( const float *X, const float *N, float *S, const float *m,
                            unsigned int nvols, unsigned int ntimes, int3 vsize, int w, int z_slice) {

// Indices
    
    int3 coord2D = make_int3(blockIdx.x*blockDim.x + threadIdx.x,blockIdx.y*blockDim.y + threadIdx.y,0);
    int3 coord3D = make_int3(blockIdx.x*blockDim.x + threadIdx.x,blockIdx.y*blockDim.y + threadIdx.y,z_slice);
    uint64_t idx2D = lindx(coord2D,vsize);
    // uint64_t idx3D = lindx(coord3D,vsize);
    
// Do nothing if we have gone out of bounds due to the grid*block size
// being larger than the image size
        
    if (coord2D.x >= vsize.x || coord2D.y >= vsize.y) return;
    
// Declare local variables
        
    int3 cpos; //current position inside the for loops
    int i, j, k, vol, time; 
    float cdat; 
    float rdat;
    float denom = 0;
    float rw;
    float R;

    // unsigned int idx;
    uint64_t idx;
            
    float num[MAX_NVOLS]; // = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    float c[MAX_NVOLS];

    for (i = 0; i < nvols; i++) {
        
        idx = vsize.x*vsize.y*vsize.z*((uint64_t) i)+lindx(coord3D,vsize);
     
        num[i] = 0.0;
        c[i]   = -1.0f / ( 2.0f * m[i] * m[i] * N[idx] * N[idx] + EPS );
        
    }

// Compute and apply filter
    
    // Traverse neighborhood
    for (k = -w; k <= w; k++) { // slice offset, z
        
        // mirror at edges as needed
        cpos.z = coord3D.z+k;
        if (cpos.z < 0) cpos.z = -cpos.z;
        if (cpos.z > vsize.z-1) cpos.z = vsize.z - 1 - k;
        
        for (j = -w; j <= w; j++) { // row offset, y
            
            cpos.y = coord3D.y+j;
            if (cpos.y < 0) cpos.y = -cpos.y;
            if (cpos.y > vsize.y-1) cpos.y = vsize.y - 1 - j;
            
            for (i = -w; i <= w; i++) { // col offset, x
                
                // skip if not inside flat, radial domain
                if (sqrtf((float) i*i + j*j + k*k) > 1.01*w) continue;
                
                cpos.x = coord3D.x+i;
                if (cpos.x < 0) cpos.x = -cpos.x;
                if (cpos.x > vsize.x-1) cpos.x = vsize.x - 1 - i;
                
                // Sum weighted results for each timepoint f' = f1*R1 + f2*R2 + ... / (R1 + R2 + ...)
                for (time = 0; time < ntimes; time++) {
                
                    // Sum up the range weights for each volume to be jointly filtered
                    R = 0.0;
                    for (vol = 0; vol < nvols; vol++) { 

                        cdat = X[vsize.x*vsize.y*vsize.z*((uint64_t) time*nvols+vol)+lindx(cpos,vsize)];
                        rdat = S[vsize.x*vsize.y*vol+idx2D];
                        rw = cdat - rdat;
                        R += c[vol]*rw*rw;

                    }

                    R = expf(R);
                    denom += R;

                    // Apply the kernel to each volume to be jointly filtered
                    for (vol = 0; vol < nvols; vol++) { 

                        cdat = X[((uint64_t) time*nvols+vol)*vsize.x*vsize.y*vsize.z+lindx(cpos,vsize)];
                        num[vol] += cdat*R;

                    }
                
                }
             
            }
        }  
    }
    
// Write the filtration results
// Results stored in the first nvols slices
        
    for (vol = 0; vol < nvols; vol++) { 
    
        rdat = num[vol]/(denom+EPS);

        S[vsize.x*vsize.y*vol+idx2D] = rdat;

//         if (rdat == rdat) { // Only update if not NaN
// 
//             S[vol*vsize.x*vsize.y+idx2D] = rdat;
// 
//         }
    
    }
    
}


// REQUIRES: kernel diameter = KERNEL_DIAMETER, threads/block > KERNEL_DIAMETER
__global__ void conv3D_separable( float *Slice, const float *X, const float *A, const int dim, const int3 vsize, const int w, const int3 ksize, const unsigned int z_slice ) {
    
    int3 coord2D = make_int3(blockIdx.x*blockDim.x + threadIdx.x,blockIdx.y*blockDim.y + threadIdx.y,0);
    int3 coord3D = make_int3(blockIdx.x*blockDim.x + threadIdx.x,blockIdx.y*blockDim.y + threadIdx.y,z_slice);
    uint64_t tidx  = lindx(make_int3(threadIdx.x,threadIdx.y,0),make_int3(blockDim.x,blockDim.y,0));
    uint64_t idx2D = lindx(coord2D,vsize);
    uint64_t idx3D = lindx(coord3D,vsize);
    
    // Load 1D, separated resampling kernel into shared memory
    __shared__ float A_[KERNEL_DIAMETER];
    
    if (tidx < KERNEL_DIAMETER) {
     
        A_[tidx] = A[tidx];
        
    }
            
    __syncthreads();
    
    // Do nothing if we have gone out of bounds due to the grid*block size
    // being larger than the image size
    if (coord2D.x >= vsize.x || coord2D.y >= vsize.y) return;
    
    // Declare loop variable
    int i = 0;
    
    // Perform 3D convolution
    
    // Do the z dimension first to prevent inter-slice dependence
    // I*Z
    int3 cpos = coord3D; //current position inside the for loops
    for (i = -w; i <= w; i++) { // slice offset, z
        
        if (dim == 1) {
        
            // mirror at edges as needed
            cpos.z = coord3D.z+i;
            if (cpos.z < 0) cpos.z = -cpos.z;
            if (cpos.z > vsize.z-1) cpos.z = vsize.z - 1 - i;

            Slice[idx2D] += X[lindx(cpos,vsize)]*A_[w-i];
            
        }
        else if (dim == 2) {
            
            cpos.y = coord3D.y+i;
            if (cpos.y < 0) cpos.y = -cpos.y;
            if (cpos.y > vsize.y-1) cpos.y = vsize.y - 1 - i;
        
            Slice[idx2D] += X[lindx(cpos,vsize)]*A_[w-i];
        
        }
        else {
            
            cpos.x = coord3D.x+i;
            if (cpos.x < 0) cpos.x = -cpos.x;
            if (cpos.x > vsize.x-1) cpos.x = vsize.x - 1 - i;
        
            Slice[idx2D] += X[lindx(cpos,vsize)]*A_[w-i];
            
        }
        
    }
    
}

__global__ void weightedpluscopy2D(float* out, float* in, float weight, const int3 vsize) {
 
    // requires compute 2.0 or greater
    int3 coord = make_int3(blockIdx.x*blockDim.x + threadIdx.x,blockIdx.y*blockDim.y + threadIdx.y,0);
    
    // Do nothing if we have gone out of bounds due to the grid*block size being larger than the image size
    if (coord.x >= vsize.x || coord.y >= vsize.y) return;
    
    uint64_t idx = lindx(coord,vsize);
    
    out[idx] += weight*in[idx];
    
}

//***                                 ***
//*** CUDA Kernels - Noise Estimation ***
//***                                 ***

// Currently hard-wired for 32x32 block size...
// Can be called more than once with different offests to average multiple estimates for smooth transitions
// Call with 1024 threads/block (1 thread per element)
// Use norm_val to get correct normalization in light of multiple overlapping calls
__global__ void grid_MAD_2D_32(const float *slice_2D_in, float *slice_2D_out, float norm_val, int offsetx, int offsety, int szx, int szy) {
    
// 1) Read block into shared memory
        
    // statically allocated shared memory, 4 KB per kernel
    // ideally, we would use less memory per kernel, but we need a local buffer for parallel median filtering
    __shared__ float s[32*32];
      
    int x = ((int) (blockDim.x * blockIdx.x + threadIdx.x)) + offsetx;
    int y = ((int) (blockDim.y * blockIdx.y + threadIdx.y)) + offsety;
    int tid = blockDim.x * threadIdx.y + threadIdx.x;

    // mirror at edges as needed
    if (x < 0) x = -x;
    if (x > szx-1) x = 2*szx - x - 2;

    if (y < 0) y = -y;
    if (y > szy-1) y = 2*szy - y - 2;

    s[tid] = slice_2D_in[szx*y + x];

    // Since we are going to access memory read by other threads, we need all reads to be complete before continuing
    __syncthreads();
    
        
// 2) Apply Haar framelet
        
    // x: (1/sqrt(2)) * [-1, 1]
    // y: (1/sqrt(2)) * [ 1;-1]
            
    float a, b, c, d;
    int use_x = threadIdx.x;
    int use_y = threadIdx.y;

    if ( use_x > blockDim.x-2 ) use_x = blockDim.x-2;
    if ( use_y > blockDim.y-2 ) use_y = blockDim.y-2;

    int tid2 = blockDim.x * use_y + use_x;

    a = s[tid2];
    b = s[tid2+1];
    c = s[tid2+32];
    d = s[tid2+33];

    // avoid race conditions, since overlapping shared memory values are used and updated
    __syncthreads();

    s[tid] = 0.5f * fabsf( -a + b + c - d );

    // Since we are going to access memory read by other threads, we need all updates to be complete before continuing
    __syncthreads();
        
// 3) Compute median absolute deviation (noise estimate)
// currently uses a parallelized bubble sort, but there are supposedly cheaper ways to do this with selection algorithms
        
    for (int reps = 0; reps <= (blockDim.x*blockDim.y)/2; reps++) {
        
        // sort even pairs
        if (tid % 2 == 0 && tid < blockDim.x*blockDim.y-1 ) {
            
            a = s[tid];
            b = s[tid+1];

            if ( a > b ) {
             
                s[tid]   = b;
                s[tid+1] = a;
                
            }
            
        }

        __syncthreads();
                
        // sort odd pairs
        if (tid % 2 == 1 && tid < blockDim.x*blockDim.y-1 ) {
            
            a = s[tid];
            b = s[tid+1];

            if ( a > b ) {
             
                s[tid]   = b;
                s[tid+1] = a;
                
            }
            
        }

        __syncthreads();
        
    }
        
    // no need to average the two middle values
    float MAD = s[(blockDim.x*blockDim.y)/2] / 0.6745f;

// 4) Copy noise estimate to output noise map
        
    // reset these variables because they may have been moved to stay in bounds
    x = ((int) (blockDim.x * blockIdx.x + threadIdx.x)) + offsetx;
    y = ((int) (blockDim.y * blockIdx.y + threadIdx.y)) + offsety;

    if ( x >= 0 && x < szx && y >= 0 && y < szy ) {
        
        slice_2D_out[szx*y + x] += MAD / norm_val;
        
    }
    
}


//***      ***
//*** HOST ***
//***      ***

// Slicewise, joint BF
void jointBF4D_cuda(float **X, float **Xr, float **Xf, float **Xn,
                    const float *A, const float *At, const float* m, 
                    uint64_t sx, uint64_t sy, uint64_t sz,
                    int w, uint64_t nvols, uint64_t ntimes,
                    int return_noise_map, int perform_resampling) {
            
// Size variables
    
    // Kernel diameter
    uint64_t diam = 2*w+1;

    // Number of bytes of input data
    uint64_t sxy         = sx*sy;
    uint64_t Xbuffbytes  = sxy*diam*sizeof(float); // size of one z buffer
    uint64_t Xbuffbytes_ = sxy*(diam-1)*sizeof(float); // size of one z buffer, minus the last slice
    uint64_t Abytes      = 3*diam*sizeof(float);
    uint64_t Atbytes     = ntimes*sizeof(float);
    uint64_t Sbytes      = sxy*sizeof(float);

    // volume buffer and kernel sizes
    int3 vsize = make_int3(sx,sy,diam);
    int3 ksize = make_int3(diam,diam,diam);

    // Block and grid size for 2D-2D slice operations
    dim3 gridSize2D((vsize.x + B_SIZE_2D - 1) / B_SIZE_2D, (vsize.y + B_SIZE_2D - 1) / B_SIZE_2D);
    dim3 blockSize2D(B_SIZE_2D,B_SIZE_2D);
    
    // Block and grid size for 3D buffer operations
    // dim3 gridSize3D((vsize.x + B_SIZE_3D - 1) / B_SIZE_3D, (vsize.y + B_SIZE_3D - 1) / B_SIZE_3D, (vsize.z + B_SIZE_3D - 1) / B_SIZE_3D);
    // dim3 blockSize3D(B_SIZE_3D,B_SIZE_3D,B_SIZE_3D);

    // Block and grid size for 3D to 2D buffer operations
    dim3 gridSize2D_((vsize.x + B_SIZE_3D_2D - 1) / B_SIZE_3D_2D, (vsize.y + B_SIZE_3D_2D - 1) / B_SIZE_3D_2D);
    dim3 blockSize2D_(B_SIZE_3D_2D,B_SIZE_3D_2D);

    // Grided noise estimation
    // We need an extra block along each dimension to cover the full 2D slice when using a negative offset  
    dim3 gridN( (vsize.x + 32 - 1) / 32 + 1, (vsize.y + 32 - 1) / 32 + 1 );
    dim3 blockN( 32, 32 );

// Copy constant arrays to the device
        
    float *Ad, *Atd; 
        
    if ( perform_resampling == 1 ) {
        
        // Resampling kernel => device
        cudaMalloc((void**)&Ad,Abytes);
        cudaMemcpy(Ad,A,Abytes,cudaMemcpyHostToDevice);

        // Temporal resampling kernel => device
        cudaMalloc((void**)&Atd,Atbytes);
        cudaMemcpy(Atd,At,Atbytes,cudaMemcpyHostToDevice);

    }

    float *md; // device side regularization strength multiplier
    cudaMalloc((void**)&md,nvols*sizeof(float));
    cudaMemcpy(md,m,nvols*sizeof(float),cudaMemcpyHostToDevice);

// GPU memory allocations
        
    float* Xzd; // Input data => z buffers
    cudaMalloc((void**)&Xzd,Xbuffbytes*nvols*ntimes);

    float* Xzdr; // Input data => z buffers for pre-resampled volumes
    if ( perform_resampling == 0 ) cudaMalloc((void**)&Xzdr,Xbuffbytes*nvols);

    float* Xzn; // Input data => z buffers for gridded noise level estimates
    cudaMalloc((void**)&Xzn,Xbuffbytes*nvols);

    float *tempd_buffer; // temporary buffer for a single volume
    cudaMalloc((void**)&tempd_buffer,Xbuffbytes);

    float* Szd; // Input data => current slice buffers
    cudaMalloc((void**)&Szd,Sbytes*nvols);

    float *Sd2; // device side single slice buffers
    cudaMalloc((void**)&Sd2,Sbytes);
        
// copy initial slice buffers
        
    int time, vol;
    int ctime = (int) (ntimes-1)/2;
        
    // we need to set this memory to zeros because grid_MAD_2D_32<<<>>>() adds to the current values
    cudaMemset(Xzn,0,Xbuffbytes*nvols);

    for (vol = 0; vol < nvols; vol++) {

        // filter input data
        for (time = 0; time < ntimes; time++) {

            cudaMemcpy(Xzd+sxy*diam*(time*nvols+vol),X[time*nvols + vol],Xbuffbytes,cudaMemcpyHostToDevice);

        }

        // pre-resampled volume
        if ( perform_resampling == 0 ) cudaMemcpy(Xzdr+sxy*diam*vol,Xr[vol],Xbuffbytes,cudaMemcpyHostToDevice);

        for ( int n = 0; n < diam; n++ ) {
              
            // average together four overlapping estimates to ensure smoothness
            // estimate noise for the time point being filtered only
            grid_MAD_2D_32<<<gridN,blockN>>>(Xzd+sxy*diam*(ctime*nvols+vol)+sxy*n, Xzn+sxy*diam*vol+sxy*n, 4.0f,   0,   0, vsize.x, vsize.y);
            grid_MAD_2D_32<<<gridN,blockN>>>(Xzd+sxy*diam*(ctime*nvols+vol)+sxy*n, Xzn+sxy*diam*vol+sxy*n, 4.0f, -16, -16, vsize.x, vsize.y);
            grid_MAD_2D_32<<<gridN,blockN>>>(Xzd+sxy*diam*(ctime*nvols+vol)+sxy*n, Xzn+sxy*diam*vol+sxy*n, 4.0f,   0, -16, vsize.x, vsize.y);
            grid_MAD_2D_32<<<gridN,blockN>>>(Xzd+sxy*diam*(ctime*nvols+vol)+sxy*n, Xzn+sxy*diam*vol+sxy*n, 4.0f, -16,   0, vsize.x, vsize.y);
                
        }

        if ( return_noise_map == 1 ) cudaMemcpy(Xn[vol],Xzn+sxy*diam*vol,Xbuffbytes,cudaMemcpyDeviceToHost);
        
    }

// Per-slice filter loop
        
    uint64_t z_slice;

    for (int k = 0; k < sz; k++) { // for all z slices
            
    // Update kernel buffer along z as needed
        
        if (k > w && k < sz - w) { // update the z buffer if we are not within the kernel radius of the edges
                
            for (vol = 0; vol < nvols; vol++) {

            // Copy new volume data to GPU
                    
                for (time = 0; time < ntimes; time++) {
                    
                    // We can't call cudaMemcpy with overlapping memory regions, so we use an intermediate variable to shift the z buffer
                    cudaMemcpy( tempd_buffer, Xzd+sxy*diam*(time*nvols+vol)+sxy, Xbuffbytes_, cudaMemcpyDeviceToDevice);
                    cudaMemcpy( Xzd+sxy*diam*(time*nvols+vol), tempd_buffer, Xbuffbytes_, cudaMemcpyDeviceToDevice ); // copy back the shifted buffer
                    cudaMemcpy( Xzd+sxy*diam*(time*nvols+vol)+sxy*(diam-1), X[time*nvols+vol]+(k+w)*sxy, Sbytes, cudaMemcpyHostToDevice); // fill in the new z slice
                            
                }

            // copy resampled volume data to GPU, as needed
                    
                if ( perform_resampling == 0 ) {
                    
                    cudaMemcpy( tempd_buffer, Xzdr+sxy*diam*vol+sxy, Xbuffbytes_, cudaMemcpyDeviceToDevice);
                    cudaMemcpy( Xzdr+sxy*diam*vol, tempd_buffer, Xbuffbytes_, cudaMemcpyDeviceToDevice ); // copy back the shifted buffer
                    cudaMemcpy( Xzdr+sxy*diam*vol+sxy*(diam-1),Xr[vol]+(k+w)*sxy,Sbytes,cudaMemcpyHostToDevice); // fill in the new z slice
                    
                } 
                            
            // noise estimation for the new slice
                    
                cudaMemcpy( tempd_buffer, Xzn+sxy*diam*vol+sxy, Xbuffbytes_, cudaMemcpyDeviceToDevice);
                cudaMemcpy( Xzn+sxy*diam*vol, tempd_buffer, Xbuffbytes_, cudaMemcpyDeviceToDevice ); // copy back the shifted buffer

                // we need to set this memory to zeros because grid_MAD_2D_32<<<>>>() adds to the current values
                cudaMemset(Xzn+sxy*diam*vol+sxy*(diam-1),0,Sbytes);

                // average together four overlapping estimates to ensure smoothness
                grid_MAD_2D_32<<<gridN,blockN>>>(Xzd+sxy*diam*(ctime*nvols+vol)+sxy*(diam-1), Xzn+sxy*diam*vol+sxy*(diam-1), 4.0f,   0,   0, vsize.x, vsize.y);
                grid_MAD_2D_32<<<gridN,blockN>>>(Xzd+sxy*diam*(ctime*nvols+vol)+sxy*(diam-1), Xzn+sxy*diam*vol+sxy*(diam-1), 4.0f, -16, -16, vsize.x, vsize.y);
                grid_MAD_2D_32<<<gridN,blockN>>>(Xzd+sxy*diam*(ctime*nvols+vol)+sxy*(diam-1), Xzn+sxy*diam*vol+sxy*(diam-1), 4.0f,   0, -16, vsize.x, vsize.y);
                grid_MAD_2D_32<<<gridN,blockN>>>(Xzd+sxy*diam*(ctime*nvols+vol)+sxy*(diam-1), Xzn+sxy*diam*vol+sxy*(diam-1), 4.0f, -16,   0, vsize.x, vsize.y);
            
                // If we want to see them, we need to copy the noise estimates back to the host.
                if ( return_noise_map == 1 ) {
                 
                    cudaMemcpy(Xn[vol]+(k+w)*sxy,Xzn+sxy*diam*vol+sxy*(diam-1),Sbytes,cudaMemcpyDeviceToHost);
                    
                }

            }
            
            z_slice = w;
            
        }
        else if (k <= w) { // we are at the beginning
            
            z_slice = k;
            
        }
        else { // we are at the end
            
            // z_slice = 2*w - (sz - k + 1);
            z_slice = diam + k - sz;
            
        }
        
    // Perform resampling OR use correct slice from supplied, resampled data

        // (Separable) Kernel resampling
        // Combine timepoints linearly with the At temporal weights

        if ( perform_resampling == 1 ) {
            
            cudaMemset(Szd,0,Sbytes*nvols);
        
            for (time = 0; time < ntimes; time++) { // for all timepoints

                for (vol = 0; vol < nvols; vol++) { // for all volumes at the current slice

                    // check();
                    for (int i = 0; i < 3; i++) { // 3 separated kernels

                        // initialize the temporary slices
                        cudaMemcpy( tempd_buffer, Xzd+sxy*diam*(time*nvols+vol), Xbuffbytes, cudaMemcpyDeviceToDevice);

                        for (int j = 0; j < 3; j++) { // 3 convolution operations: I*z*x*y

                            cudaMemset(Sd2,0,Sbytes);
                            conv3D_separable<<<gridSize2D, blockSize2D>>>(Sd2, tempd_buffer, Ad+i*diam, j+1, vsize, w, ksize, z_slice);

                            if (j < 2) {
                                
                                cudaMemcpy( tempd_buffer+z_slice*sxy, Sd2, Sbytes, cudaMemcpyDeviceToDevice);

                            }

                        }

                        // Accumulate results in Xzd: Szd_out = temporal_weight(vol)*Xzd_in*z1*x1*y1 + temporal_weight(vol)*Xzd_in*z2*x2*y2 + temporal_weight(vol)*Xzd_in*z3*x3*y3
                        weightedpluscopy2D<<<gridSize2D, blockSize2D>>>(Szd+sxy*vol,Sd2,At[time],vsize);

                    }
                    // check();
                }

            }

        }
        else {
            
            for (vol = 0; vol < nvols; vol++) {
                
                cudaMemcpy( Szd+sxy*vol, Xzdr+sxy*diam*vol+z_slice*sxy, Sbytes, cudaMemcpyDeviceToDevice );
                
            }
            
        }

        // Compute Joint BF num/denom
        jointBF_4D<<<gridSize2D_, blockSize2D_>>>(Xzd, Xzn, Szd, md, nvols, ntimes, vsize, w, z_slice);

        // Copy the filtered slices into the output volumes
        // The results of 4D filtration are stored in the first nvols slices
        for (vol = 0; vol < nvols; vol++) { // for all volumes at the current slice

            // cudaMemcpy(Xf[vol]+k*sxy,tempd_slice+sxy*vol,Sbytes,cudaMemcpyDeviceToHost);
            cudaMemcpy(Xf[vol]+k*sxy,Szd+sxy*vol,Sbytes,cudaMemcpyDeviceToHost);

        }

    }

    // Free memory allocated by cudaMalloc in case we integrate this into a larger program later on
    if ( perform_resampling == 1 ) {
        cudaFree(Ad);
        cudaFree(Atd);
    }
    cudaFree(Xzd);
    if ( perform_resampling == 0 ) cudaFree(Xzdr);
    cudaFree(Xzn);
    cudaFree(tempd_buffer);
    cudaFree(Szd);
    cudaFree(Sd2);
    cudaFree(md);
                
}











