// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "mex.h"
#include <cuda_runtime.h>

#include <stdint.h>

#include "error_chk.h"
#include "jointBF4D.h"

// Mex Interface with Matlab
// v5: Use spatially adaptive noise estimation.

// [ Xf, noise_sig ] = jointBF4D( X, nvols, ntimes, A, At, size, radius, multiplier, gpu_idx, return_noise_map, Xr )
// INPUTS:
//                  X: (single*) vectorized data to be filtered; 3D volume x volumes to be jointly filter x time points to extend the filtration domain
//              nvols: (int32)   number of volumes to jointly filter
//             ntimes: (int32)   number of time points along which to extend the filtration domain
//                  A: (single*) x,y,z factored resampling kernel (expected size: radius x 3)
//                 At: (single*) temporal resampling weights (vector with ntimes entries; used to combine resampling results between timepoints; sums to 1)
//               size: (int32*)  volume size in voxels (x,y,z)
//             radius: (int32)   filter radius (width = 2*radius + 1)
//         multiplier: (single*) scales filtration strength as this multiple of the estimated noise standard deviation (vector with nvols entries)
//            gpu_idx: (optional) (int32)   hardware index pointing to the GPU to be used for BF (defaults to the system default)
//   return_noise_map: (optional) (int32)   (0) do not return noise map; (1) return noise map (defaults to 0 to reduce memory transfers)
//                 Xr: (optional) (single*) if size(Xr) == nx*ny*nz*nvols, take resampled values from these volumes; otherwise, compute resampled values with the A, At kernels (defaults to computing resampled values)
//
// OUTPUTS:
//                 Xf: (single*) vectorized, filtered volumes ( prod(size) x nvols )
//          noise_sig: (single*) returned if return_noise_map == 1; estimated noise standard deviation ( prod(size) x nvols )

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
 
    // Check for the appropriate number of arguments
    if (nrhs <  8) mexErrMsgTxt("At least 8 input arguments expected!");
    if (nrhs > 11) mexErrMsgTxt("Between 8 and 11 input arguments expected!");
    
    // Check the precision of the inputs
    if (!mxIsClass(prhs[0],"single")){ // X
        mexErrMsgTxt("First input argument (grayscale data) must be single precision.");  
    }
    
    if (!mxIsClass(prhs[1],"int32")){ // nvols
        mexErrMsgTxt("Second input argument (number of volumes to jointly filter) must be int32.");  
    }
    
    if (!mxIsClass(prhs[2],"int32")){ // ntimes
        mexErrMsgTxt("Third input argument (number of timepoints) must be int32.");  
    }
    
    if (!mxIsClass(prhs[3],"single")){ // A
        mexErrMsgTxt("Fourth input argument (resampling kernel) must be single precision.");   
    }
    
    if (!mxIsClass(prhs[4],"single")){ // At
        mexErrMsgTxt("Fifth input argument (temporal resampling kernel) must be single precision.");   
    }
    
    if (!mxIsClass(prhs[5],"int32")){ // size
        mexErrMsgTxt("Sixth input argument (per volume size vector) must be int32.");
    }
    
    if (!mxIsClass(prhs[6],"int32")){ // radius
        mexErrMsgTxt("Seventh input argument (kernel radius) must be int32.");
    }
    
    if (!mxIsClass(prhs[7],"single")){ // multiplier
        mexErrMsgTxt("Eighth input argument (bandwidth multiplier) must be single precision.");
    }
    
    if (nrhs >= 9) {
    
        if (!mxIsClass(prhs[8],"int32")){ // gpu_idx
            mexErrMsgTxt("Ninth input argument (gpu_idx) must be int32.");
        }
    
    }
    
    if (nrhs >= 10) {
    
        if (!mxIsClass(prhs[9],"int32")){ // return_noise_map
            mexErrMsgTxt("Tenth input argument (return_noise_map flag) must be int32.");
        }
    
    }
    
    if ( nrhs == 11 ) {
    
        if (!mxIsClass(prhs[10],"single")){ // perform_resampling
            mexErrMsgTxt("Eleventh input argument (resampled volume) must be single precision.");  
        }
    
    }
    
    // Parse size parameters
    
        nvols  = *((int*) mxGetData(prhs[1]));
        ntimes = *((int*) mxGetData(prhs[2]));
        size   = (int*) mxGetPr(prhs[5]);
        radius = *((int*) mxGetData(prhs[6]));
        
        // Check size
        check_len(prhs, 6, 5, 3);
        
        sx = size[0];
        sy = size[1];
        sz = size[2];
        
        if ( nvols < 1 ) {
            
            mexErrMsgTxt("The number of input volumes (nvols) must be at least 1.");
            
        }
        
        if ( ntimes < 1 ) {
            
            mexErrMsgTxt("The number of input times (ntimes) must be at least 1.");
            
        }
        
        if ( radius < 1 ) {
            
            mexErrMsgTxt("Filtration kernel radius (radius) must be at least 1.");
            
        }
        
        if ( sx < 2*radius+1 || sy < 2*radius+1 || sz < 2*radius+1 ) {
            
            mexErrMsgTxt("Size parameters (size) must be at least 2*radius+1.");
            
        }
        
    // Parse X
    
        X = (float*) mxGetData(prhs[0]);
        
        // Check size
        check_len(prhs, 1, 0, sx*sy*sz*nvols*ntimes);
        
    // Parse Xr
        
        if ( nrhs == 11 ) { // the user specified a resampled volume

            Xr = (float*) mxGetData(prhs[10]);

            if ( mxGetNumberOfElements(prhs[10]) == sx*sy*sz*nvols ) {

                // We already have resampled volumes; no resampling is needed.
                perform_resampling = 0;

            }
            else if ( mxGetNumberOfElements(prhs[10]) > 1 ) {

                // Warn the user in case this is not the expected behavior.
                mexPrintf("Xr has more than 1 element, but not the expected x*y*z*nvols elements. Kernel resampling will be performed instead of using resampled volumes.");

                // we need to perform resampling
                perform_resampling = 1;

            }
            else {

                // we need to perform resampling
                perform_resampling = 1;

            }

        }
        else { // the user did not specify a resampled volume; use kernel resampling

            // we need to perform resampling
            perform_resampling = 1;

        }

    // Resampling kernel
        
        if ( perform_resampling == 1 ) {
            
            A  = (float*) mxGetData(prhs[3]);
            At = (float*) mxGetData(prhs[4]);
            
            // Check size
            check_len(prhs, 4, 3, (2*radius+1)*3);
            
            // Check size
            check_len(prhs, 5, 4, ntimes);
            
        }
        
    // Regularization strength multiplier
        
        multiplier = (float*) mxGetData(prhs[7]);
        
        check_len(prhs, 8, 7, nvols);
        
        for ( int i = 0; i < nvols; i++ ) {
         
            if ( multiplier[i] <= 0.0f ) {
         
                mexErrMsgTxt("Regularization strength multiplier(s) must be > 0.");
            
            }
            
        }
        
    // GPU to use for bilateral filtration
        
        if ( nrhs >= 9 ) { // the user specified a gpu index
        
            gpu_idx = *((int*) mxGetData(prhs[8]));

            validate_GPU( gpu_idx );

            cudaSetDevice( gpu_idx );
        
        }
        // else: use the system default
        
    // Return the noise map for debugging purposes?
        
        if ( nrhs >= 10 ) { // the user specified a return_noise_map flag
        
            return_noise_map = *((int*) mxGetData(prhs[9]));

            if ( return_noise_map < 0 || return_noise_map > 1 ) {

                mexErrMsgTxt("The return_noise_map flag (tenth input argument) must be 0 or 1.");

            }
            
        }
        else {
         
            return_noise_map = 0;
            
        }
        
        if ( return_noise_map == 0 ) {

            if (nlhs > 1) mexErrMsgTxt("Too many output arguments! Did you intend to return a noise map (return_noise_map = 1) ?");

        }
        else {

            if (nlhs > 2) mexErrMsgTxt("Too many output arguments!");

        }
        
    // Allocate output (for 4D problems, output size < input size)
        
        const mwSize out_size_Xf[2] = {sx*sy*sz,nvols};
        plhs[0] = mxCreateNumericArray(2,out_size_Xf,mxSINGLE_CLASS,mxREAL);
        Xf = (float*) mxGetData(plhs[0]);
        
    // If we are returning one, output the noise map. (for 4D problems, output size < input size)
        
        if ( return_noise_map == 1 ) {
         
            const mwSize out_size_Xn[2] = {sx*sy*sz,nvols};
            plhs[1] = mxCreateNumericArray(2,out_size_Xn,mxSINGLE_CLASS,mxREAL);
            Xn = (float*) mxGetData(plhs[1]);
            
        }
    
    // Create arrays of pointers to the beginning of each volume within X and Xf
        
        Xpts  = (float**) malloc(ntimes * nvols * sizeof(float*));
        Xfpts = (float**) malloc(nvols * sizeof(float*));
        Xnpts = (float**) malloc(nvols * sizeof(float*));
        Xrpts = (float**) malloc(nvols * sizeof(float*));
        
        for (uint64_t i = 0; i < nvols*ntimes; i++) {

            Xpts[i]  = X + i*sx*sy*sz;

        }
        
        for (uint64_t i = 0; i < nvols; i++) {

            Xfpts[i]                                = Xf + i*sx*sy*sz;
            if ( return_noise_map == 1 )   Xnpts[i] = Xn + i*sx*sy*sz;
            if ( perform_resampling == 0 ) Xrpts[i] = Xr + i*sx*sy*sz;

        }
    
    // Run joint BF on the GPU

        jointBF4D_cuda(Xpts, Xrpts, Xfpts, Xnpts, A, At, multiplier, sx, sy, sz, radius, nvols, ntimes, return_noise_map, perform_resampling);
        
    // Free memory allocated by malloc
        
        free(Xpts);
        free(Xfpts);
        free(Xnpts);
        free(Xrpts);
        
    // Check for outstanding CUDA errors
    
        check(99);
    
    // Make sure the device is done executing CUDA code
        
        cudaDeviceSynchronize();
    
}

// Check the validity of arguments based on their length

void check_len(const mxArray* prhs[], const int arg_num, const int idx, const uint64_t len)
{

    char err_string[200];

    if ( mxGetNumberOfElements(prhs[idx]) != len ) {

        sprintf(err_string, "Input argument %i does not have expected length of %lu.\n", arg_num, len);

        mexErrMsgTxt(err_string);

    }

}

// Make sure the requested GPU is valid before setting the active device

void validate_GPU( int gpu_idx ) {

    // Make sure at least one GPU is visible
    
        int nDevices_found;
    
        cudaGetDeviceCount(&nDevices_found);

        if (nDevices_found == 0) {

            ERR("No GPUs found.");

        }

    // Make sure the requested GPU index is legal
        
        if ( gpu_idx < 0 || gpu_idx > nDevices_found-1 ) {

            ERR("Specified GPU does not exist or is not visible. (NOTE: GPU indexing starts from 0.)");

        }

}
