// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include "cuda_runtime.h"
#include "make_patches.h"

// Assumed: Odd patch dimensions, px_out = py_out = pz_out
void make_patches( const float *vol, float *patches, size_t z_slice,
                   size_t px_out, size_t py_out, size_t pz_out,
                   size_t sx, size_t sy, size_t sz,
                   size_t nx, size_t ny, size_t nz, size_t nvol,
                   int gpu_idx ) {
    
    // Error checks
    
        if (nvol < 1) {
            
            printf("A minimum of one volume is required.");
            return;
            
        }
        
        if ( z_slice < 0 || z_slice >= nz) {
         
            printf("Z_slice parameter is out of bounds.");
            return;
            
        }
        
        if ( px_out % 2 == 0 || py_out % 2 == 0 || pz_out % 2 == 0 ) {
         
          printf("Patch dimensions currently required to be odd.");
           return;
            
        }
        
        if ( px_out != py_out || py_out != pz_out || px_out != pz_out ) {
         
            printf("Patch x,y,z dimensions currently assumed to be equal.");
            return;
            
        }
        
        if ( px_out < sx || py_out < sy || pz_out < sz ){
            
            printf("Patch dimensions must be >= to stride dimensions");
            return;
            
        }
        
//         if ( nx < sz || ny < sy || nz < sz ) {
//             
//             printf("Input volume dimensions must be >= to stride dimensions");
//             return;
//             
//         }

    // Set GPU to use

        int nDevices_found;

        // Total number of GPUs visible on the system
        cudaGetDeviceCount(&nDevices_found);

        if (nDevices_found == 0) {

            printf("No GPUs found.");
            return;

        }

        if ( gpu_idx < 0 || gpu_idx > nDevices_found-1 ) {

            printf("GPU specified for use does not exist or is not visible. (NOTE: GPU indexing starts from 0.)");
            return;

        }

        // Set device to use for patching
        cudaSetDevice( gpu_idx );
    
    // allocate size parameters
    
        size_t D_rows = nvol*px_out*py_out*pz_out; // one patch worth of voxels / volume
    
        int3 sz3 = make_int3(nx,ny,nz);
    
    // execution configuration
    
        dim3 gsP = dim3( sz3.x, 1, 1);
        dim3 bsP = dim3( MIN(D_rows/nvol,1024) ,1,1); // dim3(D_rows/nvol,1,1);
        
    // Create offset map (on the GPU) for extracting patches...  only need one map for all volumes
    
        int *offset_map = (int *) malloc(3*(D_rows/nvol)*sizeof(int));
        
        fill_offset_map_square( offset_map, D_rows/nvol, (px_out - 1) / 2 );
        
        int *offset_map_;
        cudaMallocManaged( (void **) &offset_map_, 3*(D_rows/nvol)*sizeof(int), cudaMemAttachGlobal );
        memcpy( offset_map_, offset_map, 3*(D_rows/nvol)*sizeof(int) );
        
        free(offset_map);
        
    // memory allocation
        
        float *X_, *Xp_;
        
        size_t szxy0 = sz3.x*sz3.y;
        size_t szxy  = ( (int) ( ( ny + sy - 1 ) / sy ) ) * ( (int) ( ( nx + sx - 1 ) / sx ) );
    
        cudaMallocManaged( (void **) &X_, szxy0 * sz3.z * nvol * sizeof(float), cudaMemAttachGlobal ); // sz3.x * sz3.y * sz3.z * nvol
        memcpy( X_, vol, szxy0 * sz3.z * nvol * sizeof(float) );
        
        cudaMallocManaged( (void **) &Xp_, D_rows * szxy * sizeof(float), cudaMemAttachGlobal ); // D_rows * sz3.x*sz3.y

    // function call
    
        extract_patches_stride_call(X_, Xp_, offset_map_, z_slice, sz3, nvol, D_rows/nvol, sx, sy, sz, gsP, bsP);
        
    // Device synchronize and copy to output
        
        cudaDeviceSynchronize();
        
        memcpy( patches, Xp_, D_rows * szxy * sizeof(float) );
        
    // Free cuda memory

        cudaFree( X_ );
        cudaFree( Xp_ );
        cudaFree( offset_map_ );
        
    // check(5);
    
}

void return_patches( const float *patches, float *vol, size_t z_slice,
                     size_t px_out, size_t py_out, size_t pz_out,
                     size_t sx, size_t sy, size_t sz,
                     size_t nx, size_t ny, size_t nz, size_t nvol,
                     int gpu_idx ) {

     // Error checks

        if (nvol < 1) {

            printf("A minimum of one volume is required.");
            return;

        }

        if ( z_slice < 0 || z_slice >= nz) {

            printf("Z_slice parameter is out of bounds.");
            return;

        }

        if ( px_out % 2 == 0 || py_out % 2 == 0 || pz_out % 2 == 0 ) {

          printf("Patch dimensions currently required to be odd.");
           return;

        }

        if ( px_out != py_out || py_out != pz_out || px_out != pz_out ) {

            printf("Patch x,y,z dimensions currently assumed to be equal.");
            return;

        }

        if ( px_out < sx || py_out < sy || pz_out < sz ){

            printf("Patch dimensions must be >= to stride dimensions");
            return;

        }


    // Set GPU to use

        int nDevices_found;

        // Total number of GPUs visible on the system
        cudaGetDeviceCount(&nDevices_found);

        if (nDevices_found == 0) {

            printf("No GPUs found.");
            return;

        }

        if ( gpu_idx < 0 || gpu_idx > nDevices_found-1 ) {

            printf("GPU specified for use does not exist or is not visible. (NOTE: GPU indexing starts from 0.)");
            return;

        }

        // Set device to use for patching
        cudaSetDevice( gpu_idx );
    
    // allocate size parameters
    
        size_t D_rows = nvol*px_out*py_out*pz_out; // one patch worth of voxels / volume
        int3 sz3 = make_int3(nx,ny,nz);
    
    // execution configuration
    
        dim3 gsP = dim3( sz3.x, 1, 1);
        dim3 bsP = dim3( MIN(D_rows/nvol,1024) ,1,1); // dim3(D_rows/nvol,1,1);
        
    // Create offset map (on the GPU) for extracting patches...  only need one map for all volumes
    
        int *offset_map = (int *) malloc(3*(D_rows/nvol)*sizeof(int));
        
        fill_offset_map_square( offset_map, D_rows/nvol, (px_out - 1) / 2 );
        
        int *offset_map_;
        cudaMallocManaged( (void **) &offset_map_, 3*(D_rows/nvol)*sizeof(int), cudaMemAttachGlobal );
        memcpy( offset_map_, offset_map, 3*(D_rows/nvol)*sizeof(int) );
        
        free(offset_map);
    
    // memory allocation
        
        float *X_, *Xp_;
        
        size_t szxy0 = sz3.x*sz3.y;
        size_t szxy  = ( (int) ( ( ny + sy - 1 ) / sy ) ) * ( (int) ( ( nx + sx - 1 ) / sx ) );
    
        cudaMallocManaged( (void **) &X_, szxy0 * sz3.z * nvol * sizeof(float), cudaMemAttachGlobal ); // sz3.x * sz3.y * sz3.z * nvol
        
        cudaMallocManaged( (void **) &Xp_, D_rows * szxy * sizeof(float), cudaMemAttachGlobal ); // D_rows * sz3.x*sz3.y
        memcpy( Xp_, patches, D_rows * szxy * sizeof(float) );
        
    // function call
                            
        return_patches_stride_call(Xp_, X_, offset_map_, z_slice, sz3, nvol, D_rows/nvol, sx, sy, sz, gsP, bsP);
        
    // Device synchronize and copy to output
        
        cudaDeviceSynchronize();
        
        memcpy( vol, X_, szxy0 * sz3.z * nvol * sizeof(float) );
        
    // Free cuda memory

        cudaFree( X_ );
        cudaFree( Xp_ );
        cudaFree( offset_map_ );
    
}