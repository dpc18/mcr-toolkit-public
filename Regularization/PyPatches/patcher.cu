// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.


// Functions for extracting patches from and returning patches to one or more volumes
// Supports reliable undersampling factors of 1, 4, and 8.
// Supports D_rows <= 8192 voxels

// #include "cuda_runtime.h"
#include "patcher.cuh"

// ***      ***
// *** HOST ***
// ***      ***

    // Compute an offset map to facilitate mappings between
    // 3D patches and 1D indices.
    void fill_offset_map(int *offset_map, const int len, const int radius) {

        unsigned int count = 0;
        int i, j, k;

        for (i = -radius; i <= radius; i++) { // compute the number of voxels inside the patch
            for (j = -radius; j <= radius; j++) {
                for (k = -radius; k <= radius; k++) {

                    if (sqrtf((float) (i*i + j*j + k*k)) >= 1.01f*radius) continue;

                    offset_map[count] = i;
                    offset_map[count+len] = j;
                    offset_map[count+2*len] = k;

                    count++;

                }
            }
        }

    }
    
    // Compute an offset map to facilitate mappings between
    // 3D patches and 1D indices.
    // Square patches
    void fill_offset_map_square(int *offset_map, const int len, const int radius) {

        unsigned int count = 0;
        int i, j, k;

        for (i = -radius; i <= radius; i++) { // compute the number of voxels inside the patch
            for (j = -radius; j <= radius; j++) {
                for (k = -radius; k <= radius; k++) {

                    // if (sqrtf((float) (i*i + j*j + k*k)) >= 1.01f*radius) continue;

                    offset_map[count] = i;
                    offset_map[count+len] = j;
                    offset_map[count+2*len] = k;

                    count++;

                }
            }
        }

    }

// ***        ***
// *** DEVICE ***
// ***        ***

    // Normalize regularization results with the number of overlapping patches at each voxel
    // grid_dim = dim3((sz.x+BLOCK_SZ-1)/BLOCK_SZ,1,1)
    // block_dim = dim3(BLOCK_SZ,1,1)
    __global__ void normalize(float *d, float *count, int3 sz, const int nvol) {

        int x = blockIdx.x*blockDim.x + threadIdx.x;

        if (x >= sz.x) return;

        unsigned int idx1, idx2;
        // int val;
        float val;

        unsigned int vol_sz = sz.x*sz.y*sz.z; 

        for (int z = 0; z < sz.z; z++) {

            idx1 = x + sz.x*sz.y*z;

            for (int y = 0; y < sz.y; y++) {

                idx2 = idx1 + sz.x*y;

                val = count[idx2]; // /nvol; // compensate for adding 1 nvol times to count but not d

                if (val > 0) {

                    for (int nv = 0; nv < nvol; nv++) {

                        d[idx2 + nv*vol_sz] /= val;

                    }

                }

            }

        }

    }

// ***                        ***
// *** DEVICE - Full Sampling ***
// ***                        ***

    // Extract 3D patches from a 2D slice along z.
    // Requirements
    // voxels / patch = blockDim.x
    // extracts patches from one z slice
    // ILP along y
    // Offset map written offsets_x, offsets_y, offsets_z for coalesced reads.
    // assumes sz.x, sz.y, sz.z not evenly divisible by samp
    // grid_dim = dim3(sz.x,1,1)
    // block_dim = dim3(offset map length,1,1)
    __global__ void extract_patches_full(const float *Vol, float *Patches, const int* om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols) 
    {

        int i, j, k, y2;
        unsigned int idx;
        int x = blockIdx.x;
        unsigned int om_len = drows_nvols; // blockDim.x;
        unsigned int om_len_nv = om_len*nvol;
        int szy1 = sz.y-1;
        // unsigned int tid = threadIdx.x;
        unsigned int vol_sz = sz.x*sz.y*sz.z;

        int zk, xi;
        unsigned int offset0;

        // handle patches > 1024
        for (int tid = threadIdx.x; tid < drows_nvols; tid += 1024) {

            i = om[tid];
            j = om[om_len+tid];
            k = om[2*om_len+tid];

            zk = z + k;
            xi = x + i;

            // Mirror as needed.
            if (zk < 0) zk = -zk;
            if (zk > sz.z - 1) zk = sz.z - 1 - k;

            if (xi < 0) xi = -xi;
            if (xi > sz.x-1) xi = sz.x - 1 - i;

            offset0 = sz.x*sz.y*zk + xi;

            for (int nv = 0; nv < nvol; nv++) {

                // offsetp = x*om_len_nv + (tid + nv*om_len); // patches offset

                for (int y = 0; y < sz.y; y++) {

                    y2 = y + j;

                    if (y2 < 0) y2 = -y2;
                    if (y2 > szy1) y2 = szy1 - j;

                    idx = offset0 + sz.x*y2;

                    Patches[om_len_nv*(sz.x*y + x) + nv*om_len + tid] = Vol[idx + nv*vol_sz];

                }

            }

        }

    }

    __global__ void return_patches_full(const float *Patches, float *d, float *count, float* gamma, const int *om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols) {

        int i, j, k, y2;
        unsigned int idx, offset0;
        int x = blockIdx.x;
        unsigned int om_len = drows_nvols; // blockDim.x;
        unsigned int om_len_nv = om_len*nvol;
        int szy1 = sz.y-1;
        // unsigned int tid = threadIdx.x;
        unsigned int vol_sz = sz.x*sz.y*sz.z;

        int zk, xi;
        float weight;

        // handle patches > 1024
        for (int tid = threadIdx.x; tid < drows_nvols; tid += 1024) {

            i = om[tid];
            j = om[om_len+tid];
            k = om[2*om_len+tid];

            zk = z + k;
            xi = x + i;

            // Mirror as needed.
            if (zk < 0)  continue; // zk = -zk;
            if (zk > sz.z - 1)  continue; // zk = sz.z - 1 - k;

            if (xi < 0)  continue; // xi = -xi;
            if (xi > sz.x-1)  continue; // xi = sz.x - 1 - i;

            for (int nv = 0; nv < nvol; nv++) {

                offset0 = sz.x*sz.y*zk + xi + nv*vol_sz; // vol offset

                for (int y = 0; y < sz.y; y++) {

                    y2 = y + j;

                    if (y2 < 0)  continue; // y2 = -y2;
                    if (y2 > szy1)  continue; // y2 = szy1 - j;

                    idx = offset0 + sz.x*y2;

                    weight = gamma[ MAX_ATOMS * ( sz.x*y + x )];
                    weight = 1.0f / (1.0f + weight*weight);

                    atomicAdd(d + idx, weight*Patches[om_len_nv*(sz.x*y + x) + nv*om_len + tid]);

                    if (nv == 0) { // Only do this for the first volume to avoid extra atomic operations

                        // atomicAdd(count + idx, 1);
                        atomicAdd(count + idx,  weight); // add sparsity level weighting

                    }

                }

            }

        }

    }

// ***                           ***
// *** DEVICE - 4x Undersampling ***
// ***                           ***

    // Extract 3D patches from a 2D slice along z.
    // Requirements
    // voxels / patch = blockDim.x
    // extracts patches from one z slice
    // ILP along y
    // Offset map written offsets_x, offsets_y, offsets_z for coalesced reads.
    // assumes sz.x, sz.y, sz.z not evenly divisible by samp
    // grid_dim = dim3(sz.x,1,1)
    // block_dim = dim3(offset map length,1,1)
    __global__ void extract_patches_4x(const float *Vol, float *Patches, const int* om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols) 
    {

        int x = blockIdx.x;
        int zmod = z % 2;
        int xmod = x % 2;

        if ( (zmod == 0 && xmod == 1) || (zmod == 1 && xmod == 0)) return; // quit early if all y's are empty

        int i, j, k, y2;
        unsigned int idx;
        unsigned int om_len = drows_nvols; // blockDim.x;
        unsigned int om_len_nv = drows_nvols*nvol; // om_len*nvol;
        int szy1 = sz.y-1;
        // unsigned int tid = threadIdx.x;
        unsigned int vol_sz = sz.x*sz.y*sz.z;

        int zk, xi;
        unsigned int offset0;

        // handle patches > 1024
        for (int tid = threadIdx.x; tid < drows_nvols; tid += 1024) {

            i = om[tid];
            j = om[om_len+tid];
            k = om[2*om_len+tid];

            zk = z + k;
            xi = x + i;

            // Mirror as needed.
            if (zk < 0) zk = -zk;
            if (zk > sz.z - 1) zk = sz.z - 1 - k;

            if (xi < 0) xi = -xi;
            if (xi > sz.x-1) xi = sz.x - 1 - i;

            offset0 = sz.x*sz.y*zk + xi;

            for (int nv = 0; nv < nvol; nv++) {

                if (xmod == 0){ // zmod == 0

                    for (int y = 0; y < sz.y; y += 2) { // y's with y % 2 == 0

                        y2 = y + j;

                        if (y2 < 0) y2 = -y2;
                        if (y2 > szy1) y2 = szy1 - j;

                        idx = offset0 + sz.x*y2;

                        Patches[om_len_nv*((sz.x/2)*(y/2) + (x/2)) + nv*om_len + tid] = Vol[idx + nv*vol_sz];

                    }

                }
                else {  // zmod == 1, xmod == 1

                    for (int y = 1; y < sz.y; y += 2) { // y's with y % 2 == 1

                        y2 = y + j;

                        if (y2 < 0) y2 = -y2;
                        if (y2 > szy1) y2 = szy1 - j;

                        idx = offset0 + sz.x*y2;

                        Patches[om_len_nv*((sz.x/2)*(y/2) + (x/2)) + nv*om_len + tid] = Vol[idx + nv*vol_sz];

                    }

                }

            }

        }

    }

    __global__ void return_patches_4x(const float *Patches, float *d, float *count, float* gamma, const int *om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols) {

        int x = blockIdx.x;
        int zmod = z % 2;
        int xmod = x % 2;

        if ( (zmod == 0 && xmod == 1) || (zmod == 1 && xmod == 0)) return; // quit early if all y's are empty

        int i, j, k, y2;
        unsigned int idx, offset0;
        unsigned int om_len = drows_nvols; // blockDim.x;
        unsigned int om_len_nv = om_len*nvol;
        int szy1 = sz.y-1;
        // unsigned int tid = threadIdx.x;
        unsigned int vol_sz = sz.x*sz.y*sz.z;

        int zk, xi;
        float weight;

        // handle patches > 1024
        for (int tid = threadIdx.x; tid < drows_nvols; tid += 1024) {

            i = om[tid];
            j = om[om_len+tid];
            k = om[2*om_len+tid];

            zk = z + k;
            xi = x + i;

            // Mirror as needed.
            if (zk < 0) continue; //  zk = -zk;
            if (zk > sz.z - 1) continue; //  zk = sz.z - 1 - k;

            if (xi < 0) continue; //  xi = -xi;
            if (xi > sz.x-1) continue; //  xi = sz.x - 1 - i;

            for (int nv = 0; nv < nvol; nv++) {

                offset0 = sz.x*sz.y*zk + xi + nv*vol_sz; // vol offset

                if (xmod == 0){ // zmod == 0

                    for (int y = 0; y < sz.y; y += 2) { // y's with y % 2 == 0

                        y2 = y + j;

                        if (y2 < 0) continue; //  y2 = -y2;
                        if (y2 > szy1) continue; //  y2 = szy1 - j;

                        idx = offset0 + sz.x*y2;

                        weight = gamma[ MAX_ATOMS * ( (sz.x/2)*(y/2) + (x/2) )];
                        weight = 1.0f / (1.0f + weight*weight);

                        atomicAdd(d + idx, weight*Patches[om_len_nv*((sz.x/2)*(y/2) + (x/2)) + nv*om_len + tid]);

                        if (nv == 0) { // Only do this for the first volume to avoid extra atomic operations

                            // atomicAdd(count + idx, 1);
                            atomicAdd(count + idx,  weight); // add sparsity level weighting

                        }

                    }

                }
                else {  // zmod == 1, xmod == 1

                    for (int y = 1; y < sz.y; y += 2) { // y's with y % 2 == 1

                        y2 = y + j;

                        if (y2 < 0)  continue; // y2 = -y2;
                        if (y2 > szy1) continue; //  y2 = szy1 - j;

                        idx = offset0 + sz.x*y2;

                        weight = gamma[ MAX_ATOMS * ( (sz.x/2)*(y/2) + (x/2) )];
                        weight = 1.0f / (1.0f + weight*weight);

                        atomicAdd(d + idx, weight*Patches[om_len_nv*((sz.x/2)*(y/2) + (x/2)) + nv*om_len + tid]);

                        if (nv == 0) { // Only do this for the first volume to avoid extra atomic operations

                            // atomicAdd(count + idx, 1);
                            atomicAdd(count + idx, weight ); // add sparsity level weighting

                        }

                    }

                }

            }

        }

    }

// ***                           ***
// *** DEVICE - 8x Undersampling ***
// ***                           ***

    // Extract 3D patches from a 2D slice along z.
    // Requirements
    // voxels / patch = blockDim.x
    // extracts patches from one z slice
    // ILP along y
    // Offset map written offsets_x, offsets_y, offsets_z for coalesced reads.
    // assumes sz.x, sz.y, sz.z not evenly divisible by samp
    // grid_dim = dim3(sz.x,1,1)
    // block_dim = dim3(offset map length,1,1)
    __global__ void extract_patches_8x(const float *Vol, float *Patches, const int* om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols)
    {

        int x = blockIdx.x;
        int zmod = z % 4;
        int xmod = x % 2;

        if ( (zmod == 2 && xmod == 0) || (zmod == 0 && xmod == 1)) return; // quit early if all y's are empty

        int i, j, k, y2;
        unsigned int idx;
        unsigned int om_len = drows_nvols; // blockDim.x;
        unsigned int om_len_nv = drows_nvols*nvol; // om_len*nvol;
        int szy1 = sz.y-1;
        // unsigned int tid = threadIdx.x;
        unsigned int vol_sz = sz.x*sz.y*sz.z;

        int zk, xi;
        unsigned int offset0;

        // handle patches > 1024
        for (int tid = threadIdx.x; tid < drows_nvols; tid += 1024) {

            i = om[tid];
            j = om[om_len+tid];
            k = om[2*om_len+tid];

            zk = z + k;
            xi = x + i;

            // Mirror as needed.
            if (zk < 0) zk = -zk;
            if (zk > sz.z - 1) zk = sz.z - 1 - k;

            if (xi < 0) xi = -xi;
            if (xi > sz.x-1) xi = sz.x - 1 - i;

            offset0 = sz.x*sz.y*zk + xi;

            for (int nv = 0; nv < nvol; nv++) {

                if (zmod == 0 && xmod == 0){

                    for (int y = 0; y < sz.y; y += 2) { // y's with y % 2 == 0

                        y2 = y + j;

                        if (y2 < 0) y2 = -y2;
                        if (y2 > szy1) y2 = szy1 - j;

                        idx = offset0 + sz.x*y2;

                        Patches[om_len_nv*((sz.x/2)*(y/2) + (x/2)) + nv*om_len + tid] = Vol[idx + nv*vol_sz];

                    }

                }
                else if (xmod == 1) {

                    for (int y = 1; y < sz.y; y += 2) { // y's with y % 2 == 1

                        y2 = y + j;

                        if (y2 < 0) y2 = -y2;
                        if (y2 > szy1) y2 = szy1 - j;

                        idx = offset0 + sz.x*y2;

                        Patches[om_len_nv*((sz.x/2)*(y/2) + (x/2)) + nv*om_len + tid] = Vol[idx + nv*vol_sz];

                    }

                }

            }

        }

    }

    __global__ void return_patches_8x(const float *Patches, float *d, float *count, float* gamma, const int *om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols) {

        int x = blockIdx.x;
        int zmod = z % 4;
        int xmod = x % 2;

        if ( (zmod == 2 && xmod == 0) || (zmod == 0 && xmod == 1) ) return; // quit early if all y's are empty

        int i, j, k, y2;
        unsigned int idx, offset0;
        unsigned int om_len = drows_nvols; // blockDim.x;
        unsigned int om_len_nv = om_len*nvol;
        int szy1 = sz.y-1;
        // unsigned int tid = threadIdx.x;
        unsigned int vol_sz = sz.x*sz.y*sz.z;

        int zk, xi;
        float weight;

        // handle patches > 1024
        for (int tid = threadIdx.x; tid < drows_nvols; tid += 1024) {

            i = om[tid];
            j = om[om_len+tid];
            k = om[2*om_len+tid];

            zk = z + k;
            xi = x + i;

            // Mirror as needed.
            if (zk < 0) continue; // zk = -zk;
            if (zk > sz.z - 1) continue; // zk = sz.z - 1 - k;

            if (xi < 0) continue; //  xi = -xi;
            if (xi > sz.x-1) continue; //  xi = sz.x - 1 - i;

            for (int nv = 0; nv < nvol; nv++) {

                offset0 = sz.x*sz.y*zk + xi + nv*vol_sz; // vol offset

                if (zmod == 0 && xmod == 0){

                    for (int y = 0; y < sz.y; y += 2) { // y's with y % 2 == 0

                        y2 = y + j;

                        if (y2 < 0) continue; //  y2 = -y2;
                        if (y2 > szy1) continue; //  y2 = szy1 - j;

                        idx = offset0 + sz.x*y2;

                        weight = gamma[ MAX_ATOMS * ( (sz.x/2)*(y/2) + (x/2) )];
                        weight = 1.0f / (1.0f + weight*weight);

                        atomicAdd(d + idx, weight*Patches[om_len_nv*((sz.x/2)*(y/2) + (x/2)) + nv*om_len + tid]);

                        if (nv == 0) { // Only do this for the first volume to avoid extra atomic operations

                            // atomicAdd(count + idx, 1);
                            atomicAdd(count + idx,  weight); // add sparsity level weighting

                        }

                    }

                }
                else if (xmod == 1) {

                    for (int y = 1; y < sz.y; y += 2) { // y's with y % 2 == 1

                        y2 = y + j;

                        if (y2 < 0) continue; //  y2 = -y2;
                        if (y2 > szy1) continue; //  y2 = szy1 - j;

                        idx = offset0 + sz.x*y2;

                        weight = gamma[ MAX_ATOMS * ( (sz.x/2)*(y/2) + (x/2) )];
                        weight = 1.0f / (1.0f + weight*weight);

                        atomicAdd(d + idx, weight*Patches[om_len_nv*((sz.x/2)*(y/2) + (x/2)) + nv*om_len + tid]);

                        if (nv == 0) { // Only do this for the first volume to avoid extra atomic operations

                            // atomicAdd(count + idx, 1);
                            atomicAdd(count + idx,  weight); // add sparsity level weighting

                        }

                    }

                }

            }

        }

    }
    
// ***                           ***
// *** DEVICE - Strided Sampling ***
// ***                           ***
    
    void extract_patches_stride_call(const float *Vol, float *Patches, const int* om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols,
                                unsigned int s_x, unsigned int s_y, unsigned int s_z, dim3 grid, dim3 block)
    {
     
        extract_patches_stride<<<grid,block>>>( Vol, Patches, om, z, sz, nvol, drows_nvols, s_x, s_y, s_z); 
        
    }

    // Extract 3D patches from a 2D slice along z.
    // Requirements
    // voxels / patch = blockDim.x
    // extracts patches from one z slice
    // ILP along y
    // Offset map written offsets_x, offsets_y, offsets_z for coalesced reads.
    // assumes sz.x, sz.y, sz.z not evenly divisible by samp
    // grid_dim = dim3(sz.x,1,1)
    // block_dim = dim3(offset map length,1,1)
    __global__ void extract_patches_stride(const float *Vol, float *Patches, const int* om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols,
                                           unsigned int s_x, unsigned int s_y, unsigned int s_z) 
    {
        
        int x = blockIdx.x;
        // int zmod = z % s_z;
        int xmod = x % s_x;

        if (xmod != 0) return; // quit early if we are not sampling this patch

        int i, j, k, y2;
        unsigned int idx;
        unsigned int om_len = drows_nvols; // blockDim.x;
        unsigned int om_len_nv = om_len*nvol;
        unsigned int patch_num;
        int szy1 = sz.y-1;
        // unsigned int tid = threadIdx.x;
        unsigned int vol_sz = sz.x*sz.y*sz.z;

        int zk, xi;
        unsigned int offset0;

        // handle patches > 1024
        for (int tid = threadIdx.x; tid < drows_nvols; tid += 1024) {

            i = om[tid];
            j = om[om_len+tid];
            k = om[2*om_len+tid];

            zk = z + k;
            xi = x + i;

            // Mirror as needed.
            if (zk < 0) zk = -zk;
            if (zk > sz.z - 1) zk = sz.z - 1 - k;

            if (xi < 0) xi = -xi;
            if (xi > sz.x-1) xi = sz.x - 1 - i;

            offset0 = sz.x*sz.y*zk + xi;

            for (int nv = 0; nv < nvol; nv++) {

                // offsetp = x*om_len_nv + (tid + nv*om_len); // patches offset

                // for (int y = 0; y < sz.y; y++) {
                for ( int y = 0; y < sz.y; y += s_y ) {
                    
                    patch_num = ( (int) (x / s_x) ) + ( (int) (y / s_y) ) * ( (int) ( ( sz.x + s_x - 1 ) / s_x ) );

                    y2 = y + j;

                    if (y2 < 0) y2 = -y2;
                    if (y2 > szy1) y2 = szy1 - j;

                    idx = offset0 + sz.x*y2;

                    // Patches[om_len_nv*(sz.x*y + x) + nv*om_len + tid] = Vol[idx + nv*vol_sz];
                    Patches[om_len_nv*patch_num + nv*om_len + tid] = Vol[idx + nv*vol_sz];

                }

            }

        }

    }
    
    void return_patches_stride_call(const float *Patches, float *d, const int *om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols,
                               unsigned int s_x, unsigned int s_y, unsigned int s_z, dim3 grid, dim3 block)
    {
     
        return_patches_stride<<<grid,block>>>( Patches, d, om, z, sz, nvol, drows_nvols, s_x, s_y, s_z); 
        
    }

    __global__ void return_patches_stride(const float *Patches, float *d, const int *om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols,
                                          unsigned int s_x, unsigned int s_y, unsigned int s_z) // , float* gamma
    {
        
        int x = blockIdx.x;
        // int zmod = z % s_z;
        int xmod = x % s_x;

        if (xmod != 0) return; // quit early if we are not returning to this patch

        int i, j, k, y2;
        unsigned int idx, offset0;
        // int x = blockIdx.x;
        unsigned int om_len = drows_nvols; // blockDim.x;
        unsigned int om_len_nv = om_len*nvol;
        unsigned int patch_num;
        int szy1 = sz.y-1;
        // unsigned int tid = threadIdx.x;
        unsigned int vol_sz = sz.x*sz.y*sz.z;

        int zk, xi;
        // float weight;

        // handle patches > 1024
        for (int tid = threadIdx.x; tid < drows_nvols; tid += 1024) {

            i = om[tid];
            j = om[om_len+tid];
            k = om[2*om_len+tid];

            zk = z + k;
            xi = x + i;

            // Mirror as needed.
            if (zk < 0) continue; // zk = -zk;
            if (zk > sz.z - 1) continue; // zk = sz.z - 1 - k;

            if (xi < 0) continue; // xi = -xi;
            if (xi > sz.x-1) continue; // xi = sz.x - 1 - i;

            for (int nv = 0; nv < nvol; nv++) {

                offset0 = sz.x*sz.y*zk + xi + nv*vol_sz; // vol offset

                // for (int y = 0; y < sz.y; y++) {
                for ( int y = 0; y < sz.y; y += s_y ) {
                    
                    patch_num = ( (int) (x / s_x) ) + ( (int) (y / s_y) ) * ( (int) ( ( sz.x + s_x - 1 ) / s_x ) );

                    y2 = y + j;

                    if (y2 < 0) continue; // y2 = -y2;
                    if (y2 > szy1) continue; // y2 = szy1 - j;

                    idx = offset0 + sz.x*y2;

                    // weight = gamma[ MAX_ATOMS * ( sz.x*y + x )];
                    // weight = 1.0f / (1.0f + weight*weight);

                    // atomicAdd(d + idx, weight*Patches[om_len_nv*(sz.x*y + x) + nv*om_len + tid]);
                    
                    // atomicAdd(d + idx, weight*Patches[om_len_nv*patch_num + nv*om_len + tid]);
                    
                    atomicAdd(d + idx, Patches[om_len_nv*patch_num + nv*om_len + tid]);

                    //if (nv == 0) { // Only do this for the first volume to avoid extra atomic operations

                        // atomicAdd(count + idx, 1);
                     //   atomicAdd(count + idx,  weight); // add sparsity level weighting

                    //}

                }

            }

        }

    }
