// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.


// Functions for extracting patches from and returning patches to one or more volumes
// Supports reliable undersampling factors of 1, 4, and 8.
// Supports D_rows <= 8192 voxels

#ifndef PATCHER

    #define PATCHER

    // ***           ***
    // *** FUNCTIONS ***
    // ***           ***

    #define MAX_ATOMS 0 // not currently used here...

    void fill_offset_map(int *offset_map, const int len, const int radius);
    void fill_offset_map_square(int *offset_map, const int len, const int radius);

    // __global__ void normalize(float *d, unsigned int *count, int3 sz, const int nvol);
    __global__ void normalize(float *d, float *count, int3 sz, const int nvol);

    // Full sampling (use all overlapping patches)
    __global__ void extract_patches_full(const float *Vol, float *Patches, const int* om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols);
    __global__ void return_patches_full(const float *Patches, float *d, float *count, float* gamma, const int *om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols);

    // 4x undersampling
    __global__ void extract_patches_4x(const float *Vol, float *Patches, const int* om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols) ;
    __global__ void return_patches_4x(const float *Patches, float *d, float *count, float* gamma, const int *om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols);

    // 8x undersampling
    __global__ void extract_patches_8x(const float *Vol, float *Patches, const int* om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols);
    __global__ void return_patches_8x(const float *Patches, float *d, float *count, float* gamma, const int *om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols);

    // Strided sampling
    void extract_patches_stride_call(const float *Vol, float *Patches, const int* om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols,
                                unsigned int s_x, unsigned int s_y, unsigned int s_z, dim3 grid, dim3 block);
    __global__ void extract_patches_stride(const float *Vol, float *Patches, const int* om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols,
                                                  unsigned int s_x, unsigned int s_y, unsigned int s_z);
    void return_patches_stride_call(const float *Patches, float *d, const int *om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols,
                               unsigned int s_x, unsigned int s_y, unsigned int s_z, dim3 grid, dim3 block);
    __global__ void return_patches_stride(const float *Patches, float *d, const int *om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols,
                                               unsigned int s_x, unsigned int s_y, unsigned int s_z);

#endif // PATCHER