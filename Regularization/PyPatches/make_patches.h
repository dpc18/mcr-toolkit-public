// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MAKE_PATCHES

    #define MAKE_PATCHES

    #include "cuda_runtime.h"
    #include <stdint.h>

    #define MAX(x, y) (((x) > (y)) ? (x) : (y))
    #define MIN(x, y) (((x) < (y)) ? (x) : (y))

    extern "C" {

        void make_patches( const float *vol, float *patches, size_t z_slice, size_t px_out, size_t py_out, size_t pz_out,
                           size_t sx, size_t sy, size_t sz, size_t nx, size_t ny, size_t nz, size_t nvol, int gpu_idx );

        void return_patches( const float *patches, float *vol, size_t z_slice, size_t px_out, size_t py_out, size_t pz_out,
                             size_t sx, size_t sy, size_t sz, size_t nx, size_t ny, size_t nz, size_t nvol, int gpu_idx );

    }
    
    extern void fill_offset_map_square(int *offset_map, const int len, const int radius);

    extern void extract_patches_stride_call(const float *Vol, float *Patches, const int* om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols,
                                            unsigned int s_x, unsigned int s_y, unsigned int s_z, dim3 grid, dim3 block);

    extern void return_patches_stride_call(const float *Patches, float *d, const int *om, const int z, const int3 sz, const int nvol, const unsigned int drows_nvols,
                                           unsigned int s_x, unsigned int s_y, unsigned int s_z, dim3 grid, dim3 block);
    
    
#endif // MAKE_PATCHES