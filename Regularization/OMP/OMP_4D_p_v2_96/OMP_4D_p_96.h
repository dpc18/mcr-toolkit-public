// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.


#ifndef OMP_4D_P

    #define OMP_4D_P

    // ***                  ***
    // *** GLOBAL VARIABLES ***
    // ***                  ***

    float *Xp, *D;
    float err;
    int na, prad, nvol;
    int *szXp;
    size_t D_rows;
    size_t np;
    
    // mwIndex *Gamma_col, *Gamma_row; // col, row indices for non-zero elements
    // double *Gamma_val; // for some reason matlab only does double precision sparse matrices
    
    float *Gamma_host;
    unsigned int *idx_host;
    
    // ***      ***
    // *** HOST ***
    // ***      ***

    // Call CUDA code
    extern void OMP_4D_p_cuda();

#endif // OMP_4D_P