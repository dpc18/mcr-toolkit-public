// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.


#include "mex.h"
#include <cuda_runtime.h>
#include "cublas_v2.h"

#include "OMP_4D_p_96.h"
// #include "OMP_shared.h"
// ASSUMES MAX_ATOMS = 96 ....

// ***     ***
// *** MEX ***
// ***     ***

// Patches => Patches
// [Gamma, Idx] = OMP_4D_p(Xp, D, err, na, prad, szXp, nvol, np)
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
    
    // Check inputs
    
        if (nrhs != 8)
            mexErrMsgTxt("Eight input arguments required! ( [Gamma, Idx] = OMP_4D_p(Xp, D, err, na, prad, szXp, nvol, np) )");

        if (nlhs != 2)
            mexErrMsgTxt("Two output argument required! ( [Gamma, Idx] = OMP_4D_p(Xp, D, err, na, prad, szXp, nvol, np) )");

        // Check the precision of the inputs
        if (!mxIsClass(prhs[0],"single")){ // Xp
            mexErrMsgTxt("First input argument (patch matrix) must be single precision.");  
        }

        if (!mxIsClass(prhs[1],"single")){ // D
            mexErrMsgTxt("Second input argument (dictionary) must be single precision.");  
        }

        if (!mxIsClass(prhs[2],"single")){ // err
            mexErrMsgTxt("Third input argument (error tolerance, sigma) must be single precision.");  
        }

        if (!mxIsClass(prhs[3],"int32")){ // na
            mexErrMsgTxt("Fourth input argument (atom number) must be int32.");
        }

        if (!mxIsClass(prhs[4],"int32")){ // prad
            mexErrMsgTxt("Fifth input argument (patch radius) must be int32.");
        }
        
        if (!mxIsClass(prhs[5],"int32")){ // szXp
            mexErrMsgTxt("Sixth input argument (patch matrix dimensions) must be int32.");
        }
        
        if (!mxIsClass(prhs[6],"int32")){ // nvol
            mexErrMsgTxt("Seventh input argument (number of volumes) must be int32.");
        }
        
        if (!mxIsClass(prhs[7],"int32")){ // np
            mexErrMsgTxt("Eighth input argument (number of patches) must be int32.");
        }
        
    // Parse inputs
    
        // Pointer to volume data
        Xp = (float*) mxGetData(prhs[0]);

        // Pointer to dictionary
        D = (float*) mxGetData(prhs[1]);

        // Error tolerance
        err = *((float*) mxGetData(prhs[2]));

        // Size parameters
        na = *((int*) mxGetData(prhs[3])); // number of atoms in the dictionary
        prad = *((int*) mxGetData(prhs[4])); // patch radius (size)
        szXp = (int*) mxGetData(prhs[5]);
        nvol = *((int*) mxGetData(prhs[6]));
        np = *((unsigned int*) mxGetData(prhs[7])); // number of patches in Xp
    
    // Check to make sure the patch radius is reasonable
        
        if ( prad > 6 ) { // changed from 4, 5

            mexErrMsgTxt("Compiled patch radius limit (prad = 6) exceeded.");

        }
    
    // Check to make sure the specified sizes match the passed data
        
        if (nvol < 1) {
            
            mexErrMsgTxt("A minimum of one volume is required.");
            
        }
        
        if (err < 0) {
            
            mexErrMsgTxt("Error tolerance must be non-negative.");
            
        }
    
        // compute patch size
        size_t counter = 0;
        for (int i = -prad; i <= prad; i++) {
            for (int j = -prad; j <= prad; j++) {
                for (int k = -prad; k <= prad; k++) {
         
                    if ( sqrtf((float) (i*i+j*j+k*k)) < 1.01f*prad ) {
                    
                        counter++;
                    
                    }
                    
                }
            }
        }
        
        D_rows = nvol*counter; // one patch worth of voxels / volume
    
        const size_t *szD = mxGetDimensions(prhs[1]); // dictionary dimensions
        mwSize D_dims = mxGetNumberOfDimensions(prhs[1]);
        size_t counter2 = 1;
        for (int i = 0; i < D_dims; i++) { // provided total number of voxels
     
            counter2 *= szD[i];
        
        }
        
        if (na*D_rows != counter2) {
     
             mexErrMsgTxt("Input dictionary does not match implied dimensions.");
        
        }
        
        if ( D_dims != 2 )
        {
         
            mexErrMsgTxt("Dictionary is not in a 2D matrix.");
            
        }
        
        if (na != szD[1]) {
         
            mexErrMsgTxt("Second dimension of the dictionary matrix must equal the specified number of atoms.");
            
        }
 
        const size_t *szXp_in = mxGetDimensions(prhs[0]); // patch matrix
        mwSize Xp_dims = mxGetNumberOfDimensions(prhs[0]);
        counter2 = 1;
        for (int i = 0; i < Xp_dims; i++) {
     
            counter2 *= szXp_in[i];
        
        }
        
        Xp_dims = mxGetN(prhs[5]); // patch matrix size vector
        counter = 1;
        for (int i = 0; i < Xp_dims; i++)
        {
     
            counter *= szXp[i];
        
        }
        
        if (counter != counter2)
        {
     
            mexErrMsgTxt("Input patch matrix does not match specified dimensions.");
        
        }
        
        if ( Xp_dims != 2 )
        {
         
            mexErrMsgTxt("Patch data is not in a 2D matrix.");
            
        }
        
        if (np != szXp_in[1]) {
         
            mexErrMsgTxt("Second dimension of the patch matrix must equal the specified number of patches.");
            
        }
        
    // Assign output
    const mwSize siz[2] = {96,np};
    const mwSize siz2[2] = {96,np};
    plhs[0] = mxCreateNumericArray(2,siz,mxSINGLE_CLASS,mxREAL);
    plhs[1] = mxCreateNumericArray(2,siz2,mxUINT32_CLASS,mxREAL);
    Gamma_host = (float*) mxGetData(plhs[0]);
    idx_host = (unsigned int *) mxGetData(plhs[1]);
    
    // Assign output (sparse, real, double precision)
    // plhs[0] = mxCreateSparse(na, np, 96*np,(mxComplexity) 0);
    // Gamma_val = mxGetPr(plhs[0]); // values
    // Gamma_col = mxGetJc(plhs[0]); // column (only increment at the end of each column)
    // Gamma_row = mxGetIr(plhs[0]); // row (inner loop)
    
    // Perform patch-based orthogonal matching pursuit
    OMP_4D_p_cuda();
    
    // Make sure the device is done executing CUDA code.
    cudaDeviceSynchronize();
    
    // Error Checking
    cudaError_t error = cudaGetLastError();
    plhs[2] = mxCreateLogicalScalar(error == cudaSuccess);
    if (error != cudaSuccess) {
        
        mexPrintf("CUDA ERROR: %s\n",cudaGetErrorString(error));
        
    }
    
    // Reset the device to free memory and to allow CUDA profiling
    // cudaThreadExit();
    cudaDeviceReset();
    
}