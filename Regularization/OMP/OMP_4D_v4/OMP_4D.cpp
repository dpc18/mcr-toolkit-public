// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.


#include "mex.h"
#include <cuda_runtime.h>
#include "cublas_v2.h"

#include "OMP_4D.h"

// ***     ***
// *** MEX ***
// ***     ***

// limits:
// 13x13x13 radial patch size
// 8192 voxels per dictionary / patch column (D_rows = nvol*counter <= 8192)

// Volume => Volume
// d = OMP_4D(X, D, err, na, prad, szX, nvol, samp, idx_rng)
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
    
    // Check inputs
    
        if (nrhs != 9)
            mexErrMsgTxt("Nine input arguments required! ( d = OMP_4D(X, D, err, na, prad, szX, nvol, samp, idx_rng) )");

        if (nlhs != 1)
            mexErrMsgTxt("One output argument required! ( d = OMP_4D(X, D, err, na, prad, szX, nvol, samp, idx_rng) )");

        // Check the precision of the inputs
        if (!mxIsClass(prhs[0],"single")){ // X
            mexErrMsgTxt("First input argument (grayscale data) must be single precision.");  
        }

        if (!mxIsClass(prhs[1],"single")){ // D
            mexErrMsgTxt("Second input argument (dictionary) must be single precision.");  
        }

        if (!mxIsClass(prhs[2],"single")){ // err
            mexErrMsgTxt("Third input argument (error tolerance, sigma) must be single precision.");  
        }

        if (!mxIsClass(prhs[3],"int32")){ // na
            mexErrMsgTxt("Fourth input argument (atom number) must be int32.");
        }

        if (!mxIsClass(prhs[4],"int32")){ // prad
            mexErrMsgTxt("Fifth input argument (patch radius) must be int32.");
        }
        
        if (!mxIsClass(prhs[5],"int32")){ // szX
            mexErrMsgTxt("Sixth input argument (volume dimensions) must be int32.");
        }
        
        if (!mxIsClass(prhs[6],"int32")){ // nvol
            mexErrMsgTxt("Seventh input argument argument (number of volumes) must be int32.");
        }
        
        if (!mxIsClass(prhs[7],"int32")){ // samp
            mexErrMsgTxt("Eighth input argument (undersampling factor) must be int32.");
        }
        
        if (!mxIsClass(prhs[8],"int32")){ // idx_rng
            mexErrMsgTxt("Ninth input argument (atom index range for sparse coding) must be int32.");
        }
        
    // Parse inputs
    
        // Pointer to volume data
        X = (float*) mxGetData(prhs[0]);

        // Pointer to dictionary
        D = (float*) mxGetData(prhs[1]);

        // Error tolerance
        err = *((float*) mxGetData(prhs[2]));

        // Size parameters
        na = *((int*) mxGetData(prhs[3])); // number of atoms in the dictionary
        prad = *((int*) mxGetData(prhs[4])); // patch radius (size)
        
        szX = (int*) mxGetData(prhs[5]);
        nvol = *((int*) mxGetData(prhs[6]));
        // skip = (int*) mxGetData(prhs[7]);
        samp = *((int*) mxGetData(prhs[7]));
        
        idx_rng = (int*) mxGetData(prhs[8]);
    
    // Check to make sure the patch radius is reasonable
        
        if ( prad > 6 ) { // changed from 4, 5

            mexErrMsgTxt("Compiled patch radius limit (prad = 6) exceeded.");

        }
    
    // Check to make sure the specified sizes match the passed data
        
        if (nvol < 1) {
            
            mexErrMsgTxt("A minimum of one volume is required.");
            
        }
        
        if (err < 0) {
            
            mexErrMsgTxt("Error tolerance must be non-negative.");
            
        }
    
        // compute patch size
        size_t counter = 0;
        for (int i = -prad; i <= prad; i++) {
            for (int j = -prad; j <= prad; j++) {
                for (int k = -prad; k <= prad; k++) {
         
                    if ( sqrtf((float) (i*i+j*j+k*k)) < 1.01f*prad ) {
                    
                        counter++;
                    
                    }
                    
                }
            }
        }
        
        D_rows = nvol*counter; // one patch worth of voxels / volume
    
        const size_t *szD = mxGetDimensions(prhs[1]); // dictionary dimensions
        mwSize D_dims = mxGetNumberOfDimensions(prhs[1]);
        size_t counter2 = 1;
        for (int i = 0; i < D_dims; i++) { // provided total number of voxels
     
            counter2 *= szD[i];
        
        }
        
        if (na*D_rows != counter2) {
     
            mexErrMsgTxt("Input dictionary does not match implied dimensions.");
        
        }
        
        const size_t *szX_in = mxGetDimensions(prhs[0]); // volume
        mwSize X_dims = mxGetNumberOfDimensions(prhs[0]);
        counter2 = 1;
        for (int i = 0; i < X_dims; i++) {
     
            counter2 *= szX_in[i];
        
        }
        
        X_dims = mxGetN(prhs[5]); // size vector
        counter = 1;
        for (int i = 0; i < X_dims; i++)
        {
     
            counter *= szX[i];
        
        }
        
        if (counter != counter2)
        {
     
            mexErrMsgTxt("Input volume(s) does/do not match specified dimensions.");
        
        }
        
        if ( (X_dims != 3) && (X_dims != 4) )
        {
         
            mexErrMsgTxt("Volume data must have three (four) dimensions or size vector not passed in as row vector.");
            
        }
        
        // make sure each dimension is larger than the kernel diameter
        if (szX[0] < (2*prad+1) || szX[1] < (2*prad+1) || szX[2] < (2*prad+1)) {
         
            mexErrMsgTxt("Volume dimensions must be greater than or equal to the kernel diameter.");
            
        }
        
        if (nvol > 1) {
         
            if (szX[3] != nvol) {
             
                mexErrMsgTxt("Fourth size parameter does not match the number of volumes specified.");
                
            }
            
        }
        
        if (samp < 1) {
            
            mexErrMsgTxt("Undersampling factor must be at least one.");
            
        }
        
        //if ( (samp != 1) && (((szX[0] / samp) * samp == szX[0]) || ((szX[1] / samp) * samp == szX[1]) || ((szX[2] / samp) * samp == szX[2])) ) {
            
        //    mexWarnMsgTxt("One or more dimensions is evenly divisible by the undersampling factor. This may result in sub-optimal performance.");
            
        //}
        
        if (samp > 1 && samp < 4) {
         
            mexWarnMsgTxt("Undersampling factor adjusted to 4.");
            
            samp = 4;
            
        }
        
        if (samp > 4 && samp != 8) {
         
            mexWarnMsgTxt("Undersampling factor adjusted to 8.");
            
            samp = 8;
            
        }
        
        mwSize idx_dims = mxGetN(prhs[8]); // reused variable
        
        if (idx_dims != 2) {
            
            mexErrMsgTxt("Atom index range must be a row vector with two elements.");
            
        }
        
        if ( (idx_rng[0] < 0) || (idx_rng[1] > (na-1)) ) {
            
            mexErrMsgTxt("Atom index range is invalid relative to the number of dictionary atoms. Valid range is [0 na-1].");
            
        }
    
    // Assign output
    const mwSize siz[2] = {szX[0]*szX[1]*szX[2]*nvol,1};
    plhs[0] = mxCreateNumericArray(2,siz,mxSINGLE_CLASS,mxREAL);
    d = (float*) mxGetData(plhs[0]);
    
    // Perform patch-based orthogonal matching pursuit
    OMP_4D_cuda();
    
    // Make sure the device is done executing CUDA code.
    cudaDeviceSynchronize();
    
    // Error Checking
    cudaError_t error = cudaGetLastError();
    plhs[2] = mxCreateLogicalScalar(error == cudaSuccess);
    if (error != cudaSuccess) {
        
        mexPrintf("CUDA ERROR: %s\n",cudaGetErrorString(error));
        
    }
    
    // free arrays generated within the mex function
    // free szX_in;
    // free szD;
    // free siz;
    
    // Reset the device to free memory and to allow CUDA profiling
    // cudaThreadExit();
    cudaDeviceReset();
    
}