// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#include "mex.h"
#include <cuda_runtime.h>
#include "cublas_v2.h"
#include <algorithm>
// #include "DEBUG.h"
// #include "cuPrintf.cu"

// #include "patches_4D.h"
// #include "patches_4D_under.h" // allows patch undersampling
// #include "patches_4D_under_v2.h" // allows patch undersampling with reliable factors (1, 4, 8; checkerboard)
// #include "OMP_shared.h"
// #include "OMP_shared_v2.h" // allows discriminative dictionary encoding through update_Xp_v3
#include "OMP_shared_v3.h" // supports D_rows <= 8192
#include "patches_4D_under_v4.h" // allows patch undersampling with reliable factors (1, 4, 8; checkerboard); weights overlapping patches by sparsity level

// ***                  ***
// *** EXTERN VARIABLES ***
// ***                  ***

extern float *X, *D, *d;
extern float err;
extern int na, prad, nvol;
extern int *szX;
extern int samp;
extern unsigned int D_rows;
extern int *idx_rng;

// ***        ***
// *** DEVICE ***
// ***        ***

__global__ void count_gamma(float *gamma) {
 
    unsigned int offset = blockIdx.x*MAX_ATOMS;
    
    unsigned int count = 0;
    
    // short-circuits before trying to read gamma[offset+MAX_ATOMS]
    while ( ( count < MAX_ATOMS ) && ( gamma[offset+count] != 0 ) ) {
        
        count++;
        
    }
    
    gamma[offset] = count;
    
}

// ***      ***
// *** HOST ***
// ***      ***

// Batch orthogonal matching pursuit adapted from:
// "Efficient Implementation of the K-SVD Algorithm using Batch Orthogonal Matching Pursuit."
// Ron Rubinstein, Michael Zibulevsky, and Michael Elad. 2008.
// Technion - Computer Science Department - Technical Report CS-2008-08.revised - 2008
void OMP_4D_cuda()
{
    
    // Double-sized shared memory banks (?)
    // cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte);
    
    int3 sz = make_int3(szX[0],szX[1],szX[2]);
    
    unsigned int szxy = 0;
    
    if (samp == 8 || samp == 4) { // since we still have the same number of patches per z slice used
        
        // szxy = (sz.x*sz.y)/4;
        szxy = ((sz.x+1)*(sz.y+1))/4; // make sure we have extra patches allocated for odd vs. even dimensions
        
    }
    else {
        
        szxy = (sz.x*sz.y)/samp;
        
    }
    
    unsigned int szxy0 = sz.x*sz.y;
    
    int *idx_rngd;
    cudaMalloc( (void **) &idx_rngd, 2*sizeof(int)); // sz.x * sz.y * sz.z * nvol
    cudaMemcpy(idx_rngd, idx_rng, 2*sizeof(int), cudaMemcpyHostToDevice);
    
    // Execution Configurations
    
        // Coefficient (A) operations
        dim3 gsA = dim3(szxy,1,1); // # of patches in Xp
        dim3 bsA = dim3(BLOCK_SZ,1,1);

        if ( na % BLOCK_SZ != 0 ) {

            mexErrMsgTxt("Number of atoms in dictionary must be divisible by block size.");

        }

        if (na < BLOCK_SZ) { // chunck_num > 1024) { // very unlikely, but just in case

            // mexErrMsgTxt("Atom number and block size combination exceeds hardware limit of 1024 threads/block.");

            mexErrMsgTxt("Insufficient number of dictionary atoms.");

        }
        
        // execution configuration for patch operations
        dim3 gsP = dim3(sz.x,1,1);
        dim3 bsP = dim3( min(D_rows/nvol,1024) ,1,1); // dim3(D_rows/nvol,1,1);
        
        // execution configuration for normalization operations
        dim3 gsN = dim3((sz.x+BLOCK_SZ-1)/BLOCK_SZ,1,1);
        dim3 bsN = dim3(BLOCK_SZ,1,1);
        
        // execution configuration for patch updates
        dim3 gsXp = dim3(szxy,1,1);
        dim3 bsXp = dim3( min(D_rows,1024) ,1,1);
        
        if (MAX_ATOMS > D_rows) {
     
            mexErrMsgTxt("Maximum number of atoms per patch exceeds number of rows per vectorized patch.");
        
        }
        
        // execution configuration for L updates
        dim3 gsL, bsL;
        
        // execution configuration for Gamma updates
        dim3 gsGam, bsGam;
    
        // execution configuration for Beta updates
        // dim3 gsB, bsB;
        
        // reduction operation with eps
        
        int drows_padded = 1 << (int) ceil(log((float) D_rows)/log(2.0f));
        
        dim3 gsEps = dim3(szxy,1,1);
        dim3 bsEps = dim3( min(drows_padded/2, 1024) ,1,1); // so we waste fewer threads inside the kernel
        
        if (drows_padded > 8192) {
         
            mexPrintf("Padded patch size exceeds compiled thread limits.");
            
        }

    // Create offset map (on the GPU) for extracting patches...  only need one map for all volumes
    
        int *offset_map = (int *) malloc(3*(D_rows/nvol)*sizeof(int));

        fill_offset_map(offset_map, D_rows/nvol, prad);
        
        int *offset_map_;
        cudaMalloc((void **) &offset_map_, 3*(D_rows/nvol)*sizeof(int));
        cudaMemcpy(offset_map_, offset_map, 3*(D_rows/nvol)*sizeof(int), cudaMemcpyHostToDevice);
        
        free(offset_map);

    // Preallocate memory

        float *X_, *d_, *Xp_, *D_, *G_, *means_, *ones_, *A00_, *A0_, *L_, *Gamma_, *B_; //, *A0abs_;
        cudaMalloc( (void **) &X_, szxy0*sz.z*nvol*sizeof(float)); // sz.x * sz.y * sz.z * nvol
        cudaMalloc( (void **) &d_, szxy0*sz.z*nvol*sizeof(float)); // sz.x * sz.y * sz.z * nvol
        cudaMalloc( (void **) &Xp_, D_rows*szxy*sizeof(float)); // D_rows * sz.x*sz.y
        cudaMalloc( (void **) &D_, D_rows*na*sizeof(float)); // D_rows * na
        cudaMalloc( (void **) &G_, na*na*sizeof(float)); // na * na
        cudaMalloc( (void **) &A00_, na*szxy*sizeof(float)); // stores D'*Xp, na * sz.x*sz.y
        cudaMalloc( (void **) &A0_, na*szxy*sizeof(float)); // A00_ after orthogonality updates, na x sz.x*sz.y
        cudaMalloc( (void **) &Gamma_, MAX_ATOMS*szxy*sizeof(float)); // Coefficients for chosen atoms, MAX_ATOMS x sz.x*sz.y
        cudaMalloc( (void **) &B_, na*szxy*sizeof(float)); // Intermediate Beta variable, MAX_ATOMS x sz.x*sz.y
        cudaMalloc( (void **) &means_, szxy*sizeof(float)); // patch means, 1 x sz.x*sz.y
        cudaMalloc( (void **) &ones_, D_rows*sizeof(float)); // ones vector for cuBLAS routines, 1 x D_rows
    
        set_float<<<dim3(D_rows,1,1),dim3(1,1,1)>>>(ones_,1,1.0f);

        unsigned int Lsz0 = (MAX_ATOMS*MAX_ATOMS + MAX_ATOMS)/2;
        unsigned int Lsz;
        unsigned int atoms1; // used later when atoms < MAX_ATOMS
        cudaMalloc( (void **) &L_, Lsz0*szxy*sizeof(float)); // ((MAX_ATOMS*MAX_ATOMS + MAX_ATOMS)/2) x sz.x*sz.y
    
        // if (Lsz0 > 1024) {

        //    mexErrMsgTxt("Maximum number of atoms per patch (44) exceeded.");

        // }
    
        unsigned int *indices_;
        cudaMalloc( (void **) &indices_, MAX_ATOMS*szxy*sizeof(unsigned int)); // sz.x*sz.y x MAX_ATOMS
        // cudaMalloc( (void**) &count_, szxy0*sz.z*sizeof(unsigned int));
        
        // make this float instead so it can also include sparsity level weighting
        float *count_;
        cudaMalloc( (void**) &count_, szxy0*sz.z*sizeof(float));
    
        // Copy passed data
        cudaMemcpy(X_, X, nvol*szxy0*sz.z*sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(D_, D, D_rows*na*sizeof(float), cudaMemcpyHostToDevice);
        
        // zero additive arrays
        cudaMemset(count_, 0, szxy0*sz.z*sizeof(unsigned int));
        cudaMemset(d_, 0, nvol*szxy0*sz.z*sizeof(float));
        
    // Residual tracking
        
        float *eps_, *delta_, *delta_old_;
        cudaMalloc( (void**) &eps_, szxy*sizeof(float));
        cudaMalloc( (void**) &delta_, szxy*sizeof(float));
        cudaMalloc( (void**) &delta_old_, szxy*sizeof(float));
        int padded_atoms1;
        dim3 gsD = dim3(szxy,1,1);

    // Check to make sure we will not run into problems with the limits of 32-bit ints
        
        if (((size_t) na)*((size_t) szxy) > 2e9) {

            mexErrMsgTxt("Specified dimensions may exceed limits of 32-bit integers.");

        }
        
        if (((size_t) nvol)*((size_t) sz.z*szxy0) > 2e9) {

            mexErrMsgTxt("Specified dimensions may exceed limits of 32-bit integers.");

        }
    
    // Create a handle for CUBLAS
        
        cublasHandle_t handle;
        cublasCreate(&handle);
        // cublasStatus_t stat;
        
    // cuBLAS coefficients
        
        float alpha = 1.0f;
        float beta = 0.0f;
        float beta2 = -1.0f;
        float alpha_drows = 1.0f/((float) D_rows);
    
    // Precompute G = D'*D
    
        // C = ? op ( A ) op ( B ) + ? C
        // cublasSgemm(cuBLAS handle, transpose flag A, transpose flag B, m, n, k, *alpha, *A, lda, *B, ldb, *beta, *C, ldc)
        cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, na, na, D_rows, &alpha, D_, D_rows, D_, D_rows, &beta, G_, na);
    
    // Main Loop over z slices to fit within RAM limits
        
    for (int slice = 0; slice < sz.z; slice++) {
        
        if (samp == 8 && slice % 2 == 1) {
            
            continue;
                
        }
        
        // Zero-out re-used data
        
            cudaMemset(delta_old_, 0, szxy*sizeof(float)); // must be reset between slices
            cudaMemset(Gamma_, 0, MAX_ATOMS*szxy*sizeof(float)); // zero values for coefficients not needed vs. error criterion
        
        // Extract patches X => Xp
            
            if (samp == 8) {
                
                extract_patches_8x<<<gsP, bsP>>>(X_, Xp_, offset_map_, slice, sz, nvol, D_rows/nvol); // D_rows/nvol offset, vol offset
                
            }
            else if (samp == 4) {
                
                extract_patches_4x<<<gsP, bsP>>>(X_, Xp_, offset_map_, slice, sz, nvol, D_rows/nvol); // D_rows/nvol offset, vol offset
                
            }
            else {
                
                extract_patches_full<<<gsP, bsP>>>(X_, Xp_, offset_map_, slice, sz, nvol, D_rows/nvol); // D_rows/nvol offset, vol offset
                
            }
                
        // Remove patch means
        
            // compute means with cuBLAS (i.e. the fast way that uses a little more memory)
        
            // cublasSgemv(cublasHandle_t handle, cublasOperation_t trans, int m, int n, const float *alpha, const float *A, int lda,
            // const float *x, int incx, const float *beta, float *y, int incy)
            // y = ? op ( A ) x + ? y
            // mean = (1/D_rows) * Xp_'*(ones) + 0*zeros
            cublasSgemv(handle, CUBLAS_OP_T, D_rows, szxy, &alpha_drows, Xp_, D_rows, ones_, 1, &beta, means_, 1);
        
            // subtract means from each column
            
            // cublasSger(cublasHandle_t handle, int m, int n, const float *alpha, const float *x, int incx, const float *y, int incy, float *A, int lda);
            // A = ? x y T + A
            // Xp_ = -ones_*mean_' + Xp_
            // GPU_vals(Xp_,D_rows*szxy,18);
            cublasSger(handle, D_rows, szxy, &beta2, ones_, 1, means_, 1, Xp_, D_rows);
        
        // compute A00 = D'*Xp
        
            cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, na, szxy, D_rows, &alpha, D_, D_rows, Xp_, D_rows, &beta, A00_, na);
            cudaMemcpy(A0_,A00_,na*szxy*sizeof(float),cudaMemcpyDeviceToDevice);
            
        // compute eps = diag(Xp'*Xp)
            
            // Add one to minimize bank conflicts given power of 2 size?
            init_eps<<<gsEps,bsEps,min(drows_padded,2048)*sizeof(float)+1>>>(Xp_, eps_, D_rows, drows_padded);
        
        // To avoid fragmenting the operations, iterate to a maximum number of atoms
        // Also convenient for preallocating memory.
        // NOTE: This will not likely work out well for dictionary learning.
        // TO DO: Find some way to eval. the convergence criterion per patch / column?
        for (int atoms = 0; atoms < MAX_ATOMS; atoms++) {
            
            // k = argmax(k) abs(a_k) (parallel reduce to abs max index; blocks map to columns of A)
            
                // abs_max_v2<<<gsA2, bsA2, na_padded*sizeof(float)>>>(A0_, indices_+atoms*szxy, na);
                abs_max<<<gsA, bsA, na*sizeof(float)>>>(A0_, indices_+atoms*szxy, na);

            // Cholesky factorization update
                
                atoms1 = atoms+1;
                Lsz = (atoms1*atoms1 + atoms1)/2;

                gsL = dim3(szxy,1,1);
                bsL = dim3(min(Lsz,1024),1,1); // don't exceed threads / block limit

                if (atoms == 0) {

                    set_float<<<dim3(szxy,1,1),dim3(1,1,1)>>>(L_,Lsz0,1.0f);

                }
                else {

                    // Solve for w: L * w = G_I,k
                    // Insert w into L matrices
                    
                    // pass eps and err to quit early if possible
                    update_L<<<gsL, bsL, (Lsz + atoms)*sizeof(float)>>>(L_, G_, indices_, atoms, szxy, na, eps_, err*err, Lsz);

                }
             
            // Gamma := Solve for c: L*L'*c = A00_I
            // TO DO: Use cublasSgelsBatched?
            
                update_Gamma<<<gsL, bsL, (Lsz + 2*(atoms + 1))*sizeof(float)>>>(Gamma_, eps_, L_, indices_, A00_, err*err, szxy, atoms, na, Lsz);
                
            // Sets Beta to zero prior to the update
            // Beta = G_I * gamma_I
            // A0 = A00 - Beta
                
                // pass eps and err to quit early if possible
                update_Beta_v4<<<gsA, bsA, na*sizeof(float)>>>(G_, Gamma_, B_, indices_, A0_, A00_, na, szxy, atoms, eps_, err*err);
            
            // Update delta, delta_old
            // delta = Gamma'_I * Beta_I
            // eps = eps - delta + delta_old
            // delta_old = delta
                
                padded_atoms1 = (1 << (int) ceil(log((float) atoms1)/log(2.0f)));
                update_delta<<<gsD,dim3(padded_atoms1,1,1),padded_atoms1*sizeof(float)>>>(B_, Gamma_, indices_, delta_, na, atoms1);
                
                update_eps<<<dim3((szxy+BLOCK_SZ-1)/BLOCK_SZ,1,1),dim3(BLOCK_SZ,1,1)>>>(eps_, delta_, delta_old_, szxy);
            
        }
        
        // Reconstruct patches from coefficients
        // Sets Xp to zero prior to the update
            
            // update_Xp_v2<<<gsXp, bsXp>>>(Xp_, D_, Gamma_, indices_, D_rows, szxy);
            update_Xp_v3<<<gsXp, bsXp>>>(Xp_, D_, Gamma_, indices_, D_rows, szxy, idx_rngd);
        
        // Add back patch means
            
            cublasSger(handle, D_rows, szxy, &alpha, ones_, 1, means_, 1, Xp_, D_rows);
     
        // return patches Xp => d
            
            // set the first element in gamma equal to the number of non-zero coefficients for patch weighting
            count_gamma<<<dim3(szxy,1,1),dim3(1,1,1)>>>(Gamma_);
            
            if (samp == 8) {
                
                return_patches_8x<<<gsP, bsP>>>(Xp_, d_, count_, Gamma_, offset_map_, slice, sz, nvol, D_rows/nvol);
                
            }
            else if (samp == 4) {
                
                return_patches_4x<<<gsP, bsP>>>(Xp_, d_, count_, Gamma_, offset_map_, slice, sz, nvol, D_rows/nvol);
                
            }
            else {
                
                return_patches_full<<<gsP, bsP>>>(Xp_, d_, count_, Gamma_, offset_map_, slice, sz, nvol, D_rows/nvol);
                
            }
        
    }
                
    normalize<<<gsN, bsN>>>(d_, count_, sz, nvol);
    
    // GPU_vals(d_,szxy*sz.z,11);

    cudaMemcpy(d, d_, nvol*szxy0*sz.z*sizeof(float), cudaMemcpyDeviceToHost);
    
    // Destroy cublas handle
    cublasDestroy(handle);
    
}
