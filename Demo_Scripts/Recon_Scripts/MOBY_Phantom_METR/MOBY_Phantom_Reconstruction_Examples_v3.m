% %     Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % ** See this reference for more details on the simulated MOBY phantom projection data:
    % Clark, D. P., Holbrook, M., Lee, C. L., & Badea, C. T. (2019).
    % Photon-counting cine-cardiac CT in the mouse.
    % PloS one, 14(9), e0218417.
    % https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0218417
    
    % Segars, W. P., Tsui, B. M., Frey, E. C., Johnson, G. A., & Berr, S. S. (2004).
    % Development of a 4-D digital mouse phantom for molecular imaging research.
    % Molecular Imaging & Biology, 6(3), 149-159.
    
    % Long, Y., Fessler, J. A., & Balter, J. M. (2010).
    % 3D forward and back-projection for X-ray CT using separable footprints.
    % IEEE transactions on medical imaging, 29(11), 1839-1850.
    
    % Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
    % Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
    % Physics in Medicine & Biology, 49(11), 2209.
    
    % Van der Vorst, H. A. (1992).
    % Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
    % SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
    
    % Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
    % Cone-beam reprojection using projection-matrices.
    % IEEE transactions on medical imaging, 22(10), 1202-1214.
    
    % Sleijpen, G. L., & Van Gijzen, M. B. (2010).
    % Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
    % SIAM journal on scientific computing, 32(5), 2687-2709.
    % Algorithm 3.1
    
    % Regularization with patch-based singular value thresholding (pSVT)
    % Kim, K., Ye, J. C., Worstell, W., Ouyang, J., Rakvongthai, Y., El Fakhri, G., & Li, Q. (2014).
    % Sparse-view spectral CT reconstruction using spectral patch-based low-rank penalty.
    % IEEE transactions on medical imaging, 34(3), 748-760.
    
    % Regularization with rank-sparse kernel regression (RSKR)
    % Clark, D. P., & Badea, C. T. (2017).
    % Hybrid spectral CT reconstruction.
    % PloS one, 12(7), e0180324.
    
%% If you are attempting to run this script directly, define these variables...
%  otherwise, they are predefined from the "run_demos.m" script.

    % Path to the supplementary data sets
    % dpath_base = '/media/x-ray/blkbeauty4/MCR_Toolkit_Datasets';
    
    % Location where results will be saved
    % outpath_base = '/media/x-ray/blkbeauty4/MCR_Toolkit_Results';
    
    % GPUs to use for computation
    % MATLAB's gpuDevice(idx+1) function or NVIDIA's smi tool (idx) can be use to 
    % determine which index corresponds with which physical GPU.
    % gpu_list = int32([0]);       % Use 1 GPU, the system default
    % gpu_list = int32([0,1]);     % Use 2 GPUs
    % gpu_list = int32([0,1,2,3]); % Use 4 GPUs 
    
%% Analytical and iterative reconstruction examples using the MOBY mouse phantom (3D, 3D + ME, 3D + TR, 3D + ME + TR)
% Simulated MOBY phantom projection data
% Noise and attenuation matched to in vivo cardiac micro-CT data acquired in a mouse (see references)
% Energy thresholds: 25, 34, 40, 55 keV
% Source spectrum: 80 kVp tungsten

%  Data paths - Modify these as needed...

    % Location of sample projection data and physiological monitoring file
    dpath = fullfile(dpath_base,'MOBY_Phantom');
    
    % Location to save intermediate data and reconstruction results
    outpath = fullfile(outpath_base,'MOBY_Phantom');
    
    if ~exist(outpath,'dir')
       
        mkdir(outpath);
        
    end
    
    % Unique name to append to image files saved after reconstruction
    recon_name = 'MOBY_v3';
    
%% Relative paths
    
    % MOBY Phantom: Log-transformed, simulated PCD projections to use for reconstruction
    projection_data = fullfile(dpath,'Yn_v2.nii.gz');

    % Temporal gating
    gating = fullfile(dpath,'18_04_17_100516.csv');
    
    % Reference volume to use for error computations
    X_ref_file = fullfile(dpath,'X_ref2_all_t_all_th.nii.gz');
    
%% Load projection data

    Y = load_nii(projection_data);
    Y = Y.img;
    
    szy = size(Y);
    nu = szy(1); % detector columns
    nv = szy(2); % detector rows
    np = szy(3); % number of projections
    ne = szy(4); % number of PCD energy bins
    
    % number of cardiac phases to reconstruct
    nt = 10;
    
%% Reconstruction geometry
    
    % reconstructed volume size (voxels)
    nx = 400; % Should be divisible by 8
    ny = 400; % Should be divisible by 8
    nz = 160;
    
    % sampling angles (degrees)
    ap     = (360*3 - 10)/np;
    angles = single(0:ap:(360*3-10-ap));
    rotdir = -1; % (1) nominal rotation direction, (-1) flip the rotation direction
    
    % geometric magnification
    lsd = 831.1812; % source-detector distance (mm)
    lso = 679.8042; % source-object distance (mm)
    magnification = lsd/lso;
    
    % detector pixel size (mm)
    du = 0.15;
    dv = 0.15;
    
    % intersection of the central ray with the detector (detector elements)
    uc = 272.7158; % detector column 
    vc = 128.7056; % detector row
    
    % detector rotation parameters (radians)
    eta = -0.001679195;
    sig = -0.0001778029;
    phi = -8.115117e-05;
    
    % reconstructed voxel size (mm)
    dx = du/magnification;
    dy = du/magnification;
    dz = du/magnification;
    
    % z slice to preview during iterative reconstruction
    slice_num = round(nz/2);

    % angular offset for coarse alignment of each chain (radians)
    ao = 0;

    % Helical geometry: pitch
    rotation            = 3*360-10; % degrees
    travel              = 12.5; % mm
    rotations           = rotation / 360;
    travel_per_rotation = travel / rotations;
    collimation         = ( dv / magnification ) * nv; % mm
    pitch               = travel_per_rotation / collimation;
    
    % fan angle (assuming the source is well centered on the detector)
    fan_angle = 2 * atan2d( ( nu * du / 2 ), lsd );  % degrees
    
    % Helical geometry: z travel
    mm_per_proj = travel/np; 
    zo          = mm_per_proj*(0:(np-1));  % z position of the central ray for each projection (mm)
    zo_         = round(np/2)*mm_per_proj; % z position of the center of the reconstructed volume (mm)
    
    geo_params = double([lsd lso uc vc eta sig phi]);
    
%% Create a geometry file for projection matrix calculations

    offset_ang = 0;
    nsets      = 1;
    nprojset   = np;
    
    params = [geo_params zo_ ao];
    
    geo_all = fullfile(outpath,'geometry_file_all_projections.geo');

    % Projection weights (all ones when reconstructing using all projections)
    weights = ones(1,np);
    
    % Make geometry file for all projection
    geo_lines = make_geo_file_helical( geo_all, params, du, dv, rotdir, np, angles, offset_ang, nsets, nprojset, weights, zo );
    
%% Set up reconstruction operators for helical, cone-beam reconstruction

    % Flags

        FBP = 1;                    % analytical reconstruction
        simple_back_projection = 0; % simple backprojection
        implicit_mask = 1;          % (0) - do not use implicit reconstruction mask; (1) use implicit cylindrical reconstruction mask
        explicit_mask = 0;          % (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask (box); (2) use explicit reconstruction mask (arbitrary)
        use_affine = 0;             % (0) - affine transform is not used (even if one is provided); (1) - affine transform is used

        % Reference affine transform
        % Dummy parameters for no affine, minimal computation
        % NOTE: When the affine causes a volume change (i.e. it is non-rigid), the resultant FBP will not be properly normalized.
        Rt_aff = single([1 0 0 0 1 0 0 0 1 0 0 0]);
        
        scale = 1.0;

    % Sample reconstruction masks

        if (explicit_mask == 0) % no explicit mask

            rmask = uint16(0);

        elseif (explicit_mask == 1)  % used z range explicit mask (good for boxes)

            % Test explicit mask
            % Reproduces implicit mask
            rmask = zeros(nx,ny,'single');
            [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            rmask = repmat(rmask,[1 1 nz]);        
            % rmask = rmask(:);
            clear x1 x2 x3;

            rmask = mask_zrange( rmask );

            rmask = uint16(rmask(:));

        else % use for arbitrary shapes... slower

            % Test explicit mask
            % Reproduces implicit mask
            rmask = zeros(nx,ny,'single');
            [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            rmask = repmat(rmask,[1 1 nz]);
            % rmask = rmask(:);
            clear x1 x2 x3;

            zmask = mask_zrange( rmask );

            rmask = cat(3,zmask,rmask);

            rmask = uint16(rmask(:));

        end
    
    % Parameters for helical reconstruction
    
        cy_detector      = 0; % (0) - detector is flat; (1) detector is cylindrical; (2) cylindrical, cone-parallel rebinning; (3) flat panel, cone-parallel rebinning; (4) array of parallel beams (synchrotron geometry)
        cy_detector_WFBP = 0;
        
        use_norm_vol      = 0; % (0) - do not allocate / use count normalization volume, (1) allocate / use count normalization volume during backprojection
        use_norm_vol_WFBP = 1;
        
        % fan angle increment for cylindrical detectors (radians)
        db = 0;

        % trade-off between z resolution and dose efficiency when performing WFBP reconstruction
        % uses a cosine^2 window
        % full scan (minimum necessary after rebinning)
        % limit_angular_range = collimation * pitch * 360/360 / 2; % use 360 degrees of data
        
        short_scan_range = collimation * pitch * ( 180 + fan_angle ) / 360;
        % limit_angular_range = short_scan_range / 2;              % use 180+ degrees of data
        
        % control the use of divergent rays with cos^2 weighting (no computation benefit)
        limit_angular_range = short_scan_range / collimation;
    
    % Parameter vectors for reconstruction operators
    % These parameters are passed to the GPU and must have the correct precision.

        % reprojection, unfiltered backprojection
        int_params    = int32([nu nv np nx ny nz simple_back_projection use_affine implicit_mask explicit_mask cy_detector      use_norm_vol      ]);
        
        % filtered backprojection
        int_params_f  = int32([nu nv np nx ny nz FBP                    use_affine implicit_mask explicit_mask cy_detector_WFBP use_norm_vol_WFBP ]);
        
        % reprojection, unfiltered backprojection, filtered backprojection
        double_params = double([du dv dx dy dz zo_ ao lsd lso uc vc eta sig phi scale db limit_angular_range]);
        
    % Make a Ram-Lak filter for cone-beam backprojection
        
        lenu = 2^(ceil(log(nu-1.0)/log(2.0))); % next power of 2
        filt_name = fullfile(outpath,['Ram-Lak_' num2str(lenu) '.txt']);
        make_FBP_filter( filt_name, du, lenu, 'ram-lak', 1.0 );

%% Temporal gating...

    % The MOBY phantom projections correspond to cardiac phases computed from a real mouse ECG signal...

    % channels
    % 1: Time
    % 2: Angle
    % 3: ECG
    % 4: Respiration
    % 5: Trigger 1 (this has to do with the source, not the detector)
    % 6: Trigger 2
    % 7: Det Out 1 (use the lagging edge to find when projections were taken)
    % 8: Det Out 2

        signals = readmatrix(gating);

        step = fliplr([repmat(2.5,[1 6]) repmat(-2.5,[1 6])]./(3*5));
    
    % Trigger - gating uses 1 trigger w/ another projection every 10 ms (5, 2 ms time steps)
    
        trigger0 = find(signals(:,5) > 0,1,'first');
      
        triggers = trigger0:5:(trigger0+5*(np-1));
        
    % Cardiac
    
        ECG = signals(:,3);
        ECG = ECG - min(ECG(:));
        norm_e = medfilt1(ECG,11);
        
        ECG = ECG - norm_e; 
        
        ECG2 = convn(ECG,ECG(end:-1:1),'same');
        
        len = length(ECG2);
        corr = ECG2(round(len/2)-118:round(len/2)+118);
        
        corr = corr - mean(corr);
        
        corr = corr./sum(corr);
        
        ECG3 = convn(convn(ECG,corr,'same'),corr,'same');
        
        ECG3 = ECG3 .* max(ECG) ./ max(ECG3);
        
        ECG3 = ECG3 .* norm(ECG) / norm(ECG3);
        
        [~,R_locs1] = findpeaks(ECG3,'MinPeakDistance',40,'MinPeakHeight',0.3*max(ECG3)); % 100);
        
        % figure(12); plot(ECG3,'k'); hold on; scatter(R_locs1,ECG3(R_locs1),'r');
        % figure(12); plot(signals(:,3),'k'); hold on; scatter(R_locs1,signals(R_locs1,3),'r');
        
    % Cardiac phases
    
        time1 = signals(:,1); % offset to left-edge triggers (5 ms)
        tRs = time1(R_locs1);
        phases = zeros(1,length(triggers));

        for i = 1:length(triggers)

            % Cardiac
            start_R = find(time1(triggers(i)) >= tRs,1,'last');
            end_R = find(time1(triggers(i)) < tRs,1,'first');
            phases(i) = (time1(triggers(i)) - tRs(start_R))/(tRs(end_R)-tRs(start_R));

        end
        
        cphases = 100*phases;
        
    % Temporal weights - create projection weights corresponding to each cardiac phase to be reconstructed

        p = 10; % phases

        W_ = Gaussian_weights_trunc_v2( p, 100, 3 );

        Wt = zeros(np,p);

        for t = 1:p

            Wt(:,t) = W_(round(cphases)+1,t);

        end

        Wt = bsxfun(@rdivide,Wt,sum(Wt,1));
        
%% Initialize reconstruction operators
% Be sure to "clear DD_init" before clearing recon. allocations or resetting the GPU(s)!
% Note: Any outstanding recon allocations will be properly deallocated when Matlab is closed.

    clear DD_init;
    
    tic;

    recon_alloc_f = DD_init(int_params_f, double_params, filt_name, geo_all, Rt_aff, rmask, gpu_list);
    
    toc;
    
    W0  = ones(1,np);
    R   = @(x,w) DD_project(x,recon_alloc,w);
    Rtf = @(y,w) DD_backproject(y,recon_alloc_f,int32(1),w);
        
%% 1) Perform analytical reconstruction using all projections (time averaged, energy threshold 1)

    fprintf(['\n\nReconstructing the time average at energy threshold 1...\n']);

    tic;
    
    X_WFBP = Rtf( Y(:,:,:,1), W0 );
    
    toc;
    
    X_WFBP = reshape(X_WFBP,[nx ny nz]);
    
    save_nii(make_nii(X_WFBP),fullfile(outpath,['X_' recon_name '_avg_th_1.nii']));


%% 2) Perform analytical reconstruction for each energy threshold (time averaged)
% Since PCD data is co-registered, we can use the same geometry for each energy threshold.

    fprintf(['\n\nReconstructing the time average at each energy threshold (ME)...\n']);

    X_WFBP = zeros([nx*ny*nz ne],'single');
    
    tic;

    for s = 1:ne
        
        fprintf(['\nReconstructing energy threshold ' num2str(s) '...\n\n']);

        X_WFBP(:,s) = Rtf( Y(:,:,:,s), W0 );
        
    end
    
    toc;
    
    X_WFBP = reshape(X_WFBP,[nx ny nz ne]);
    
    save_nii(make_nii(X_WFBP),fullfile(outpath,['X_' recon_name '_t_avg_all_th.nii']));

%% 3) Perform analytical reconstruction for each time point (energy threshold 1)

    fprintf(['\n\nReconstructing 10 cardiac phases (TR) at energy threshold 1...\n']);

    X_WFBP = zeros([nx*ny*nz nt],'single');
    
    tic;

    for t = 1:nt
        
        fprintf(['\nReconstructing cardiac phase ' num2str(t) '...\n']);

        X_WFBP(:,t) = Rtf( Y(:,:,:,1), Wt(:,t) );
        
    end
    
    toc;
    
    X_WFBP = reshape(X_WFBP,[nx ny nz nt]);
    
    save_nii(make_nii(X_WFBP),fullfile(outpath,['X_' recon_name '_all_t_th_1.nii']));

%% 4) Perform analytical reconstruction for each time point and energy threshold
% Since PCD data is co-registered, we can use the same geometry for each energy threshold.

    fprintf(['\n\nReconstructing 10 cardiac phases (TR) at each of 4 energy thresholds (ME)...\n']);

    X_WFBP = zeros([nx*ny*nz nt ne],'single');
    
    tic;
    
    for s = 1:ne

        for t = 1:nt

            fprintf(['\nReconstructing cardiac phase ' num2str(t) ', threshold ' num2str(s) '...\n']);

            X_WFBP(:,t,s) = Rtf( Y(:,:,:,s), Wt(:,t) );

        end
    
    end
    
    toc;
    
    X_WFBP = reshape(X_WFBP,[nx ny nz nt ne]);
    
    save_nii(make_nii(X_WFBP),fullfile(outpath,['X_' recon_name '_all_t_all_th.nii']));

%% Set up parameter file for iterative reconstruction

    % Reference attenuation measurements for water at each energy threhsold
    % units: 1/[voxel length]
    attn_water = [0.002753553 0.002654050 0.002528615 0.002425031];

% Input / output

    rt.run_name       = recon_name;       % identifier string for specific results

    rt.outpath        = outpath;          % location where results are saved
    
    rt.computeRMSE    = 1;                % (0) do not comptue RMSE (i.e. for in vivo data), (1) compute RMSE each time the reconstruction is updated (simulations)
    rt.X_ref          = X_ref_file;       % reference volume string; noise free reference data

    rt.do_save        = 0;                % (0) return final results only, (1) save final results only, (2) save intermediate results and final results

    rt.Y              =  projection_data; % projection data string, nifti file

    rt.breg_iter      = 4;                % total number of times to update the reconstructed results (initialization + regularized iterations)
    
    rt.bicgstabl_iter = 6;                % data fidelity update iterations (bicgstabl with l = 2)

    rt.preview_slice  = slice_num;        % z slice used for previewing reconstructed results
    rt.display_range  = [0 0.8];          % minimum and maximum fractions of the dynamic range of CT attenuation values for consistent display purposes

% Projection / Backprojection

    rt.int_params    = int32(int_params);     % int32 parameters
    rt.int_params_f  = int32(int_params_f);   % int32 parameters
    rt.double_params = double(double_params); % double precision parameters used for geometric calculations
    rt.filt_name     = filt_name;             % FBP filter
    rt.geo_all       = geo_all;               % geometry file
    rt.Rt_aff        = single(Rt_aff);        % affine transform
    rt.rmask         = uint16(rmask);         % volumetric masking
    rt.gpu_list      = gpu_list;              % GPU(s) to use for reconstruction ( Matlab getDevice(#) indices - 1), int32
    
    rt.Wt            = Wt;                    % temporal weights
    rt.geo_lines     = geo_lines;             % We need the geometry file for all projections to allow us to sub-select rows for temporal reconstruction
    % rt.snum          = 1;                   % number of subsets to use for reconstruction
    
% Sizes

    rt.p   = nt;         % number of temporal phases
    rt.e   = ne;         % number of spectral samples
    rt.sz  = [nx,ny,nz]; % full volume size
    rt.szy = [nu,nv,np]; % projection data size

% Regularization

    % regularization strength
    rt.mu = 0.005;
    
    % regularization, RSKR (energy)
    rt.stride      = 3;            % subsampling to deal with correlated noise
    rt.w           = int32(6);     % filtration kernel radius
    rt.w2          = single(0.8);  % kernel resampling bandwidth
    rt.lambda0     = single(1.5);  % regularization strength
    rt.attn_water  = attn_water;   % attempt to equalize the noise level between spectral samples, following water normalization
    rt.g           = 0.5;          % exponential regularization scaling with singular value ratio ( lambda0*(E(1,1)./diag(E)').^(g) )
    
    % regularization, pSVT (time)
    rt.apply_pSVT      = 1;   % (0) do not apply pSVT; (1) Apply pSVT when there is a time dimension
    rt.w_pSVT          = 3;   % kernel diameter, pSVT
    rt.nu_pSVT         = 0.7; % non-convex singular value exponent
    rt.multiplier_pSVT = 0.3; % multiplier for singular value threshold
    
    % directory of the version of BF to use for RSKR
    rt.BF_path = fileparts(which('jointBF4D'));
    
% Save for later use or reuse

    save(fullfile(outpath,'recon_template.mat'),'rt');
    

%% 1) Perform iterative reconstruction using all projections (time averaged, energy threshold 1)

    fprintf(['\n\nPerforming iterative reconstruction of the time average at energy threshold 1...\n']);

    % NOTE: Because of the way memory management works in MATLAB, projections are cleared
    %       here to save RAM and reloaded inside of the iterative reconstruction function.
    clear Y;

    clear DD_init;

    load(fullfile(outpath,'recon_template.mat'));
    
    rt.p = 1;
    rt.e = 1;
    
    [ X, RMSEs1 ] = Recon_5D_Data_v3( rt );
    
    save_nii(make_nii(reshape(X,[rt.sz rt.p rt.e]),rt.double_params(3:5)),fullfile(outpath,['X_' recon_name '_iter_time_avg_energy_1.nii']));

%% 2) Perform iterative reconstruction for each energy threshold (time averaged)

    fprintf(['\n\nPerforming iterative reconstruction of the time average at each energy threshold (ME)...\n']);

    clear Y;

    clear DD_init;

    load(fullfile(outpath,'recon_template.mat'));
    
    rt.p = 1;
    
    [ X, RMSEs2 ] = Recon_5D_Data_v3( rt );
    
    save_nii(make_nii(reshape(X,[rt.sz rt.p rt.e]),rt.double_params(3:5)),fullfile(outpath,['X_' recon_name '_iter_time_avg_energy_all.nii']));

%% 3) Perform iterative reconstruction for each time point (energy threshold 1)

    clear Y;

    clear DD_init;

    load(fullfile(outpath,'recon_template.mat'));
    
    rt.e = 1;
    
    [ X, RMSEs3 ] = Recon_5D_Data_v3( rt );
    
    save_nii(make_nii(reshape(X,[rt.sz rt.p rt.e]),rt.double_params(3:5)),fullfile(outpath,['X_' recon_name '_iter_time_all_energy_1.nii']));

%% 4) Perform iterative reconstruction for each time point and energy threshold

    clear Y;

    clear DD_init;
    
    load(fullfile(outpath,'recon_template.mat'));

    [ X, RMSEs4 ] = Recon_5D_Data_v3( rt );
    
    save_nii(make_nii(reshape(X,[rt.sz rt.p rt.e]),rt.double_params(3:5)),fullfile(outpath,['X_' recon_name '_iter_time_all_energy_all.nii']));




