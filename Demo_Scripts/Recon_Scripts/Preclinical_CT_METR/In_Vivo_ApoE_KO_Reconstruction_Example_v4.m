% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References

    % Clark, D. P., Holbrook, M., Lee, C. L., & Badea, C. T. (2019).
    % Photon-counting cine-cardiac CT in the mouse.
    % PloS one, 14(9), e0218417.
    % https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0218417
    
    % Long, Y., Fessler, J. A., & Balter, J. M. (2010).
    % 3D forward and back-projection for X-ray CT using separable footprints.
    % IEEE transactions on medical imaging, 29(11), 1839-1850.
    
    % Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
    % Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
    % Physics in Medicine & Biology, 49(11), 2209.
    
    % Van der Vorst, H. A. (1992).
    % Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
    % SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
    
    % Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
    % Cone-beam reprojection using projection-matrices.
    % IEEE transactions on medical imaging, 22(10), 1202-1214.
    
    % Sleijpen, G. L., & Van Gijzen, M. B. (2010).
    % Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
    % SIAM journal on scientific computing, 32(5), 2687-2709.
    % Algorithm 3.1
    
    % Regularization with patch-based singular value thresholding (pSVT)
    % Kim, K., Ye, J. C., Worstell, W., Ouyang, J., Rakvongthai, Y., El Fakhri, G., & Li, Q. (2014).
    % Sparse-view spectral CT reconstruction using spectral patch-based low-rank penalty.
    % IEEE transactions on medical imaging, 34(3), 748-760.
    
    % Regularization with rank-sparse kernel regression (RSKR)
    % Clark, D. P., & Badea, C. T. (2017).
    % Hybrid spectral CT reconstruction.
    % PloS one, 12(7), e0180324.
    
    % ApoE knockout mouse model
    % Zhang, S. H., Reddick, R. L., Piedrahita, J. A., & Maeda, N. (1992).
    % Spontaneous hypercholesterolemia and arterial lesions in mice lacking apolipoprotein E.
    % Science, 258(5081), 468-471.
    
    % Nelder-Mead optimization algorithm.
    % Lagarias, J. C., Reeds, J. A., Wright, M. H., & Wright, P. E. (1998). 
    % Convergence properties of the Nelder--Mead simplex method in low dimensions.
    % SIAM Journal on optimization, 9(1), 112-147.
    
%% Import data from original tif files
%  repackage to nifiti files for sharing
%  ignore this code block

%     bpath = '/media/justify/Paper_Data/2018_Med_Phys_Letter_PLOS_ONE_PCD_Cardiac/CT_Meeting_2018/2018_04_17_CardiacRT';
% 
%     data = fullfile(bpath,'10-04-59_180416-5/series_id_00001_threshold_%i_image_id_%06i.tif');
%     air  = fullfile(bpath,'12-51-24_Airraws/series_id_00001_threshold_%i_image_id_%06i.tif');
%     
%     outpath = '/media/justify/DPC/MCR_Toolkit_Reconstructions/Mouse_Cardiac_ApoE_KO_180417_5_v2/';
%     
%     nu   = 515; % projection dimensions
%     nv   = 257;
%     np   = 9000;
%     
%     nadp = 200; % number of air raws per threshold
%     
%     du = 0.150; % projection pixel size in mm
%     dv = 0.150;
%     
%     % read projection data
%     
%         warning off;
% 
%         Y = zeros(nu,nv,np,4,'single');
%         
%         for th = 1:4
%         
%             for i = 1:np
%                 
%                 Y(:,:,i,th) = imread( sprintf(data, th-1, i) )';
% 
%             end
%             
%             disp(['Done with Y, th ' num2str(th)]);
%         
%         end
%         
%         % resave projections in nifti format, uint16
%         save_nii(make_nii(uint16(Y),[du,dv,1]),fullfile(outpath,'Y.nii.gz'));
% 
%     % read air raw data
% 
%         I0 = zeros(nu, nv, nadp, 4, 'single');
% 
%         for th = 1:4
%         
%             for i = 1:nadp
%                 
%                 I0(:,:,i,th) = imread( sprintf(air, th-1, i) )';
% 
%             end
%             
%             disp(['Done with air, th ' num2str(th)]);
%         
%         end
%         
%         % resave projections in nifti format, uint16
%         save_nii(make_nii(uint16(I0),[du,dv,1]),fullfile(outpath,'I0.nii.gz'));
%         
%         warning on;

%% If you are attempting to run this script directly, define these variables...
%  otherwise, they are predefined from the "run_demos.m" script.

    % Path to the supplementary data sets
    % dpath_base = '/media/x-ray/blkbeauty4/MCR_Toolkit_Datasets';
    
    % Location where results will be saved
    % outpath_base = '/media/x-ray/blkbeauty4/MCR_Toolkit_Results';
    
    % GPUs to use for computation
    % MATLAB's gpuDevice(idx+1) function or NVIDIA's smi tool (idx) can be use to 
    % determine which index corresponds with which physical GPU.
    % gpu_list = int32([0]);       % Use 1 GPU, the system default
    % gpu_list = int32([0,1]);     % Use 2 GPUs
    % gpu_list = int32([0,1,2,3]); % Use 4 GPUs 

%% 1) In vivo mouse data, ApoE KO model
% METR iterative reconstruction example
% Energy thresholds: 25, 34, 40, 55 keV
% Source spectrum: 80 kVp tungsten

    % Results path
    outpath = fullfile(outpath_base,'Mouse_Cardiac_PCCT');
    
    % Data set path
    dpath = fullfile(dpath_base,'Mouse_Cardiac_ApoE_KO_180417_5');
    
    if ~exist(outpath,'dir')
       
        mkdir(outpath);
        
    end
    
    % Unique name for results
    recon_name = '180416-5_v2';
    
    % Relative paths
        
    % Raw projections, 4 energy thresholds
    data    = fullfile(dpath,'Y.nii.gz');            
    
    % Air raws, 4 energy thresholds
    air     = fullfile(dpath,'I0.nii.gz');
    
    % Physiological gating signals
    gating  = fullfile(dpath,'18_04_17_100516.csv');
    
    % Resave projection data after log normalization, gap interpolation, and ring artifact correction
    % File size: ~20 GB
    projection_data = fullfile(outpath,'Yc.nii');
    
%% 2) Preprocess projections
% air normalization, log transform, gain correction, gap interpolation, ring correction

    if ~isfile( projection_data )
        
    % Projection pre-processing: air normalization, log transform, exposure correction

        % Projections to exposure correct
        % simple rescaling to reduce underexposure artifacts present in the first few projections of each acquisition
        fix_proj = 1:25;
        
        enforce_non_negativity = 0; % (0) don't enforce non-negativity; (1) negative line integrals are set to zero

        Y = transform_projections_v1( data, air, fix_proj, enforce_non_negativity );
        
        % save_nii(make_nii(Y),fullfile(outpath,'Y_log.nii'));
        
    % Spectral ring artifact correction
    
        % Rationale: Defects in PCDs can yield gain artifacts in reconstructed images (ring artifacts).
        %            These defects can vary slowly over time and tend to be correlated across energy bins.
        %            Apply the SVD accross PCD projection energies to eliminate redundancy and to yield zero 
        %            mean in lower order components.
        %            Use RPCA to factor slowly varying gain (ring) artifacts from more rapidly varying sinogram data.
    
        % Approach:
        % (1) SVD decomposition of projections accross the energy dimension
        % (2) Remove rings from each component using RPCA
        %     (the first component has low spatial frequencies making it harder to deal
        %      with; the lower order components may be dominated by noise,
        %      depending on the exposure per projection, making RPCA ill-conditioned)
        % (3) Reverse the SVD decomposition of projections accross the energy dimension
    
        % RPCA factorization is more effective with helical projection data than with circular projections.
        % Works best on data where negative values have not been truncated to zero (?)
        % Computation time depends on how fast RPCA converges

        sinogram_rank     = 3; % Number of degrees of freedom for describing gain variations over the projection acquisition time
        summarize_results = 1; % (0) no display; (1) display a quality assurance image after ring reduction 
        return_LP         = 1; % (0) do nothing; (1) reintroduce low spatial frequencies into the first component to reduce errors
        
        % number of spectral projection components to dering
        % set to 1 here because the other 3 components are dominated by noise
        % due to the short exposures used for cardiac CT in the mouse (10 ms)
        limit_rank = 1;
        
        % Elapsed time is 5725.287087 seconds.
        Y = spectral_ring_correction( Y, sinogram_rank, summarize_results, return_LP, limit_rank );
        
        % Enforce non-negativity on deringed line integrals.
        Y(Y < 0) = 0;
        
        save_nii(make_nii(Y),projection_data);
        
    else
       
        Y = load_nii(projection_data);
        Y = Y.img;
        
    end
    
    
%% 3) Specify data acquisition parameters

    sdd = 831.1812; % source-detector distance (mm)
    sod = 679.8042; % source-object distance (mm)
    
    mag = sdd/sod; % geometric magnification

    np     = 9000; % projection number
    ap     = (360*3 - 10)/np; % angular increment (degrees)
    angles = single(0:ap:(360*3-10-ap)); % projection angles
    rotdir = -1;

    % geometry

    % Detector pixel size (mm)
    du = 0.15;
    dv = 0.15;
    
    % Detector pixel count
    nu = 515;
    nv = 257;
    
    % Reconstructed voxel size
    dx = du/mag;
    dy = du/mag;
    dz = du/mag;
    
    % Reconstructed volume dimensions
    nx = 360;
    ny = 360;
    nz = 256;
    
    e = 4;  % number of energy thresholds
    p = 10; % number of time points to reconstruct

    % preview slice for iterative reconstruction
    slice_num = round(nz/2);

    % Reconstruct data with an angular offset (radians)
    ao = 0;

    % pitch
    rotation            = 3*360-10; % degrees
    rotations           = rotation / 360;
    travel              = 12.5; % mm
    travel_per_rotation = travel / rotations;
    detector            = 40;   % detector height, mm
    collimation         = detector / mag; % ~z coverage of the object, mm
    pitch               = travel_per_rotation / collimation;
    
    % fan angle (assuming the source is well centered on the detector)
    fan_angle = 2 * atan2d( ( nu * du / 2 ), sdd );  % degrees
    
    % z offset
    mm_per_proj = travel/np;
    zo = mm_per_proj*(0:(np-1));
    zo_ = round(np/2)*mm_per_proj;
    
%% 4) Geometric parameters: initial guess or previously optimized parameters

    offset_ang = 0;
    nsets      = 1;
    nprojset   = np;
    geo_all    = fullfile(outpath,'geo_all.geo');
    
    if isfile( fullfile(outpath,'geo_params.mat') )
        
        load(fullfile(outpath,'geo_params.mat'));
        
        params = [optim_params zo_ ao];
        
    else   

        % [sdd sod uc vc eta sig phi]
        params = [sdd sod 270 125 0.0 0.0 0.0 zo_ ao];
    
    end
    
    % Make a geometry file for all projections.
    % Save the lines of the geometry file (geo_lines) to make it easier to reparse the geometry file for temporal reconstruction.
    % For PCCT, the geometry is shared across all energy thresholds on the same detector.
    weights   = ones(1,np);
    geo_lines = make_geo_file_helical( geo_all, params, du, dv, rotdir, np, angles, offset_ang, nsets, nprojset, weights, zo );
    
%% 5) Reconstruction parameters
    
    % Additional flags
    
        FBP = 1;                    % (1) FBP, (0) simple backprojection
        simple_back_projection = 0;
        implicit_mask = 1;          % (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
        explicit_mask = 0;          % (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask; (2) use volumetric reconstruction mask
        use_affine = 0;             % (0) - affine transform is not used (even if one is provided); (1) - affine transform is used
                
        scale = 1; % Backprojection scaling

        % control the use of divergent rays (trade off between dose efficiency, computation time, and z spatial resolution)
        % NOTE: In pratice, it would be better to collimate the beam physically rather than digitally.
        % limit_multiplier    = 8; % 4;
        % limit_angular_range = limit_multiplier * detector * pitch * 360 / 360 / 2;
        
        % control the use of divergent rays with cos^2 weighting (no computation benefit)
        short_scan_range    = collimation * pitch * ( 180 + fan_angle ) / 360;
        limit_angular_range = short_scan_range / collimation;
        
        % (0) - do not allocate / use count normalization volume, (1) allocate / use count normalization volume during backprojection
        use_norm_vol        = 0; 
        use_norm_vol_WFBP   = 1;

    % Dummy parameters for no affine, minimal computation
    
        % NOTE: When the affine causes a volume change (i.e. it is non-rigid), the resultant FBP will not be properly normalized.
        %       Also, inversion of the z axis is not supported.
        Rt_aff = single([1 0 0 0 1 0 0 0 1 0 0 0]);

    % Explicit mask - image domain reconstruction mask
    
        if (explicit_mask == 0) % no explicit mask

            rmask = uint16(0);

        elseif (explicit_mask == 1)  % used z range explicit mask (good for boxes)

            % Test explicit mask
            % Reproduces implicit mask
            rmask = zeros(nx,ny,'single');
            % [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            [x1, x2] = ndgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            rmask = repmat(rmask,[1 1 nz]);

            % rmask2 = zeros(nx,ny,nz,'single');
            % rmask2(:,:,385:end-384) = repmat(rmask,[1 1 256]);
            % rmask = rmask2;
            % clear rmask2;

            % rmask = rmask(:);
            clear x1 x2 x3;

            rmask = mask_zrange( rmask );

            rmask = uint16(rmask(:));

        else % use for arbitrary shapes... slower

            % Test explicit mask
            % Reproduces implicit mask
            rmask = zeros(nx,ny,'single');
            % [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            [x1, x2] = ndgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
            rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
            rmask = repmat(rmask,[1 1 nz]);
            % rmask = rmask(:);
            clear x1 x2 x3;

            zmask = mask_zrange( rmask );

            rmask = cat(3,zmask,rmask);

            rmask = uint16(rmask(:));

        end
        
    % Detector type
    
        % (0) - detector is flat; (1) detector is cylindrical; (2) cylindrical, cone-parallel rebinning; (3) flat panel, cone-parallel rebinning; (4) array of parallel beams (synchrotron geometry)
        cy_detector =  0;
        
        db          = -1; % fan angle increment (radians) (only used for cylindrical detectors)

    % Parameter vectors

        % Required parameters
        int_params    = int32([nu nv np nx ny nz simple_back_projection use_affine]);
        int_params_f  = int32([nu nv np nx ny nz FBP                    use_affine]);
        double_params = cat(2,[du dv dx dy dz zo_ ao],params(1:7));
        
        % Add optional parameters
        int_params    = cat(2, int_params   , int32([implicit_mask explicit_mask cy_detector use_norm_vol     ]));
        int_params_f  = cat(2, int_params_f , int32([implicit_mask explicit_mask cy_detector use_norm_vol_WFBP]));
        double_params = cat(2, double_params, [scale db limit_angular_range]);
        
    % Make a Ram-Lak filter for cone-beam backprojection
        
        lenu = 2^(ceil(log(nu-1.0)/log(2.0))); % next power of 2
        filt_name = fullfile(outpath,['Ram-Lak_' num2str(lenu) '.txt']);
        make_FBP_filter( filt_name, du, lenu, 'ram-lak', 1.0 );
    
%% 6) Reconstruction operators

    clear DD_init;

    tic;
    recon_alloc = DD_init(int_params_f(:), double_params(:), filt_name, geo_all, Rt_aff(:), rmask, gpu_list);
    toc;
    
    W0  = ones(1,np);
    Rtf = @(x,w) DD_backproject(x,recon_alloc,int32(1),w);
    
%% 7) Assess geometry with analytical reconstruction (time averaged)

    X = zeros(nx*ny*nz,e,'single');
    Y = reshape(Y,[nu*nv*np e]);

    for s = 1:e
        
        tic;
        
        X(:,s) = Rtf(Y(:,s),W0);
        
        toc;
        
    end
    
    save_nii(make_nii(reshape(X,[nx ny nz e])),fullfile(outpath,'X_WFBP.nii'));
    
%% 8) Refine the geometry if needed
% Steps:
%   a) Create a geometry file using candidate parameters, params
%   b) Perform reconstruction with WFBP
%   c) Reproject the reconstruction
%   d) Compare the reprojections with the original projections using mutual information (MI) 
%   e) Update the geometry parameters to improve the MI using the Nelder-Mead optimization algorithm
% NOTE: Absolute references are needed to calibrate the source-detector and source-object distances. They cannot be optimized with this method.

    if ~isfile( fullfile(outpath,'geo_params.mat') )

        Y = reshape(Y,[nu*nv*np e]);

        % Clear reconstruction allocations, since we need to declare a new one each time we change the geometry.
        clear DD_init;

        Y_in = Y(:,1); % all thresholds share the same geometry for PCD data, so use the projections with the lowest noise level

        geo_temp = fullfile(outpath,'temp.geo');
        a_usamp  = 4; % projection undersampling for speed
        disp_rng = [0 0.8];
        szy      = [nu nv np];
        
        % (0) Text output only
        % (1) Dispaly CT images and reprojection residuals
        display_results = 1;

        % [sdd sod uc vc eta sig phi]
        initial_params = params(1:7);
        deltas         = [0 0 5 5 0.001 0.001 0.001]; % generous guess as to how close the optimal solution is to the initial guess

        % Reset persistent variables if they already exist
        clear geo_calibration_w_MI;

        start_t = tic;
        optim_params = geo_calibration_w_MI( Y_in(:), filt_name, szy, int_params, double_params, initial_params, geo_temp, angles, rotdir, deltas, a_usamp, disp_rng, zo, zo_, gpu_list, display_results );
        toc(start_t);
        
        clear Y_in;
        
        save(fullfile(outpath,'geo_params.mat'),'optim_params');
        
    % Updates to use the new geometry parameters without having to restart the script from block (4)
    
        params    = [optim_params zo_ ao];
        geo_lines = make_geo_file_helical( geo_all, params, du, dv, rotdir, np, angles, offset_ang, nsets, nprojset, weights, zo );
        
        % Parameter vectors
        double_params = cat(2,[du dv dx dy dz zo_ ao],params(1:7));
        double_params = cat(2, double_params, [scale db limit_angular_range]);
        
        % Make sure we don't try to use out-dated reconstruction operators
        clear Rtf DD_init;
     
    end
    
%% 9) Cardiac gating

    % channels
    % 1: Time
    % 2: Angle
    % 3: ECG
    % 4: Respiration
    % 5: Trigger 1 (this has to do with the source, not the detector)
    % 6: Trigger 2
    % 7: Det Out 1 (use the lagging edge to find when projections were taken)
    % 8: Det Out 2

        signals = readmatrix(gating);

        step = fliplr([repmat(2.5,[1 6]) repmat(-2.5,[1 6])]./(3*5));
    
    % Trigger - gating uses 1 trigger w/ another projection every 10 ms (5, 2 ms time steps)
    
        trigger0 = find(signals(:,5) > 0,1,'first');
      
        triggers = trigger0:5:(trigger0+5*(np-1));
        
    % Cardiac
    
        ECG = signals(:,3);
        ECG = ECG - min(ECG(:));
        norm_e = medfilt1(ECG,11);
        
        ECG = ECG - norm_e; 
        
        ECG2 = convn(ECG,ECG(end:-1:1),'same');
        
        len = length(ECG2);
        corr = ECG2(round(len/2)-118:round(len/2)+118);
        
        corr = corr - mean(corr);
        
        corr = corr./sum(corr);
        
        ECG3 = convn(convn(ECG,corr,'same'),corr,'same');
        
        ECG3 = ECG3 .* max(ECG) ./ max(ECG3);
        
        ECG3 = ECG3 .* norm(ECG) / norm(ECG3);
        
        [~,R_locs1] = findpeaks(ECG3,'MinPeakDistance',40,'MinPeakHeight',0.3*max(ECG3)); % 100);
        
        % figure(12); plot(ECG3,'k'); hold on; scatter(R_locs1,ECG3(R_locs1),'r');
        % figure(12); plot(signals(:,3),'k'); hold on; scatter(R_locs1,signals(R_locs1,3),'r');
        
    % Cardiac phases
    
        time1 = signals(:,1); % offset to left-edge triggers (5 ms)
        tRs = time1(R_locs1);
        phases = zeros(1,length(triggers));

        for i = 1:length(triggers)

            % Cardiac
            start_R = find(time1(triggers(i)) >= tRs,1,'last');
            end_R = find(time1(triggers(i)) < tRs,1,'first');
            phases(i) = (time1(triggers(i)) - tRs(start_R))/(tRs(end_R)-tRs(start_R));

        end
        
        cphases = 100*phases;
        
    % Temporal weights - create projection weights corresponding to each cardiac phase to be reconstructed

        p = 10; % phases

        W_ = Gaussian_weights_trunc_v2( p, 100, 3 );

        Wt = zeros(np,p);

        for t = 1:p

            Wt(:,t) = W_(round(cphases)+1,t);

        end

        Wt = bsxfun(@rdivide,Wt,sum(Wt,1));
    
%% 10) Set up parameter file for iterative reconstruction

    % Reference attenuation measurements for water at each energy threhsold
    attn_water = [0.002753553 0.002654050 0.002528615 0.002425031];

% Input / output

    rt.run_name       = recon_name;       % identifier string for specific results

    rt.outpath        = outpath;          % location where results are saved
    
    rt.computeRMSE    = 0;                % (0) do not comptue RMSE (i.e. for in vivo data), (1) compute RMSE each time the reconstruction is updated (simulations)
    rt.X_ref          = '';               % reference volume string; noise free reference data

    rt.do_save        = 1;                % (0) return final results only, (1) save final results only, (2) save intermediate results and final results

    rt.Y              =  projection_data; % projection data string, nifti file

    rt.breg_iter      = 4;                % total number of times to update the reconstructed results (initialization + regularized iterations)
    
    rt.bicgstabl_iter = 6;                % data fidelity update iterations (bicgstabl with l = 2)

    rt.preview_slice  = slice_num;        % z slice used for previewing reconstructed results
    rt.display_range  = [0 0.8];          % minimum and maximum fractions of the dynamic range of CT attenuation values for consistent display purposes

% Projection / Backprojection

    rt.int_params    = int32(int_params);     % int32 parameters
    rt.int_params_f  = int32(int_params_f);   % int32 parameters
    rt.double_params = double(double_params); % double precision parameters used for geometric calculations
    rt.filt_name     = filt_name;             % FBP filter
    rt.geo_all       = geo_all;               % geometry file
    rt.Rt_aff        = single(Rt_aff);        % affine transform
    rt.rmask         = uint16(rmask);         % volumetric masking
    rt.gpu_list      = gpu_list;              % GPU(s) to use for reconstruction ( Matlab getDevice(#) indices - 1), int32
    
    rt.Wt            = Wt;                    % temporal weights
    rt.geo_lines     = geo_lines;             % We need the geometry file for all projections to allow us to sub-select rows for temporal reconstruction
    % rt.snum          = 1;                   % number of subsets to use for reconstruction
    
% Sizes

    rt.p   = p;          % number of temporal phases
    rt.e   = e;          % number of spectral samples
    rt.sz  = [nx,ny,nz]; % full volume size
    rt.szy = [nu,nv,np]; % projection data size

% Regularization

    % regularization strength
    rt.mu = 0.005;
    
    % regularization, RSKR (energy)
    rt.stride      = 3;            % subsampling to deal with correlated noise
    rt.w           = int32(6);     % filtration kernel radius
    rt.w2          = single(0.8);  % kernel resampling bandwidth
    rt.lambda0     = single(1.5);  % regularization strength
    rt.attn_water  = attn_water;   % attempt to equalize the noise level between spectral samples, following water normalization
    rt.g           = 0.5;          % exponential regularization scaling with singular value ratio ( lambda0*(E(1,1)./diag(E)').^(g) )
    
    % regularization, pSVT (time)
    rt.apply_pSVT      = 1;   % (0) do not apply pSVT; (1) Apply pSVT when there is a time dimension
    rt.w_pSVT          = 3;   % kernel diameter, pSVT
    rt.nu_pSVT         = 0.7; % non-convex singular value exponent
    rt.multiplier_pSVT = 0.3; % multiplier for singular value threshold
    
    % directory of the version of BF to use for RSKR
    rt.BF_path = fileparts(which('jointBF4D'));
    
% Save for later use or reuse

    save(fullfile(outpath,'recon_template.mat'),'rt');
    
%% 11) Perform iterative reconstruction for each time point and energy threshold

    clear Y;

    clear DD_init;

    load(fullfile(outpath,'recon_template.mat'));

    % NOTE: To conserve system memory, the projections are cleared from the Matlab workspace and loaded in within this reconstruction function.
    [ X, RMSEs ] = Recon_5D_Data_v3( rt );
    
%% 12) Material decomposition (measurements taken using ImageJ)
% NOTE: 'non_neg_decomp' uses parfor and Matlab's Parallel Computing toolbox. Replace this with a standard for loop if needed.

    fprintf('\n\nPerforming material decomposition on iterative reconstruction results...\n\n');

    X = load_nii(fullfile(outpath,[recon_name '_iter4.nii']));
    X = X.img;

    % Attenuation measurements in reference material vials (units: 1 / dx)
    I_12  = [0.00499204 0.00470891 0.00429607 0.00369881]; % Iodine, 12 mg/mL + water
    Au_5  = [0.00330354 0.00300334 0.00286322 0.00270112]; % Gold, 5 mg/mL + water (supplement for calcium, since we are not imaging the K-edge of gold)
    water = [0.00281243 0.00263110 0.00255049 0.00249053]; % water only
    
    I  = (I_12 - water) / 12; % (attn in 1 / voxel size) / (mg / mL)
    Au = (Au_5 - water) / 5;  % (attn in 1 / voxel size) / (mg / mL)
    
    M = [I;Au;water];
    
    C = non_neg_decomp(reshape(X,[nx*ny*nz*p e]), M);
    
    C = reshape(C,[nx ny nz p 3]);
    
    save_nii(make_nii(C,[dx dy dz]),fullfile(outpath,[recon_name '_iter4_C.nii']));






    
    
    
    
    
    
    

































