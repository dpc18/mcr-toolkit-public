% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References
    
    % Long, Y., Fessler, J. A., & Balter, J. M. (2010).
    % 3D forward and back-projection for X-ray CT using separable footprints.
    % IEEE transactions on medical imaging, 29(11), 1839-1850.
    
    % Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
    % Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
    % Physics in Medicine & Biology, 49(11), 2209.
    
    % Van der Vorst, H. A. (1992).
    % Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
    % SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
    
    % Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
    % Cone-beam reprojection using projection-matrices.
    % IEEE transactions on medical imaging, 22(10), 1202-1214.
    
    % Sleijpen, G. L., & Van Gijzen, M. B. (2010).
    % Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
    % SIAM journal on scientific computing, 32(5), 2687-2709.
    % Algorithm 3.1
    
%% Extract projections - requires Windows + Matlab
%  NOTE: We bypass this step which extracts projection data and acquisition information from a *.ptr file. 

% bpath = '/media/justify/Paper_Data/Med_Phys_2022_Recon_Toolkit_Release/Siemens_DS_DE';
% 
% ptr_file = fullfile(bpath,'Gammex_Duke.CT.Dual_Energy_DE_Abdomen_VNC_(Adult).601.RAW.20190531.134700.185977.2019.05.31.15.02.56.599000.1005923.ptr');

%     slices = 0;
% 
% % Chain A
% 
%     chain = 1;
%     ScanNo = 1;
%     read_ECG = 0;
%     ECG_polarity = 1;
%     [ info_out, slice_info, angles, tpos, Y, ECG, cardiac_phases, HR_Mean, HR_Std ] = import_data_v3( ptr_file, slices, chain, ScanNo, read_ECG, ECG_polarity );
% 
%     if size(Y,3) > 32767 % nifti will not hold all of the data
%        
%         sz = size(Y);
%         
%         outfile = fullfile(outpath,['ChainA_raw_' num2str(sz(1)) '_' num2str(sz(2)) '_' num2str(sz(3)) '.bin']);
%         
%         fid = fopen(outfile,'wb');
%         
%         fwrite(fid,single(Y(:)),'single');
%         
%         fclose(fid);
%         
%     else
%         
%         outfile = fullfile(outpath,'ChainA_raw.nii.gz');
%         
%         save_nii(make_nii(single(Y)),outfile);
%         
%     end
%     
%     clear Y;
%     
%     save(fullfile(outpath,'paramsA.mat'),'info_out','slice_info','angles','tpos','ECG','cardiac_phases','HR_Mean','HR_Std');
%     
%     disp('Done with chain A...');
%     
% % Chain 2
% 
%     chain = 2;
%     ScanNo = 1;
%     read_ECG = 0;
%     ECG_polarity = 1;
%     [ info_out, slice_info, angles, tpos, Y, ECG, cardiac_phases, HR_Mean, HR_Std ] = import_data_v3( ptr_file, slices, chain, ScanNo, read_ECG, ECG_polarity );
% 
%     if size(Y,3) > 32767 % nifti will not hold all of the data
%        
%         sz = size(Y);
%         
%         outfile = fullfile(outpath,['ChainB_raw_' num2str(sz(1)) '_' num2str(sz(2)) '_' num2str(sz(3)) '.bin']);
%         
%         fid = fopen(outfile,'wb');
%         
%         fwrite(fid,single(Y(:)),'single');
%         
%         fclose(fid);
%         
%     else
%         
%         outfile = fullfile(outpath,'ChainB_raw.nii.gz');
%         
%         save_nii(make_nii(single(Y)),outfile);
%         
%     end
%     
%     clear Y;
%     
%     save(fullfile(outpath,'paramsB.mat'),'info_out','slice_info','angles','tpos','ECG','cardiac_phases','HR_Mean','HR_Std');
%     
%     disp('Done with chain B...');

%% If you are attempting to run this script directly, define these variables...
%  otherwise, they are predefined from the "run_demos.m" script.

    % Path to the supplementary data sets
    % dpath_base = '/media/x-ray/blkbeauty4/MCR_Toolkit_Datasets';
    
    % Location where results will be saved
    % outpath_base = '/media/x-ray/blkbeauty4/MCR_Toolkit_Results';
    
    % GPUs to use for computation
    % MATLAB's gpuDevice(idx+1) function or NVIDIA's smi tool (idx) can be use to 
    % determine which index corresponds with which physical GPU.
    % gpu_list = int32([0]);       % Use 1 GPU, the system default
    % gpu_list = int32([0,1]);     % Use 2 GPUs
    % gpu_list = int32([0,1,2,3]); % Use 4 GPUs

%% Source data

    % Results path
    outpath = fullfile(outpath_base,'Siemens_Flash_DS_DE');
    
    % Data set path
    dpath = fullfile(dpath_base,'Siemens_Flash_DS_DE');
    
    if ~exist(outpath,'dir')
       
        mkdir(outpath);
        
    end

%% Load Data

    pitch  = 0.8;
    np_rot = 1152;
    
    load(fullfile(dpath,'paramsA.mat'));
    angles_A = angles;
    tpos_A = tpos;
    slice_info_A = slice_info;
    info_out_A = info_out;
    
    load(fullfile(dpath,'paramsB.mat'));
    angles_B = angles;
    tpos_B = tpos;
    slice_info_B = slice_info;
    info_out_B = info_out;
    
    Y_A = load_nii(fullfile(dpath,'ChainA_raw.nii.gz')); Y_A = Y_A.img;
    Y_B = load_nii(fullfile(dpath,'ChainB_raw.nii.gz')); Y_B = Y_B.img;
    
    Y_A = single(Y_A) ./ 2294.5;
    Y_B = single(Y_B) ./ 2294.5;
    
    sz_A = size(Y_A);
    sz_B = size(Y_B); % original size
    
    % Compensate for differing definitions of +z
    Y_A = flipdim(Y_A,2);
    Y_B = flipdim(Y_B,2);
    
%% Shared parameters

    % Larger field of view for iterative reconstruction
    nx = 864; % 832;
    ny = 864; % 832;
    nz = 256;
    
%% Chain A - geometry

    cA.nu     = sz_A(1);
    cA.nv     = sz_A(2);
    cA.dx     = 0.6;
    cA.dy     = 0.6;
    cA.dz     = 0.6;
    cA.eta    = 0;
    cA.sig    = 0;
    cA.phi    = 0;
    cA.rotdir = 1;
    cA.np     = size(Y_A,3);
    
    z_start_A = tpos_A(1);
    z_end_A   = tpos_A(end);
    
    % NOTE: This is determined by the scanner geometry and is not a reconstruction variable.
    cA.slicewidth = 0.6;
    
    cA.n_channels = sz_A(3); % cA.nu;

    % central ray
    cA.lsd = 1085.6;
    cA.lso = 595;
    cA.lod = cA.lsd - cA.lso;

    cA.du = 2*1085.6*tand(0.067864/2);
    cA.dv = 0.6*cA.lsd/cA.lso;

    % angular increment between detector elements
    cA.db = atan2(cA.du,cA.lsd); % radians

    cA.anode_angle = 7; % degrees

    % Detector alignment
    cA.AMu = 1.125;
    cA.AMv = 0;
    
    cA.uc = (cA.nu - 1)/2;
    cA.vc = (cA.nv - 1)/2;
    
    cA.zo = tpos_A(1) + (tpos_A(end)-tpos_A(1))/2.0;
    cA.ao = 0;
    
    % Short-scan weighting (rebinned projections)
    cA.collimation         = cA.nv * cA.slicewidth;
    cA.limit_angular_range = 0.5 * cA.collimation * pitch * 180 / 360;
    cA.limit_multiplier    = 1.1; % don't limit the collimation when performing algebraic updates
    
    % Full-scan weighting (rebinned projections)
    % cA.collimation         = cA.nv * cA.slicewidth;
    % cA.limit_angular_range = 0.5 * cA.collimation * pitch * 360 / 360;
    % cA.limit_multiplier    = 1.0;
    
%% Chain B - geometry

    cB.nu = sz_B(1);
    cB.nv = sz_B(2);
    cB.np = sz_B(3);
    cB.dx = cA.dx;
    cB.dy = cA.dy;
    cB.dz = cA.dz;
    cB.eta = 0;
    cB.sig = 0;
    cB.phi = 0;
    cB.rotdir = 1;
    cB.np = size(Y_B,3);
    
    % NOTE: This is determined by the scanner geometry and is not a reconstruction variable.
    cB.slicewidth = 0.6;
    
    cB.n_channels = cB.nu;

    % central ray
    cB.lsd = 1085.6;
    cB.lso = 595;
    cB.lod = cB.lsd - cB.lso;

    cB.du = 2*1085.6*tand(0.067864/2);
    cB.dv = 0.6*cB.lsd/cB.lso;

    % angular increment between detector elements
    cB.db = atan2(cB.du,cB.lsd); % radians

    cB.anode_angle = 7; % degrees

    % Detector alignment
    cB.AMu = 1.125;
    cB.AMv = 0;
    
    cB.uc = (cB.nu - 1)/2;
    cB.vc = (cB.nv - 1)/2;
    
    cB.zo = cA.zo;
    cB.ao = 0;
    
    % Short-scan weighting (rebinned projections)
    cB.collimation         = cB.nv * cB.slicewidth;
    cB.limit_angular_range = 0.5 * cB.collimation * pitch * 180 / 360;
    cB.limit_multiplier    = 1.1;
    
    % Full-scan weighting (rebinned projections)
    % cB.collimation         = cB.nv * cB.slicewidth;
    % cB.limit_angular_range = 0.5 * cB.collimation * pitch * 360 / 360;
    % cB.limit_multiplier    = 1.0;
    
%% Additional reconstruction parameters

    % Flags
    implicit_mask = 1; % (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
    explicit_mask = 0; % (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask
    use_affine = 0;    % (0) - affine transform is not used (even if one is provided); (1) - affine transform is used

    % Explicit mask
    % Note: The mask_zrange function is currently in the toolkit Addons.
    if (explicit_mask == 0) % no explicit mask

        rmask = uint16(0);

    elseif (explicit_mask == 1)  % used z range explicit mask (good for boxes)

        % Test explicit mask
        % Reproduces implicit mask
        % rmask = zeros(nx,ny,'single');
        % [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
        % rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
        % rmask = repmat(rmask,[1 1 nz]);
        % rmask = rmask(:);
        % clear x1 x2 x3;

        mask = mask_zrange( mask );

        mask = uint16(mask(:));

    else % use for arbitrary shapes... slower

        % % Test explicit mask
        % % Reproduces implicit mask
        % rmask = zeros(nx,ny,'single');
        % [x2, x1] = meshgrid((0:nx-1)-nx/2,(0:ny-1)-ny/2);
        % rmask(sqrt(x1.^2 + x2.^2) <= min(nx/2,ny/2)) = 1;
        % rmask = repmat(rmask,[1 1 nz]);
        % % rmask = rmask(:);
        % clear x1 x2 x3;

        rmask = load_nii(fullfile(outpath,'rmask2.nii'));
        rmask = rmask.img;

        zmask = mask_zrange( rmask );

        rmask = uint16(cat(3,zmask,rmask));

        % rmask = uint16(rmask(:));

    end

    % Dummy parameters for no affine, minimal computation
    Rt_aff = single([1 0 0 0 1 0 0 0 1 0 0 0]);
    
    r_os  = 1;
    cA.mag = cA.lsd / cA.lso;
    cB.mag = cB.lsd / cB.lso;
    
    if cA.mag ~= cB.mag
       
        error('Assumption of equal magnification between chains violated.');
        
    end

    % Double parameters for reconstruction operations.
    scale = 1.0;
    % double_params_A       = double([cA.du             cA.dv cA.dx cA.dy cA.dz cA.zo cA.ao cA.lsd cA.lso cA.uc      cA.vc cA.eta cA.sig cA.phi scale cA.db cA.limit_angular_range]);
    double_params_rebin_A  = double([cA.du/r_os/cA.mag cA.dv cA.dx cA.dy cA.dz cA.zo cA.ao cA.lsd cA.lso cA.uc*r_os cA.vc cA.eta cA.sig cA.phi scale cA.db cA.limit_angular_range]);
    double_params_rebin_A2 = double([cA.du/r_os/cA.mag cA.dv cA.dx cA.dy cA.dz cA.zo cA.ao cA.lsd cA.lso cA.uc*r_os cA.vc cA.eta cA.sig cA.phi scale cA.db cA.limit_multiplier * cA.limit_angular_range]);
    
    % double_params_B       = double([cB.du             cB.dv cB.dx cB.dy cB.dz cB.zo cB.ao cB.lsd cB.lso cA.uc      cB.vc cB.eta cB.sig cB.phi scale cB.db cB.limit_angular_range]);
    double_params_rebin_B  = double([cB.du/r_os/cB.mag cB.dv cB.dx cB.dy cB.dz cB.zo cB.ao cB.lsd cB.lso cA.uc*r_os cB.vc cB.eta cB.sig cB.phi scale cB.db cB.limit_angular_range]);
    double_params_rebin_B2 = double([cB.du/r_os/cB.mag cB.dv cB.dx cB.dy cB.dz cB.zo cB.ao cB.lsd cB.lso cA.uc*r_os cB.vc cB.eta cB.sig cB.phi scale cB.db cB.limit_multiplier * cB.limit_angular_range]);

    % Integer parameters for cylindrical projections, algebraic reconstruction (using the rebinned projections)
    FBP              = 0; % (0) We cannot perform analytical reconstruction directly with clindrical projections.
    use_norm_vol     = 0; % (0) Use implicit weighting when performing algebraic reconstruction.
    det_type         = 2; % (0) - detector is flat; (1) detector is cylindrical; (2) detector is cylindrical, backproject rebinned projections
    int_params_A     = int32([cA.nu cA.nv cA.np nx ny nz FBP use_affine implicit_mask explicit_mask det_type use_norm_vol]);
    int_params_B     = int32([cA.nu cB.nv cB.np nx ny nz FBP use_affine implicit_mask explicit_mask det_type use_norm_vol]);

    % Integer parameters for rebinned projections, analytical reconstruction
    FBP              = 1; % (1) Load and use a Fourier filter for analytical reconstruction. Can be overridden when declaring the reconstruction operator.  
    use_norm_vol     = 1; % (1) Normalize each voxel by the weighted sum of contributing line integrals.
    det_type         = 2; % (0) - detector is flat; (1) detector is cylindrical; (2) detector is cylindrical, backproject rebinned projections
    int_params_Af    = int32([cA.nu*r_os cA.nv cA.np nx ny nz FBP use_affine implicit_mask explicit_mask det_type use_norm_vol]);
    int_params_Bf    = int32([cA.nu*r_os cB.nv cB.np nx ny nz FBP use_affine implicit_mask explicit_mask det_type use_norm_vol]);
    
%% Geometry files

    % Geometry file
    geofile_A  = fullfile(outpath,'geofile_A.geo');
    params_A   = [double_params_rebin_A(8:14) double_params_rebin_A(6:7)];
    geolines_A = make_geo_file_cylindrical( geofile_A, params_A, cA.du/r_os/cA.mag, cA.dv, cA.rotdir, cA.np, angles_A, cA.AMu, cA.AMv, tpos_A, cA.db );
    delete(geofile_A); % delete a pre-existing geo file, if there is one by the same name
    save(geofile_A,'geolines_A','-ASCII'); % save the geometry file

    % Geometry file
    geofile_B  = fullfile(outpath,'geofile_B.geo');
    params_B   = [double_params_rebin_B(8:14) double_params_rebin_B(6:7)];
    geolines_B = make_geo_file_cylindrical( geofile_B, params_B, cB.du/r_os/cB.mag, cB.dv, cB.rotdir, cB.np, angles_B, cB.AMu, cB.AMv, tpos_B, cB.db );
    delete(geofile_B); % delete a pre-existing geo file, if there is one by the same name
    save(geofile_B,'geolines_B','-ASCII'); % save the geometry file
    
%% Reconstruction filter to be applied after rebinning

    % FBP filters (only needed after rebinning)
    filter_type = 'ram-lak';
    
    cutoff      = 1.0;
    lenu        = 2^(ceil(log(cA.nu*r_os-1.0)/log(2.0))); % next power of 2
    filt_name_A = fullfile(outpath,[filter_type '_' num2str(lenu) '.txt']);
    make_FBP_filter( filt_name_A, cA.du/r_os/cA.mag, lenu, filter_type, cutoff );
    
%% Parameters for projection rebinning

    % Y_B      = single(0);
    GPU_idx  = int32(gpu_list(1)); % currently only runs on one GPU
    det_type = int32(1); % (0) flat-panel, (1) cylindrical, (2) cylindrical + temporal

    % should this be signed?
    ap   = 360 / np_rot;
    zrot = abs( tpos_A(end)-tpos_A(1) ) / ( ( cA.np - 1 ) * ap ); % z travel in mm / degree

    nu_A = cA.nu;
    nu_B = cB.nu;
    
    if (cA.nv ~= cB.nv) || (cA.np ~= cB.np) || (cA.rotdir ~= cB.rotdir) || (cA.db ~= cB.db) || ...
       (cA.du ~= cB.du) || (cA.lsd ~= cB.lsd) || (cA.lso ~= cB.lso)
       
        error('Rebinning assumption(s) violated.');
        
    end

    % Set nu_B = 0 to rebin only chain A
    % Set nu_B = # to rebin chain B completed with chain A data
    int_params_Ab    = int32([ nu_A,    0, cA.nv, cA.np, r_os, cA.rotdir ]);
    int_params_Bb    = int32([ nu_A, nu_B, cA.nv, cA.np, r_os, cB.rotdir ]);

    delta_theta = ap;                      % assumes input angular increment = output angular increment
    delta_beta  = atan2d( cA.du, cA.lsd ); % degrees
    uc_A        = cA.uc;
    % vc_A        = vc;                    % Not currently supported.
    uc_B        = cB.uc;
    du_out      = cA.du / r_os / cA.mag;   % oversample the output to better preserve spatial resolution    
    theta_B     = -median(angles_A - angles_B,'omitnan'); % degrees
    scale_A_B   = 1.0;                     % Rescaling to apply when copying chain A data to chain B

    % The AMu parameter is set to zero, since it doesn't currently work.
    double_params_b = double([ cA.lsd, zrot, delta_theta, delta_beta, cA.dv, uc_A, uc_B, du_out, theta_B, scale_A_B, cA.mag, cA.AMu ]);
    
%% Cone-parallel rebinning for each imaging chain

    tic;

    % Chain A
    Y_Ab = cyrebin( Y_A(:), Y_B(:), int_params_Ab, double_params_b, GPU_idx, det_type );
    
    % Chain B
    Y_Bb = cyrebin( Y_A(:), Y_B(:), int_params_Bb, double_params_b, GPU_idx, det_type );

    toc;
    
    % save_nii(make_nii(reshape(Y_Ab,[cA.nu*r_os cA.nv cA.np]),[cA.du/r_os/cA.mag cA.dv 1]),fullfile(outpath,'Y_Ab.nii'));
    % save_nii(make_nii(reshape(Y_Bb,[cA.nu*r_os cB.nv cB.np]),[cB.du/r_os/cB.mag cB.dv 1]),fullfile(outpath,'Y_Bb.nii'));

%% Reconstruction operators
% Be sure to "clear DD_init" before clearing recon. allocations or resetting the GPU(s)!
% Note: Any outstanding recon allocations will be properly deallocated when Matlab is closed.

    clear DD_init;

    tic;

    % NOTE: geofile_B contains some incorrect parameters compared with 
    recon_alloc_A = DD_init(int_params_Af(:), double_params_rebin_A(:), filt_name_A, geofile_A, Rt_aff(:), rmask, gpu_list);
    recon_alloc_B = DD_init(int_params_Bf(:), double_params_rebin_B(:), filt_name_A, geofile_B, Rt_aff(:), rmask, gpu_list);
    
    toc;
    
    W_A = ones(1,cA.np);
    W_B = W_A;
    W_B(~isfinite(angles_B)) = 0;
    
    Rtf_A = @(x,w) DD_backproject(x,recon_alloc_A,int32(1),w);
    Rtf_B = @(x,w) DD_backproject(x,recon_alloc_B,int32(1),w);
    
%% Analytical reconstruction (~30 seconds, 864x864x256, 1x RTX8000)

    tic;
    
    X_A = Rtf_A( Y_Ab(:), W_A );
    X_B = Rtf_B( Y_Bb(:), W_B );
    
    toc;
    
    X_A = reshape(X_A,[nx,ny,nz]);
    X_B = reshape(X_B,[nx,ny,nz]);
    
    save_nii(make_nii(X_A,[cA.dx cA.dy cA.dz]),fullfile(outpath,'X_Ab_2.nii'));
    save_nii(make_nii(X_B,[cB.dx cB.dy cB.dz]),fullfile(outpath,'X_Bb_2.nii'));
    
    X_A_ = mean(X_A(:,:,50:200),3);
    X_B_ = mean(X_B(:,:,50:200),3);

    if isempty(javachk('awt'))
        imtool(mat2gray([ X_A_ X_B_ ]));
        imtool(mat2gray(X_A_-X_B_));
    else
        disp('Java awt not available. Figure display suppressed.');
    end

%% Algebraic updates (~XXX seconds, 864x864x256, 1x RTX8000) - Not used...

% % Reconstruction operators
% 
%     clear Rtf DD_init;
% 
%     tic;
% 
%     recon_alloc_A = DD_init(int_params_A(:), double_params_rebin_A2(:), filt_name_A, geofile_A, Rt_aff(:), rmask, gpu_list);
%     recon_alloc_B = DD_init(int_params_B(:), double_params_rebin_B2(:), filt_name_A, geofile_B, Rt_aff(:), rmask, gpu_list);
% 
%     toc;
% 
%     R  = {@(x,w) DD_project(x,recon_alloc_A,w)             , @(x,w) DD_project(x,recon_alloc_B,w)};
%     Rt = {@(x,w) DD_backproject(x,recon_alloc_A,int32(0),w), @(x,w) DD_backproject(x,recon_alloc_B,int32(0),w) };
% 
% % Ordered subsets
% 
%     num_iter = 2;
%     snum     = 3;
%     ell      = 2;
% 
%     subsets = int32(bitrevorder(1:2^ceil(log2(snum))));
%     subsets = subsets(subsets <= snum);
% 
%     W = zeros(cA.np,snum);
% 
%     for i = 1:snum
% 
%         W(i:snum:end,i) = 1;
% 
%     end
% 
%     W = W(:,subsets);
% 
%     W_A2 = W;
%     W_B2 = W .* W_B';
% 
% % Algebraic updates (~400 seconds per chain, 1x RTX 8000)
% 
%     % Chain A
% 
%         const = Rt{1}( Y_Ab(:), sum(W_A2,2));
% 
%         fun = @(x,w) Rt{1}(R{1}(x,w),w);
% 
%         tic;
%         [ X_A_out, resvec ] = bicgstabl_os( fun, const, X_A(:), num_iter, W_A2, ell );
%         toc;
% 
%         disp(['Chain A - L2 Residuals: ']);
%         resvec
% 
%     % Chain B
% 
%         const = Rt{2}( Y_Bb(:), sum(W_B2,2));
% 
%         fun = @(x,w) Rt{2}(R{2}(x,w),w);
% 
%         tic;
%         [ X_B_out, resvec ] = bicgstabl_os( fun, const, X_B(:), num_iter, W_B2, ell );
%         toc;
% 
%         disp(['Chain B - L2 Residuals: ']);
%         resvec
% 
%     X_A_out = reshape(X_A_out,[nx,ny,nz]);
%     X_B_out = reshape(X_B_out,[nx,ny,nz]);
% 
%     X_A_out_ = mean(X_A_out(:,:,50:200),3);
%     X_B_out_ = mean(X_B_out(:,:,50:200),3);
% 
%     if isempty(javachk('awt'))
%         imtool(mat2gray([ X_A_ X_B_; X_A_out_ X_B_out_ ]));
%     else
%         disp('Java awt not available. Figure display suppressed.');
%     end
% 
%     save_nii( make_nii( cat(4,X_A_out,X_B_out), [cA.dx,cA.dy,cA.dz]), fullfile(outpath,'X_Alg.nii'));
































