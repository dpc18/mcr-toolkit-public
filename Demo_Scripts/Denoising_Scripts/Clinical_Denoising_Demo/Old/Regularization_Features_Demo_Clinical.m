% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% Note: Additional bilateral filtration examples can be found in
%        /MCR_Toolkit/Builder_Scripts/JointBF4D_v5_Builder.m and executed
%        after running the first cell of the master_builder.m script.

%% References

    % Tomasi, C., & Manduchi, R. (1998, January).
    % Bilateral filtering for gray and color images.
    % In Sixth international conference on computer vision (IEEE Cat. No. 98CH36271) (pp. 839-846). IEEE.
    
    % Takeda, H., Farsiu, S., & Milanfar, P. (2007).
    % Kernel regression for image processing and reconstruction.
    % IEEE Transactions on image processing, 16(2), 349-366.
    
    % Clark, D. P., Ghaghada, K., Moding, E. J., Kirsch, D. G., & Badea, C. T. (2013).
    % In vivo characterization of tumor vasculature using iodine and gold nanoparticles and dual energy micro-CT.
    % Physics in Medicine & Biology, 58(6), 1683.
    
    % Clark, D. P., & Badea, C. T. (2017).
    % Hybrid spectral CT reconstruction.
    % PloS one, 12(7), e0180324.
    
    % Clark, D. P., Allphin A. J., Mowery Y. M., Badea C. T. (2022)
    % Photon-Counting X-ray CT Perfusion Imaging in Animal Models of Cancer.
    % Paper presented at: The 7th International Conference on Image Formation in X-Ray Computed Tomography; Baltimore, MD.
    % https://ct-meeting.org/wp-content/uploads/2022/06/CT_Meeting_Proceedings.pdf

%% Test data - Modify paths as needed

    % Results path
    outpath = '/media/justify/DPC/MCR_Toolkit_Reconstructions/Reg_Features_Clinical_v2';
    
    % Data set path
    dpath   = '/media/x-ray/blkbeauty4/MCR_Toolkit_Datasets/Clinical_PCCT/Clinical_Cardiac_PCCT.nii.gz';

    % Preclinical cardiac PCCT
    X_ = load_nii(dpath);
    X_ = single(X_.img);

    % Volume size
    sz = size(X_);
    p  = sz(4);
    e  = sz(5);
    sz = sz(1:3);
    
    attn_scaling = ones([1,e]); % already normalized to HU, noise level roughly equal between channels
    % slices       = 180:219;
    pad_noise    = 0;           % (0) no noise padding needed
    water_std0   = ones([1,e]); % already normalized to HU, noise level roughly equal between channels
    
%% Filtration parameters

    % Joint BF parameters

        % Temporal resampling (not used)
        At = [1];

        % kernel radius (currently fixed at 6 => 13 voxel kernel diameter)
        w  = 6;

        % kernel resampling bandwidth (higher values = greater smoothing)
        w2_0 = 0.01;
        w2   = 1.2;
        
        w2_in = single(w2);

        % filter strength by energy channel
        lambda0 = 1.4;
        m       = lambda0 * water_std0 / min( water_std0 );
        
        % Construct factored spatial resampling kernel
        A_  = single(make_A_approx_3D_v2(w, w2  ));
        A_0 = single(make_A_approx_3D_v2(w, w2_0));
        
    % Additional RSKR parameters
    
        % exponent of filter strength scaling by singular value ratio for RSKR
        g = 0.5;

        % break up correlated noise by filtering at a coarser resolution scale
        stride = 3;
        
    % pSVT parameters
    
        w_pSVT          =    3; % kernel diameter 
        nu_pSVT         =  0.7; % singular value ratio exponent for scaling singular value thresholding
        multiplier_pSVT =  0.3; % multiplicative singular value threshold rescaling factor
    
    % Optional. if not specified, the system default is used
    % Currently, only one GPU is used for denoising.
    
        gpu_list = [0];
    
    % Optional. when 1, adds an additional output argument: a map of local noise estimates
    
        return_noise_map = int32(0);
    
    % Optional. does nothing when specified like this
    % When a volume is provided, it is used in place of kernel resampling.
    
        Xr = zeros([1,1],'single');
    
%% If needed, pad noise outside of the reconstruction mask to avoid underestimating
%  the noise level at the edges of the reconstruction.

    X_ = reshape(X_,[prod(sz) p e]);
    
    if pad_noise == 1
        
        % Reconstruction mask (for regularization)
        mask = zeros(sz(1:2),'single');
        [x2, x1] = meshgrid((0:sz(1)-1)-sz(1)/2,(0:sz(2)-1)-sz(2)/2);
        mask(sqrt(x1.^2 + x2.^2) <= min(sz(1)/2,sz(2)/2)) = 1;
        mask = repmat(mask,[1 1 sz(3)]);
        mask = mask(:);
        clear x1 x2;

        % Add noise outside of the reconstruction mask to avoid underestimating local noise at the edges of the reconstruction mask

        sigma0 = zeros(1,e);

        for s = 1:e

            HP = reshape(X_(:,1,s),sz);
            HP = 0.5*convn(convn(HP(:,:,round(sz(3)/2))./attn_water(s),[1; -1],'same'),[1 -1],'same');

            mask_ = reshape(mask,sz)==1;

            sigma0(s) = median(abs(HP(mask_(:,:,round(sz(3)/2))))) / 0.6745;

        end

        N  = bsxfun(@times,randn([prod(sz)*p e]),sigma0.*attn_water);
        X_ = bsxfun(@times,reshape(X_,[prod(sz) p*e]),mask) + bsxfun(@times,reshape(N,[prod(sz) p*e]),1-mask);
        clear N;
        
        mask = reshape(mask,sz);
    
    else
       
        mask = ones(sz,'single');
        
    end
    
    X_ = reshape(X_,[sz p e]);
    
%% VNC / Iodine Decomposition

    % 0.50 ~ iodine enhancement ratio E2 / E1
    M = [1.0, 0.50; 1.0 1.0];
    
    decomp = @(x) single( reshape( double( reshape( x, [prod(sz)*p e] ) ) / M, [sz p e] ) );
    
%% joint BF

    fprintf('\nApplying joint BF...\n');

    nvols   = int32(e);
    ntimes  = int32(1);
    A_in    = single(A_0);
    At_in   = single(At);
    sz_in   = int32(sz);
    w_in    = int32(w);
    m_in    = single(m);
    gpu_idx = int32(gpu_list(1));

    X_out = X_;

    for t = 1:p
        
        X = single(squeeze(X_(:,:,:,t,:)));
        
        if stride > 1
        
            Xm = mosaic( reshape( X, [sz(1)*sz(2)*sz(3)*1 e] ), stride, [sz(1) sz(2) sz(3)*1]  );

            tic;
            Xf1 = jointBF4D(X ,nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map,Xr);
            Xf2 = jointBF4D(Xm,nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map,Xr);
            Xf2 = demosaic( reshape( Xf2, [sz(1)*sz(2)*sz(3)*1 e] ), stride, [sz(1) sz(2) sz(3)*1] );
            Xf = 0.5 * ( Xf1 + Xf2 );
            toc;
            
        else
            
            tic;
            Xf = jointBF4D(X,nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map,Xr);
            toc;
            
        end
       
        X_out(:,:,:,t,:) = reshape(Xf,[sz 1 e]);
        
    end
    
    mask_ = mask(:,:,20);
    
    C0    = decomp(X_);
    C_out = decomp(X_out);
    
    % imc({X_(:,:,20,1,:).*mask_,X_out(:,:,20,1,:).*mask_},[sz(1) sz(2)*e],1,1);
    imc({C0(:,:,20,1,:).*mask_,C_out(:,:,20,1,:).*mask_},[sz(1) sz(2)*e],1,1);
    
    save_nii(make_nii(X_out.*mask),fullfile(outpath,'X_out_joint_BF.nii'));
    save_nii(make_nii(C_out.*mask),fullfile(outpath,'C_out_joint_BF.nii'));
    
    save_nii(make_nii(C0.*mask),fullfile(outpath,'C0.nii'));
    save_nii(make_nii(X_.*mask),fullfile(outpath,'X0.nii'));
    
%% joint BF w/ kernel resampling

    fprintf('\nApplying joint BF with kernel resampling...\n');

    nvols   = int32(e);
    ntimes  = int32(1);
    A_in    = single(A_);
    At_in   = single(At);
    sz_in   = int32(sz);
    w_in    = int32(w);
    m_in    = single(m);
    gpu_idx = int32(gpu_list(1));

    X_out = X_;

    for t = 1:p
        
        X = single(squeeze(X_(:,:,:,t,:)));
        
        if stride > 1
        
            Xm = mosaic( reshape( X, [sz(1)*sz(2)*sz(3)*1 e] ), stride, [sz(1) sz(2) sz(3)*1]  );

            tic;
            Xf1 = jointBF4D(X ,nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map,Xr);
            Xf2 = jointBF4D(Xm,nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map,Xr);
            Xf2 = demosaic( reshape( Xf2, [sz(1)*sz(2)*sz(3)*1 e] ), stride, [sz(1) sz(2) sz(3)*1] );
            Xf = 0.5 * ( Xf1 + Xf2 );
            toc;
            
        else
            
            tic;
            Xf = jointBF4D(X,nvols,ntimes,A_in,At_in,sz_in,w_in,m_in,gpu_idx,return_noise_map,Xr);
            toc;
            
        end
       
        X_out(:,:,:,t,:) = reshape(Xf,[sz 1 e]);
        
    end
    
    C_out = decomp(X_out);
    
    % imc({X_(:,:,20,1,:).*mask_,X_out(:,:,20,1,:).*mask_},[sz(1) sz(2)*e],1,1);
    imc({C0(:,:,20,1,:).*mask_,C_out(:,:,20,1,:).*mask_},[sz(1) sz(2)*e],1,1);
    
    save_nii(make_nii(X_out.*mask),fullfile(outpath,'X_out_joint_BF_w_resamp.nii'));
    save_nii(make_nii(C_out.*mask),fullfile(outpath,'C_out_joint_BF_w_resamp.nii'));

%% RSKR w/ kernel resampling

    fprintf('\nApplying RSKR with kernel resampling to all time points...\n');
        
    sz0     = [sz(1:3) p];

    X    = reshape( single(X_), [prod(sz)*p e] );
    
    % (0) Apply kernel resampling only and not pSVT
    apply_pSVT = 0;
    
    tic;
    Xf = RSKR_pSVT_5D_v4( X, sz0, stride, w, w2_in, lambda0, g, attn_scaling, apply_pSVT, mask, w_pSVT, nu_pSVT, multiplier_pSVT, gpu_idx );
    toc;
    
    X_out = reshape(Xf,[sz p e]);
    
    C_out = decomp(X_out);
    
    % imc({X_(:,:,20,1,:).*mask_,X_out(:,:,20,1,:).*mask_},[sz(1) sz(2)*e],1,1);
    imc({C0(:,:,20,1,:).*mask_,C_out(:,:,20,1,:).*mask_},[sz(1) sz(2)*e],1,1);
    
    save_nii(make_nii(X_out.*mask),fullfile(outpath,'X_out_RSKR.nii'));
    save_nii(make_nii(C_out.*mask),fullfile(outpath,'C_out_RSKR.nii'));

%% RSKR w/ pSVT

    fprintf('\nApplying RSKR with pSVT to all time points...\n');
  
    sz0     = [sz(1:3) p];

    X    = reshape( single(X_), [prod(sz)*p e] );
    
    % (1) If there is a time dimension, apply pSVT.
    apply_pSVT = 1;
    
    tstart = tic;
    Xf = RSKR_pSVT_5D_v4( X, sz0, stride, w, w2_in, lambda0, g, attn_scaling, apply_pSVT, mask, w_pSVT, nu_pSVT, multiplier_pSVT, gpu_idx );
    toc(tstart);
    
    X_out = reshape(Xf,[sz p e]);
    
    C_out = decomp(X_out);
    
    % imc({X_(:,:,20,1,:).*mask_,X_out(:,:,20,1,:).*mask_},[sz(1) sz(2)*e],1,1);
    imc({C0(:,:,20,1,:).*mask_,C_out(:,:,20,1,:).*mask_},[sz(1) sz(2)*e],1,1);
    
    save_nii(make_nii(X_out.*mask),fullfile(outpath,'X_out_RSKR_pSVT.nii'));
    save_nii(make_nii(C_out.*mask),fullfile(outpath,'C_out_RSKR_pSVT.nii'));




















