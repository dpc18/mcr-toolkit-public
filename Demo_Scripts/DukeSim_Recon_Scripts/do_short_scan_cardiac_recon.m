% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% References
    
    % Long, Y., Fessler, J. A., & Balter, J. M. (2010).
    % 3D forward and back-projection for X-ray CT using separable footprints.
    % IEEE transactions on medical imaging, 29(11), 1839-1850.
    
    % Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
    % Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
    % Physics in Medicine & Biology, 49(11), 2209.
    
    % Van der Vorst, H. A. (1992).
    % Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
    % SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
    
    % Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
    % Cone-beam reprojection using projection-matrices.
    % IEEE transactions on medical imaging, 22(10), 1202-1214.
    
    % Sleijpen, G. L., & Van Gijzen, M. B. (2010).
    % Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
    % SIAM journal on scientific computing, 32(5), 2687-2709.
    % Algorithm 3.1
    
    % Regularization with patch-based singular value thresholding (pSVT)
    % Kim, K., Ye, J. C., Worstell, W., Ouyang, J., Rakvongthai, Y., El Fakhri, G., & Li, Q. (2014).
    % Sparse-view spectral CT reconstruction using spectral patch-based low-rank penalty.
    % IEEE transactions on medical imaging, 34(3), 748-760.
    
    % Regularization with rank-sparse kernel regression (RSKR)
    % Clark, D. P., & Badea, C. T. (2017).
    % Hybrid spectral CT reconstruction.
    % PloS one, 12(7), e0180324.
    
    % DukeSim CT Simulator (https://cvit.duke.edu/)
    % Abadi, E., Harrawood, B., Sharma, S., Kapadia, A., Segars, W. P., & Samei, E. (2018).
    % DukeSim: a realistic, rapid, and scanner-specific simulation framework in computed tomography.
    % IEEE transactions on medical imaging, 38(6), 1457-1465.
    
%% Note: This path needs to match what is specified in the parameter file used below.

    % Results path
    outpath = fullfile(outpath_base,'Dynamic_XCAT_Phantom');
    
    if ~exist(outpath,'dir')
       
        mkdir(outpath);
        
    end
    
%% Create reconstruction templates for iterative reconstruction
% NOTE: This script is provided for demonstration purposes only. The source projection data is not currently available for distribution.

    % The dpath, outpath, and gpu_list variables used in the other demo scripts are overriden by specifications in the parameter file(s).
    param_files = {fullfile(dpath_base,'XCAT_Phantom/recon_params_Duke1.txt')};

    save_rebinned_projections = 0; % (0) do not save rebinned projections, (1) save rebinned projections
    suppress_outliers = 1;         % suppress unusually high line integral values
    make_temporal_map = 0;         % WIP feature to visualize temporal resolution in the image domain
    skip_recon = 1;                % (0) Perform reconstruction within this function and return the recon_templates, (slower)
                                   % (1) Skip reconstruction within this function, return the recon_templates only (fast)

    recon_templates = Single_Source_Cardiac_Short_Scan_v2( param_files, save_rebinned_projections, suppress_outliers, ...
                                                           make_temporal_map, skip_recon );
                                                       
%% Perform iterative reconstruction

    for temp = 1:length(recon_templates)
       
        fprintf(['\n\nBeginning iterative reconstruction with template ' recon_templates{temp}.run_name '_recon_template.mat...\n\n']);
        
        Recon_5D_Data_v3( recon_templates{temp} );
        
    end
    
    
    
    