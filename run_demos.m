% %     Copyright (C) 2022, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
% %     Original Author: Darin Clark, PhD
% % 
% %     This program is free software: you can redistribute it and/or modify
% %     it under the terms of the GNU General Public License as published by
% %     the Free Software Foundation, either version 3 of the License, or
% %     (at your option) any later version.
% % 
% %     This program is distributed in the hope that it will be useful,
% %     but WITHOUT ANY WARRANTY; without even the implied warranty of
% %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %     GNU General Public License for more details.
% % 
% %     You should have received a copy of the GNU General Public License
% %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% Supplemental data required to run these demos can be downloaded from the following archive: https://doi.org/10.7924/r45m6cs1f

%% These examples complement the figures in the companion publication:

    % Darin P. Clark, Cristian T. Badea. (2022)
    % MCR Toolkit: A GPU-based toolkit for Multi-Channel Reconstruction of preclinical and clinical X-ray CT Data .
    % Medical Physics. Under review.
    
%% References

    % Long, Y., Fessler, J. A., & Balter, J. M. (2010).
    % 3D forward and back-projection for X-ray CT using separable footprints.
    % IEEE transactions on medical imaging, 29(11), 1839-1850.
    
    % Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
    % Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
    % Physics in Medicine & Biology, 49(11), 2209.
    
    % Van der Vorst, H. A. (1992).
    % Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
    % SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
    
    % Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
    % Cone-beam reprojection using projection-matrices.
    % IEEE transactions on medical imaging, 22(10), 1202-1214.
    
    % Sleijpen, G. L., & Van Gijzen, M. B. (2010).
    % Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
    % SIAM journal on scientific computing, 32(5), 2687-2709.
    % Algorithm 3.1
    
    % Regularization with patch-based singular value thresholding (pSVT)
    % Kim, K., Ye, J. C., Worstell, W., Ouyang, J., Rakvongthai, Y., El Fakhri, G., & Li, Q. (2014).
    % Sparse-view spectral CT reconstruction using spectral patch-based low-rank penalty.
    % IEEE transactions on medical imaging, 34(3), 748-760.
    
    % Regularization with rank-sparse kernel regression (RSKR)
    % Clark, D. P., & Badea, C. T. (2017).
    % Hybrid spectral CT reconstruction.
    % PloS one, 12(7), e0180324.
    
    % DukeSim CT Simulator (https://cvit.duke.edu/)
    % Abadi, E., Harrawood, B., Sharma, S., Kapadia, A., Segars, W. P., & Samei, E. (2018).
    % DukeSim: a realistic, rapid, and scanner-specific simulation framework in computed tomography.
    % IEEE transactions on medical imaging, 38(6), 1457-1465.
    
    % ** See this reference for more details on the simulated MOBY phantom projection data:
    % Clark, D. P., Holbrook, M., Lee, C. L., & Badea, C. T. (2019).
    % Photon-counting cine-cardiac CT in the mouse.
    % PloS one, 14(9), e0218417.
    % https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0218417
    
    % Segars, W. P., Tsui, B. M., Frey, E. C., Johnson, G. A., & Berr, S. S. (2004).
    % Development of a 4-D digital mouse phantom for molecular imaging research.
    % Molecular Imaging & Biology, 6(3), 149-159.
    
    % W. Segars, G. Sturgeon, S. Mendonca, J. Grimes, and B. M. Tsui. (2010)
    % 4D XCAT phantom for multimodality imaging research.
    % Medical Physics 37(9), 4902-4915. 

%% Paths
% Before running these demos, please run master_builder.m in the Toolkit root
% directory to compile the Toolkit and to set up your MATLAB path.
% 
% Fill in and assign the absolute paths here, and then run your choice of the demos below.
%
% While this script makes it easy to run the Toolkit demos from the paper,
% its primary purpose is to index the script locations for reference code.

    % Path to the MCR Toolkit
    toolkit_path = '/home/x-ray/Documents/MCR_Toolkit';
    
    % Path to the supplementary data sets
    % (Download from https://doi.org/10.7924/r45m6cs1f)
    dpath_base = '/media/x-ray/blkbeauty4/MCR_Toolkit_Datasets';
    
    % Location where results will be saved
    outpath_base = '/media/x-ray/blkbeauty4/MCR_Toolkit_Results';
    
    % GPUs to use for computation
    % MATLAB's gpuDevice(idx+1) function or NVIDIA's smi tool (idx) can be use to 
    % determine which index corresponds with which physical GPU.
    gpu_list = int32([0]);         % Use 1 GPU, the system default
    % gpu_list = int32([0,1]);     % Use 2 GPUs
    % gpu_list = int32([0,1,2,3]); % Use 4 GPUs 
    
%% Figure 2 - Image domain denoising in vivo, preclinical cardiac micro-CT data acquired with PCCT (Dectris 0804 detector).

    clearvars -except toolkit_path dpath_base outpath_base gpu_list; close all force; clear DD_init;

    fprintf('\n\nRunning in vivo preclinical PCCT denoising demo...\n\n');

    cd(fullfile(toolkit_path,'Demo_Scripts/Denoising_Scripts/Preclinical_Denoising_Demo'));
    
    run( 'Regularization_Features_Demo_Preclinical_v3.m' );

%% Figure 3 - Image domain denoising and material decomposition of clinical cardiac PCCT data (Siemens NAEOTOM Alpha scanner).
% NOTE: This example cannot currently be run because the clinical patient data cannot be redistributed;
%       however, the code is provided for reference.

    % clearvars -except toolkit_path dpath_base outpath_base gpu_list; close all force; clear DD_init;
    % 
    % fprintf('\n\nRunning clinical PCCT denoising demo...\n\n');
    % 
    % cd(fullfile(toolkit_path,'Demo_Scripts/Denoising_Scripts/Clinical_Denoising_Demo'));
    % 
    % run( 'Regularization_Features_Demo_Clinical_v2.m' );

%% Figures 4 & 5 - WFBP and regularized iterative reconstruction of the MOBY mouse phantom (ideal PCCT simulation).
%  It is recommended to run this particuar script manually, cell by cell, because the iterative reconstructions
%  require substantial computation time and resources.

    clearvars -except toolkit_path dpath_base outpath_base gpu_list; close all force; clear DD_init;

    fprintf('\n\nRunning MOBY phantom reconstruction demo...\n\n');

    cd(fullfile(toolkit_path,'Demo_Scripts/Recon_Scripts/MOBY_Phantom_METR'));
    
    run( 'MOBY_Phantom_Reconstruction_Examples_v3.m' );

%% Figure 6 - Regularized iterative reconstruction of in vivo, preclinical cardiac micro-CT data acquired with PCCT (Dectris 0804 detector).
%  This demo script provides a complete example of pre-processing raw PCCT data, fine tuning the geometry calibration, setting up and
%  performing itertive reconstruction, and performing post-reconstruction material decomposition with a non-negativity constraint.
%  NOTE: This example requires significant computation time, resources, and 30GB of free hard drive space.

    clearvars -except toolkit_path dpath_base outpath_base gpu_list; close all force; clear DD_init;

    fprintf('\n\nRunning in vivo mouse PCCT reconstruction pipeline demo...\n\n');

    cd(fullfile(toolkit_path,'Demo_Scripts/Recon_Scripts/Preclinical_CT_METR'));
    
    run( 'In_Vivo_ApoE_KO_Reconstruction_Example_v4.m' );

%% Figure 7 - Clinical cardiac CT reconstruction using the dynamic XCAT phantom and retrospective projection gating (DukeSim simulation).
% NOTE: This example cannot currently be run because the source projection data cannot be redistributed;
%       however, the code is provided for reference.
% NOTE: This example works from a parameter file designed to pipe DukeSim parameters to MCR Toolkit reconstructions.
%       The dpath, outpath, and gpu_list variables used in the other demo scripts are overriden by specifications in the parameter file.

    % clearvars -except toolkit_path dpath_base outpath_base gpu_list; close all force; clear DD_init;
    % 
    % fprintf('\n\nRunning XCAT phantom reconstruction demo...\n\n');
    % 
    % cd(fullfile(toolkit_path,'Demo_Scripts/DukeSim_Recon_Scripts'));
    % 
    % run( 'do_short_scan_cardiac_recon.m' );

%% Figure 8 - Clinical dual-source, dual-energy CT reconstruction with projection-domain data completion (Siemens Flash scanner).

    clearvars -except toolkit_path dpath_base outpath_base gpu_list; close all force; clear DD_init;

    fprintf('\n\nRunning Siemens Flash dual-source, dual-energy reconstruction demo...\n\n');

    cd(fullfile(toolkit_path,'Demo_Scripts/Recon_Scripts/Siemens_Flash_DS_DE'));
    
    run( 'Siemens_Flash_DS_DE.m' );






























